// @ts-nocheck
/* eslint-env node */
const path = require('path');

const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { mergeWithCustomize } = require('webpack-merge');

const PATHS = require('./scripts/paths.cjs');
const webpackConfig = require('./webpack.config.js');
const { htmlWebpackPluginConfig, includeSentry } = require('./webpack.config.parts.js');

module.exports = (env, argv) => {
  const baseConfig = webpackConfig(env, argv);

  return mergeWithCustomize({
    entry: 'replace',
  })(baseConfig, {
    devtool: 'cheap-module-source-map',

    entry: {
      abd_panel_dev: [path.join(PATHS.SRC, '/mocks/chrome-api.js'), ...includeSentry(path.join(PATHS.SRC, '/devtoolsPanel/index.dev.jsx'))],
    },

    plugins: [
      new ReactRefreshWebpackPlugin(),

      new HtmlWebpackPlugin(
        htmlWebpackPluginConfig({
          title: 'ABD DevTools Panel',
          filename: 'index.html',
          chunks: ['abd_panel_dev'],
        })
      ),
    ],

    devServer: {
      static: {
        directory: PATHS.DIST,
      },
      host: '0.0.0.0',
      port: 3000,
      historyApiFallback: true,
      hot: true,
      client: {
        overlay: false,
        progress: true,
      },
    },

    resolve: {
      alias: {
        'native-url': path.resolve(__dirname, 'node_modules/native-url/dist/index.js'),
      },
    },

    optimization: {
      minimize: false,
    },
  });
};
