### [2.8.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.8.1..v2.8.0) (2022-08-09)

### Bug Fixes

- **docs:** When displaying docs page, use assets from the released version instead of using `master` branch ([78d0ebe](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/78d0ebeb32d518c3fc4c0533321e5153d7692192))
- **sentry:** catch the exceptions from the CDP session ([72b2f89](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/72b2f8989f0c792c105e046f7a2a0bbe849882a7))
- **ts:** fix types safety ([6174df9](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/6174df992bc5109f04e6100d7d094fcd7ea147f2))

## [2.8.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.8.0..v2.7.1) (2022-08-02)

### Features

Add new **WRM Coverage Recorder** panel. With the new panel, you can record the coverage directly in ABD without needing to use the DevTools **Coverage** panel.

- [ABD-12](https://ecosystem.atlassian.net/browse/ABD-12) Rename "WRM coverage" to "WRM Coverage Browser" ([7939e70](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7939e703430aae7d30c55a6ef58c4ba9588738a6))
- [ABD-14](https://ecosystem.atlassian.net/browse/ABD-14) Add export coverage button ([99f9119](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/99f91195d3e494d2288bae78fbe534ccf294ad32))
- [ABD-14](https://ecosystem.atlassian.net/browse/ABD-14) export coverage as JSON file ([5b3516a](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/5b3516aac6dc359962e3d639b379b415f1a85fac))
- Add bottom toolbar to sidebar ([e9b2e95](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/e9b2e9533f7850df0d0ee72f055850da09fbf25b))
- Add Help and Feedback dialog ([c19d027](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/c19d0274b5bfba2b27da2a12213ee8515a57d7dc))
- add webpack bundle reporter ([9d6568a](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/9d6568a2d10c5e68143c711e149d40e0d9574e05))
- Better handle closing native `<dialog>` element and show backdrop ([681172a](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/681172ae8f8bdaed720baaffbcd6aa721ec4cf91))
- create Code Insights reporter for webpack bundle ([211cf00](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/211cf007102b1760c337c267609283e54736fa2b))
- display previous bundle size in webpack build ([7e04a76](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7e04a7623c2c39f1800cb9c3de8e6f4e6060de2f))
- Improve Help and Support dialog ([4bbbd55](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/4bbbd55d1eebce0e7ee47d2dc0e11e5b072b8acc))
- **lint:** Add "require-jsdoc-prop-types" custom ESLint rule ([4166297](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/4166297cb5246f5ac70eec27f68959451443d9e0))
- **recorder:** [ABD-1](https://ecosystem.atlassian.net/browse/ABD-1) Enable coverage recorder ([8b1443c](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/8b1443c4f9c1c5485c13dc12dcc42d4d27b5013e))
- Send bundle entrypoints as Code Insights annotations ([7da8c29](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7da8c2941dac6bd40b07c834989a1225a9947039))
- show bundle chunks in Code Insights annotations ([ff6ba36](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/ff6ba366360347a316c873b939d88bc6c249a3d7))
- show bundle delta change when creating Code Insights report ([b1c84fa](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/b1c84faee73724582f29308eb8b42db659d847c9))
- store reports in git notes ([2787b22](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/2787b222a897e1d24c5a276de8b90065a86482d8))

### Bug Fixes

- [ABD-37](https://ecosystem.atlassian.net/browse/ABD-37) Fix showing web-resources on the WRM Inspector caused by the compatibility issue with WRM 5.5.0+ ([128acfa](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/128acfa311d320da3e24ac1b290be705f8fb398e))
- **ci:** handle missing previous bundle reports ([a0d7a4b](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/a0d7a4b256521cd54a24ac56938fdd5f0535b597))
- **ci:** skip fetching default branch when we are on default branch already ([47ba054](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/47ba054dbbf4bff1a6a7aaccdde31148c0cd875d))
- **deps:** update patches ([48fe2af](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/48fe2af90193aa2a0e54ff853772f2130c42e612))
- fix fetching missing default branch and git notes on CI ([ebb7ce1](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/ebb7ce1f9e6d2e96a3d7d524d0761bbb0b4380c5))
- fix getting git notes ([98bcd06](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/98bcd06441e5cd441ae3e42d47e1d61a9b0c6386))
- fix initializing plugin ([6394aae](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/6394aae5d00cac3103fdc619f6c10f4d8e08bb07))
- fix sending fake postMessage in dev mode ([d66bbc9](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/d66bbc9d6840240609e6120bfaf820efcead6349))
- fix storing and reading notes ([659a74e](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/659a74eef228bd170f258b0b4d215b69e01e670d))

### [2.7.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.7.1..v2.7.0) (2022-07-27)

### Bug Fixes

- [ABD-37](https://ecosystem.atlassian.net/browse/ABD-37) Fix showing web-resources on the WRM Inspector caused by the compatibility issue with WRM 5.5.0+ ([2a90c0e](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/2a90c0ecb25fdb379039d8794b2dce5274554564))

## [2.7.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.7.0..v2.6.2) (2022-01-11)

### Features

- **coverage-browser:** clear values of filters fields when clicking on "clear filters" button ([37916d7](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/37916d78932d804840341da0b852b0fcba808afb))
- show hint message when the filters didn't match any web-resources ([af81d51](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/af81d518e95c03f8e5cb7897dba0a537a9c2b1fe))
- **wrm-inspector:** clear values of filters fields when clicking on "clear filters" button ([075571c](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/075571c1a88c7483d2d9056724c9fb255ea4e0ca))

### Bug Fixes

- Don't block anchor event listeners on PanelHint ([33ecdab](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/33ecdabdae9b59e327e430044158ee778d0e9b26))
- **table:** fix rendering tables on Firefox ([7cc19f4](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7cc19f49bf547395812373d1daf4110e2e04288f))

### [2.6.2](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.6.2..v2.6.1) (2022-01-10)

### Bug Fixes

- fix initializing Sentry in content script ([b5da4e0](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/b5da4e0275ef904cb16e8467083a2645fc3d4356))

### [2.6.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.6.1..v2.6.0) (2022-01-10)

### Bug Fixes

- Fix compiling coverage JSON schema for production bundle ([7cb9ecf](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7cb9ecf88194d156da4939d3ebe0c1f10312f8cf))
- Fix display web-resource size ([f193ba3](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/f193ba3d0093af16635dddf9d8202f00fd68d940))
- Fix rendering coverage panel when recording coverage ([8b7622f](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/8b7622fcbdba951772c05c3f6dda6beaffb0f2a0))
- Fix running extension in local dev mode ([803ae6a](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/803ae6a607820db58a94894fa9f22ac115ef477c))

## [2.6.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.6.0..v2.5.1) (2022-01-10)

- Convert the project to use TypeScript with JSDoc support.
- [ABD-35](https://ecosystem.atlassian.net/browse/ABD-35) Add Sentry integration.
- [ABD-1](https://ecosystem.atlassian.net/browse/ABD-1) Add a new experimental feature for Coverage Recording.

### Features

- [ABD-1](https://ecosystem.atlassian.net/browse/ABD-1) Add coverage panel ([7465de8](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7465de8124fe82dd238696f9ba2c1e340229a6b1))
- [ABD-1](https://ecosystem.atlassian.net/browse/ABD-1) Hide the coverage recorder behind the dev feature flag ([47ac48a](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/47ac48aee1441079900e22379e29489e45aa4143))
- [ABD-1](https://ecosystem.atlassian.net/browse/ABD-1): Change columns order ([db15e4f](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/db15e4ffde2b682b4fcb72b4eebebb0ae6fbf7bc))
- [ABD-1](https://ecosystem.atlassian.net/browse/ABD-1): Update the tooltips message ([c128304](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/c1283049d64b3ac0206a42527391304d67fd3903))
- [ABD-13](https://ecosystem.atlassian.net/browse/ABD-13) Firefox does not support debugger API ([6f021e4](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/6f021e46f813b98d621afb2726cc7e70ff6db4a3))
- [ABD-15](https://ecosystem.atlassian.net/browse/ABD-15) [ABD-17](https://ecosystem.atlassian.net/browse/ABD-17) Handle debugger errors ([757aef3](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/757aef300ec1a1b628526f96af8b246bfeb95b9c))
- [ABD-25](https://ecosystem.atlassian.net/browse/ABD-25) Add confirmation dialog ([0c7c230](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/0c7c230d3790e06e5e6db6d5d9415bf7f36d6e3c))
- [ABD-26](https://ecosystem.atlassian.net/browse/ABD-26) disable the toolbar buttons when collecting coverage ([7708f22](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7708f22df652ee2b1bd9a22db2ae71ad1dcab82c))
- [ABD-26](https://ecosystem.atlassian.net/browse/ABD-26) Merge coverage toolbars and panels ([27b711e](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/27b711e7994773f8be860cf17428e78303fab40a))
- [ABD-26](https://ecosystem.atlassian.net/browse/ABD-26) Show progress bar when recording coverage ([94a9c75](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/94a9c7534b8e103383b2ca51a57121205e5117b4))

### Bug Fixes

- [ABD-1](https://ecosystem.atlassian.net/browse/ABD-1) Fix displaying dialog element in Firefox ([f1bbeff](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/f1bbeff04123313ff55f1bd1680060a703169e49))
- [ABD-34](https://ecosystem.atlassian.net/browse/ABD-34) Fix showing hint when WRM coverage is missing in the coverage report ([a71fdb2](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/a71fdb2e4ffa39e11bdc621a4c365b741782acbe))
- [ABD-34](https://ecosystem.atlassian.net/browse/ABD-34) Fix showing hint when WRM coverage is missing in the report ([91e7426](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/91e742648d90843370ea9c03b22dfc2feed82973))
- clean-up selected web-resource when the resources are removed ([a2ba0c7](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/a2ba0c7367e8328436ef12078500d12e9ba60906))
- Fix clearing coverage when resource is selected ([629af27](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/629af2725f8285e85e26abbd544766b7e9345afd))
- Fix WRM inspector toolbar ([bd1b52b](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/bd1b52b351cc0ba712a8768ea44ed29151a06dc2))

### [2.5.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.5.1..v2.5.0) (2021-12-15)

### Bug Fixes

- Fix updating tab content ([a46715f](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/a46715f88c05f7ad7f9a67a43e35dbeb59aabd29))

## [2.5.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.5.0..v2.4.1) (2021-12-02)

### Features

- Add analytics events to tabs ([2e661c0](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/2e661c0c87eb288d11d44c582d789b37642b0f13))
- Add more analytics events to the WRM Coverage panel ([5672f0a](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/5672f0a958312a1d53da8bdb24a06b248b5aa9d0))
- Add more analytics events to the WRM Inspector and Coverage panels ([7aead8d](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/7aead8d5c8e6559073142bdfc93302810c127523))

### Bug Fixes

- [ABD-33](https://ecosystem.atlassian.net/browse/ABD-33) Fix rendering only first few rows on virutal tables ([1d3965c](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/1d3965cd9ad36eb36e860ccb622587172d27222b))
- Fix unmounting component ([656644e](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/656644eeabb5a415911e8bfbdabffbddd8d63c65))
- Fix wrong or missing hooks dependencies ([9760820](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/97608203a5c9e8726d70dc54e85dc8708ac1af84))
- Optimize rendering of the virtualized table and react-table ([007b589](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/007b589d054a01ed882127ba0619ac1fb60357dc))

### [2.4.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.4.1..v2.4.0) (2021-10-27)

Shrink the overall extension size by:

- reducing the size of JavaScript assets
- removing the additional static assets from the extension package
- avoiding generating sourcemaps when building the extension for production

### Bug Fixes

- [ABD-29](https://ecosystem.atlassian.net/browse/ABD-29) Use custom monaco TS WebWorker module ([f4f0cba](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/f4f0cba241818f24f2666ac75a950a23281a2777))

## [2.4.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.4.0..v2.3.1) (2021-10-20)

- We renamed the extension to **Atlassian Browser DevTools** (ABD).
- Shrink the extension size by loading the big images from the external server.

- Use a dark theme in extension popup panel
- Use a dark theme in extension guide

### Features

- [ABD-28](https://ecosystem.atlassian.net/browse/ABD-28) Fix casing ([823f606](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/823f6066fc80686e088e68c082eb7c305759ddeb))
- [ABD-28](https://ecosystem.atlassian.net/browse/ABD-28) Rename project to Atlassian Browser Devtools (ABD) ([95bcb23](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/95bcb2310c621299781f287a4872762a7a738b78))
- [ABD-29](https://ecosystem.atlassian.net/browse/ABD-29) Shrink extension size ([8ca6763](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/8ca6763bff3c10c4a9d0718adaf3a8868813b79c))
- [ABD-29](https://ecosystem.atlassian.net/browse/ABD-29) Shrink extension size ([bfe591d](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/bfe591d2693073a21c36b79b6ce6fd4ea38f5afc))
- [ABD-28](https://ecosystem.atlassian.net/browse/ABD-28) Migrate repository ([46b77c4](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/46b77c4f2adf0146cd7ab9b15b865ba7e83a6f74))

### [2.3.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.3.1..v2.3.0) (2021-10-14)

### Bug Fixes

- Remove "tabs" permissions ([bbe5ae4](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/bbe5ae4603cfe9f8d0dbd317f7c3d9c993c76a78))

## [2.3.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.3.0..v2.2.2) (2021-10-13)

- Improve the coverage visualization of web-resources. Now, you can quickly identify when the web-resources are quite big or unused.
- Added `is:big` and `is:unused` keyword filters you can use in search input to find the big or unused web-resources.
- Added two toggles to filter the big or unused web-resources quickly.

- Changed how the context selector dropdown works. Now, you can filter the list and choose to show resource that are loader by context and directly loaded without using a context.

- Fixed a bug when the "type" said "unknown" for a web-resource that was loaded without using context.

### Features

- [SPFE-726](https://ecosystem.atlassian.net/browse/SPFE-726) prepare feature for displaying a better resources intel ([cce9e7e](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/cce9e7e713ccc987eaa37c2b6ea10ceba638c39e))
- [SPFE-728](https://ecosystem.atlassian.net/browse/SPFE-728) improve highlighting big resource in WRM inspector panel ([af6ce63](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/af6ce63854bd6313851275cb5fcce69ca4050e60))
- [SPFE-736](https://ecosystem.atlassian.net/browse/SPFE-736) refactor bottom toolbar in coverage panel ([91fba6b](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/91fba6b0673e3f79913b8227f8b98e6f70ea6bf9))
- [SPFE-736](https://ecosystem.atlassian.net/browse/SPFE-736) show checkbox to quickly filter big resources in WRM inspector ([bb7bea2](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/bb7bea2e720132b3d680de63015e9676a0ea1254))
- [SPFE-736](https://ecosystem.atlassian.net/browse/SPFE-736) show two checkbox to quickly filter unused or big resources in WRM coverage inspector ([46ad2f4](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/46ad2f4df2b0353a578e274682c1db666c4701bd))
- rename and improve context filtering ([f9fee63](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/f9fee6334b8c9806d6bd608d271e749a6639d7e8))
- show plugin name under the web-resource details panel ([4781f04](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/4781f042c72e5aee7c8a3e31c9cd6049cee012d0))
- SPFE-XYZ hide core plugins ([ad4c417](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/ad4c417798fc275226428a8e7f88e79ec50b4e2e))
- SPFE-XYZ hide core plugins in coverage panel ([6ab8f64](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/6ab8f64ad45c05245318c8587c43ca9345a9f183))
- SPFE-XYZ Store plugins from UPM in a Map ([09e70cf](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/09e70cf372675e7d4d42658030058ee06ac62a68))
- **ui:** display context names as label elements ([2fa647f](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/2fa647f64cb25ae14e0907ed30d33727d7257132))

### Bug Fixes

- Fix displaying batch and resource types when resource is not loaded from a batch ([5c3be62](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/5c3be62692d697cebf5783c42da07af90263cb4f))
- fix line-height issues ([fa00540](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/fa005405b818de6fef6f36d5c3d83d813e346f35))
- improve column sizing ([18818d2](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/18818d2f9b17b76214b4183f086125d25c06e892))
- SPFE-XYZ fix litering logic ([212dcef](https://bitbucket.org/atlassian/atlassian-browser-devtools/commits/212dcef290ab0532f98b774d5ab7c4b5ab3a89aa))

### [2.2.2](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.2.2..v2.2.1) (2021-06-18)

### [2.2.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.2.1..v2.2.0) (2021-06-18)

- Fix checking if the extension is running locally.

## [2.2.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.2.0..v2.1.1) (2021-06-17)

- Added support for a dark theme. If you prefer devtools in black you will 🖤 it!
- Reduce the extension size by removing the unused Monaco editor features.

### [2.1.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.1.1..v2.1.0) (2021-06-15)

## [2.1.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.1.0..v2.0.1) (2021-06-15)

We have added the **Source** tab to the **WRM Inspector** and **WRM Coverage** panels. You can use the new Source tab to check the content of the web-resource file.

The new Source panel includes the "Format code" button that can help you with prettifying the minimized content of the web-resource.

### [2.0.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.0.1..v2.0.0-rc-2) (2020-12-01)

The major release of **Atlassian Browser DevTools** (ABD). With the version 2.0 we have added two new panels:

- **WRM Inspector**

  displays a list of all the web-resources that are loaded on the current page.

- **WRM Coverage**

  helps you find out what is the real usage of all the web-resources loaded on the page.

You can learn more about how to use the two panels from the [README](./README.md) file.

## [2.0.0-rc-2](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.0.0-rc-2..v2.0.0-rc-1) (2020-11-30)

## [2.0.0-rc-1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v2.0.0-rc-1..v1.3.0) (2020-11-24)

## [1.3.0](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/v1.3.0..1.0.2) (2018-03-26)

### [1.0.2](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/1.0.2..1.0.1) (2017-12-04)

### [1.0.1](https://bitbucket.org/atlassian/atlassian-browser-devtools/branches/compare/1.0.1..1.0.0) (2017-12-04)

## 1.0.0 (2017-12-04)
