# TODO

## Code base

- [x] Remove less variables
- [ ] Remove all polyfills and improve bundling time
- [ ] Remove less precompiler if possible

## Project

- [ ] extract reusable components

## Components

### Tables

- [ ] Refactor code for selecting table rows
- [ ] Add columns resizing

### Panels

- [ ] refactor a sidebar to use buttons instead of spans
- [ ] improve a11y and add focus outline

## Features

- [ ] Error boundary
- [ ] Catch and report error

### Web-resources

- [ ] refactor token filtering to use values with spaces
- [ ] store selected column and sorting direction in local storage

## Mocks

- **dev mode** Create mocked dev server for the REST requests e.g. plugins

## Docs

- [x] The documentation page should use the GIT tag and release version number to reference the images that we are loading from the Bitbucket
