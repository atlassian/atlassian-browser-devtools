/* eslint-env node */
module.exports = {
  // The options reference can be found here:
  // https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-core
  options: {
    preset: {
      // Config reference: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-conventionalcommits
      name: 'conventionalcommits',
      issuePrefixes: ['SPFE-', 'ABD-'],
      issueUrlFormat: 'https://ecosystem.atlassian.net/browse/{{prefix}}{{id}}',

      // Default values: https://github.com/conventional-changelog/conventional-changelog/blob/master/packages/conventional-changelog-conventionalcommits/writer-opts.js#L193-L201

      commitUrlFormat: '{{host}}/{{owner}}/{{repository}}/commits/{{hash}}', // We need custom format to support Bitbucket commit URL
      compareUrlFormat: '{{host}}/{{owner}}/{{repository}}/branches/compare/{{currentTag}}..{{previousTag}}', // We need custom format to support Bitbucket compare URL
    },
  },
};
