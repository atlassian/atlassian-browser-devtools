// @ts-nocheck
/* eslint-env node */
const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const camelCase = require('lodash/camelCase');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { DefinePlugin } = require('webpack');

const {
  default: WebpackBundleReporterPlugin,
  getGitNotesBundleReportProvider,
} = require('./scripts/bundle/webpackPlugin/webpackBundleReporterPlugin.cjs');
const PATHS = require('./scripts/paths.cjs');
const { htmlWebpackPluginConfig, includeSentry } = require('./webpack.config.parts.js');

module.exports = (env, { mode }) => {
  const isDevelopment = mode === 'development';

  return {
    // To skip generating sourcemaps in production mode we need to skip providing the "devtools" property
    // devtool: 'none',

    entry: {
      // The background file is referenced in the manifest.json file
      abd_background: includeSentry(path.join(PATHS.SRC, '/background/index.js')),

      // The file is injected on the page from the background page
      abd_content_script: includeSentry(path.join(PATHS.SRC, '/contentScript/index.js')),

      // The file is loaded by the abd_devtools_page.html page
      abd_devtools_page: includeSentry(path.join(PATHS.SRC, '/devtools/index.js')),

      // The file is loaded by the devtools page
      abd_panel: includeSentry(path.join(PATHS.SRC, '/devtoolsPanel/index.jsx')),

      // The page is opened by background page on first install or extension update
      abd_onboarding: includeSentry(path.join(PATHS.SRC, '/onboarding/index.jsx')),
    },

    output: {
      filename: '[name].js',
      chunkFilename: '[name].[chunkhash:8].js',
      publicPath: '',
      globalObject: 'self', // Required by monaco
    },

    resolve: {
      extensions: ['.js', '.json', '.jsx', '.ts'],
    },

    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
                plugins: [isDevelopment && require.resolve('react-refresh/babel')].filter(Boolean),
              },
            },
          ],
        },

        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
              },
            },
          ],
        },

        {
          test: /\.less$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                modules: {
                  exportLocalsConvention: (name) => {
                    // Similar to "camelCaseOnly" plus keeps the first letter with the original case
                    // We need so the IntelliJ IDE can provide a valid autocompletion

                    return name.charAt(0) + camelCase(name.substr(1));
                  },
                  localIdentName: isDevelopment ? '[name]__[local]' : '[hash:base64:24]',
                },
              },
            },
            {
              loader: 'less-loader',
              options: {
                sourceMap: true,
                lessOptions: {
                  strictMath: true,
                },
              },
            },
          ],
        },

        {
          test: /\.(png|jpe?g|gif)$/,
          type: 'asset',
        },

        {
          test: /\.(ttf|eot|woff2?)$/,
          type: 'asset/resource',
        },

        {
          test: /\.md/,
          type: 'asset/source',
        },

        {
          test: /\.svg$/,
          type: 'asset',
          use: [
            {
              loader: 'svgo-loader',
              options: {
                plugins: [
                  { name: 'removeTitle' },
                  {
                    name: 'convertColors',
                    params: {
                      shorthex: false,
                    },
                  },
                  { name: 'convertPathData' },
                ],
              },
            },
          ],
        },
      ],
    },

    plugins: [
      new WebpackBundleReporterPlugin({
        logOutput: true,
        reportLocation: path.join(PATHS.ROOT, 'reports/webpack-bundle-report.json'),
        prevBundleReportProvider: getGitNotesBundleReportProvider('webpack-bundle-report'),
      }),

      new MiniCssExtractPlugin({
        ignoreOrder: true,
        filename: isDevelopment ? '[name].css' : '[name].[chunkhash:8].css',
      }),

      new HtmlWebpackPlugin(
        htmlWebpackPluginConfig({
          title: 'ABD DevTools',
          filename: 'abd_devtools_page.html',
          chunks: ['abd_devtools_page'],
        })
      ),

      new HtmlWebpackPlugin(
        htmlWebpackPluginConfig({
          title: 'ABD DevTools Panel',
          filename: 'abd_panel.html',
          chunks: ['abd_panel'],
          prependScripts: [
            isDevelopment
              ? // Add external react-devtools connection. Refer to the CONTRIBUTION.md file
                'http://localhost:8097'
              : undefined,
          ],
        })
      ),

      new HtmlWebpackPlugin(
        htmlWebpackPluginConfig({
          title: 'ABD Onboarding',
          filename: 'abd_onboarding.html',
          chunks: ['abd_onboarding'],
        })
      ),

      new CopyWebpackPlugin({
        patterns: [
          {
            from: path.join(PATHS.SRC, '/manifest.json'),
            to: PATHS.DIST,
            transform(content) {
              if (!isDevelopment) {
                return content;
              }

              const cspParser = require('content-security-policy-parser');
              const cspBuilder = require('content-security-policy-builder');

              const manifest = JSON.parse(content.toString());
              const csp = cspParser(manifest.content_security_policy);

              // In dev mode, we need to update extension Content Security Policy, so we can allow extension running
              // inside a browser to connect with the external React Devtools.
              // Refer to the NPM package for more information https://www.npmjs.com/package/react-devtools
              csp['script-src'].push('http://localhost:8097/');

              const newManifest = Object.assign(manifest, {
                content_security_policy: cspBuilder({
                  directives: csp,
                }),
              });

              return Buffer.from(JSON.stringify(newManifest, null, 2));
            },
          },
          {
            from: path.join(PATHS.SRC, '/icons/'),
            to: path.join(PATHS.DIST, '/icons/'),
            async transform(content) {
              // Skip transformation in dev mode
              if (isDevelopment) {
                return content;
              }

              // Optimize png files
              const {
                default: { buffer: imageminBuffer },
              } = await import('imagemin');
              const imageminPngquant = require('imagemin-pngquant');

              return imageminBuffer(content, {
                plugins: [
                  // https://www.npmjs.com/package/imagemin-pngquant
                  imageminPngquant({
                    quality: [0.5, 0.8],
                  }),
                ],
              });
            },
          },
          {
            from: path.join(PATHS.SRC, '/popups/'),
            to: path.join(PATHS.DIST, '/popups/'),
          },

          // In Dev mode we want to include all the local Documentation images
          isDevelopment
            ? {
                from: path.join(PATHS.ROOT, '/docs/'),
                to: path.join(PATHS.DIST, '/docs/'),
              }
            : undefined,
        ].filter(Boolean),
      }),

      // Env. "extension" means we are running app as an installed extension (either local extension or extension from the market)
      // Env. "browser" means we are running the app as regular browser page (not extension) e.g. run app from webpack-dev-server
      new DefinePlugin({
        IS_EXTENSION: env && env.extension,
        IS_BROWSER: env && env.browser,
      }),
    ],

    optimization: {
      minimize: true,
      chunkIds: 'named',
    },
  };
};
