/* eslint-disable jsdoc/multiline-blocks */

declare module '*.less' {
  // eslint-disable-next-line @typescript-eslint/consistent-indexed-object-style
  const classes: { [className: string]: string };

  export default classes;
}

declare module '*.md' {
  const document: string;

  export default document;
}

interface HTMLDialogElement extends HTMLDialogElement {
  open: boolean;
  returnValue: string;

  /**
   * Closes the dialog element.
   *
   * The argument, if provided, provides a return value.
   */
  close(returnValue?: string): void;

  /**
   * Displays the dialog element.
   */
  show(): void;
  showModal(): void;
}

declare module 'dialog-polyfill' {
  const registerDialog: (dialog: HTMLDialogElement) => void;

  export default {
    registerDialog,
  };
}
