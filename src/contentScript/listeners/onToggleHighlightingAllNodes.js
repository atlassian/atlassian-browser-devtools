import { DEVTOOLS_CLOSED_EVENT } from '../../common/events/events.js';
import { toggleHighlightAllNodes } from '../deprecations/actions/deprecationHighlighter.js';
import state from '../deprecations/state.js';

/**
 * @template MessagePayloadT
 * @typedef {import('./types').MessageListener<MessagePayloadT>} MessageListener
 */

const listener = /** @type {MessageListener<void>} */ ({
  message: DEVTOOLS_CLOSED_EVENT,
  listener: function onToggleHighlightingAllNodesListener() {
    state.highlightNodes = false;

    toggleHighlightAllNodes();
  },
});

export default listener;
