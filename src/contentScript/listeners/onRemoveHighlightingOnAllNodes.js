import { AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES_EVENT } from '../../common/events/events.js';
import { removeHighlightingOnAllNodes } from '../deprecations/actions/deprecationHighlighter.js';

/**
 * @template MessagePayloadT
 * @typedef {import('./types').MessageListener<MessagePayloadT>} MessageListener
 */

const listener = /** @type {MessageListener<void>} */ ({
  message: AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES_EVENT,
  listener: function onRemoveHighlightingOnAllNodesListener() {
    removeHighlightingOnAllNodes();
  },
});

export default listener;
