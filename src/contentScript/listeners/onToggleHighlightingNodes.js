import { AUI_TOGGLE_HIGHLIGHTING_NODES_EVENT } from '../../common/events/events.js';
import { toggleHighlightAllNodes } from '../deprecations/actions/deprecationHighlighter.js';
import state from '../deprecations/state.js';

/** @typedef {import('../../common/events/events.js').AuiToggleNodeEventPayload} AuiToggleNodeEventPayload */

/**
 * @template MessagePayloadT
 * @typedef {import('./types').MessageListener<MessagePayloadT>} MessageListener
 */

const listener = /** @type {MessageListener<AuiToggleNodeEventPayload>} */ ({
  message: AUI_TOGGLE_HIGHLIGHTING_NODES_EVENT,
  listener: function onToggleHighlightingNodesListener(message) {
    const { highlight } = message;

    state.highlightNodes = highlight;

    toggleHighlightAllNodes();
  },
});

export default listener;
