import { AUI_UNHIGHLIGHT_NODE_EVENT } from '../../common/events/events.js';
import { unhighlightNode } from '../deprecations/actions/deprecationHighlighter.js';

/** @typedef {import('../../common/events/events.js').AuiUnhighlightNodeEventPayload} AuiUnhighlightNodeEventPayload */

/**
 * @template MessagePayloadT
 * @typedef {import('./types').MessageListener<MessagePayloadT>} MessageListener
 */

const listener = /** @type {MessageListener<AuiUnhighlightNodeEventPayload>} */ ({
  message: AUI_UNHIGHLIGHT_NODE_EVENT,
  listener: function onUnhighlightNodeListener(message) {
    const { nodeId } = message;

    unhighlightNode(nodeId);
  },
});

export default listener;
