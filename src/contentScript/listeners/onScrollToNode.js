import { AUI_SCROLL_TO_NODE_EVENT } from '../../common/events/events.js';
import scrollToNode from '../deprecations/actions/scrollToNode.js';

/** @typedef {import('../../common/events/events.js').AuiUnhighlightNodeEventPayload} AuiUnhighlightNodeEventPayload */

/**
 * @template MessagePayloadT
 * @typedef {import('./types').MessageListener<MessagePayloadT>} MessageListener
 */

const listener = /** @type {MessageListener<AuiUnhighlightNodeEventPayload>} */ ({
  message: AUI_SCROLL_TO_NODE_EVENT,
  listener: function onScrollToNodeEvent(message) {
    const { nodeId } = message;

    scrollToNode(nodeId);
  },
});

export default listener;
