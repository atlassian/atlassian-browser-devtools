import { AUI_HIGHLIGHT_NODE_EVENT } from '../../common/events/events.js';
import { highlightNode } from '../deprecations/actions/deprecationHighlighter.js';

/** @typedef {import('../../common/events/events.js').AuiHighlightNodeEventPayload} AuiHighlightNodeEventPayload */

/**
 * @template MessagePayloadT
 * @typedef {import('./types').MessageListener<MessagePayloadT>} MessageListener
 */

const listener = /** @type {MessageListener<AuiHighlightNodeEventPayload>} */ ({
  message: AUI_HIGHLIGHT_NODE_EVENT,
  listener: function onHighlightNode(message) {
    const { nodeId } = message;

    highlightNode(nodeId);
  },
});

export default listener;
