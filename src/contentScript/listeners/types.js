/**
 * @template {void} MessagePayloadT
 * @typedef {(payload: MessagePayloadT) => void} MessageListenerCallback
 */

/**
 * @template {void} MessagePayloadT
 * @typedef {Object} MessageListener
 * @property {string} message
 * @property {MessageListenerCallback<MessagePayloadT>} listener
 */

export {};
