/**
 * @typedef {Object} AuiDeprecation
 * @property {string} selector
 * @property {string} displayName
 * @property {string} alternativeName
 * @property {string} extraInfo
 * @property {string} auiVersion
 * @property {string} version
 * @property {string} removeInVersion
 */

// TODO: BLOGIDEA Write this down as notes
/**
 * @typedef {AuiDeprecation & Object} AuiDeprecationWithMeta
 * @property {string} nodeId
 * @property {number} timestamp
 */

export {};
