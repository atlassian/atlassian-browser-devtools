import cssDeprecations from './cssDeprecations.json';
import getAuiVersion from '../helpers/auiVersionHelper.js';

const versions = Object.keys(cssDeprecations);

const getCssSelectors = () => {
  const auiVersion = getAuiVersion();
  const applicableVersions = versions;

  return applicableVersions
    .map((version) =>
      cssDeprecations[version].map((deprecation) => ({
        ...deprecation,
        version,
        auiVersion,
      }))
    )
    .reduce((result, selectors) => result.concat(selectors), []);
};

export default getCssSelectors;
