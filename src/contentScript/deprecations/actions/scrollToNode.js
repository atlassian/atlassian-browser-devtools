import { scrollIntoView } from '../../../common/scrollIntoView.js';
import { getNode } from '../nodesRegistry.js';

/**
 * @param {string} nodeId
 */
const scrollToNode = (nodeId) => {
  const node = getNode(nodeId);

  if (!node) {
    return;
  }

  scrollIntoView(node, {
    block: 'nearest',
  });
};

export default scrollToNode;
