import throttle from 'lodash/throttle';

import { getNode, getNodesIds, clearRegistry } from '../nodesRegistry.js';
import state from '../state.js';

const { body } = document;

const updateNodeMaskPosition = (node, mask) => {
  const { width, height, left, top } = node.getBoundingClientRect();
  const { left: docLeft, top: docTop } = body.getBoundingClientRect();

  /* eslint-disable no-param-reassign */
  mask.style.width = `${width}px`;
  mask.style.height = `${height}px`;
  mask.style.top = `${top + Math.abs(docTop)}px`;
  mask.style.left = `${left + Math.abs(docLeft)}px`;
  /* eslint-enable no-param-reassign */
};

const getHighlightingMask = (nodeId) => {
  const node = getNode(nodeId);

  if (!node) {
    return null;
  }

  const mask = document.getElementById(nodeId.replace(/-/g, ''));

  return mask;
};

export const highlightNode = (nodeId) => {
  const node = getNode(nodeId);

  if (!node) {
    return;
  }

  if (getHighlightingMask(nodeId)) {
    return;
  }

  const mask = document.createElement('div');

  mask.id = nodeId.replace(/-/g, '');
  mask.style.border = '2px solid rgba(255, 50, 50, 0.7)';
  mask.style.backgroundColor = 'rgba(255, 50, 50, 0.5)';
  mask.style.position = 'absolute';
  mask.style.zIndex = '1000000000';

  updateNodeMaskPosition(node, mask);

  mask.addEventListener('mouseenter', () => {
    mask.style.display = 'none';
  });

  node.addEventListener('mouseleave', () => {
    mask.style.display = 'block';
  });

  body.appendChild(mask);
};

export const unhighlightNode = (nodeId) => {
  const mask = getHighlightingMask(nodeId);

  if (mask) {
    mask.remove();
  }
};

export const toggleHighlightAllNodes = () => {
  const { highlightNodes } = state;

  const nodesIds = getNodesIds();
  const command = highlightNodes ? highlightNode : unhighlightNode;

  nodesIds.forEach((nodeId) => command(nodeId));
};

export const removeHighlightingOnAllNodes = () => {
  getNodesIds().forEach((nodeId) => unhighlightNode(nodeId));

  clearRegistry();
};

const updateNodeMasksPositions = () => {
  const nodesIds = getNodesIds();

  nodesIds
    .map((nodeId) => ({ nodeId, mask: getHighlightingMask(nodeId) }))
    .filter(({ mask, nodeId }) => mask && nodeId)
    .forEach(({ mask, nodeId }) => {
      const node = getNode(nodeId);

      updateNodeMaskPosition(node, mask);
    });
};

const eventCallback = throttle(updateNodeMasksPositions, 250);

window.addEventListener('resize', eventCallback, true);
window.addEventListener('scroll', eventCallback, true);
