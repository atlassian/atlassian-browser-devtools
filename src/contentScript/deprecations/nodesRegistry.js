import { v4 as uuid } from 'uuid';

const domRegistry = new WeakMap();
const nodeRegistry = new Map();

export const getNodeId = (node) => domRegistry.get(node) || uuid();

export const storeNode = (nodeId, node) => {
  domRegistry.set(node, nodeId);
  nodeRegistry.set(nodeId, node);
};

export const getNode = (nodeId) => nodeRegistry.get(nodeId);

export const getNodesIds = () => Array.from(nodeRegistry.keys());

export const clearRegistry = () => {
  nodeRegistry.forEach((value, key) => {
    nodeRegistry.delete(key);
  });
};
