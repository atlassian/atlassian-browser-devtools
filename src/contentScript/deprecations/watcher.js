import elementMatchesSelector from './matcher.js';
import MutationObserver from './mutationObserver.js';
import { nodeAddedNotifier, nodeRemovedNotifier } from './notifiers/notifier.js';
import { contentScriptLogger as logger } from '../../common/logger.js';

/** @typedef {import('./types').AuiDeprecation} AuiDeprecation */

/** @type {AuiDeprecation[]} */
const watching = [];

/**
 * @param {AuiDeprecation} deprecation
 */
const watchSelector = ({ selector, displayName, alternativeName, extraInfo, auiVersion, version, removeInVersion }) => {
  watching.push({
    selector,
    displayName,
    alternativeName,
    extraInfo,
    auiVersion,
    version,
    removeInVersion,
  });
};

/**
 * @param {AuiDeprecation} deprecation
 */
export const registerSelector = ({ selector, ...config }) => {
  if (Array.isArray(selector)) {
    selector.forEach((cssSelector) =>
      watchSelector({
        ...config,
        selector: cssSelector,
      })
    );
  } else {
    watchSelector({ selector, ...config });
  }
};

/**
 * @param {Node} node
 * @returns {boolean}
 */
const isSupportedNode = (node) => node instanceof HTMLElement;

/**
 * @param {Node} node
 */
const onNodeAdded = (node) => {
  if (!isSupportedNode(node)) {
    return;
  }

  watching.forEach((deprecation) => {
    const { selector } = deprecation;

    if (elementMatchesSelector(node, selector)) {
      nodeAddedNotifier(node, deprecation);
    }
  });
};

/**
 * @param {Node} node
 */
const onNodeRemoved = (node) => {
  if (!isSupportedNode(node)) {
    return;
  }

  nodeRemovedNotifier(node);
};

/**
 * @param {Node} target
 * @returns {MutationObserver}
 */
const initDeprecationsObserver = (target) => {
  const observer = new MutationObserver((mutationsList) => {
    for (const { addedNodes, removedNodes } of mutationsList) {
      Array.from(addedNodes).forEach((node) => onNodeAdded(node));
      Array.from(removedNodes).forEach((node) => onNodeRemoved(node));
    }
  });

  const config = {
    childList: true,
    subtree: true,
    characterData: false,
  };

  logger('Initializing AUI deprecations observer');
  observer.observe(target, config);

  return observer;
};

/**
 * @param {Document} target
 */
const scanDomForDeprecations = (target) => {
  logger('Scanning current DOM state');

  watching.forEach((deprecation) => {
    const { selector } = deprecation;

    try {
      const result = target.querySelectorAll(selector);

      Array.from(result).forEach((node) => nodeAddedNotifier(node, deprecation));
    } catch (e) {
      logger(`Wrong or empty selector "${selector}`, e);
    }
  });
};

export const initWatcher = (target = window.document) => {
  /** @type {MutationObserver} */ let observer;

  const attachListener = () => {
    if (document.readyState !== 'complete') {
      return;
    }

    scanDomForDeprecations(target);
    observer = initDeprecationsObserver(target);
  };

  if (document.readyState === 'complete') {
    attachListener();
  } else {
    document.addEventListener('readystatechange', attachListener);
  }

  return function cleanUp() {
    if (observer) {
      logger('Disconnecting AUI deprecations observer');

      observer.disconnect();
    }
  };
};
