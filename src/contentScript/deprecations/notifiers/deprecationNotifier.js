import { AUI_DEPRECATION_EVENT } from '../../../common/events/events.js';
import nodeHydrate from '../../helpers/nodeHydrate.js';
import { sendMessageToBackgroundPage } from '../../portConnection.js';
import { getNodeId, storeNode } from '../nodesRegistry.js';

/** @typedef {import('../types').AuiDeprecation} AuiDeprecation */

/**
 * @param {Node} node
 * @param {AuiDeprecation} deprecation
 */
const deprecationNotifier = (node, deprecation) => {
  const nodeId = getNodeId(node);

  storeNode(nodeId, node);

  sendMessageToBackgroundPage(AUI_DEPRECATION_EVENT, {
    ...deprecation,
    nodeId,
    timestamp: Date.now(),
    node: nodeHydrate(node),
  });
};

export default deprecationNotifier;
