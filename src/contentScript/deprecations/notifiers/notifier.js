import deprecationNotifier from './deprecationNotifier.js';
import { toggleHighlightAllNodes, unhighlightNode } from '../actions/deprecationHighlighter.js';
import { getNodeId } from '../nodesRegistry.js';

/** @typedef {import('../types').AuiDeprecation} AuiDeprecation */

/**
 * @param {Node} node
 * @param {AuiDeprecation} deprecation
 */
export const nodeAddedNotifier = (node, deprecation) => {
  deprecationNotifier(node, deprecation);
  toggleHighlightAllNodes();
};

/**
 * @param {Node} node
 */
export const nodeRemovedNotifier = (node) => {
  const nodeId = getNodeId(node);

  unhighlightNode(nodeId);
};
