import getCssSelectors from './getCssSelectors.js';
import { initWatcher, registerSelector } from './watcher.js';
import { contentScriptLogger as logger } from '../../common/logger.js';
import whenDevtoolsClosed from '../whenDevtoolsClosed.js';

const initDeprecationsWatcher = () => {
  logger('Registering deprecation watcher');

  const selectors = getCssSelectors();

  selectors.forEach(registerSelector);
  const cleanUp = initWatcher();

  // eslint-disable-next-line promise/catch-or-return
  whenDevtoolsClosed().then(() => {
    cleanUp();
  });
};

export default initDeprecationsWatcher;
