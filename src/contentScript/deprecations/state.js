/**
 * @typedef {Object} State
 * @property {boolean} highlightNodes
 */

const state = /** @type {State} */ ({
  highlightNodes: false,
});

export default state;
