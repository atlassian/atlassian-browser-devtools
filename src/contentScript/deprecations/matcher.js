import { contentScriptLogger as logger } from '../../common/logger.js';

const spaceRegExp = /\s+/g;

const simpleMatchesSelector = (node, selector) => node.matches(selector);

const complexMatchesSelector = (node, selector) => {
  const firsSelector = selector.split(spaceRegExp).shift().trim();

  // Check and match first element for the performance reason
  const isFirstMatching = simpleMatchesSelector(node, firsSelector);

  if (!isFirstMatching) {
    return false;
  }

  const clonedNode = node.cloneNode(true);
  const parent = document.createElement('div');

  parent.appendChild(clonedNode);

  let result = false;

  try {
    result = Boolean(parent.querySelector(selector));
  } catch (e) {
    logger(`Wrong or empty selector "${selector}`, e);
  }

  return result;
};

const elementMatchesSelector = (node, selector) => {
  const isComplexSelector = selector.match(/\s+/);
  const matcherFn = isComplexSelector ? complexMatchesSelector : simpleMatchesSelector;

  return matcherFn(node, selector);
};

export default elementMatchesSelector;
