const nodeHydrate = (node) => node.outerHTML;

export default nodeHydrate;
