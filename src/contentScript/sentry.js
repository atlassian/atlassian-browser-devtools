import { initSentry } from '../common/sentry/sentry.js';

try {
  // Sentry doesn't work inside a browser content script...
  // https://github.com/getsentry/sentry-javascript/issues/4079
  // https://github.com/getsentry/sentry-javascript/issues/3886
  initSentry('contentScript');
} catch (e) {
  // eslint-disable-next-line no-empty
}
