import { addMessageListener } from './portConnection.js';
import { DEVTOOLS_CLOSED_EVENT } from '../common/events/events.js';

/** @type {Promise<void>} */
const onDevtoolsClosedPromise = new Promise((resolve) => {
  addMessageListener(DEVTOOLS_CLOSED_EVENT, () => {
    resolve();
  });
});

const whenDevtoolsClosed = () => onDevtoolsClosedPromise;

export default whenDevtoolsClosed;
