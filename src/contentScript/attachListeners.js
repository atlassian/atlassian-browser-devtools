import * as listeners from './listeners/listeners.js';
import { addMessageListener } from './portConnection.js';

const attachListeners = () => {
  for (const { message, listener } of Object.values(listeners)) {
    // @ts-expect-error TSS2345 The listener type is somehow invalid...
    addMessageListener(message, listener);
  }
};

export default attachListeners;
