import attachListeners from './attachListeners.js';
import initDeprecationsWatcher from './deprecations/deprecations.js';
import { initConnection } from './portConnection.js';
import initWrmResourcesWatcher from './wrmResources/wrmResources.js';
import { contentScriptLogger as logger } from '../common/logger.js';

logger('Injecting content script on the page');

initConnection();
attachListeners();

initDeprecationsWatcher();
initWrmResourcesWatcher();
