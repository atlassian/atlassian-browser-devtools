import { WRM_CSS_RESOURCE_EVENT } from '../../../common/events/events.js';
import { sendMessageToBackgroundPage } from '../../portConnection.js';

const wrmCssResourceNotifier = (url) => {
  sendMessageToBackgroundPage(WRM_CSS_RESOURCE_EVENT, {
    timestamp: Date.now(),
    url,
  });
};

export default wrmCssResourceNotifier;
