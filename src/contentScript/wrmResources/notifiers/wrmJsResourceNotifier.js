import { WRM_JS_RESOURCE_EVENT } from '../../../common/events/events.js';
import { sendMessageToBackgroundPage } from '../../portConnection.js';

const wrmJsResourceNotifier = (url) => {
  sendMessageToBackgroundPage(WRM_JS_RESOURCE_EVENT, {
    timestamp: Date.now(),
    url,
  });
};

export default wrmJsResourceNotifier;
