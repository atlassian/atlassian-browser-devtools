import { initWatcher } from './watcher.js';
import { contentScriptLogger as logger } from '../../common/logger.js';
import whenDevtoolsClosed from '../whenDevtoolsClosed.js';

const initWrmResourcesWatcher = () => {
  logger('Registering WRM resources watcher');

  const cleanUp = initWatcher();

  // eslint-disable-next-line promise/catch-or-return
  whenDevtoolsClosed().then(() => {
    cleanUp();
  });
};

export default initWrmResourcesWatcher;
