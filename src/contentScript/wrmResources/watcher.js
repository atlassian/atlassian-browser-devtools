import { wrmJsResourceNotifier, wrmCssResourceNotifier } from './notifiers/notifiers.js';
import { contentScriptLogger as logger } from '../../common/logger.js';
import MutationObserver from '../deprecations/mutationObserver.js';

const webresourceBatchUrlRegExp = /_\/download\/resources\//;
const contextBatchUrlRegExp = /_\/download\/contextbatch\//;

/**
 * @template {HTMLElement | HTMLScriptElement} T
 * @param {T} element
 * @returns {boolean}
 */
const isWebResourceScriptNode = (element) => {
  // The inserted node can be a text node, comment etc.
  // https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType
  if (element.nodeType !== Node.ELEMENT_NODE || element.tagName.toLowerCase() !== 'script') {
    return false;
  }

  const url = /** @type {HTMLScriptElement} */ (element).src;

  return Boolean(url.match(webresourceBatchUrlRegExp)) || Boolean(url.match(contextBatchUrlRegExp));
};

/**
 * @template {HTMLElement | HTMLLinkElement} T
 * @param {T} element
 * @returns {boolean}
 */
const isWebResourceLinkElement = (element) => {
  // The inserted node can be a text node, comment etc.
  // https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType
  if (element.nodeType !== Node.ELEMENT_NODE || element.tagName.toLowerCase() !== 'link') {
    return false;
  }

  const url = /** @type {HTMLLinkElement} */ (element).href;

  return Boolean(url.match(webresourceBatchUrlRegExp)) || Boolean(url.match(contextBatchUrlRegExp));
};

/**
 * @param {Document} target
 * @returns {string[]}
 */
const getWrmScriptUrls = (target) => {
  /** @type {HTMLScriptElement[]} */
  const elements = Array.from(target.querySelectorAll('script'));

  return elements.filter((scriptElement) => isWebResourceScriptNode(scriptElement)).map((scriptElement) => scriptElement.src);
};

/**
 * @param {Document} target
 * @returns {string[]}
 */
const getWrmStylesheetsUrls = (target) => {
  /** @type {HTMLLinkElement[]} */
  const elements = Array.from(target.querySelectorAll('link[rel="stylesheet"]'));

  return elements.filter((linkElement) => isWebResourceLinkElement(linkElement)).map((linkElement) => linkElement.href);
};

/**
 * @param {Document} target
 * @returns {void}
 */
const scanDomForWebResources = (target) => {
  logger('Scanning current page for WRM usage');

  for (const url of getWrmScriptUrls(target)) {
    wrmJsResourceNotifier(url);
  }

  for (const url of getWrmStylesheetsUrls(target)) {
    wrmCssResourceNotifier(url);
  }
};

/**
 * @template {HTMLElement} T
 * @typedef {Object} NodeHandler
 * @property {(node: T) => boolean} isMatchingNode
 * @property {(node: T) => void} handler
 */

const nodeHandlers = [
  /** @type {NodeHandler<HTMLScriptElement>} */ {
    isMatchingNode: isWebResourceScriptNode,
    handler: (scriptElement) => {
      const url = scriptElement.src;

      wrmJsResourceNotifier(url);
    },
  },

  /** @type {NodeHandler<HTMLLinkElement>} */ {
    isMatchingNode: isWebResourceLinkElement,
    handler: (linkElement) => {
      const url = linkElement.href;

      wrmCssResourceNotifier(url);
    },
  },
];

/**
 * @param {HTMLElement} element
 */
const onNodeAdded = (element) => {
  for (const { handler, isMatchingNode } of nodeHandlers) {
    if (isMatchingNode(element)) {
      handler(element);
    }
  }
};

/**
 * @param {Document} target
 * @returns {MutationObserver}
 */
const initWebResourcesObserver = (target) => {
  const observer = new MutationObserver((mutationsList) => {
    for (const { addedNodes } of mutationsList) {
      const elements = /** @type {HTMLElement[]} */ (Array.from(addedNodes));

      elements.forEach((element) => onNodeAdded(element));
    }
  });

  const config = {
    childList: true,
    subtree: true,
    characterData: false,
  };

  logger('Initializing WebResources observer');
  observer.observe(target, config);

  return observer;
};

export const initWatcher = (target = window.document) => {
  /** @type {MutationObserver} */ let observer;

  const attachListener = () => {
    if (document.readyState !== 'complete') {
      return;
    }

    scanDomForWebResources(target);
    observer = initWebResourcesObserver(target);
  };

  if (document.readyState === 'complete') {
    attachListener();
  } else {
    document.addEventListener('readystatechange', attachListener);
  }

  return function cleanUp() {
    if (observer) {
      logger('Disconnecting WebResources observer');

      observer.disconnect();
    }
  };
};
