import { CONTENT_SCRIPT_INJECTED_EVENT, PAGE_LOADED_EVENT } from '../common/events/events.js';
import { contentScriptLogger as logger } from '../common/logger.js';
import { CONTENT_SCRIPT_PORT } from '../common/ports.js';
import { withSentryWrapper } from '../common/sentry/sentry.js';

/**
 * @template {Object} T
 * @typedef {(payload: T) => void} MessageListener
 */
const messageListeners = /** @type {Map<string, MessageListener<unknown>[]>} */ (new Map());

const portConnection = chrome.runtime.connect({
  name: CONTENT_SCRIPT_PORT,
});

export const sendMessageToBackgroundPage = (topic, message) => {
  logger(`Sending a message with topic "${topic}"`, message);

  // TODO: Rename to sendMessageBackgroundScript
  portConnection.postMessage({ topic, message });
};

/**
 * @param {string} topic
 * @param {MessageListener<unknown>} listener
 */
export const addMessageListener = (topic, listener) => {
  let /** @type {MessageListener<unknown>[]} */ listeners;

  if (messageListeners.has(topic)) {
    listeners = /** @type {MessageListener<unknown>[]} */ (messageListeners.get(topic));
  } else {
    listeners = [];
    messageListeners.set(topic, listeners);
  }

  listeners.push(listener);
};

/**
 * @param {string} topic
 * @param {MessageListener<unknown>} listener
 */
export function removeMessageListener(topic, listener) {
  if (!messageListeners.has(topic)) {
    return;
  }

  const newListeners = /** @type {MessageListener<unknown>[]} */ (messageListeners.get(topic)).filter((l) => l !== listener);

  messageListeners.set(topic, newListeners);
}

export const initConnection = () => {
  logger('Initializing connection with ABD background script');

  portConnection.onMessage.addListener(
    withSentryWrapper(({ topic, message }) => {
      logger(`Received a message "${topic}" from`, message);

      if (!messageListeners.has(topic)) {
        return;
      }

      const listeners = /** @type {MessageListener<unknown>[]} */ (messageListeners.get(topic));

      for (const listener of listeners) {
        try {
          listener(message);
        } catch (e) {
          logger(`Error while handling message "${topic}"`, e);
        }
      }
    })
  );

  sendMessageToBackgroundPage(CONTENT_SCRIPT_INJECTED_EVENT);

  // Send the loaded event in case the devtools panel was opened after the page was already loaded
  sendMessageToBackgroundPage(PAGE_LOADED_EVENT);
};
