// We need the feature flag since Firefox doesn't support the debugger APIs yet
export const IS_DEBUGGER_API_SUPPORTED = 'debugger' in chrome;
