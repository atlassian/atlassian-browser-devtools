import { AUI_DEPRECATION_EVENT } from '../../../common/events/events.js';
import { devtoolsPanelLogger as logger } from '../../../common/logger.js';
import { actions } from '../../stores/auiDeprecationsStore.js';

/**
 * @template EventsT
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerWithStore<EventsT, EventPayloadT, StateT>} EventListenerWithStore
 */

/**
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerCallback<EventPayloadT, StateT>} EventListenerCallback
 */

/** @typedef {import('../../stores/auiDeprecationsStore.js').RootStateSlice} RootStateSlice */

/** @typedef {import('../../../common/events/events.js').AuiDeprecationEventPayload} AuiDeprecationEventPayload */
/** @typedef {import('../../../contentScript/deprecations/types').AuiDeprecationWithMeta} AuiDeprecationWithMeta */

/** @type {EventListenerCallback<AuiDeprecationEventPayload, RootStateSlice>} */
const onDeprecation = ({ dispatch }, deprecation) => {
  logger('New AUI deprecation on the page');

  dispatch(actions.storeDeprecation(deprecation));
};

const listener = /** @type {EventListenerWithStore<[AUI_DEPRECATION_EVENT], AuiDeprecationEventPayload, RootStateSlice>} */ ({
  events: [AUI_DEPRECATION_EVENT],
  listener: onDeprecation,
});

export default listener;
