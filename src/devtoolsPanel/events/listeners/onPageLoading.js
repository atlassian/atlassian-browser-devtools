import { PAGE_LOADING_EVENT } from '../../../common/events/events.js';
import { devtoolsPanelLogger as logger } from '../../../common/logger.js';
import { actions } from '../../stores/pageStore.js';

/**
 * @template EventT
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerWithStore<EventT, EventPayloadT, StateT>} EventListenerWithStore
 */

/**
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerCallback<EventPayloadT, StateT>} EventListenerCallback
 */

/** @typedef {import('../../stores/pageStore.js').RootStateSlice} RootStateSlice */
/** @typedef {import('../../../common/events/events.js').PAGE_LOADING_EVENT} PAGE_LOADING_EVENT */
/** @typedef {import('../../../common/events/events.js').PageLoadingEventPayload} PageLoadingEventPayload */

/** @type {EventListenerCallback<PageLoadingEventPayload, RootStateSlice>} */
const onPageLoading = ({ dispatch }) => {
  logger('Inspected page is reloading');

  dispatch(actions.setPageIsLoading());
};

const listener = /** @type {EventListenerWithStore<[PAGE_LOADING_EVENT], PageLoadingEventPayload, RootStateSlice>} */ ({
  events: [PAGE_LOADING_EVENT],
  listener: onPageLoading,
});

export default listener;
