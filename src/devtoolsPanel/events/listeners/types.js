/**
 * @template StateT
 * @typedef {import('../attachListeners.js').ListenerStoreApi<StateT>} ListenerStoreApi
 */

/**
 * @template EventPayloadT
 * @template StateT
 * @typedef {(api: ListenerStoreApi<StateT>, payload: EventPayloadT) => void} EventListenerCallback
 */

/**
 * @template {string[]} EventsT
 * @template EventPayloadT
 * @template StateT
 * @typedef {Object} EventListenerWithStore
 * @property {EventsT} events
 * @property {EventListenerCallback<EventPayloadT, StateT>} listener
 */

export {};
