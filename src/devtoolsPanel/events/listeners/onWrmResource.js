import { WRM_CSS_RESOURCE_EVENT, WRM_JS_RESOURCE_EVENT } from '../../../common/events/events.js';
import { devtoolsPanelLogger as logger } from '../../../common/logger.js';
import { actions } from '../../stores/wrmInspectorStore.js';

/**
 * @template EventT
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerWithStore<EventT, EventPayloadT, StateT>} EventListenerWithStore
 */

/**
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerCallback<EventPayloadT, StateT>} EventListenerCallback
 */

/** @typedef {import('../../stores/auiHighlightingStore.js').RootStateSlice} RootStateSlice */
/** @typedef {import('../../../common/events/events.js').WRM_JS_RESOURCE_EVENT} WRM_JS_RESOURCE_EVENT */
/** @typedef {import('../../../common/events/events.js').WRM_CSS_RESOURCE_EVENT} WRM_CSS_RESOURCE_EVENT */
/** @typedef {import('../../../common/events/events.js').WrmJsResourceEventPayload} WrmJsResourceEventPayload */
/** @typedef {import('../../../common/events/events.js').WrmCssResourceEventPayload} WrmCssResourceEventPayload */
/** @typedef {import('../../stores/wrmInspectorStore.js').WrmResource} WrmResource */

/** @type {EventListenerCallback<WrmJsResourceEventPayload | WrmCssResourceEventPayload, RootStateSlice>} */
const onWrmResource = ({ dispatch }, { url }) => {
  logger('New WRM resource was loaded on the page', url);

  fetch(url)
    .then((r) => r.text())
    .then((code) => {
      /** @type {WrmResource} */
      const resource = {
        url,
        code,
      };

      dispatch(actions.storeResource(resource));
    })
    .catch(() => {
      console.error('Failed to fetch web-resources', {
        url,
      });
    });
};

const listener =
  /** @type {EventListenerWithStore<[WRM_JS_RESOURCE_EVENT, WRM_CSS_RESOURCE_EVENT], WrmJsResourceEventPayload | WrmCssResourceEventPayload, RootStateSlice>} */ ({
    events: [WRM_JS_RESOURCE_EVENT, WRM_CSS_RESOURCE_EVENT],
    listener: onWrmResource,
  });

export default listener;
