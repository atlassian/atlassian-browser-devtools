import { CONTENT_SCRIPT_INJECTED_EVENT } from '../../../common/events/events.js';
import { devtoolsPanelLogger as logger } from '../../../common/logger.js';
import { selectors } from '../../stores/auiHighlightingStore.js';
import { toggleHighlightNodes } from '../emitters/highlithing.js';

/**
 * @template EventT
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerWithStore<EventT, EventPayloadT, StateT>} EventListenerWithStore
 */

/**
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerCallback<EventPayloadT, StateT>} EventListenerCallback
 */

/** @typedef {import('../../stores/auiHighlightingStore.js').RootStateSlice} RootStateSlice */
/** @typedef {import('../../../common/events/events.js').ContentScriptInjectedEventPayload} ContentScriptInjectedEventPayload */
/** @typedef {import('../../../common/events/events.js').CONTENT_SCRIPT_INJECTED_EVENT} CONTENT_SCRIPT_INJECTED_EVENT */

/** @type {EventListenerCallback<ContentScriptInjectedEventPayload, RootStateSlice>} */
const onContentScriptInjected = ({ getState }) => {
  const state = getState();

  logger('Content Script was injected on the page');

  const highlightDeprecatedElements = selectors.getHighlightDeprecatedElements(state);

  if (highlightDeprecatedElements) {
    toggleHighlightNodes(highlightDeprecatedElements);
  }
};

const listener =
  /** @type {EventListenerWithStore<[CONTENT_SCRIPT_INJECTED_EVENT], ContentScriptInjectedEventPayload, RootStateSlice>} */ ({
    events: [CONTENT_SCRIPT_INJECTED_EVENT],
    listener: onContentScriptInjected,
  });

export default listener;
