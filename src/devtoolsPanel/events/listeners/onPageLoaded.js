import { batch } from 'react-redux';

import { PAGE_LOADED_EVENT } from '../../../common/events/events.js';
import { devtoolsPanelLogger as logger } from '../../../common/logger.js';
import auiDetector from '../../services/aui/auiDetector.js';
import getAuiVersion from '../../services/aui/getAuiVersion.js';
import getBaseUrl from '../../services/wrm/getBaseUrl.js';
import getWrmContextPath from '../../services/wrm/getWrmContextPath.js';
import wrmDetector from '../../services/wrm/wrmDetector.js';
import { actions } from '../../stores/pageStore.js';

/**
 * @template EventsT
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerWithStore<EventsT, EventPayloadT, StateT>} EventListenerWithStore
 */

/**
 * @template EventPayloadT
 * @template StateT
 * @typedef {import('./types.js').EventListenerCallback<EventPayloadT, StateT>} EventListenerCallback
 */

/** @typedef {import('../../stores/pageStore.js').RootStateSlice} RootStateSlice */
/** @typedef {import('../../../common/events/events.js').PAGE_LOADED_EVENT} PAGE_LOADED_EVENT */
/** @typedef {import('../../../common/events/events.js').PageLoadedEventPayload} PageLoadedEventPayload */

/** @type {EventListenerCallback<PageLoadedEventPayload, RootStateSlice>} */
const onPageLoaded = ({ dispatch }) => {
  logger('Inspected page was loaded');

  dispatch(actions.setPageWasLoaded());

  // eslint-disable-next-line promise/catch-or-return
  Promise.all([auiDetector(), getAuiVersion(), wrmDetector(), getWrmContextPath(), getBaseUrl()]).then(
    ([hasAuiResult, auiVersion, hasWrm, wrmContextPath, baseUrl]) => {
      batch(() => {
        dispatch(actions.setHasAui(hasAuiResult));
        dispatch(actions.setAuiVersion(auiVersion));
        dispatch(actions.setHasWrm(hasWrm));
        dispatch(actions.setWrmContextPath(wrmContextPath));
        dispatch(actions.setBaseUrl(baseUrl));
      });
    }
  );
};

const listener = /** @type {EventListenerWithStore<[PAGE_LOADED_EVENT], PageLoadedEventPayload, RootStateSlice>} */ ({
  events: [PAGE_LOADED_EVENT],
  listener: onPageLoaded,
});

export default listener;
