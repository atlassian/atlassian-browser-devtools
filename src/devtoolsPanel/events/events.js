/** @typedef {import('../../common/events/events.js').WrmJsResourceEventPayload} WrmJsResourceEventPayload */
/** @typedef {import('../../common/events/events.js').WrmCssResourceEventPayload} WrmCssResourceEventPayload */

/** @typedef {import('../../common/events/events.js').PageLoadingEventPayload} PageLoadingEventPayload */
/** @typedef {import('../../common/events/events.js').PageLoadedEventPayload} PageLoadedEventPayload */

/** @typedef {import('../../common/events/events.js').ContentScriptInjectedEventPayload} ContentScriptInjectedEventPayload */

/** @typedef {import('../../common/events/events.js').AuiDeprecationEventPayload} AuiDeprecationEventPayload */
/** @typedef {import('../../common/events/events.js').AuiHighlightNodeEventPayload} AuiHighlightNodeEventPayload */
/** @typedef {import('../../common/events/events.js').AuiUnhighlightNodeEventPayload} AuiUnhighlightNodeEventPayload */
/** @typedef {import('../../common/events/events.js').AuiScrollToNodeEventPayload} AuiScrollToNodeEventPayload */
/** @typedef {import('../../common/events/events.js').AuiToggleNodeEventPayload} AuiToggleNodeEventPayload */
/** @typedef {import('../../common/events/events.js').AuiRemoveHighlightingOnAllNodesEventPayload} AuiRemoveHighlightingOnAllNodesEventPayload */

/**
 * @typedef {Object} Events
 * @property {WrmJsResourceEventPayload} WRM_JS_RESOURCE
 * @property {WrmCssResourceEventPayload} WRM_CSS_RESOURCE
 * @property {PageLoadingEventPayload} PAGE_LOADING
 * @property {PageLoadedEventPayload} PAGE_LOADED
 * @property {ContentScriptInjectedEventPayload} CONTENT_SCRIPT_INJECTED The AUI Deprecations events
 * @property {AuiDeprecationEventPayload} AUI_DEPRECATION
 * @property {AuiHighlightNodeEventPayload} AUI_HIGHLIGHT_NODE
 * @property {AuiUnhighlightNodeEventPayload} AUI_UNHIGHLIGHT_NODE
 * @property {AuiScrollToNodeEventPayload} AUI_SCROLL_TO_NODE
 * @property {AuiToggleNodeEventPayload} AUI_TOGGLE_HIGHLIGHTING_NODES
 * @property {AuiRemoveHighlightingOnAllNodesEventPayload} AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES
 */

export {};
