import {
  AUI_HIGHLIGHT_NODE_EVENT,
  AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES_EVENT,
  AUI_SCROLL_TO_NODE_EVENT,
  AUI_TOGGLE_HIGHLIGHTING_NODES_EVENT,
  AUI_UNHIGHLIGHT_NODE_EVENT,
} from '../../../common/events/events.js';
import { dispatchMessage } from '../../portConnection.js';

/** @typedef {import('../../../common/events/events.js').AuiHighlightNodeEventPayload} AuiHighlightNodeEventPayload */
/** @typedef {import('../../../common/events/events.js').AuiUnhighlightNodeEventPayload} AuiUnhighlightNodeEventPayload */
/** @typedef {import('../../../common/events/events.js').AuiScrollToNodeEventPayload} AuiScrollToNodeEventPayload */
/** @typedef {import('../../../common/events/events.js').AuiToggleNodeEventPayload} AuiToggleNodeEventPayload */

export const highlightNode = (nodeId) => {
  dispatchMessage(AUI_HIGHLIGHT_NODE_EVENT, /** @type {AuiHighlightNodeEventPayload} */ ({ nodeId }));
};

/**
 * @param {string} nodeId
 * @returns {void}
 */
export const unhighlightNode = (nodeId) => {
  dispatchMessage(AUI_UNHIGHLIGHT_NODE_EVENT, /** @type {AuiUnhighlightNodeEventPayload} */ ({ nodeId }));
};

/**
 * @param {string} nodeId
 * @returns {void}
 */
export const scrollToNode = (nodeId) => {
  dispatchMessage(AUI_SCROLL_TO_NODE_EVENT, /** @type {AuiScrollToNodeEventPayload} */ ({ nodeId }));
};

/**
 * @param {boolean} highlight
 * @returns {void}
 */
export const toggleHighlightNodes = (highlight) => {
  dispatchMessage(AUI_TOGGLE_HIGHLIGHTING_NODES_EVENT, /** @type {AuiToggleNodeEventPayload} */ ({ highlight }));
};

/**
 * @returns {void}
 */
export const removeHighlightingOnAllNodes = () => {
  dispatchMessage(AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES_EVENT);
};
