import * as listeners from './listeners/listeners.js';
import { addMessageListener } from '../portConnection.js';

/**
 * @template StateT
 * @typedef {Object} ListenerStoreApi
 * @property {import('redux').Dispatch} dispatch
 * @property {() => StateT} getState
 */

/**
 * @param {import('redux').Store} store
 * @returns {void}
 */
const attachListeners = (store) => {
  const { dispatch, getState } = store;
  const /** @type {ListenerStoreApi<unknown>} */ storeApi = { dispatch, getState };

  for (const { events, listener } of Object.values(listeners)) {
    for (const event of events) {
      addMessageListener(event, (payload) => {
        // @ts-expect-error TSS2345 The storeApi and payload types are somehow invalid...
        listener(storeApi, payload);
      });
    }
  }
};

export default attachListeners;
