import { devtoolsPanelLogger as logger } from '../../../common/logger.js';
const evaluateCodeInInspectedWindow = (code) => {
  logger(`Evaluating code "${code}" in ContentScript`);

  return new Promise((resolve, reject) =>
    // TODO: Extract Chrome API for mocking and testing purpose
    chrome.devtools.inspectedWindow.eval(code, (result, exception) => {
      logger(`Evaluated code "${code}" in ContentScript with result`, {
        result,
        exception,
      });

      if (exception) {
        reject(exception);
      } else {
        resolve(result);
      }
    })
  );
};

export default evaluateCodeInInspectedWindow;
