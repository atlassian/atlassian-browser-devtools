import { isWebResourceBig, isWebResourceUnused } from '../../helpers/webResourceConditions.js';

/** @typedef {import('../../DevtoolsPanelApp/Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */
/** @typedef {import('../../DevtoolsPanelApp/Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJsonEntry} CoverageJsonEntry */
/** @typedef {import('../webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

const wrmRegExp = /\/\* module-key = '(?<key>.*?)', location = '(?<file>.*?)' \*\//g;
const webresourceBatchUrlRegExp = /_\/download\/resources\/(?<key>.*?)\/(?<file>.*?)(?:\?|$)/;

const contextRegExp = /\/contextbatch\/([^/]+)\/([^/]+)/;

/**
 * @typedef {Object} CoverageRange
 * @property {number} start
 * @property {number} end
 */

/**
 * @typedef {Object} WrmModule
 * @property {string} wrmKey
 * @property {string} wrmFile
 * @property {string} url
 * @property {number} start
 * @property {number} end
 * @property {number} totalBytes
 * @property {number} usedBytes
 * @property {string} code
 * @property {CoverageRange[]} ranges
 */

/**
 * @param {WrmModule} m
 * @param {CoverageRange} range
 */
function addRangeTo(m, range) {
  if (!m) {
    return;
  }

  m.ranges.push(range);

  const left = Math.max(m.start, range.start);
  const right = Math.min(m.end, range.end);

  m.usedBytes += right - left;
}

/**
 * @param {RegExpMatchArray} match
 * @param {RegExpMatchArray} nextMatch
 * @param {string} code
 * @returns {string}
 */
function getCode(match, nextMatch, code) {
  // code starts after the comment
  const start = /** @type {number} */ (match.index) + match[0].length;

  if (!nextMatch) {
    return code.substr(start);
  }

  const moduleCode = code.substring(start + 1, /** @type {number} */ (nextMatch.index) - 1);

  return moduleCode.trim();
}

/**
 * @typedef {Object & WebResourceMeta} WebResourceCoverageMeta
 * @property {number} used
 * @property {number} usedBytes
 * @property {number} unused
 * @property {number} unusedBytes
 * @property {boolean} isUnused
 */

/**
 * @param {CoverageJson} coverageData
 * @returns {WebResourceCoverageMeta}
 */
export function processCoverage(coverageData) {
  // keep wrm file coverage data indexed in a few ways
  const coverageByWrmModule = /** @type {WebResourceCoverageMeta[]} */ [];

  // prep the data
  coverageData.forEach((coverageDatum) => {
    const datum = /** @type {CoverageJsonEntry} */ (coverageDatum);

    const url = datum.url;
    const code = datum.text || '';

    const modules = /** @type {WrmModule[]} */ [];

    // check for wrm modules in the file
    const matches = [...code.matchAll(wrmRegExp)];

    if (matches.length) {
      // the URL is for a context batch.

      // walk through wrm modules in the file
      let lastModule;
      for (let i = 0, max = matches.length; i < max; i++) {
        const match = matches[i];
        const nextMatch = matches[i + 1];

        const wrmKey = match[1];
        const wrmFile = match[2];
        // code starts after the comment
        const start = /** @type {number} */ (match.index) + match[0].length;

        // if it's the first match...
        // TODO: Verify that....
        // if (typeof lastModule === 'undefined') {
        // module headers sometimes appear after some banner code,
        // like WRMCB try-catch blocks.
        // if (start !== 0) {
        //   lastModule = {
        //     wrmKey: 'other',
        //     wrmFile: 'unknown',
        //     url,
        //     start: 0,
        //     end: -1,
        //     ranges: [],
        //     totalBytes: 0,
        //     usedBytes: 0,
        //     code: getCode(match, nextMatch, code),
        //   };
        //   modules.push(lastModule);
        // }
        // }

        // first, populate module data
        const module = /** @type {WrmModule} */ ({
          wrmKey,
          wrmFile,
          url,
          start,
          end: -1,
          ranges: [],
          totalBytes: 0,
          usedBytes: 0,
          code: getCode(match, nextMatch, code),
        });
        if (lastModule) {
          lastModule.end = start - 1;
          lastModule.totalBytes = lastModule.end - lastModule.start;
        }

        // track the data points
        modules.push(module);

        // keep track of the last file
        lastModule = module;
      }

      // fix final module's end value
      if (lastModule) {
        lastModule.end = code.length;
        lastModule.totalBytes = lastModule.end - lastModule.start;
      }
    } else {
      // the URL could be for a single web-resource file
      const fromUrl = url.match(webresourceBatchUrlRegExp);
      if (fromUrl) {
        const fromUrlGroups = /** @type {{ key: string; file: string }} */ (fromUrl.groups);

        // this whole file is for a single web-resource file
        modules.push({
          wrmKey: fromUrlGroups.key,
          wrmFile: fromUrlGroups.file,
          url,
          start: 0,
          end: code.length,
          ranges: [],
          totalBytes: code.length,
          usedBytes: 0,
          code,
        });
      } else {
        return;
      }
    }

    // next, iterate through ranges & push in to appropriate bucket
    let m = 0;
    let lastStart, lastEnd;
    datum.ranges.forEach((/** @type {CoverageRange} */ range) => {
      let module = modules[m];

      if (!module) {
        console.error('CoverageProcessor: Wrong module', {
          modules,
          module,
          m,
        });
        return;
      }

      if (lastStart === range.start) {
        range.start = lastEnd;
      } else {
        lastStart = range.start;
      }
      lastEnd = range.end;
      const fitLeft = range.start < module.end;
      const fitRight = range.end <= module.end;

      // the range doesn't belong to this block.
      if (!fitLeft) {
        m++;
        return;
      }

      // do some calcs.
      addRangeTo(module, range);

      // the range runs across this block and the next one.
      if (!fitRight) {
        m++;
        // need to add this range to the next module, too.
        addRangeTo(modules[m], range);
        return;
      }
    });

    // Re-organise... but also augment the data.

    modules.forEach((module) => {
      // eslint-disable-next-line no-shadow
      const { wrmKey, wrmFile, url, totalBytes, usedBytes, code } = module;

      // Extract context and batch type
      const contextMatch = url.match(contextRegExp);
      const batchType = contextMatch ? contextMatch[1].toLowerCase() : null;
      const isLoadedFromBatch = Boolean(batchType);

      // eslint-disable-next-line no-shadow
      const context = contextMatch ? contextMatch[2].split(',').filter((context) => context.charAt(0) !== '-') : [];
      const unusedBytes = totalBytes - usedBytes;
      const fileExt = wrmFile.split('.').pop();

      coverageByWrmModule.push({
        wrmKey,
        wrmFile,
        url,
        totalBytes,
        used: (usedBytes * 100) / totalBytes,
        usedBytes,
        unused: (unusedBytes * 100) / totalBytes,
        unusedBytes,
        type: isLoadedFromBatch ? batchType : fileExt,
        isLoadedFromBatch,
        batchType,
        context,
        code,
      });
    });
  });

  // Decorator and sort data
  return coverageByWrmModule
    .map((/** @type {WebResourceCoverageMeta} */ webResource) => {
      const isUnused = isWebResourceUnused(webResource);
      const isBig = isWebResourceBig(webResource);

      const newWebResource = /** @type {WebResourceCoverageMeta} */ (
        Object.assign(webResource, {
          isUnused,
          isBig,
        })
      );

      return newWebResource;
    })
    .sort(
      (/** @type {WebResourceCoverageMeta} */ webResourceA, /** @type {WebResourceCoverageMeta} */ webResourceB) =>
        webResourceB.unusedBytes - webResourceA.unusedBytes
    );
}
