/** @typedef {import('../../DevtoolsPanelApp/Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

/**
 * @param {File} file
 * @returns {Promise<CoverageJson>}
 */
export const readCoverageFile = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = (event) => {
      /** @type {string} */
      const content = event.target?.result?.toString() ?? '';

      try {
        const json = /** @type {CoverageJson} */ (JSON.parse(content));

        resolve(json);
      } catch (error) {
        reject(error);
      }
    };

    reader.onerror = (error) => {
      reject(error);
    };

    reader.readAsText(file);
  });
};
