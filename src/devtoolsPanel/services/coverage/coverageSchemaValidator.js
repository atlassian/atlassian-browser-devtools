/** @typedef {import('../../DevtoolsPanelApp/Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

/**
 * @param {CoverageJson} input
 * @returns {Promise<boolean>}
 */
const coverageSchemaValidator = async (input) => {
  /** @type {(schema: CoverageJson) => boolean} */
  let validate;

  // @ts-expect-error IS_EXTENSION is globally defined
  if (IS_EXTENSION) {
    // For production code we need to pre-compile the schema to avoid using eval.
    // The browser extensions enforce disabling the unsafe-eval policy
    ({ default: validate } = await import('./coverageSchemaValidatorCompiled.js'));

    if (typeof validate !== 'function') {
      throw new Error('Failed to import compiled schema validator');
    }
  }
  // In dev mode we want to use schema and AJS validator
  else {
    const { default: Ajv } = await import('ajv');
    const schema = await import('./coverage.schema.json');

    const ajv = new Ajv({
      strict: true,
      strictSchema: true,
      strictTuples: false,
    });

    validate = ajv.compile(schema);
  }

  return validate(input);
};

export default coverageSchemaValidator;
