import evaluateCodeInInspectedWindow from '../chrome/evaluteInInspectedWindow.js';

const detectionStatement = 'Boolean(window.AJS && window.AJS.version)';

const auiDetector = () => evaluateCodeInInspectedWindow(detectionStatement);

export default auiDetector;
