import evaluateCodeInInspectedWindow from '../chrome/evaluteInInspectedWindow.js';

const versionStatement = 'window.AJS && window.AJS.version';

const getAuiVersion = () => evaluateCodeInInspectedWindow(versionStatement);

export default getAuiVersion;
