import { isWebResourceBig } from '../../helpers/webResourceConditions.js';

const wrmRegExp = /\/\* module-key = '(?<key>.*?)', location = '(?<file>.*?)' \*\//g;
const webresourceBatchUrlRegExp = /_\/download\/resources\/(?<key>.*?)\/(?<file>.*?)(?:\?|$)/;

const contextRegExp = /\/contextbatch\/([^/]+)\/([^/]+)/;

/**
 * @param {RegExpMatchArray} match
 * @param {RegExpMatchArray} nextMatch
 * @param {string} code
 * @returns {string}
 */
function getCode(match, nextMatch, code) {
  // code starts after the comment
  const start = /** @type {number} */ (match.index) + match[0].length;

  if (!nextMatch) {
    return code.substr(start);
  }

  const moduleCode = code.substring(start + 1, /** @type {number} */ (nextMatch.index) - 1);

  return moduleCode.trim();
}

/**
 * @typedef {Object} WebResourceMeta
 * @property {string} wrmFile
 * @property {string} code
 * @property {string} wrmKey
 * @property {number} totalBytes
 * @property {string} type
 * @property {boolean} isLoadedFromBatch
 * @property {string | null} batchType
 * @property {string[]} context
 * @property {boolean} isBig
 * @property {string} url
 */

/**
 * @param {Object} options
 * @param {string} options.url
 * @param {string} options.code
 * @returns {import('./webResourcesProcessor').WebResourceMeta[]}
 */
export function processScript({ url, code }) {
  const modules = [];

  // check for wrm modules in the file
  const matches = [...code.matchAll(wrmRegExp)];

  if (matches.length) {
    // the URL is for a context batch.

    // walk through wrm modules in the file
    let lastModule;

    for (let i = 0, max = matches.length; i < max; i++) {
      const match = matches[i];
      const nextMatch = matches[i + 1];

      const wrmKey = match[1];
      const wrmFile = match[2];
      // code starts after the comment
      const start = /** @type {number} */ (match.index) + match[0].length;

      // if it's the first match...
      // TODO: Verify that....
      // if (typeof lastModule === 'undefined') {
      //   // module headers sometimes appear after some banner code,
      //   // like WRMCB try-catch blocks.
      //   if (start !== 0) {
      //     lastModule = {
      //       wrmKey: 'other',
      //       wrmFile: 'unknown',
      //       url,
      //       start: 0,
      //       end: -1,
      //       totalBytes: 0,
      //       code: getCode(match, nextMatch, code),
      //     };
      //     modules.push(lastModule);
      //   }
      // }

      // first, populate module data
      const module = {
        wrmKey,
        wrmFile,
        url,
        start,
        end: -1,
        totalBytes: 0,
        code: getCode(match, nextMatch, code),
      };
      if (lastModule) {
        lastModule.end = start - 1;
        lastModule.totalBytes = lastModule.end - lastModule.start;
      }

      // track the data points
      modules.push(module);

      // keep track of the last file
      lastModule = module;
    }

    // fix final module's end value
    if (lastModule) {
      lastModule.end = code.length;
      lastModule.totalBytes = lastModule.end - lastModule.start;
    }
  } else {
    // the URL could be for a single web-resource file
    const fromUrl = url.match(webresourceBatchUrlRegExp);

    if (fromUrl) {
      const fromUrlGroups = /** @type {{ key: string; file: string }} */ (fromUrl.groups);

      // this whole file is for a single web-resource file
      modules.push({
        wrmKey: fromUrlGroups.key,
        wrmFile: fromUrlGroups.file,
        url,
        start: 0,
        end: code.length,
        totalBytes: code.length,
        code,
      });
    } else {
      return [];
    }
  }

  return modules
    .map((module) => {
      // eslint-disable-next-line no-shadow
      const { wrmKey, wrmFile, url, totalBytes, code } = module;

      // Extract context and batch type
      const contextMatch = url.match(contextRegExp);
      const batchType = contextMatch ? contextMatch[1].toLowerCase() : null;
      const isLoadedFromBatch = Boolean(batchType);

      // eslint-disable-next-line no-shadow
      const context = contextMatch ? contextMatch[2].split(',').filter((context) => context.charAt(0) !== '-') : [];
      const fileExt = /** @type {string} */ wrmFile.split('.').pop();

      /** @type {WebResourceMeta} */
      return {
        wrmKey,
        wrmFile,
        url,
        totalBytes,
        type: isLoadedFromBatch ? batchType : fileExt,
        isLoadedFromBatch,
        batchType,
        context,
        code,
      };
    })
    .map((webResource) => {
      const isBig = isWebResourceBig(webResource);

      const newWebResource = /** @type {WebResourceMeta} */ (
        Object.assign(webResource, {
          isBig,
        })
      );

      return newWebResource;
    });
}
