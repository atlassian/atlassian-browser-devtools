import evaluateCodeInInspectedWindow from '../chrome/evaluteInInspectedWindow.js';

const detectionStatement = "Boolean(window.WRM && (typeof window.WRM.require === 'function'))";

const wrmDetector = () => evaluateCodeInInspectedWindow(detectionStatement);

export default wrmDetector;
