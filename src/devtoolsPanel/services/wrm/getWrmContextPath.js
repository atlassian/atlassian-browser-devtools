import evaluateCodeInInspectedWindow from '../chrome/evaluteInInspectedWindow.js';

// In old version of Atlassian products the contextPath function was part of the AJS namespace
const ajsStatement = "window.AJS && (typeof window.AJS.contextPath === 'function') && window.AJS.contextPath()";
const wrmStatement = "window.WRM && (typeof window.WRM.contextPath === 'function') && window.WRM.contextPath()";

const contextPathDetection = `((${ajsStatement}) || (${wrmStatement})) ?? null`;

const getWrmContextPath = () => evaluateCodeInInspectedWindow(contextPathDetection);

export default getWrmContextPath;
