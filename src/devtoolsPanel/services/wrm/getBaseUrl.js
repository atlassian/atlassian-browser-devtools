import evaluateCodeInInspectedWindow from '../chrome/evaluteInInspectedWindow.js';

const locationStatement = 'window.location.origin';

const getBaseUrl = () => evaluateCodeInInspectedWindow(locationStatement);

export default getBaseUrl;
