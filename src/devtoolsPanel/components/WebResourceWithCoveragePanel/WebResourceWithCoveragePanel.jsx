import { memo, useCallback } from 'react';

import WebResourceInfo from './WebResourceWithCoverageInfo.jsx';
import styles from './WebResourceWithCoveragePanel.less';
import { getEventName, trackEvent } from '../../../common/analytics/analytics.js';
import { useAnalyticsContext } from '../AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import DependencyTree from '../DependencyTree/DependencyTree.jsx';
import ErrorWrapper from '../ErrorWrapper/ErrorWrapper.jsx';
import PanelHint from '../PanelHint/PanelHint.jsx';
import PanelWrapper from '../PanelWrapper/PanelWrapper.jsx';
import Tabs, { Tab } from '../Tabs/Tabs.jsx';
import WebResourceSourcePanel from '../WebResourceSoucePanel/WebResourceSourcePanel.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/** @type {React.FC<{ webResource: WebResourceMeta }>} */
const WebResourceWithCoveragePanel = ({ webResource }) => {
  const analyticsContext = useAnalyticsContext();

  const onTabSelected = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'RESOURCE_DETAILS', 'TAB_SELECTED'), { value });
    },
    [analyticsContext]
  );

  const content = webResource ? (
    <Tabs onTabSelected={onTabSelected}>
      <Tab label="Summary">
        <WebResourceInfo webResource={webResource} />
      </Tab>
      <Tab label="Dependency Tree">
        <DependencyTree webResource={webResource} />
      </Tab>
      <Tab label="Source">
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <ErrorWrapper fallback={<p>We can't display the source code for this resource</p>}>
          <WebResourceSourcePanel webResource={webResource} />
        </ErrorWrapper>
      </Tab>
    </Tabs>
  ) : (
    <PanelHint>Click on the table row item to display more information about the web-resource.</PanelHint>
  );

  return <PanelWrapper className={styles.WebResourceWithCoveragePanel}>{content}</PanelWrapper>;
};

export default memo(WebResourceWithCoveragePanel);
