import { memo } from 'react';

import styles from './WebResourceWithCoverageInfo.less';
import { usePlugin } from '../../DevtoolsPanelApp/Plugins/PluginsProvider.jsx';
import { formatSize } from '../../helpers/formatSize.js';
import { getContextLabel } from '../../helpers/webResoucesUtils.js';
import Percentage from '../Percentage/Percentage.jsx';
import ResourceSizeFormatted from '../WebResources/ResourceSizeFormatted.jsx';
import UnusedBytesFormatted from '../WebResources/UnusedBytesFormatted.jsx';
import UnusedPercentageFormatted from '../WebResources/UnusedPercentageFormatted.jsx';
import WebResourceLabel from '../WebResources/WebResourceLabel/WebResourceLabel.jsx';
import WebResourceLabelsGroup from '../WebResources/WebResourceLabel/WebResourcesGroup.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

/** @type {React.FC<{ webResource: WebResourceCoverageMeta }>} */
const WebResourceWithCoverageInfo = ({ webResource }) => {
  const { isLoadedFromBatch } = webResource;
  const plugin = usePlugin(webResource);

  return (
    <div className={styles.WebResourceWithCoverageInfo}>
      <dl>
        <dt>Resource key</dt>
        <dd>{webResource.wrmKey}</dd>

        <dt>Plugin name</dt>
        <dd>{plugin !== null ? plugin.name : 'unknown'}</dd>

        <dt>Resource file</dt>
        <dd>{webResource.wrmFile}</dd>

        <dt>Type</dt>
        <dd>{webResource.type}</dd>

        <dt>Loaded from batch</dt>
        <dd>{isLoadedFromBatch ? 'Yes' : 'No'}</dd>

        {!isLoadedFromBatch && (
          <>
            <dt>Resource URL</dt>
            <dd>
              <a href={webResource.url} rel="nofollow noreferrer" target="_blank">
                {webResource.url}
              </a>
            </dd>
          </>
        )}

        {isLoadedFromBatch && (
          <>
            <dt>Batch type</dt>
            <dd>{webResource.batchType ? webResource.batchType.toUpperCase() : 'unknown'}</dd>
          </>
        )}

        {isLoadedFromBatch && (
          <>
            <dt>Context(s)</dt>
            <dd>
              <WebResourceLabelsGroup>
                {webResource.context.map((context) => (
                  <WebResourceLabel key={context}>{getContextLabel(context)}</WebResourceLabel>
                ))}
              </WebResourceLabelsGroup>
            </dd>

            <dt>Batch URL</dt>
            <dd>
              <a href={webResource.url} rel="nofollow noreferrer" target="_blank">
                {webResource.url}
              </a>
            </dd>
          </>
        )}

        <dt>Resource size</dt>
        <dd>
          <ResourceSizeFormatted webResource={webResource} />
        </dd>

        <dt>Used bytes</dt>
        <dd>
          {formatSize(webResource.usedBytes)} <Percentage>{webResource.used}</Percentage>
        </dd>

        <dt>Unused bytes</dt>
        <dd>
          <UnusedBytesFormatted webResource={webResource} /> <UnusedPercentageFormatted webResource={webResource} />
        </dd>
      </dl>
    </div>
  );
};

export default memo(WebResourceWithCoverageInfo);
