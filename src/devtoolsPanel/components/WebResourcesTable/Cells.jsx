import { Fragment, memo } from 'react';

import ResourceSizeFormatted from '../WebResources/ResourceSizeFormatted.jsx';
import { originalRowPropsMemoComparator, valuePropsMemoComparator } from '../WebResourcesWithCoverageTable/Cells.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/** @type {React.FC<{ row: { original: WebResourceMeta } }>} */
const ResourceSizeCell = ({ row }) => {
  const webResource = row.original;

  return <ResourceSizeFormatted webResource={webResource} />;
};

/** @type {React.FC<{ value: string }>} */
const ResourceTypeCell = ({ value: resourceType }) => <Fragment>{resourceType ? resourceType.toUpperCase() : 'unknown'}</Fragment>;

const ResourceSizeCellMemoed = memo(ResourceSizeCell, originalRowPropsMemoComparator);
const ResourceTypeCellMemoed = memo(ResourceTypeCell, valuePropsMemoComparator);

export { ResourceSizeCellMemoed as ResourceSizeCell, ResourceTypeCellMemoed as ResourceTypeCell };
