import { memo, useMemo } from 'react';

import { ResourceTypeCell, ResourceSizeCell } from './Cells.jsx';
import SelectableTable from '../Table/SelectableTable.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

const colGroups = (
  <colgroup>
    <col />
    {/* Resource key column */}
    <col width="65" />
    {/* Resource type column */}
    <col width="95" />
    {/* Size column */}
  </colgroup>
);

/**
 * @typedef {Object} RowComponentProps
 * @property {unknown} rowData
 * @property {string} className
 * @property {React.ReactNode} children
 */

/** @type {React.FC<{ webResources: WebResourceMeta[]; RowComponent: React.FC<RowComponentProps>; onSelected: () => void }>} >} */
const WebResourcesTable = ({ webResources, onSelected, RowComponent }) => {
  const columns = useMemo(
    () => [
      { Header: 'Resource key', accessor: 'wrmKey' },
      { Header: 'Type', accessor: 'type', Cell: ResourceTypeCell },
      {
        Header: 'Size',
        accessor: 'totalBytes',
        Cell: ResourceSizeCell,
        cellStyle: { textAlign: 'right' },
      },
    ],
    []
  );
  const data = useMemo(() => {
    return webResources.map((webResource) => {
      const { wrmKey, wrmFile, url } = webResource;
      const rowId = `${wrmKey}-${wrmFile}-${url}`;

      return { ...webResource, rowId };
    });
  }, [webResources]);

  return <SelectableTable RowComponent={RowComponent} colGroups={colGroups} columns={columns} data={data} onSelected={onSelected} />;
};

export default memo(WebResourcesTable);
