import classNamesBind from 'classnames/bind';
import { forwardRef, memo } from 'react';

import styles from './WebResourceRow.less';
import { Row } from '../Table/Row.jsx';

const classNames = classNamesBind.bind(styles);

/** @typedef {import('react')} React */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

/** @type {React.ForwardRefExoticComponent<{ rowData: WebResourceCoverageMeta; className: string; children: React.ReactNode }>} */
const WebResourceRow = forwardRef((props, ref) => {
  const { rowData, className, children, ...rowProps } = props;

  return (
    <Row
      {...rowProps}
      className={classNames(className, {
        [styles.WebResourceIsBig]: rowData.isBig,
        [styles.WebResourceIsUnused]: rowData.isUnused,
      })}
      innerRef={ref}
    >
      {children}
    </Row>
  );
});

WebResourceRow.displayName = 'WebResourceRow';

export default memo(WebResourceRow);
