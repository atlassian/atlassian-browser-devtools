import classNames from 'classnames';
import { memo } from 'react';

import styles from './Icon.less';

export const ExperimentIcon = 'ExperimentIcon';

/** @typedef {import('react')} React */

/** @type {React.FC<{ icon: string }>} */
const Icon = ({ icon }) => {
  return <span className={classNames(styles.Icon, styles[icon])} />;
};

export default memo(Icon);
