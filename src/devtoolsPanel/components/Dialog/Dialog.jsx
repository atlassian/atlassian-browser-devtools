import dialogPolyfill from 'dialog-polyfill';
import { memo, useCallback, useLayoutEffect, useRef } from 'react';

import styles from './Dialog.less';
import { IS_FIREFOX } from '../../../common/devMode.js';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; showCloseButton?: boolean; onClose?: () => void }>} */
const Dialog = ({ onClose, children, showCloseButton = true }) => {
  const modalRef = /** @type {React.MutableRefObject<HTMLDialogElement>} */ (useRef());
  const buttonRef = /** @type {React.MutableRefObject<HTMLButtonElement>} */ (useRef());

  useLayoutEffect(function openOnMount() {
    const modal = modalRef.current;

    if (IS_FIREFOX) {
      dialogPolyfill.registerDialog(modal);
    }

    modal.showModal();

    if (buttonRef.current) {
      buttonRef.current.focus();
    }

    return () => {
      if (modal.open) {
        modal.open = false;
      }
    };
  }, []);

  useLayoutEffect(
    function closeOnEscape() {
      const modal = modalRef.current;

      const closeHandler = () => {
        if (typeof onClose === 'function') {
          onClose();
        }
      };

      modal.addEventListener('close', closeHandler);

      return () => {
        modal.removeEventListener('close', closeHandler);
      };
    },
    [onClose]
  );

  const close = useCallback(() => {
    if (modalRef.current && modalRef.current.close) {
      modalRef.current.close();
    }

    if (typeof onClose === 'function') {
      onClose();
    }
  }, [onClose]);

  return (
    <dialog ref={modalRef} className={styles.Dialog}>
      {children}
      {showCloseButton && (
        <p className={styles.Buttons}>
          <button ref={buttonRef} className={styles.Button} type="button" onClick={close}>
            Close
          </button>
        </p>
      )}
    </dialog>
  );
};

export default memo(Dialog);
