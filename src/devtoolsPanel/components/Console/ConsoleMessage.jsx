import classNamesBind from 'classnames/bind';
import { format } from 'date-fns';
import { memo } from 'react';

import styles from './ConsoleMessage.less';

const classNames = classNamesBind.bind(styles);
const timeFormat = 'HH:mm:ss.SSS';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; showTimestamps?: boolean; timestamp: number; anchor?: React.ReactNode; isError?: boolean; isWarning?: boolean; className?: string | null }>} */
const ConsoleMessage = ({ children, showTimestamps = false, timestamp, anchor, isError = false, isWarning, className }) => {
  const formattedTimestamp = showTimestamps ? format(new Date(timestamp), timeFormat) : false;

  const messageClassName = classNames(
    styles.ConsoleMessageWrapper,
    {
      ConsoleMessageError: isError,
      ConsoleMessageWarning: isWarning,
    },
    className
  );

  const timestampNode = formattedTimestamp ? <span className={styles.ConsoleTimestamp}>{formattedTimestamp}</span> : null;

  return (
    <div className={messageClassName}>
      <div className={styles.ConsoleMessageRow}>
        {timestampNode}

        <span className={styles.ConsoleMessageText}>{children}</span>

        <span className={styles.ConsoleAnchor}>{anchor}</span>
      </div>
    </div>
  );
};

export default memo(ConsoleMessage);
