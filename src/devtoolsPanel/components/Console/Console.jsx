import { memo } from 'react';

import styles from './Console.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNodeArray; emptyOutput: React.ReactNode }>} */
const Console = ({ children = [], emptyOutput = null }) => {
  const output = children && children.length ? <div className={styles.ConsoleOutput}>{children}</div> : emptyOutput;

  return <div className={styles.Console}>{output}</div>;
};

export default memo(Console);
