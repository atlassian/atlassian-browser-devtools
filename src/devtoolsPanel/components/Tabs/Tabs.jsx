import classNames from 'classnames';
import { useState, Children, useLayoutEffect, memo, useMemo, useRef, Fragment } from 'react';

import styles from './Tabs.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; onTabSelected?: (arg0: string) => void }>} */
const Tabs = ({ children, onTabSelected }) => {
  const [selectedTab, setSelectedTab] = /** @type {[number | null, (arg0: number | null) => void]} */ (useState(null));

  let tabRef = /** @type {React.MutableRefObject<React.ReactElement>} */ (useRef());
  let preSelected = null;

  Children.forEach(children, (child, index) => {
    const { isSelected } = /** @type {React.ReactElement} */ (child).props;

    if (isSelected || preSelected === null) {
      preSelected = index;
    }
  });

  preSelected = preSelected === null ? 0 : preSelected;

  const tabsLabels = useMemo(() => {
    return Children.map(children, (child, index) => {
      const reactChild = /** @type {React.ReactElement} */ (child);

      const { label } = reactChild.props;
      const isSelected = selectedTab !== null ? selectedTab === index : preSelected === index;

      if (isSelected) {
        tabRef.current = reactChild;
      }

      return (
        /* eslint-disable react/jsx-no-bind */
        <TabLabelMemoed
          isSelected={isSelected}
          onClick={() => {
            // TODO: Refactor me
            if (typeof onTabSelected === 'function') {
              onTabSelected(label);
            }

            setSelectedTab(index);
          }}
        >
          {label}
        </TabLabelMemoed>
        /* eslint-disable react/jsx-no-bind */
      );
    });
  }, [children, onTabSelected, preSelected, selectedTab]);

  useLayoutEffect(() => {
    if (selectedTab === null) {
      setSelectedTab(0);
    }
  }, [selectedTab]);

  const tabElement = tabRef.current;

  return (
    <div className={styles.Tabs}>
      <div className={styles.TabLabels}>{tabsLabels}</div>
      <div className={styles.TabContent}>{tabElement}</div>
    </div>
  );
};

/** @type {React.FC<{ children: React.ReactNode; isSelected: boolean; onClick: () => void }>} */
const TabLabel = ({ children, onClick, isSelected }) => (
  <button className={classNames(styles.TabLabel, { [styles.TabLabelActive]: isSelected })} type="button" onClick={onClick}>
    {children}
  </button>
);

const TabLabelMemoed = memo(TabLabel);

/** @type {React.FC<{ children: React.ReactNode; label: string; isSelected?: boolean }>} */
// eslint-disable-next-line no-unused-vars
export const Tab = ({ children, label, isSelected }) => {
  return <Fragment>{children}</Fragment>;
};

export default memo(Tabs);
