import classNames from 'classnames';
import { memo } from 'react';

import styles from './ToolbarButton.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; className?: string; title?: string; disabled?: boolean; onClick: () => void }>} */
const ToolbarButton = ({ children, className, onClick, title, disabled = false }) => (
  <button className={classNames(styles.ToolbarButton, className)} disabled={disabled} title={title} onClick={onClick}>
    {children}
  </button>
);

export default memo(ToolbarButton);
