import { forwardRef, memo, useCallback, useMemo, useRef } from 'react';
import Split from 'react-split';

import styles from './SplitPanel.less';

/** @typedef {import('react')} React */
/** @typedef {import('split.js')} SplitJs */

/** @type {React.ForwardRefExoticComponent<{ children: React.ReactNode; direction?: string; sizes: number[]; minSizes: number | number[] }>} */
const SplitPanel = forwardRef(({ children, direction = 'horizontal', sizes, minSizes }, ref) => {
  const splitRef = /** @type {React.MutableRefObject<{ split: { parent: HTMLElement } }>} */ (useRef());

  const style = useMemo(() => {
    return {
      display: 'flex',
      flexDirection: direction === 'horizontal' ? 'row' : 'column',
      flex: 1,
    };
  }, [direction]);

  const getGutterStyle = useCallback(() => {
    const gutterStyles =
      direction === 'horizontal'
        ? {
            marginLeft: '-6px',
            marginRight: '-6px',
            width: '12px',
            cursor: 'ew-resize',
          }
        : {
            marginTop: '-6px',
            marginBottom: '-6px',
            height: '12px',
            cursor: 'ns-resize',
          };

    return {
      userSelect: 'initial',
      zIndex: 1,
      ...gutterStyles,
    };
  }, [direction]);

  const onDragEnd = /** @type {(sizes: number[]) => void} */ (
    useCallback(() => {
      const { split } = splitRef.current;
      const gutterEl = /** @type {HTMLElement} */ (split.parent.querySelector(':scope > .gutter'));

      // Defer the style update since the Split.js will erase all the styles...
      setTimeout(() => {
        Object.assign(gutterEl.style, getGutterStyle());
      });
    }, [getGutterStyle])
  );

  return (
    <div ref={ref} className={styles.SplitPanel}>
      {/* @ts-expect-error missing types */}
      <Split
        ref={splitRef}
        cursor={direction === 'horizontal' ? 'col-resize' : 'row-resize'}
        direction={direction}
        gutterSize={1}
        gutterStyle={getGutterStyle}
        minSize={minSizes}
        sizes={sizes}
        style={style}
        onDragEnd={onDragEnd}
      >
        {children}
      </Split>
    </div>
  );
});

SplitPanel.displayName = 'SplitPanel';

const cssCustomPropertyName = '--panel-min-height';
const panelMinHeight = getComputedStyle(document.documentElement).getPropertyValue(cssCustomPropertyName);

if (!panelMinHeight) {
  throw new Error(`Can't read the value from "${cssCustomPropertyName}" CSS Custom Property`);
}

export const PANEL_MIN_HEIGHT = parseInt(panelMinHeight);

export default memo(SplitPanel);
