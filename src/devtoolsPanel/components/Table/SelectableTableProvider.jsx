import { createContext, memo, useCallback, useContext, useMemo, useState } from 'react';

/** @typedef {import('react')} React */

/**
 * @typedef {Object} SelectableTableContext
 * @property {number} selectedRow
 * @property {(arg0: number) => void} selectRow
 */

const context = /** @type {React.Context<SelectableTableContext>} */ (
  createContext({
    selectedRow: -1,
    // eslint-disable-next-line no-unused-vars
    selectRow(arg0) {},
  })
);

const { Provider } = context;

/** @type {React.FC<{ children: React.ReactNode }>} */
const SelectableTableProvider = ({ children }) => {
  const [selectedRow, setSelectedRow] = /** @type {[number | null, (arg0: number | null) => void]} */ (useState(null));

  const selectRow = useCallback((rawRowId) => {
    const rowId = parseInt(rawRowId);
    setSelectedRow(rowId);
  }, []);

  const contextValue = /** @type {SelectableTableContext} */ (
    useMemo(
      () => ({
        selectedRow,
        selectRow,
      }),
      [selectedRow, selectRow]
    )
  );

  return <Provider value={contextValue}>{children}</Provider>;
};

const SelectableTableProviderMemoed = memo(SelectableTableProvider);

export const useSelectRow = (rawRowId, rowData, onSelected) => {
  const { selectRow } = useContext(context);

  const rowId = parseInt(rawRowId);

  return useCallback(() => {
    selectRow(rowId);
    onSelected(rowData);
  }, [selectRow, rowId, onSelected, rowData]);
};

export const useIsRowSelected = (rawRowId) => {
  const { selectedRow } = useContext(context);

  const rowId = parseInt(rawRowId);
  return selectedRow === rowId;
};

const findNextRow = (rows, selectedRowId) => {
  const selectedRow = rows.find(({ id }) => parseInt(id) === selectedRowId);
  const currentIndex = rows.indexOf(selectedRow);
  const nextIndex = Math.min(currentIndex + 1, rows.length - 1);
  const nextRow = rows[nextIndex];

  return nextRow;
};

const findPrevRow = (rows, selectedRowId) => {
  const selectedRow = rows.find(({ id }) => parseInt(id) === selectedRowId);
  const currentIndex = rows.indexOf(selectedRow);
  const nextIndex = Math.max(currentIndex - 1, 0);
  const prevRow = rows[nextIndex];

  return prevRow;
};

export const useSelectNextRow = (onSelected, rows) => {
  const { selectedRow: selectedRowId, selectRow } = useContext(context);

  return useCallback(() => {
    if (selectedRowId === null) {
      return;
    }

    const newRow = findNextRow(rows, selectedRowId);

    selectRow(newRow.id);
    onSelected(newRow.original);
  }, [onSelected, rows, selectRow, selectedRowId]);
};

export const useSelectPrevRow = (onSelected, rows) => {
  const { selectedRow: selectedRowId, selectRow } = useContext(context);

  return useCallback(() => {
    if (selectedRowId === null) {
      return;
    }

    const newRow = findPrevRow(rows, selectedRowId);

    selectRow(newRow.id);
    onSelected(newRow.original);
  }, [onSelected, rows, selectRow, selectedRowId]);
};

export { SelectableTableProviderMemoed as SelectableTableProvider };
