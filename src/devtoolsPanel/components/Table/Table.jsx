import classNames from 'classnames';
import { forwardRef, memo } from 'react';

import styles from './Table.less';

/** @typedef {import('react')} React */

/** @type {React.ForwardRefExoticComponent<{ children?: React.ReactNode; className?: string }>} */
const Table = forwardRef(({ children, className, ...props }, ref) => (
  <table className={classNames(styles.Table, className)} {...props} ref={ref}>
    {children}
  </table>
));

Table.displayName = 'Table';

export default memo(Table);
