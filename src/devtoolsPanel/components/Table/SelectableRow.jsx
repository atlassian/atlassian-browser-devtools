import { memo, useLayoutEffect, useRef } from 'react';

import Row from './Row.jsx';
import styles from './SelectableRow.less';
import { useIsRowSelected, useSelectRow } from './SelectableTableProvider.jsx';
import { scrollIntoView } from '../../../common/scrollIntoView.js';

/** @typedef {import('react')} React */
/** @typedef {import('./Row').RowProps} RowProps */

// FIXME: Verify types here. We are passing rowData an onClick to a Row... should we ?
/** @typedef {RowProps & { rowData: unknown; onClick: () => void }} SelectableRowProps */

/** @type {React.FC<{ children: React.ReactNode; isEven: boolean; RowComponent: React.FC<SelectableRowProps>; rowData: unknown; rowId; number; onSelected: (arg0: unknown) => void }>} */
const SelectableRow = ({ children, rowId, onSelected, isEven, RowComponent, rowData = Row }) => {
  const selectRow = useSelectRow(rowId, rowData, onSelected);
  const isSelected = useIsRowSelected(rowId);
  const rowRef = /** @type {React.MutableRefObject<HTMLTableRowElement>} */ (useRef());

  useLayoutEffect(
    function scrollRowIntoView() {
      const rowEl = rowRef.current;

      if (isSelected) {
        scrollIntoView(rowEl, {
          block: 'nearest',
        });
      }
    },
    [isSelected]
  );

  return (
    <RowComponent
      ref={rowRef}
      className={{
        [styles.SelectableRow]: true,
        [styles.RowSelected]: isSelected,
      }}
      isEven={isEven}
      rowData={rowData}
      onClick={selectRow}
    >
      {children}
    </RowComponent>
  );
};

export default memo(SelectableRow);
