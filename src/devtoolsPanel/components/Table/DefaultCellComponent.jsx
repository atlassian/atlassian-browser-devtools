import { Fragment, memo } from 'react';

const valuePropsComparator = (prevProps, nextProps) => {
  return prevProps.value === nextProps.value;
};

/** @typedef {import('react')} React */

/** @type {React.FC<{ value: any }>} */
const DefaultCellComponent = ({ value }) => <Fragment>{value}</Fragment>;

// We want to memoize the component to avoid re-rendering and optimize the performance issue with react-table
export default memo(DefaultCellComponent, valuePropsComparator);
