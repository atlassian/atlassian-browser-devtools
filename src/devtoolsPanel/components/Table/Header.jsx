import classNames from 'classnames';
import { memo } from 'react';

import styles from './Header.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; className?: string; title?: string }>} */
const Header = ({ children, className, ...props }) => {
  // Remove title...
  // eslint-disable-next-line no-unused-vars
  const { title, ...rest } = props;

  return (
    <th className={classNames(styles.Header, className)} {...rest}>
      {children}
    </th>
  );
};

export default memo(Header);
