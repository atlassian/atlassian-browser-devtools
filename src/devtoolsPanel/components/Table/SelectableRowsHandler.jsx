import { memo, useEffect, Fragment } from 'react';

import { useSelectNextRow, useSelectPrevRow } from './SelectableTableProvider.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; rows: React.ReactNode; onSelected: () => void }>} */
const SelectableRowsHandler = ({ children, rows, onSelected }) => {
  const selectPrevRow = useSelectPrevRow(onSelected, rows);
  const selectNextNow = useSelectNextRow(onSelected, rows);

  useEffect(
    function keyboardNavigation() {
      const handler = (event) => {
        const isKeyDown = event.keyCode === 40;
        const isKeyUp = event.keyCode === 38;

        if (isKeyDown || isKeyUp) {
          event.preventDefault();

          if (isKeyUp) {
            selectPrevRow();
          } else {
            selectNextNow();
          }
        }
      };

      window.addEventListener('keydown', handler);

      return () => {
        window.removeEventListener('keydown', handler);
      };
    },
    [rows, selectNextNow, selectPrevRow]
  );

  return <Fragment>{children}</Fragment>;
};

export default memo(SelectableRowsHandler);
