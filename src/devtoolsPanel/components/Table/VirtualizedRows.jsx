import debounce from 'lodash/debounce';
import { memo, useEffect, useMemo, useRef, useState } from 'react';

import CellWrapper from './CellWrapper.jsx';
import Row from './Row.jsx';
import SelectableRow from './SelectableRow.jsx';
import Table from './Table.jsx';
import styles from './VirtualizedRows.less';

/** @typedef {import('react')} React */

/**
 * @typedef {Object} ScrollingState
 * @property {number} visibleRowsCount
 * @property {number} topOffset
 * @property {number} visibleRowOffset
 * @property {number} rowsOnTopCount
 */

const ROW_HEIGHT = 21;

/**
 * @param {HTMLDivElement} container
 * @returns {ScrollingState}
 */
const getScrollingState = (container) => {
  const { scrollTop, clientHeight: viewportHeight } = container;

  const extraRows = 100;

  const rowsOnTopCount = Math.max(0, Math.floor(scrollTop / ROW_HEIGHT) - extraRows);
  const visibleRowsCount = Math.ceil(viewportHeight / ROW_HEIGHT) + extraRows * 2;
  const visibleRowOffset = rowsOnTopCount;

  return {
    topOffset: scrollTop,
    rowsOnTopCount,
    visibleRowsCount,
    visibleRowOffset,
  };
};

/**
 * @typedef {Object} RowComponentProps
 * @property {unknown} rowData
 * @property {string} className
 * @property {React.ReactNode} children
 */

/**
 * @typedef {Object} Props
 * @property {any} tableProps
 * @property {any} bodyProps
 * @property {React.ReactNode} colGroups
 * @property {any[]} columns
 * @property {any[]} rows
 * @property {React.FC<RowComponentProps>} RowComponent
 * @property {(arg0: unknown) => void} prepareRow
 * @property {() => void} onSelected
 */

/** @type {React.FC<Props>} */
const VirtualizedRows = ({ tableProps, bodyProps, colGroups, columns, rows, prepareRow, onSelected, RowComponent = Row }) => {
  const fakeRowRef = /** @type {React.MutableRefObject<HTMLTableRowElement>} */ (useRef());
  const tableRef = /** @type {React.MutableRefObject<HTMLElement>} */ (useRef());
  const wrapperRef = /** @type {React.MutableRefObject<HTMLDivElement>} */ (useRef());
  const innerRef = /** @type {React.MutableRefObject<HTMLDivElement>} */ (useRef());

  const [scrollingState, setScrollingState] = /** @type {[ScrollingState | null, (arg0: ScrollingState | null) => void]} */ (
    useState(null)
  );

  const rowsCount = useMemo(() => rows.length, [rows]);

  useEffect(
    function setInnerWrapperHeight() {
      if (innerRef.current) {
        innerRef.current.style.height = `${ROW_HEIGHT * rowsCount}px`;
      }
    },
    [rowsCount]
  );

  // Scrolling support
  useEffect(function setInitialState() {
    const container = wrapperRef.current;

    if (!container) {
      return;
    }

    const newScrollingState = getScrollingState(container);

    if (fakeRowRef.current) {
      const newHeight = newScrollingState.rowsOnTopCount * ROW_HEIGHT;

      // The fix of height value is required for Firefox to avoid rendering too long row when the height equals 0
      fakeRowRef.current.style.height = `${newHeight === 0 ? 0.01 : newHeight}px`;
    }

    setScrollingState(newScrollingState);
  }, []);

  const handleScrolling = useMemo(() => {
    return debounce(
      (/** @type {React.SyntheticEvent} */) => {
        const container = wrapperRef.current;

        // The container can't be unmounted
        if (!container) {
          return;
        }

        const newScrollingState = getScrollingState(container);

        if (fakeRowRef.current) {
          const newHeight = newScrollingState.rowsOnTopCount * ROW_HEIGHT;

          // The fix of height value is required for Firefox to avoid rendering too long row when the height equals 0
          fakeRowRef.current.style.height = `${newHeight === 0 ? 0.01 : newHeight}px`;
        }

        setScrollingState(newScrollingState);
      },
      20,
      {
        maxWait: 100,
      }
    );
  }, []);

  useEffect(
    function scrollContainer() {
      const wrapper = /** @type {HTMLDivElement} */ wrapperRef.current;

      wrapper.addEventListener('scroll', handleScrolling);

      return () => {
        wrapper.removeEventListener('scroll', handleScrolling);
      };
    },
    [handleScrolling]
  );

  useEffect(
    function resizeContainer() {
      const wrapper = /** @type {HTMLDivElement} */ wrapperRef.current;

      const resizeObserver = new ResizeObserver(() => {
        handleScrolling();
      });

      resizeObserver.observe(wrapper);

      return () => {
        resizeObserver.disconnect();
      };
    },

    [handleScrolling]
  );

  // Calculate visible rows
  const visibleRows = useMemo(() => {
    if (!scrollingState) {
      return null;
    }

    const { visibleRowsCount, visibleRowOffset } = scrollingState;

    const newRows = [];
    const limit = Math.min(visibleRowOffset + visibleRowsCount, rows.length);
    for (let i = visibleRowOffset; i < limit; i++) {
      const row = rows[i];

      prepareRow(row);

      /* eslint-disable react/jsx-key */
      newRows.push(
        /* eslint-disable react/jsx-key, react/jsx-no-bind */
        <SelectableRow
          {...row.getRowProps()}
          RowComponent={RowComponent}
          isEven={i % 2 === 0}
          rowData={row.original}
          rowId={row.id}
          onSelected={onSelected}
        >
          {row.cells.map((cell) => {
            //debugger;
            return <CellWrapper {...cell.getCellProps([{ style: cell.column.cellStyle }])}>{cell.render('Cell')}</CellWrapper>;
          })}
        </SelectableRow>
      );
      /* eslint-enable react/jsx-key, react/jsx-no-bind */
    }

    return newRows;
  }, [RowComponent, onSelected, prepareRow, rows, scrollingState]);

  return (
    /* @ts-expect-error ts-migrate(TS2322) FIXME: Property 'ref' does not exist on HTMLDivElement. */
    <div ref={wrapperRef} className={styles.VirtualizedRows} rel="rows-outer-wrapper">
      <div ref={innerRef} className={styles.InnerWrapper}>
        <Table className={styles.Table} rel="rows" {...tableProps} ref={tableRef}>
          {colGroups}
          <tbody {...bodyProps}>
            <Row key="fake-row" ref={fakeRowRef} className={styles.FakeRow}>
              <CellWrapper colspan={columns.length} />
            </Row>
            {visibleRows}
            <Row key="empty-row" className="RowEmpty">
              {columns.map(({ accessor }) => (
                <CellWrapper key={accessor} />
              ))}
            </Row>
          </tbody>
        </Table>
      </div>
    </div>
  );
};

export default memo(VirtualizedRows);
