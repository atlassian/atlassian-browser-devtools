import classNamesBind from 'classnames/bind';
import { forwardRef, memo } from 'react';

import styles from './Row.less';

const classNames = classNamesBind.bind(styles);

/** @typedef {import('react')} React */
/** @typedef {import('classnames').Argument} ClassNameArgument */

/**
 * @typedef {Object} RowProps
 * @property {React.ReactNode} children
 * @property {React.Ref<HTMLTableRowElement>} [ref]
 * @property {ClassNameArgument} className
 * @property {boolean} [isEven]
 */

/** @type {React.FC<RowProps & { innerRef: React.Ref<HTMLTableRowElement> }>} */
const Row = ({ className, children, isEven, innerRef, ...props }) => (
  <tr
    className={classNames(styles.Row, className, {
      [styles.RowEven]: isEven,
      [styles.RowOdd]: !isEven,
    })}
    {...props}
    ref={innerRef}
  >
    {children}
  </tr>
);

const MemoedRow = memo(Row);

export { MemoedRow as Row };

/** @type {React.ForwardRefExoticComponent<RowProps>} */
const ForwardedRow = forwardRef((props, ref) => {
  return <Row innerRef={ref} {...props} />;
});

ForwardedRow.displayName = 'Row';

export default memo(ForwardedRow);
