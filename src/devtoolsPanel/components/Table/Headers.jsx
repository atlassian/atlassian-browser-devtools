import { memo } from 'react';

import styles from './Headers.less';
import Table from './Table.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; colGroups: React.ReactNode; tableProps: React.HTMLProps<HTMLTableElement> }>} */
const Headers = ({ children, colGroups, tableProps }) => (
  <Table className={styles.Headers} rel="headers" {...tableProps}>
    {colGroups}
    <thead>{children}</thead>
  </Table>
);

export default memo(Headers);
