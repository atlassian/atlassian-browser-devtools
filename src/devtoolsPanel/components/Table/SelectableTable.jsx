import debounce from 'lodash/debounce';
import { memo, useLayoutEffect, useRef } from 'react';
import { useTable, useSortBy, useRowSelect } from 'react-table';

import DefaultCellComponent from './DefaultCellComponent.jsx';
import Headers from './Headers.jsx';
import DefaultRow from './Row.jsx';
import SelectableRowsHandler from './SelectableRowsHandler.jsx';
import styles from './SelectableTable.less';
import { SelectableTableProvider } from './SelectableTableProvider.jsx';
import SortableHeader from './SortableHeader.jsx';
import VirtualizedRows from './VirtualizedRows.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/**
 * @typedef {Object} RowComponentProps
 * @property {unknown} rowData
 * @property {string} className
 * @property {React.ReactNode} children
 */

/** @type {React.FC<{ columns: Array<*>; data: Array<unknown>; colGroups: React.ReactNode; RowComponent: React.FC<RowComponentProps>; CellComponent?: React.FC; onSelected: () => void  }>} */
const SelectableTable = ({ columns, data, onSelected, colGroups, RowComponent, CellComponent = DefaultCellComponent }) => {
  const wrapperRef = /** @type {React.MutableRefObject<HTMLDivElement>} */ (useRef());

  useLayoutEffect(
    function resizeHeaders() {
      const wrapper = wrapperRef.current;
      const headers = /** @type {HTMLTableElement} */ (wrapper.querySelector('table[rel="headers"]'));
      const rows = /** @type {HTMLTableElement} */ wrapper.querySelector('table[rel="rows"]');

      if (!headers || !rows) {
        return;
      }

      const rowsWrapper = /** @type {HTMLDialogElement} */ (rows.closest('[rel="rows-outer-wrapper"]'));

      const addScrollbarGap = () => {
        const scrollWidth = rowsWrapper.offsetWidth - rowsWrapper.scrollWidth;

        headers.style.paddingRight = scrollWidth ? `${scrollWidth}px` : '';
      };

      addScrollbarGap();

      // Add listeners
      const addScrollbarGapDebounced = debounce(addScrollbarGap, 200);

      window.addEventListener('resize', addScrollbarGapDebounced);

      const resizeObserver = new ResizeObserver(addScrollbarGap);
      resizeObserver.observe(wrapper);

      // eslint-disable-next-line consistent-return
      return () => {
        window.removeEventListener('resize', addScrollbarGapDebounced);
        resizeObserver.disconnect();
      };
    },
    [data]
  );

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns,
      data,
      Cell: CellComponent,
    },
    useSortBy,
    useRowSelect
  );

  /* eslint-disable react/jsx-key */
  return (
    <div ref={wrapperRef} className={styles.SelectableTable}>
      <SelectableTableProvider>
        <SelectableRowsHandler rows={rows} onSelected={onSelected}>
          <Headers colGroups={colGroups} tableProps={getTableProps()}>
            {headerGroups.map((headerGroup) => (
              <DefaultRow {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <SortableHeader column={column} {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {column.render('Header')}
                  </SortableHeader>
                ))}
              </DefaultRow>
            ))}
          </Headers>

          <VirtualizedRows
            RowComponent={RowComponent}
            bodyProps={getTableBodyProps()}
            colGroups={colGroups}
            columns={columns}
            prepareRow={prepareRow}
            rows={rows}
            tableProps={getTableProps()}
            onSelected={onSelected}
          />
        </SelectableRowsHandler>
      </SelectableTableProvider>
    </div>
  );
  /* eslint-enable react/jsx-key */
};

export default memo(SelectableTable);
