import { memo } from 'react';

import styles from './CellWrapper.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children?: React.ReactNode; style?: React.CSSProperties; colspan?: number }>} */
const CellWrapper = ({ children, style, colspan }) => (
  <td className={styles.CellWrapper} colSpan={colspan} style={style}>
    {children}
  </td>
);

export default memo(CellWrapper);
