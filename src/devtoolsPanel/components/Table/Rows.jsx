import { memo } from 'react';

import styles from './Rows.less';
import Table from './Table.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; colGroups?: React.ReactNode; tableProps?: React.HTMLProps<HTMLTableElement>; bodyProps?: React.HTMLProps<HTMLTableSectionElement> }>} */
const Rows = ({ children, colGroups, tableProps, bodyProps }) => (
  <div className={styles.RowsWrapper}>
    <Table rel="rows" {...tableProps}>
      {colGroups}
      <tbody {...bodyProps}>{children}</tbody>
    </Table>
  </div>
);

export default memo(Rows);
