import { memo } from 'react';

import Header from './Header.jsx';
import styles from './SortableHeader.less';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const IconSortDown = () => <span className={styles.IconSortDown} />;

/** @type {React.FC} */
const IconSortUp = () => <span className={styles.IconSortUp} />;

/** @type {React.FC<{ isSorted?: boolean; isDesc?: boolean }>} */
const SortIcon = ({ isSorted, isDesc }) => {
  if (!isSorted) {
    return null;
  }

  return isDesc ? <IconSortDown /> : <IconSortUp />;
};

/** @type {React.FC<{ children: React.ReactNode; column: { isSortedDesc: boolean; isSorted: boolean } }>} */
const SortableHeader = ({ column, children, ...props }) => (
  <Header className={styles.SortableHeader} {...props}>
    <span className={styles.HeaderWrapper}>
      {children}
      <SortIcon isDesc={column.isSortedDesc} isSorted={column.isSorted} />
    </span>
  </Header>
);

export default memo(SortableHeader);
