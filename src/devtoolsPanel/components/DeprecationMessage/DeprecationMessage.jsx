import classNamesBind from 'classnames/bind';
import { memo, useCallback, useMemo, useState } from 'react';
import { Inspector, chromeLight, chromeDark } from 'react-inspector';
import * as semver from 'semver';

import styles from './DeprecationMessage.less';
import { getAuiUpgradeGuideLink } from '../../helpers/links.js';
import nodeDehydrate from '../../helpers/nodeDehydrate.js';
import ConsoleMessage from '../Console/ConsoleMessage.jsx';
import { DARK_THEME, LIGHT_THEME, useTheme } from '../Theme/Theme.jsx';

/** @typedef {import('react')} React */

/** @typedef {import('../../../contentScript/deprecations/types').AuiDeprecationWithMeta} AuiDeprecationWithMeta */

const inspectorThemesMap = {
  [DARK_THEME]: chromeDark,
  [LIGHT_THEME]: chromeLight,
};

const classNames = classNamesBind.bind(styles);

const isVersionDeprecated = (version, auiVersion) => (auiVersion ? semver.gt(auiVersion, version) : false);

const getDeprecationLevel = (isDeprecated) => ({
  isError: isDeprecated,
  isWarning: !isDeprecated,
});

/**
 * @typedef {Object} Props
 * @property {AuiDeprecationWithMeta} deprecation
 * @property {(arg0: string) => void} onElementClick
 * @property {boolean} [showTimestamps]
 * @property {(arg0: string) => void} onHighlight
 * @property {(arg0: string) => void} onUnhighlight
 */

/** @type {React.FC<Props>} */
const DeprecationMessage = ({ deprecation, onElementClick, showTimestamps = false, onHighlight, onUnhighlight }) => {
  const { timestamp, node, nodeId, selector, displayName, auiVersion, version, alternativeName, extraInfo, removeInVersion } = deprecation;

  const [isHighlighted, setIsHighlighted] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));

  const highlightNode = useCallback(() => {
    setIsHighlighted(true);
    onHighlight(nodeId);
  }, [onHighlight, nodeId]);

  const unhighlightNode = useCallback(() => {
    setIsHighlighted(false);
    onUnhighlight(nodeId);
  }, [onUnhighlight, nodeId]);

  const onClickHandler = useCallback(() => {
    onElementClick(nodeId);
  }, [nodeId, onElementClick]);

  const isDeprecated = isVersionDeprecated(version, auiVersion);

  const anchor = (
    <a href={getAuiUpgradeGuideLink(version)} rel="noopener noreferrer" target="_blank">
      {isDeprecated ? `Deprecated in ${version}` : `Will be deprecated in ${version}`}
    </a>
  );

  const message = `${displayName ? `[${displayName}]` : ''} Using deprecated selector "${selector}".
       ${extraInfo || ''}
       ${alternativeName ? ` Use the ${alternativeName} pattern instead.` : ''}
       ${removeInVersion ? ` The pattern will be removed in ${removeInVersion} version of AUI.` : ''}
       `;

  const className = classNames(styles.DeprecationMessage, {
    [styles.NodeIsHighlighted]: isHighlighted,
  });

  const errorLevelProps = getDeprecationLevel(isDeprecated);

  // Inspector
  const theme = useTheme();
  const hydratedNode = useMemo(() => nodeDehydrate(node), [node]);
  // TODO: Fix correct type const inspectorTheme = /** @type {typeof import("react-inspector").chromeDark} */ (
  const inspectorTheme = /** @type {import("react-inspector").chromeDark} */ (
    useMemo(() => {
      const inspectorThemeOrg = inspectorThemesMap[theme];

      return { ...inspectorThemeOrg, BASE_BACKGROUND_COLOR: 'transparent' };
    }, [theme])
  );

  return (
    <ConsoleMessage anchor={anchor} showTimestamps={showTimestamps} timestamp={timestamp} {...errorLevelProps} className={className}>
      <div>{message}</div>

      <div className={styles.InspectorWrapper} onClick={onClickHandler} onMouseEnter={highlightNode} onMouseLeave={unhighlightNode}>
        <Inspector data={hydratedNode} table={false} theme={inspectorTheme} />
      </div>
    </ConsoleMessage>
  );
};

export default memo(DeprecationMessage);
