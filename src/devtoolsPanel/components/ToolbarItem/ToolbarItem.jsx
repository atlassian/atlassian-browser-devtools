import classNames from 'classnames';
import { memo } from 'react';

import styles from './ToolbarItem.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children?: React.ReactNode; className?: string }>} */
const ToolbarItem = ({ children = null, className }) => <span className={classNames(styles.ToolbarItem, className)}>{children}</span>;

export default memo(ToolbarItem);
