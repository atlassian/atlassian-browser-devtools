import { memo } from 'react';

import styles from './ToolbarDivider.less';
import ToolbarItem from '../ToolbarItem/ToolbarItem.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const ToolbarDivider = () => <ToolbarItem className={styles.ToolbarDivider} />;

export default memo(ToolbarDivider);
