import { memo } from 'react';

import PanelHint from '../PanelHint/PanelHint.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const EmptyDeprecationsList = () => (
  <PanelHint>
    <p>
      Watching for the usage of deprecated <strong>AUI HTML patterns</strong> on the page…
    </p>
    <p>
      When the deprecated <strong>HTML pattern</strong> will be used, you will see here detailed information about the depreciation and what
      DOM element is affected.
    </p>
  </PanelHint>
);

export default memo(EmptyDeprecationsList);
