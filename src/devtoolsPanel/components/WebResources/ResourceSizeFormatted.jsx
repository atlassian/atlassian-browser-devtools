import { Fragment, memo } from 'react';

import styles from './ResourceSizeFormatted.less';
import { formatSize } from '../../helpers/formatSize.js';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/** @type {React.FC<{ webResource: WebResourceMeta }>} */
const ResourceSizeFormatted = ({ webResource }) => {
  const formatted = formatSize(webResource.totalBytes);

  if (webResource.isBig) {
    return (
      <span
        className={styles.ResourceSizeFormattedWebResourceIsBig}
        title="This resource is too big. Please consider splitting the resource into smaller chunks."
      >
        ⚠️ {formatted}
      </span>
    );
  }

  return <Fragment>{formatted}</Fragment>;
};

export default memo(ResourceSizeFormatted);
