import { Fragment, memo } from 'react';

import styles from './UnusedBytesFormatted.less';
import { formatSize } from '../../helpers/formatSize.js';
import { isWebResourceBytesUnused, isWebResourceCompletelyUnused } from '../../helpers/webResourceConditions.js';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

/** @type {React.FC<{ webResource: WebResourceCoverageMeta }>} */
const UnusedBytesFormatted = ({ webResource }) => {
  const formatted = formatSize(webResource.unusedBytes);

  if (webResource.isUnused && isWebResourceBytesUnused(webResource)) {
    const title = isWebResourceCompletelyUnused(webResource)
      ? `This resource is completely unused. Remove it from the runtime or defer the loading phase until the resource is needed.`
      : 'This resource is barely used. Please consider removing it from the runtime or deferring the loading phase until the resource is needed.';

    return (
      <span className={styles.UnusedBytesFormattedWebResourceIsUnused} title={title}>
        ⚠️ {formatted}
      </span>
    );
  }

  return <Fragment>{formatted}</Fragment>;
};

export default memo(UnusedBytesFormatted);
