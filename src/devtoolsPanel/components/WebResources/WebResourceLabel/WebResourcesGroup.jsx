import { memo } from 'react';

import styles from './WebResourcesGroup.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode }>} */
const WebResourcesGroup = ({ children }) => {
  return <div className={styles.WebResourcesGroup}>{children}</div>;
};

export default memo(WebResourcesGroup);
