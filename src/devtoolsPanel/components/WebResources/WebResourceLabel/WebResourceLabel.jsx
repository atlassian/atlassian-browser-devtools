import { memo } from 'react';

import styles from './WebResourceLabel.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode }>} */
const WebResourceLabel = ({ children }) => <code className={styles.WebResourceLabel}>{children}</code>;

export default memo(WebResourceLabel);
