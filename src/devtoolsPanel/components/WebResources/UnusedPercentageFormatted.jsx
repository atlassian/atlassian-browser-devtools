import { memo } from 'react';

import styles from './UnusedPercentageFormatted.less';
import { isWebResourceCompletelyUnused, isWebResourcePercentageUnused } from '../../helpers/webResourceConditions.js';
import Percentage from '../Percentage/Percentage.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

// TODO: Use WebResourceCoverage one here

/** @type {React.FC<{ webResource: WebResourceCoverageMeta }>} */
const UnusedPercentageFormatted = ({ webResource }) => {
  const { isUnused, unused } = webResource;

  if (isUnused && isWebResourcePercentageUnused(webResource)) {
    const title = isWebResourceCompletelyUnused(webResource)
      ? `This resource is completely unused. Remove it from the runtime or defer the loading phase until the resource is needed.`
      : 'This resource is barely used. Please consider removing it from the runtime or deferring the loading phase until the resource is needed.';

    return (
      <span className={styles.UnusedPercentageFormattedWebResourceIsUnused} title={title}>
        ⚠️<Percentage>{unused}</Percentage>
      </span>
    );
  }

  return <Percentage>{unused}</Percentage>;
};

export default memo(UnusedPercentageFormatted);
