import classNames from 'classnames';
import dialogPolyfill from 'dialog-polyfill';
import { memo, useCallback, useLayoutEffect, useRef } from 'react';

import styles from './ConfirmationDialog.less';
import { IS_FIREFOX } from '../../../common/devMode.js';

/** @typedef {import('react')} React */

/**
 * @typedef {Object} Props
 * @property {React.ReactNode} children
 * @property {() => void} onConfirm
 * @property {string} [confirmLabel]
 * @property {string} [declineLabel]
 * @property {() => void} [onDecline]
 */

/** @type {React.FC<Props>} */
const ConfirmationDialog = ({ onConfirm, confirmLabel = 'OK', declineLabel = 'Cancel', onDecline, children }) => {
  const modalRef = /** @type {React.MutableRefObject<HTMLDialogElement>} */ (useRef());
  const buttonRef = /** @type {React.MutableRefObject<HTMLButtonElement>} */ (useRef());

  useLayoutEffect(function openOnMount() {
    const modal = modalRef.current;

    if (IS_FIREFOX) {
      dialogPolyfill.registerDialog(modal);
    }

    modal.showModal();

    if (buttonRef.current) {
      buttonRef.current.focus();
    }

    return () => {
      if (modal.open) {
        modal.open = false;
      }
    };
  }, []);

  useLayoutEffect(
    function closeOnEscape() {
      const modal = modalRef.current;

      const closeHandler = () => {
        if (typeof onDecline === 'function') {
          onDecline();
        }
      };

      modal.addEventListener('close', closeHandler);

      return () => {
        modal.removeEventListener('close', closeHandler);
      };
    },
    [onDecline]
  );

  const confirm = useCallback(() => {
    if (modalRef.current && modalRef.current.close) {
      modalRef.current.close();
    }
    onConfirm();
  }, [onConfirm]);

  const decline = useCallback(() => {
    modalRef.current.close();
    if (typeof onDecline === 'function') {
      onDecline();
    }
  }, [onDecline]);

  return (
    <dialog ref={modalRef} className={styles.ConfirmationDialog}>
      {children}
      <p className={styles.Buttons}>
        <button ref={buttonRef} className={styles.Button} type="button" onClick={decline}>
          {declineLabel}
        </button>

        <button ref={buttonRef} className={classNames(styles.Button, styles.ButtonPrimary)} type="button" onClick={confirm}>
          {confirmLabel}
        </button>
      </p>
    </dialog>
  );
};

export default memo(ConfirmationDialog);
