import { Fragment, memo, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import styles from './DependencyTree.less';
import { selectors } from '../../stores/pageStore.js';
import WebResourceLabel from '../WebResources/WebResourceLabel/WebResourceLabel.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/**
 * @typedef {Object} WebResource
 * @property {string} key
 * @property {string} resourceType
 */

/**
 * @param {WebResource} resource
 * @returns {string}
 */
const greResourceType = (resource) => {
  const type = resource.resourceType.toLowerCase();

  return type === 'javascript' ? 'js' : type;
};

/** @type {React.FC<{ webResource: WebResourceMeta }>} */
const DependencyTree = ({ webResource }) => {
  const hasWrm = useSelector(selectors.getHasWrm);
  const pageUrl = useSelector(selectors.getPageUrl);
  const [resources, setResources] = /** @type {[WebResource[] | null, (arg0: WebResource[] | null) => void]} */ (useState(null));
  const hasKey = Boolean(webResource.wrmKey && webResource.wrmKey !== 'other');

  useEffect(() => {
    setResources(null);

    if (!hasWrm || !pageUrl || !hasKey) {
      return;
    }

    fetch(`${pageUrl}/rest/webResources/1.0/resources?r=${webResource.wrmKey}`, { method: 'GET' })
      .then((r) => r.json())
      .then((response) => {
        // eslint-disable-next-line no-shadow
        let resources = /** @type {WebResource[]} */ response.resources;
        resources = resources.reverse().filter((resource) => {
          return greResourceType(resource) === webResource.batchType;
        });

        if (resources.length && resources[0].key === webResource.wrmKey) {
          resources.shift();
        }

        setResources(resources);
      })
      .catch(() => {
        console.error('Failed to fetch web-resources', {
          webResource,
        });
      });
  }, [webResource, hasWrm, pageUrl, hasKey]);

  if (!hasWrm || !pageUrl) {
    return (
      <div className={styles.DependencyTree}>
        <p>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          The <strong>WebResourceManager</strong> can't be found on this page. Please navigate to a page that is build with a{' '}
          <strong>WRM</strong>.
        </p>
      </div>
    );
  }

  if (!hasKey) {
    return (
      <div className={styles.DependencyTree}>
        <p>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          The dependency tree can't be displayed for the selected resource.
        </p>
      </div>
    );
  }

  if (!Array.isArray(resources)) {
    return <div className={styles.DependencyTree}>Loading...</div>;
  }

  return (
    <div className={styles.DependencyTree}>
      {Array.isArray(resources) && resources.length ? (
        <Fragment>
          <p>
            The web-resource <WebResourceLabel>{webResource.wrmKey}</WebResourceLabel> depends on:
          </p>
          <ul>
            {resources
              ? resources.map(({ key }) => (
                  <li key={key}>
                    <WebResourceLabel>{key}</WebResourceLabel>
                  </li>
                ))
              : null}
          </ul>
        </Fragment>
      ) : (
        <p>
          The web-resource <WebResourceLabel>{webResource.wrmKey}</WebResourceLabel> does not have any other known dependencies.
        </p>
      )}
    </div>
  );
};

export default memo(DependencyTree);
