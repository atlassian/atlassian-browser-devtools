import { memo } from 'react';

import styles from './Percentage.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: number }>} */
const Percentage = ({ children }) => {
  return <span className={styles.Percentage}>{children.toFixed(1)} %</span>;
};

export default memo(Percentage);
