import { Component, memo } from 'react';

/** @typedef {import('react')} React */

/** @type {React.ComponentClass<{ children: React.ReactNode; fallback: React.ReactNode }>} */
class ErrorWrapper extends Component {
  state = {
    hasError: false,
  };

  // eslint-disable-next-line class-methods-use-this
  componentDidCatch(error, errorInfo) {
    console.error('Component threw an unhandled exception', errorInfo && errorInfo.componentStack);
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  render() {
    const { children, fallback } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      return <>{fallback}</>;
    }

    return <>{children}</>;
  }
}

export default memo(ErrorWrapper);
