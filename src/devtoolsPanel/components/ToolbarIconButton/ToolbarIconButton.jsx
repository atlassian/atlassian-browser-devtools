import classNames from 'classnames';
import { memo } from 'react';

import styles from './ToolbarIconButton.less';
import ToolbarButton from '../ToolbarButton/ToolbarButton.jsx';

export const ClearIcon = 'ClearIcon';
export const FormatCode = 'FormatCode';
export const RecordIcon = 'RecordIcon';
export const RecordingIcon = 'RecordingIcon';
export const ExportIcon = 'ExportIcon';

/** @typedef {import('react')} React */

/** @type {React.FC<{ icon: string; title: string; disabled?: boolean; onClick: () => void }>} */
const ToolbarIconButton = ({ icon, onClick, title, disabled = false }) => {
  const className = classNames(styles.ToolbarIconButton, styles[icon], {
    [styles.ToolbarIconButtonDisabled]: disabled,
  });

  return (
    <ToolbarButton /* FIXME: fix missing support for title title={title} */ disabled={disabled} title={title} onClick={onClick}>
      <span className={className} />
    </ToolbarButton>
  );
};

export default memo(ToolbarIconButton);
