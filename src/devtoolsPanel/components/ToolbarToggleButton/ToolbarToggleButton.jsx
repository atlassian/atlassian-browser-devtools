import classNames from 'classnames';
import { memo, useCallback, useEffect, useMemo, useRef } from 'react';

import styles from './ToolbarToggleButton.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; className?: string; selected?: boolean; disabled?: boolean; onToggle: (arg0: string) => void }>} */
const ToolbarToggleButton = ({ children, className, selected = false, onToggle, disabled }) => {
  const inputRef = /** @type {React.MutableRefObject<HTMLInputElement>} */ (useRef());
  const checkboxProps = useMemo(() => (selected ? { checked: selected } : {}), [selected]);

  const onChange = useCallback(
    (event) => {
      const value = event.target.checked;

      onToggle(value);
    },
    [onToggle]
  );

  useEffect(
    function overwriteValue() {
      if (typeof selected !== 'undefined') {
        inputRef.current.checked = selected;
      }
    },
    [selected]
  );

  return (
    <label
      className={classNames(styles.ToolbarToggleButton, className, {
        [styles.ToolbarToggleButtonDisabled]: disabled,
      })}
      onChange={onChange}
    >
      <input ref={inputRef} type="checkbox" {...checkboxProps} disabled={disabled} />
      <span className={styles.Label}>{children}</span>
    </label>
  );
};

export default memo(ToolbarToggleButton);
