import { createContext, useContext, useMemo } from 'react';

/** @typedef {import('react')} React */

/** @typedef {string[] | null} AnalyticsContext */

const AnalyticsContext = /** @type {React.Context<AnalyticsContext>} */ (createContext(null));

const { Provider: InnerAnalyticsContextProvider } = AnalyticsContext;

/**
 * @returns {AnalyticsContext}
 */
export const useAnalyticsContext = () => {
  const analyticsContext = useContext(AnalyticsContext);

  return analyticsContext;
};

/** @type {React.FC<{ analyticsContext: string | string[]; children: React.ReactNode }>} */
export const AnalyticsContextProvider = ({ children, analyticsContext }) => {
  const parentAnalyticsContext = useAnalyticsContext();

  /** @type {string[]} */
  const contextValue = useMemo(() => {
    const combinedAnalyticsContext = Array.isArray(parentAnalyticsContext) ? [...parentAnalyticsContext] : [];

    if (!Array.isArray(analyticsContext)) {
      combinedAnalyticsContext.push(analyticsContext);
    } else {
      combinedAnalyticsContext.push(...analyticsContext);
    }

    return combinedAnalyticsContext;
  }, [parentAnalyticsContext, analyticsContext]);

  return <InnerAnalyticsContextProvider value={contextValue}>{children}</InnerAnalyticsContextProvider>;
};
