import { createContext, memo, useContext, useEffect, useLayoutEffect, useState } from 'react';

import { IS_DEV_MODE } from '../../../common/devMode.js';

/** @typedef {import('react')} React */

export const DARK_THEME = 'dark';
export const LIGHT_THEME = 'light';
const DEFAULT_THEME = LIGHT_THEME;

const themesClassNamesMap = {
  [DARK_THEME]: 'darkmode',
  [LIGHT_THEME]: 'lightmode',
};

const themeContext = createContext(DEFAULT_THEME);

const { Provider } = themeContext;

/** @type {React.FC<{ children: React.ReactNode; theme: string }>} */
const ThemeProvider = ({ children, theme = DEFAULT_THEME }) => {
  const [usedTheme, setUsedTheme] = /** @type {[string, (arg0: string) => void]} */ (
    useState(() => (theme === DARK_THEME ? DARK_THEME : LIGHT_THEME))
  );

  useEffect(function devModeAutoChangeTheme() {
    if (!IS_DEV_MODE) {
      return;
    }

    const colorSchemeQueryList = window.matchMedia('(prefers-color-scheme: dark)');

    const listener = (event) => {
      const isDarkTheme = event.matches;
      const newTheme = isDarkTheme ? DARK_THEME : LIGHT_THEME;

      setUsedTheme(newTheme);
    };

    colorSchemeQueryList.addEventListener('change', listener);

    // eslint-disable-next-line consistent-return
    return () => {
      colorSchemeQueryList.removeEventListener('change', listener);
    };
  }, []);

  useLayoutEffect(() => {
    const themeClassNames = Object.values(themesClassNamesMap);
    const targetEl = document.documentElement;

    // Remove previous themes class names
    for (const className of Array.from(targetEl.classList)) {
      if (themeClassNames.includes(className)) {
        targetEl.classList.remove(className);
      }
    }

    // Add new theme class name
    const themeClassName = themesClassNamesMap[usedTheme];
    targetEl.classList.add(themeClassName);
  }, [usedTheme]);

  return <Provider value={usedTheme}>{children}</Provider>;
};

const MemoedThemeProvider = memo(ThemeProvider);

export { MemoedThemeProvider as ThemeProvider };

export const useTheme = () => {
  return useContext(themeContext);
};
