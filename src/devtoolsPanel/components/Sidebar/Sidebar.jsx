import classNames from 'classnames';
import { memo } from 'react';

import styles from './Sidebar.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode, className: string }>} */
const Sidebar = ({ children, className }) => (
  <div className={classNames(styles.Sidebar, className)}>
    <ol className={styles.SidebarActionsList}>{children}</ol>
  </div>
);

const { SidebarActionItem } = styles;

export { SidebarActionItem as SidebarActionItemClassName };

export default memo(Sidebar);
