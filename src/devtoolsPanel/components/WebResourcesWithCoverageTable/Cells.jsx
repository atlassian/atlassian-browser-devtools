import { Fragment, memo } from 'react';

import { formatSize } from '../../helpers/formatSize.js';
import Percentage from '../Percentage/Percentage.jsx';
import UnusedBytesFormatted from '../WebResources/UnusedBytesFormatted.jsx';
import UnusedPercentageFormatted from '../WebResources/UnusedPercentageFormatted.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

// react-table is not super performant but we can improve that
export const valuePropsMemoComparator = (prevProps, nextProps) => {
  return prevProps.value === nextProps.value;
};

export const originalRowPropsMemoComparator = (prevProps, nextProps) => {
  return prevProps.row.original === nextProps.row.original;
};

/** @type {React.FC<{ value: number }>} */
const UsedCell = ({ value: used }) => <Percentage>{used}</Percentage>;

/** @type {React.FC<{ row: { original: WebResourceMeta } }>} */
const UnusedCell = ({ row }) => {
  const webResource = row.original;

  return <UnusedPercentageFormatted webResource={webResource} />;
};

/** @type {React.FC<{ value: number }>} */
const UsedBytesCell = ({ value: usedBytes }) => <Fragment>{formatSize(usedBytes)}</Fragment>;

/** @type {React.FC<{ row: { original: WebResourceMeta } }>} */
const UnusedBytesCell = ({ row }) => {
  const webResource = row.original;

  return <UnusedBytesFormatted webResource={webResource} />;
};

const UsedCellMemoed = memo(UsedCell, valuePropsMemoComparator);
const UnusedCellMemoed = memo(UnusedCell, originalRowPropsMemoComparator);
const UsedBytesCellMemoed = memo(UsedBytesCell, valuePropsMemoComparator);
const UnusedBytesCellMemoed = memo(UnusedBytesCell, originalRowPropsMemoComparator);

export {
  UsedCellMemoed as UsedCell,
  UnusedCellMemoed as UnusedCell,
  UsedBytesCellMemoed as UsedBytesCell,
  UnusedBytesCellMemoed as UnusedBytesCell,
};
