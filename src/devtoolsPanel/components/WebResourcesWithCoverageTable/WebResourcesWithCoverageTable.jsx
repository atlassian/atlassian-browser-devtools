import { memo, useMemo } from 'react';

import { UnusedBytesCell, UnusedCell, UsedBytesCell, UsedCell } from './Cells.jsx';
import SelectableTable from '../Table/SelectableTable.jsx';
import { ResourceTypeCell, ResourceSizeCell } from '../WebResourcesTable/Cells.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

/**
 * @typedef {Object} RowComponentProps
 * @property {unknown} rowData
 * @property {string} className
 * @property {React.ReactNode} children
 */

const colGroups = (
  <colgroup>
    {/* Resource key column */}
    <col />

    {/* Type column */}
    <col width="65" />

    {/* Size column */}
    <col width="95" />

    {/* Used bytes column */}
    <col width="95" />

    {/* Used percentage column */}
    <col width="65" />

    {/* Unused bytes column */}
    <col width="110" />

    {/* Unused percentage column */}
    <col width="75" />
  </colgroup>
);

/** @type {React.FC<{ wrmCoverage: WebResourceCoverageMeta[]; RowComponent: React.FC<RowComponentProps>; onSelected: () => void }>} */
const WebResourcesWithCoverageTable = ({ wrmCoverage, onSelected, RowComponent }) => {
  const columns = useMemo(
    () => [
      { Header: 'Resource key', accessor: 'wrmKey' },
      { Header: 'Type', accessor: 'type', Cell: ResourceTypeCell },
      {
        Header: 'Size',
        accessor: 'totalBytes',
        Cell: ResourceSizeCell,
        cellStyle: { textAlign: 'right' },
      },
      {
        Header: 'Used bytes',
        accessor: 'usedBytes',
        Cell: UsedBytesCell,
        cellStyle: { textAlign: 'right' },
      },
      {
        Header: 'Used',
        accessor: 'used',
        Cell: UsedCell,
        cellStyle: { textAlign: 'right' },
      },
      {
        Header: 'Unused bytes',
        accessor: 'unusedBytes',
        Cell: UnusedBytesCell,
        cellStyle: {
          textAlign: 'right',
        },
      },
      {
        Header: 'Unused',
        accessor: 'unused',
        Cell: UnusedCell,
        cellStyle: { textAlign: 'right' },
      },
    ],
    []
  );
  const data = useMemo(() => {
    return wrmCoverage.map((webResource) => {
      const { wrmKey, wrmFile, url } = webResource;
      const rowId = `${wrmKey}-${wrmFile}-${url}`;

      return { ...webResource, rowId };
    });
  }, [wrmCoverage]);

  return <SelectableTable RowComponent={RowComponent} colGroups={colGroups} columns={columns} data={data} onSelected={onSelected} />;
};

export default memo(WebResourcesWithCoverageTable);
