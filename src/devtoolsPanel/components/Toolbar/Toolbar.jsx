import classNames from 'classnames';
import { memo } from 'react';

import styles from './Toolbar.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; className?: string }>} */
const Toolbar = ({ children, className }) => <div className={classNames(styles.Toolbar, className)}>{children}</div>;

export default memo(Toolbar);
