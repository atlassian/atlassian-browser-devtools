import classNames from 'classnames';
import { memo } from 'react';

import styles from './BottomToolbar.less';
import Toolbar from './Toolbar.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode; className?: string }>} */
const BottomToolbar = ({ children, className }) => <Toolbar className={classNames(styles.BottomToolbar, className)}>{children}</Toolbar>;

export default memo(BottomToolbar);
