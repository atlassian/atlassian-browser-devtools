import { js as jsBeautify, css as cssBeautify } from 'js-beautify';
import { memo, useCallback, useLayoutEffect, useRef, useState } from 'react';

import { loadMonacoEditor } from './monacoLoader.js';
import styles from './WebResourceSourcePanel.less';
import PanelWrapper from '../PanelWrapper/PanelWrapper.jsx';
import { DARK_THEME, LIGHT_THEME, useTheme } from '../Theme/Theme.jsx';
import BottomToolbar from '../Toolbar/BottomToolbar.jsx';
import ToolbarIconButton, { FormatCode } from '../ToolbarIconButton/ToolbarIconButton.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('monaco-editor')} Monaco */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

const languageMap = {
  js: 'javascript',
  css: 'css',
};

const beautifyMap = {
  js: jsBeautify,
  css: cssBeautify,
};

const monacoThemeMap = {
  [DARK_THEME]: 'vs-dark',
  [LIGHT_THEME]: 'vs',
};

/** @type {React.FC<{ webResource: WebResourceMeta }>} */
const WebResourceSourcePanel = ({ webResource }) => {
  const { code, type: resourceType } = webResource;

  const theme = useTheme();

  const wrapperRef = /** @type {React.MutableRefObject<HTMLDivElement>} */ (useRef());
  const editorRef = /** @type {React.MutableRefObject<import('monaco-editor').editor.IStandaloneCodeEditor | null>} */ (useRef(null));

  const monacoRef = /** @type {React.MutableRefObject<typeof import('monaco-editor')>} */ (useRef());

  const [sourceCode, setSourceCode] = /** @type {[string, (arg0: string) => void]} */ useState(code);

  useLayoutEffect(
    function synchronizeState() {
      setSourceCode(code);
    },
    [code]
  );

  useLayoutEffect(
    function loadAndInitEditor() {
      const wrapperEl = wrapperRef.current;

      /**
       * @returns {Promise<void>}
       */
      async function loadAndInitEditorAsync() {
        const monaco = await loadMonacoEditor();
        monacoRef.current = monaco;

        const monacoTheme = monacoThemeMap[theme];

        editorRef.current = monaco.editor.create(wrapperEl, {
          value: sourceCode,
          language: languageMap[resourceType],
          readOnly: true,
          wordWrap: 'off',

          // Auto resize
          automaticLayout: true,

          // based on preferred user setting
          theme: monacoTheme,

          // Disable side mini map
          minimap: {
            enabled: false,
          },

          // Disable scrolling outside the last line
          scrollBeyondLastLine: false,

          // Disable any hover widgets
          hover: {
            enabled: false,
          },

          // Disable context menu
          contextmenu: false,

          scrollbar: {
            useShadows: false,
          },

          renderLineHighlight: 'none',
        });
      }

      loadAndInitEditorAsync();

      return function cleanUpEditor() {
        const editor = /** @type {import('monaco-editor').editor.IStandaloneCodeEditor} */ editorRef.current;

        if (editor) {
          editor.dispose();
        }

        editorRef.current = null;
      };
    },
    [theme /* we ignore the "resourceType" and "sourceCode" */]
  );

  useLayoutEffect(
    function updateCodeAndLanguageModel() {
      const editor = /** @type {import('monaco-editor').editor.IStandaloneCodeEditor} */ editorRef.current;

      if (editor) {
        editor.setValue(sourceCode);

        const monaco = monacoRef.current;

        // @ts-expect-error TS2345 getModel can be actually called without params...
        monaco.editor.setModelLanguage(editor.getModel(), languageMap[resourceType]);
      }
    },
    [sourceCode, resourceType]
  );

  const formatCode = useCallback(() => {
    // Code was formatted, so we will revert the formatting
    if (code !== sourceCode) {
      setSourceCode(code);
    } else {
      const beautifier = beautifyMap[resourceType];
      const formattedCode = beautifier(sourceCode);

      setSourceCode(formattedCode);
    }
  }, [code, sourceCode, resourceType]);

  return (
    <PanelWrapper>
      <div ref={wrapperRef} className={styles.WebResourceSourcePanel} />
      <BottomToolbar>
        <ToolbarIconButton icon={FormatCode} title="Format code" onClick={formatCode} />
      </BottomToolbar>
    </PanelWrapper>
  );
};

export default memo(WebResourceSourcePanel);
