// @ts-expect-error The MonacoEnvironment doesn't exist in the browser
// eslint-disable-next-line no-restricted-globals
self.MonacoEnvironment = {
  getWorkerUrl: function (/** @type {string} */ moduleId, /** @type {string} */ label) {
    // Monaco workers are build using esbuild. Check the scripts/buildMonacoWorkers.js file
    if (label === 'css' || label === 'scss' || label === 'less') {
      return './css.worker.js';
    }

    if (label === 'typescript' || label === 'javascript') {
      return './ts.worker.js';
    }

    return './editor.worker.js';
  },
};

/**
 * @returns {Promise<typeof import('monaco-editor')>}
 */
export async function loadMonacoEditor() {
  return await import('monaco-editor');
}
