import classNames from 'classnames';
import { forwardRef, memo } from 'react';

import styles from './PanelWrapper.less';

/** @typedef {import('react')} React */

/** @type {React.ForwardRefExoticComponent<{ children: React.ReactNode; className?: string }>} */
const PanelWrapper = forwardRef(({ children, className }, ref) => {
  return (
    <div ref={ref} className={classNames(styles.PanelWrapper, className)}>
      {children}
    </div>
  );
});

PanelWrapper.displayName = 'PanelWrapper';

export default memo(PanelWrapper);
