import { memo } from 'react';

import styles from './ToolbarSpacer.less';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const ToolbarSpacer = () => <span className={styles.ToolbarSpacer} />;

export default memo(ToolbarSpacer);
