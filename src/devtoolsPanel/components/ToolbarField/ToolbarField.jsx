import classNames from 'classnames';
import { memo, useCallback, useEffect, useRef } from 'react';

import styles from './ToolbarFields.less';
import ToolbarItem from '../ToolbarItem/ToolbarItem.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ placeholder: string; defaultValue?: string; value?: string | null; disabled?: boolean; onChange: (value: string) => void }>} */
const ToolbarField = ({ onChange, placeholder, defaultValue = '', value = null, disabled = false }) => {
  const inputRef = /** @type {React.MutableRefObject<HTMLInputElement>} */ (useRef());

  const onChangeHandler = useCallback(
    (event) => {
      onChange(event.target.value);
    },
    [onChange]
  );

  // Escape handler
  const onKeyDownHandler = useCallback(
    (event) => {
      if (event.keyCode === 27) {
        event.target.value = '';

        onChange('');
      }
    },
    [onChange]
  );

  useEffect(
    function overwriteValue() {
      if (typeof value !== 'undefined') {
        inputRef.current.value = value === null ? '' : value;
      }
    },
    [value]
  );

  return (
    <ToolbarItem className={classNames(styles.ToolbarFieldWrapper)}>
      <input
        ref={inputRef}
        className={styles.ToolbarField}
        defaultValue={defaultValue}
        disabled={disabled}
        placeholder={placeholder}
        type="text"
        onChange={onChangeHandler}
        onKeyDown={onKeyDownHandler}
      />
    </ToolbarItem>
  );
};

export default memo(ToolbarField);
