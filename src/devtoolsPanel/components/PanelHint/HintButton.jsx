import { memo } from 'react';

import ToolbarIconButton from '../ToolbarIconButton/ToolbarIconButton.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ icon: string; title: string; onClick: () => void }>} */
const HintButton = ({ onClick, icon, title }) => {
  return (
    <span
      style={{
        border: 'var(--border-secondary)',
        backgroundColor: 'var(--background-color-secondary)',
        padding: 0,
        display: 'inline-flex',
        lineHeight: 0,
        verticalAlign: 'bottom',
      }}
    >
      <ToolbarIconButton icon={icon} title={title} onClick={onClick} />
    </span>
  );
};

export default memo(HintButton);
