import { memo } from 'react';

import styles from './PanelHint.less';

/** @typedef {import('react')} React */

/** @type {React.FC<{ children: React.ReactNode }>} */
const PanelHint = ({ children }) => <div className={styles.PanelHint}>{children}</div>;

export default memo(PanelHint);
