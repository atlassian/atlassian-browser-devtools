import classNamesBind from 'classnames/bind';
import { memo, useCallback } from 'react';

import styles from './SidebarActionItem.less';

/** @typedef {import('react')} React */

const classNames = classNamesBind.bind(styles);

/** @type {React.FC<{ active: boolean; children: React.ReactNode; className?: string; panelId: string; onClick: (arg0: string) => void; }>} */
const SidebarActionItem = ({ active = false, onClick, children, className, panelId }) => {
  const onClickHandler = useCallback(() => {
    onClick(panelId);
  }, [panelId, onClick]);

  return (
    <li className={classNames(styles.SidebarActionItem, className, { Active: active })} tabIndex={0} onClick={onClickHandler}>
      <span className={styles.SidebarActionItemLabel}>{children}</span>
    </li>
  );
};

export default memo(SidebarActionItem);
