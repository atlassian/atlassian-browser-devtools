import { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { unstable_batchedUpdates as batchedUpdates } from 'react-dom';

import NoFilteredWebResourcesHint from './NoFilteredWebResourcesHint.jsx';
import WrmCoverageBottomToolbar from './WrmCoverageBottomToolbar.jsx';
import WrmCoverageToolbar from './WrmCoverageToolbar.jsx';
import { getEventName, trackEvent } from '../../../common/analytics/analytics.js';
import { getFilters } from '../../helpers/webResourcesFilters/webResourcesFilters.js';
import { filterWebResources, getWebResourceFilteringOptions } from '../../helpers/webResourcesTableUtils.js';
import { processCoverage } from '../../services/coverage/coverageProcessor.js';
import { useAnalyticsContext } from '../AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import PanelHint from '../PanelHint/PanelHint.jsx';
import PanelWrapper from '../PanelWrapper/PanelWrapper.jsx';
import SplitPanel, { PANEL_MIN_HEIGHT } from '../SplitPanel/SplitPanel.jsx';
import WebResourceRow from '../WebResourcesTable/WebResourceRow.jsx';
import WebResourcesWithCoverageTable from '../WebResourcesWithCoverageTable/WebResourcesWithCoverageTable.jsx';
import WebResourcePanel from '../WebResourceWithCoveragePanel/WebResourceWithCoveragePanel.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../helpers/webResourcesFilters/webResourcesFilters').FilterFn} FilterFn */
/** @typedef {import('../../DevtoolsPanelApp/Plugins/PluginsProvider').UpmAtlassianPlugin} UpmAtlassianPlugin */
/** @typedef {import('../../DevtoolsPanelApp/Plugins/PluginsProvider').UpmAtlassianPlugins} UpmAtlassianPlugins */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */
/** @typedef {import('../../DevtoolsPanelApp/Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

const defaultNoWebResourcesInCoverageHint = (
  <PanelHint>
    {/* eslint-disable-next-line react/no-unescaped-entities */}
    <p>This coverage file doesn't include any information about the web-resources.</p>
    <p>Try collecting the coverage again.</p>
  </PanelHint>
);

/** @type {React.FC<{ wrmCoverage: WebResourceCoverageMeta[]; noWebResourcesInCoverageHint: React.ReactElement; noFilteredWebResourcesHint: React.ReactElement; filteredWrmCoverage: WebResourceCoverageMeta[]; onResourceSelected: () => void}>} */
const WrmCoverageBrowserContent = ({
  wrmCoverage,
  noWebResourcesInCoverageHint = defaultNoWebResourcesInCoverageHint,
  noFilteredWebResourcesHint,
  filteredWrmCoverage,
  onResourceSelected,
}) => {
  // No WRM resources were found in the coverage report
  if (!Array.isArray(wrmCoverage) || !wrmCoverage.length) {
    return noWebResourcesInCoverageHint;
  }

  // No WRM resources were found after applying filter
  if (!Array.isArray(filteredWrmCoverage) || !filteredWrmCoverage.length) {
    return noFilteredWebResourcesHint;
  }

  return <WebResourcesWithCoverageTable RowComponent={WebResourceRow} wrmCoverage={filteredWrmCoverage} onSelected={onResourceSelected} />;
};

const WrmCoverageBrowserContentMemoed = memo(WrmCoverageBrowserContent);

const panelMinSizes = [200, PANEL_MIN_HEIGHT];
const panelSizes = [80, 20];

/** @type {React.FC<{ prependToolbarButtons?: React.ReactElement; noCoverageHint: React.ReactElement | null; noWebResourcesInCoverageHint: React.ReactElement; coverage: CoverageJson | null; disableToolbar?: boolean; plugins: UpmAtlassianPlugins; clearCoverage: () => void }>} */
const WrmCoverageBrowser = ({
  plugins,
  clearCoverage,
  prependToolbarButtons,
  noCoverageHint,
  noWebResourcesInCoverageHint,
  coverage = null,
  disableToolbar = false,
}) => {
  const analyticsContext = useAnalyticsContext();

  const [filterValue, setFilterValue] = /** @type {[string | null, (arg0: string | null) => void]} */ (useState(null));
  // @ts-expect-error FIXME: We have some issues with state setter here I guess...
  const [filters, setFilters] = /** @type {[FilterFn[], (arg0: FilterFn[]) => void]} */ (useState([]));
  const [showBigResources, setShowBigResources] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));
  const [showUnusedResources, setShowUnusedResources] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));
  const [hideCorePlugins, setHideCorePlugins] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));
  const [resourcesFilter, setResourcesFilter] = /** @type {[string | null, (arg0: string | null) => void]} */ (useState(null));
  const [webResource, setWebResource] = /** @type {[WebResourceCoverageMeta | null, (arg0: WebResourceCoverageMeta | null) => void]} */ (
    useState(null)
  );

  const onResourceSelected = useCallback(
    (resource) => {
      trackEvent(getEventName(analyticsContext, 'RESOURCE_SELECTED'));

      setWebResource(resource);
    },
    [analyticsContext]
  );

  const wrmCoverage = /** @type {WebResourceCoverageMeta} */ (
    useMemo(() => {
      if (!coverage) {
        return [];
      }

      return processCoverage(coverage);
    }, [coverage])
  );

  const webResourcesFilteringOptions = useMemo(() => getWebResourceFilteringOptions(wrmCoverage), [wrmCoverage]);

  const clearWebResources = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'CLEAR_WEB_RESOURCES'));

    batchedUpdates(() => {
      setFilterValue(null);
      setShowBigResources(false);
      setShowUnusedResources(false);
      setHideCorePlugins(false);
      setResourcesFilter(null);
      setFilters([]);
      clearCoverage();
    });
  }, [analyticsContext, clearCoverage]);

  const filteredWrmCoverage = useMemo(
    () =>
      filterWebResources(wrmCoverage, plugins, {
        filters,
        resourcesFilter,
        showBigResources,
        showUnusedResources,
        hideCorePlugins,
      }),
    [wrmCoverage, plugins, filters, resourcesFilter, showBigResources, showUnusedResources, hideCorePlugins]
  );

  const clearFilters = useCallback(() => {
    batchedUpdates(() => {
      setFilterValue(null);
      setShowBigResources(false);
      setShowUnusedResources(false);
      setHideCorePlugins(false);
      setResourcesFilter(null);
      setFilters([]);
    });
  }, []);

  // Clean-up selected resource when we remove the current coverage
  useEffect(
    function cleanUpSelectedResource() {
      if (!coverage) {
        setWebResource(null);
      }
    },
    [coverage]
  );

  const toggleShowBigResources = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'TOGGLE_SHOW_BIG_RESOURCES'), { value });
      setShowBigResources(value);
    },
    [analyticsContext]
  );

  const toggleShowUnusedResources = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'TOGGLE_SHOW_UNUSED_PLUGINS'), { value });

      setShowUnusedResources(value);
    },
    [analyticsContext]
  );

  const toggleHideCorePlugins = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'TOGGLE_HIDE_CORE_PLUGINS'), { value });

      setHideCorePlugins(value);
    },
    [analyticsContext]
  );

  const onFilterChange = useCallback(
    (rawValue) => {
      trackEvent(getEventName(analyticsContext, 'FILTER_CHANGED'), { value: rawValue });

      batchedUpdates(() => {
        setFilterValue(rawValue);
        setFilters(getFilters(rawValue));
      });
    },
    [analyticsContext]
  );

  const onResourceFilteringOptionSelected = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'RESOURCE_FILTERING_OPTION_SELECTED'), { value });

      setResourcesFilter(value);
    },
    [analyticsContext]
  );

  const noFilteredWebResourcesHint = <NoFilteredWebResourcesHint clearFilters={clearFilters} />;

  return (
    <SplitPanel direction="vertical" minSizes={panelMinSizes} sizes={panelSizes}>
      <PanelWrapper>
        <WrmCoverageToolbar
          clearWebResources={clearWebResources}
          disableToolbar={disableToolbar}
          filterValue={filterValue}
          hideCorePlugins={hideCorePlugins}
          prependButtons={prependToolbarButtons}
          showBigResources={showBigResources}
          showUnusedResources={showUnusedResources}
          toggleHideCorePlugins={toggleHideCorePlugins}
          toggleShowBigResources={toggleShowBigResources}
          toggleShowUnusedResources={toggleShowUnusedResources}
          webResourcesFilteringOptions={webResourcesFilteringOptions}
          webResourcesFilteringValue={resourcesFilter}
          onFilterChange={onFilterChange}
          onResourceFilteringOptionSelected={onResourceFilteringOptionSelected}
        />

        {coverage ? (
          <WrmCoverageBrowserContentMemoed
            filteredWrmCoverage={filteredWrmCoverage}
            noFilteredWebResourcesHint={noFilteredWebResourcesHint}
            noWebResourcesInCoverageHint={noWebResourcesInCoverageHint}
            wrmCoverage={wrmCoverage}
            onResourceSelected={onResourceSelected}
          />
        ) : (
          noCoverageHint
        )}

        <WrmCoverageBottomToolbar filteredWrmCoverage={filteredWrmCoverage} wrmCoverage={wrmCoverage} />
      </PanelWrapper>

      <WebResourcePanel webResource={webResource} />
    </SplitPanel>
  );
};

export default memo(WrmCoverageBrowser);
