import { memo } from 'react';
import { useDebouncedCallback } from 'use-debounce';

import styles from './WrmCoverageToolbar.less';
import Toolbar from '../Toolbar/Toolbar.jsx';
import ToolbarDivider from '../ToolbarDivider/ToolbarDivider.jsx';
import ToolbarDropdown from '../ToolbarDropdown/ToolbarDropdown.jsx';
import ToolbarField from '../ToolbarField/ToolbarField.jsx';
import ToolbarIconButton, { ClearIcon } from '../ToolbarIconButton/ToolbarIconButton.jsx';
import ToolbarToggleButton from '../ToolbarToggleButton/ToolbarToggleButton.jsx';

/** @typedef {import('react')} React */

/**
 * @typedef {Object} WrmCoverageToolbarProps
 * @property {boolean} disableToolbar
 * @property {React.ReactElement} [prependButtons]
 * @property {Object} webResourcesFilteringOptions
 * @property {() => void} clearWebResources
 * @property {string | null} filterValue
 * @property {(value: string) => void} onFilterChange
 * @property {() => void} onResourceFilteringOptionSelected
 * @property {string | null} webResourcesFilteringValue
 * @property {boolean} showBigResources
 * @property {() => void} toggleShowBigResources
 * @property {boolean} showUnusedResources
 * @property {() => void} toggleShowUnusedResources
 * @property {boolean} hideCorePlugins
 * @property {() => void} toggleHideCorePlugins
 */

/** @type {React.FC<WrmCoverageToolbarProps>} */
const WrmCoverageToolbar = ({
  disableToolbar = false,
  prependButtons = null,
  webResourcesFilteringOptions,
  clearWebResources,
  filterValue,
  onFilterChange,
  onResourceFilteringOptionSelected,
  webResourcesFilteringValue,
  toggleShowBigResources,
  showBigResources,
  toggleShowUnusedResources,
  showUnusedResources,
  toggleHideCorePlugins,
  hideCorePlugins,
}) => {
  const onFilterChangeDebounced = useDebouncedCallback(onFilterChange, 250, {
    maxWait: 500,
  });

  return (
    <Toolbar>
      {prependButtons}
      <ToolbarIconButton disabled={disableToolbar} icon={ClearIcon} title="Clear" onClick={clearWebResources} />
      <ToolbarDivider />

      <ToolbarField disabled={disableToolbar} placeholder="Filter" value={filterValue} onChange={onFilterChangeDebounced} />
      <ToolbarDivider />

      <ToolbarDropdown
        className={styles.WebResourceFilteringSelectorDropdown}
        disabled={disableToolbar}
        options={webResourcesFilteringOptions}
        selected={webResourcesFilteringValue}
        onSelect={onResourceFilteringOptionSelected}
      />
      <ToolbarToggleButton disabled={disableToolbar} selected={showBigResources} onToggle={toggleShowBigResources}>
        Show only big
      </ToolbarToggleButton>
      <ToolbarToggleButton disabled={disableToolbar} selected={showUnusedResources} onToggle={toggleShowUnusedResources}>
        Show only unused
      </ToolbarToggleButton>
      <ToolbarToggleButton disabled={disableToolbar} selected={hideCorePlugins} onToggle={toggleHideCorePlugins}>
        Hide Core Plugins
      </ToolbarToggleButton>
    </Toolbar>
  );
};

export default memo(WrmCoverageToolbar);
