import { memo, useMemo } from 'react';

import { formatSize } from '../../helpers/formatSize.js';
import BottomToolbar from '../Toolbar/BottomToolbar.jsx';
import ToolbarDivider from '../ToolbarDivider/ToolbarDivider.jsx';
import ToolbarItem from '../ToolbarItem/ToolbarItem.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

/**
 * @param {WebResourceCoverageMeta[]} wrmCoverage
 * @returns {{ totalBytes: number; unusedBytes: number; usedBytes: number }}
 */
const getCoverageSum = (wrmCoverage) => {
  const result = { totalBytes: 0, unusedBytes: 0, usedBytes: 0 };

  for (const coverage of wrmCoverage) {
    result.totalBytes += coverage.totalBytes;
    result.unusedBytes += coverage.unusedBytes;
    result.usedBytes += coverage.usedBytes;
  }

  return result;
};

/** @type {React.FC<{ wrmCoverage: WebResourceCoverageMeta[]; filteredWrmCoverage: WebResourceCoverageMeta[] }>} */
const WrmCoverageBottomToolbar = ({ wrmCoverage, filteredWrmCoverage }) => {
  const wrmCoverageCounter = useMemo(() => wrmCoverage.length, [wrmCoverage]);
  const filteredWrmCoverageCounter = useMemo(() => filteredWrmCoverage.length, [filteredWrmCoverage]);

  const { totalBytes, unusedBytes, usedBytes } = useMemo(() => getCoverageSum(wrmCoverage), [wrmCoverage]);
  const {
    totalBytes: filteredTotalBytes,
    unusedBytes: filteredUnusedBytes,
    usedBytes: filteredUsedBytes,
  } = useMemo(() => getCoverageSum(filteredWrmCoverage), [filteredWrmCoverage]);

  const isFiltered = wrmCoverage.length !== filteredWrmCoverage.length;

  return (
    <BottomToolbar>
      <ToolbarItem>
        {isFiltered ? `${filteredWrmCoverageCounter} / ${wrmCoverageCounter} resources` : `${wrmCoverageCounter} resources`}
      </ToolbarItem>
      <ToolbarDivider />
      <ToolbarItem>
        {isFiltered ? `${formatSize(filteredTotalBytes)} / ${formatSize(totalBytes)} size` : `${formatSize(totalBytes)} size`}
      </ToolbarItem>
      <ToolbarDivider />
      <ToolbarItem>
        {isFiltered ? `${formatSize(filteredUsedBytes)} / ${formatSize(usedBytes)} used bytes` : `${formatSize(usedBytes)} used bytes`}
      </ToolbarItem>
      <ToolbarDivider />
      <ToolbarItem>
        {isFiltered
          ? `${formatSize(filteredUnusedBytes)} / ${formatSize(unusedBytes)} unused bytes`
          : `${formatSize(unusedBytes)} unused bytes`}
      </ToolbarItem>
    </BottomToolbar>
  );
};

export default memo(WrmCoverageBottomToolbar);
