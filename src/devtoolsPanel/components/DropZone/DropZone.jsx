import classNames from 'classnames';
import { memo, useCallback, useEffect, useMemo, useRef } from 'react';
import { useDropzone } from 'react-dropzone';

import styles from './DropZone.less';

/** @typedef {import('react')} React */

/**
 * @typedef {Object} Props
 * @property {React.ReactNode} children
 * @property {() => void} onClick
 * @property {() => void} onDrop
 * @property {() => void} onFile
 * @property {Record<string, string[]>} filesTypes
 */

/** @type {React.FC<Props>} */
const DropZone = ({ children, filesTypes, onDrop, onClick, onFile }) => {
  const innerRef = /** @type {React.MutableRefObject<HTMLDivElement>} */ (useRef());

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: filesTypes,

    onDrop: onFile,
  });

  const rootProps = useMemo(() => {
    return {
      className: classNames(styles.DropZone, {
        [styles.DropZoneActive]: isDragActive,
      }),

      onDrop: onDrop,

      onClick: onClick,
    };
  }, [isDragActive, onClick, onDrop]);

  // This will make the links in dropzone clickable
  const listener = useCallback((event) => {
    if (event.target.tagName.toLowerCase() === 'a') {
      event.stopPropagation();
    }
  }, []);

  useEffect(() => {
    const el = innerRef.current;

    if (el) {
      el.addEventListener('click', listener, { capture: true });
    }

    return () => {
      if (el) {
        el.removeEventListener('click', listener, { capture: true });
      }
    };
  }, [listener]);

  return (
    <div {...getRootProps(rootProps)}>
      <input {...getInputProps()} />
      <div ref={innerRef} className={styles.DropZoneInner}>
        {children}
      </div>
    </div>
  );
};

export default memo(DropZone);
