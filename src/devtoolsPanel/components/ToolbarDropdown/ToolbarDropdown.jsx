import classNames from 'classnames';
import { memo, useCallback, useEffect, useMemo, useRef } from 'react';

import styles from './ToolbarDropdown.less';

/** @typedef {import('react')} React */

const renderOptions = (options) =>
  options.map((option) => {
    const { label, value, items } = option;

    if (Array.isArray(items)) {
      return (
        <optgroup key={label} label={label}>
          {renderOptions(option.items)}
        </optgroup>
      );
    }

    return (
      <option key={value} value={value}>
        {label}
      </option>
    );
  });

/** @type {React.FC<{ options: unknown; className?: string; disabled?: boolean; selected?: string | null; onSelect: (arg0: string) => void  }>} */
const ToolbarDropdown = ({ options, className, onSelect, disabled = false, selected = null }) => {
  const selectRef = /** @type {React.MutableRefObject<HTMLSelectElement>} */ (useRef());

  const onChange = useCallback(
    (event) => {
      onSelect(event.target.value);
    },
    [onSelect]
  );

  const renderedOptions = useMemo(() => {
    return renderOptions(options);
  }, [options]);

  useEffect(
    function overwriteValue() {
      if (typeof selected !== 'undefined') {
        let selectedIndex = 0;

        const { options: allOptions } = selectRef.current;

        for (let i = 0; i < allOptions.length; i++) {
          if (allOptions[i].value === selected) {
            selectedIndex = i;
            break;
          }
        }

        selectRef.current.selectedIndex = selectedIndex;
      }
    },
    [selected]
  );

  return (
    <span className={classNames(styles.ToolbarDropdown, className)}>
      <select ref={selectRef} disabled={disabled} onChange={onChange}>
        {renderedOptions}
      </select>

      <span className={styles.DropdownArrow} />
    </span>
  );
};

export default memo(ToolbarDropdown);
