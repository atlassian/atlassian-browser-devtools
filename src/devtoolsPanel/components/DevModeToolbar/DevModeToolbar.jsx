import { memo } from 'react';

import styles from './DevModeToolbar.less';
import Toolbar from '../Toolbar/Toolbar.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const DevModeToolbar = () => <Toolbar className={styles.DevModeToolbar}>⚠️ Running a local version of ABD extension in watch mode</Toolbar>;

export default memo(DevModeToolbar);
