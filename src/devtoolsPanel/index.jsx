import { render } from 'react-dom';
import 'dialog-polyfill/dist/dialog-polyfill.css';

import DevtoolsPanelApp from './DevtoolsPanelApp/DevtoolsPanelApp.jsx';
import { initConnection } from './portConnection.js';
import store from './stores/store.js';

const renderApp = (rootId = 'root') => {
  const targetEl = document.getElementById(rootId);

  render(<DevtoolsPanelApp store={store} />, targetEl);
};

initConnection();
renderApp();
