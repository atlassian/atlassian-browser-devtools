/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */
/** @typedef {import('./types.js').WebResourceFilter} WebResourceFilter */

export const tokenName = 'is';

// e.g. is:unused
const isFilterRegExp = /^(?<filterTpe>(?:unused|big))$/i;

/**
 * @param {string} value
 * @param {boolean} negate
 * @returns {WebResourceFilter | null}
 */
export function getFilter(value, negate) {
  const match = value.match(isFilterRegExp);

  if (!match) {
    return null;
  }

  const matchGroups = /** @type {{ filterTpe: string }} */ (match.groups);
  const { filterTpe } = matchGroups;

  /** @type {WebResourceFilter} */
  function isFilter(webResource) {
    let result = true;

    switch (filterTpe.toLowerCase()) {
      case 'unused':
        result = webResource.isUnused;
        break;

      case 'big':
        result = webResource.isBig;
        break;
    }

    return negate ? !result : result;
  }

  return isFilter;
}
