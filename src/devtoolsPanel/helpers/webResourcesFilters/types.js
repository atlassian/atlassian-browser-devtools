/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

/** @typedef {(webResource: WebResourceCoverageMeta) => boolean} WebResourceFilter */

export {};
