/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */
/** @typedef {import('./types.js').WebResourceFilter} WebResourceFilter */

export const tokenName = 'type';

// e.g. type:css type:js
const sizeFilterRegExp = /^(?<type>(?:css|js))$/i;

/**
 * @param {string} value
 * @param {boolean} negate
 * @returns {WebResourceFilter | null}
 */
export function getFilter(value, negate) {
  const match = value.match(sizeFilterRegExp);

  if (!match) {
    return null;
  }

  const matchGroups = /** @type {{ type: string }} */ (match.groups);
  const { type } = matchGroups;
  const resourceType = type.toLowerCase();

  /** @type {WebResourceFilter} */
  function typeFilter(webResource) {
    const input = webResource.type;
    const result = input && input.toLowerCase() === resourceType;

    return negate ? !result : result;
  }

  return typeFilter;
}
