import filesizeParser from 'filesize-parser';

/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */
/** @typedef {import('./types.js').WebResourceFilter} WebResourceFilter */

const isGt = (input, predicate) => input > predicate;
const isLt = (input, predicate) => input < predicate;
const isEq = (input, predicate) => input === predicate;

export const tokenName = 'size';

// e.g. size:>20, size:<50kb, size:>20mb
const sizeFilterRegExp = /^(?<operator>[><]?)(?<size>[0-9]+)(?<suffix>[a-zA-Z]+)?$/;

/**
 * @param {string} value
 * @param {boolean} negate
 * @returns {WebResourceFilter | null}
 */
export function getFilter(value, negate) {
  const match = value.match(sizeFilterRegExp);

  if (!match) {
    return null;
  }

  const matchGroups = /** @type {{ operator: string; size: string; suffix: string }} */ (match.groups);
  const { operator, size, suffix } = matchGroups;

  const filterFunction = operator === '>' ? isGt : operator === '<' ? isLt : isEq;

  let sizePredicate;
  try {
    sizePredicate = filesizeParser(`${size}${suffix}`);
  } catch (e) {
    return null;
  }

  /** @type {WebResourceFilter} */
  function sizeFilter(webResource) {
    const input = webResource.totalBytes;
    const result = filterFunction(input, sizePredicate);

    return negate ? !result : result;
  }

  return sizeFilter;
}
