import escapeRegExp from 'lodash/escapeRegExp';

import { tokenName as isTokenName, getFilter as getIsFilter } from './isFilter.js';
import { tokenName as typeTokenName, getFilter as getTypeFilter } from './resourceTypeFilter.js';
import { tokenName as sizeTokenName, getFilter as getSizeFilter } from './sizeFilter.js';

/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

const tokens = new Map();

tokens.set(sizeTokenName, getSizeFilter);
tokens.set(isTokenName, getIsFilter);
tokens.set(typeTokenName, getTypeFilter);

const tokensRegExp = /(?<negate>!)?(?<tokenName>[a-zA-Z-]+):(?<tokenValue>[^\s]+)/g;

const getWebResourceStringFilter = (queryRegExp, negate) => (webResource) => {
  const { wrmKey, wrmFile, url } = webResource;
  const result = Boolean(wrmKey.match(queryRegExp) || wrmFile.match(queryRegExp) || url.match(queryRegExp));

  return negate ? !result : result;
};

/**
 * @param {string} query
 * @returns {string}
 */
const escapeQuery = (query) => query.trim().toLocaleLowerCase();

/** @typedef {(webResource: WebResourceMeta) => boolean} FilterFn */

/**
 * @param {string} input
 * @returns {FilterFn[]} Filters
 */
export const getFilters = (input) => {
  const matches = Array.from(input.matchAll(tokensRegExp)).reverse();
  const filters = /** @type {FilterFn[]} */ [];
  let output = input;

  for (const { groups, index, 0: found } of matches) {
    const { tokenName, tokenValue, negate: negateRaw } = /** @type {{ tokenName: string; tokenValue: string; negate: string }} */ (groups);
    const negate = Boolean(negateRaw);

    if (!tokens.has(tokenName)) {
      continue;
    }

    const filterFactory = tokens.get(tokenName);
    const filterFn = filterFactory(tokenValue, negate);

    if (!filterFn) {
      continue;
    }

    filters.push(filterFn);

    // Replace the input
    output = output.substr(0, index) + output.substr(/** @type {number} ( */ (index) + found.length + 1);
  }

  // Split the remaining output and add filters
  for (const query of output.split(/\s+/)) {
    if (query && query.trim()) {
      let escapedQuery = escapeQuery(query);
      const negate = escapedQuery.charAt(0) === '!';

      if (negate) {
        escapedQuery = escapedQuery.substr(1);
      }

      const queryRegExp = new RegExp(escapeRegExp(escapedQuery), 'i');
      const filterFn = getWebResourceStringFilter(queryRegExp, negate);

      filters.push(filterFn);
    }
  }

  return filters;
};
