export const getContextLabel = (context) => (context === '_super' ? 'Super batch' : context);
