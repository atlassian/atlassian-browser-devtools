// Loosely based on the Google Lighthouse thresholds
// https://github.com/GoogleChrome/lighthouse/blob/6c3d6090d8243bb9e7a9f5fabc46bf6d5993c430/lighthouse-core/audits/byte-efficiency/unused-javascript.js#L23-L24
const BIG_RESOURCE_BYTES_IGNORE_THRESHOLD = 200 * 1024; // 300 KB; Resources greater than that are marked as "big"

const UNUSED_RESOURCE_BYTES_IGNORE_THRESHOLD = 30 * 1024; // 30 Kb; Resources that have 30kb unused bytes (or more) are marked as "unused"
const UNUSED_RESOURCE_PERCENTAGE_IGNORE_THRESHOLD = 80; // Resources that have 80% unused bytes (or more) are marked as "unused"

/** @typedef {import('../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */
/** @typedef {import('../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

/**
 * @param {Pick<WebResourceMeta, 'totalBytes'>} webResource
 * @returns {boolean}
 */
export function isWebResourceBig(webResource) {
  return webResource.totalBytes > BIG_RESOURCE_BYTES_IGNORE_THRESHOLD;
}

/**
 * @param {Pick<WebResourceCoverageMeta, 'unusedBytes' | 'unused'>} webResource
 * @returns {boolean}
 */
export function isWebResourceUnused(webResource) {
  return isWebResourceBytesUnused(webResource) || isWebResourcePercentageUnused(webResource);
}

/**
 * @param {Pick<WebResourceCoverageMeta, 'unusedBytes'>} webResource
 * @returns {boolean}
 */
export function isWebResourceBytesUnused(webResource) {
  return webResource.unusedBytes > UNUSED_RESOURCE_BYTES_IGNORE_THRESHOLD;
}

/**
 * @param {Pick<WebResourceCoverageMeta, 'unused'>} webResource
 * @returns {boolean}
 */
export function isWebResourcePercentageUnused(webResource) {
  return webResource.unused > UNUSED_RESOURCE_PERCENTAGE_IGNORE_THRESHOLD;
}

/**
 * @param {Pick<WebResourceCoverageMeta, 'usedBytes' | 'used'>} webResource
 * @returns {boolean}
 */
export function isWebResourceCompletelyUnused(webResource) {
  return webResource.usedBytes === 0 || webResource.used === 0;
}
