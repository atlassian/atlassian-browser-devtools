import { filesize } from 'filesize';

/**
 * @param {number} size
 * @returns {string}
 */
export const formatSize = (size) => /** @type {string} */ (filesize(size, { standard: 'iec', round: 2 }));
