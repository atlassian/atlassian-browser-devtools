import { getContextLabel } from './webResoucesUtils.js';

/** @typedef {import('../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */
/** @typedef {import('../DevtoolsPanelApp/Plugins/PluginsProvider').UpmAtlassianPlugins} UpmAtlassianPlugins */
/** @typedef {import('../DevtoolsPanelApp/Plugins/PluginsProvider').UpmAtlassianPlugin} UpmAtlassianPlugin */

export const ALL_RESOURCES = 'ALL_RESOURCES';
const ALL_CONTEXTS = 'ALL_CONTEXTS';
const NO_CONTEXT = 'NO_CONTEXT';

/**
 * @param {WebResourceCoverageMeta[]} wrmResources
 * @param {UpmAtlassianPlugins} plugins
 * @param {Object} options
 * @param {((webResource: WebResourceCoverageMeta) => boolean)[]} options.filters
 * @param {string | null} options.resourcesFilter
 * @param {boolean} [options.showBigResources]
 * @param {boolean} [options.showUnusedResources]
 * @param {boolean} [options.hideCorePlugins]
 * @returns {WebResourceCoverageMeta[]}
 */
export const filterWebResources = (
  wrmResources,
  plugins,
  { filters: customFilters = [], resourcesFilter, showBigResources = false, showUnusedResources = false, hideCorePlugins = false }
) => {
  if (!customFilters.length && !resourcesFilter && !showBigResources && !showUnusedResources && !hideCorePlugins) {
    return wrmResources;
  }

  const filterByContext =
    resourcesFilter && resourcesFilter !== ALL_RESOURCES && resourcesFilter !== ALL_CONTEXTS && resourcesFilter !== NO_CONTEXT;

  return wrmResources.filter((webResource) => {
    const { context, wrmKey } = webResource;
    const pluginKey = wrmKey.split(':')[0];

    // Show only user installed plugins
    // Filter-out non-user installed plugins
    if (hideCorePlugins && plugins.has(pluginKey)) {
      const plugin = /** @type {UpmAtlassianPlugin} */ (plugins.get(pluginKey));

      if (!plugin.userInstalled) {
        return false;
      }
    }

    // Show only context loaded web-resources
    // Filter-out when context doesn't match
    if (filterByContext && !context.includes(resourcesFilter)) {
      return false;
    }

    // Show only no-context loaded web-resources
    // Filter-out when it's loaded by context
    if (resourcesFilter && resourcesFilter === NO_CONTEXT && context.length > 0) {
      return false;
    }

    // Show only big resources
    // Filter-out when it's not big
    if (showBigResources && !webResource.isBig) {
      return false;
    }

    // Show only unused resources
    // Filter-out when it's not unused
    if (showUnusedResources && !webResource.isUnused) {
      return false;
    }

    const isMatching = customFilters.every((filterFn) => filterFn(webResource));

    if (!isMatching) {
      return false;
    }

    return true;
  });
};

/**
 * @param {WebResourceCoverageMeta[]} wrmResources
 * @returns {string[]}
 */
const getUsedContexts = (wrmResources) => {
  const contexts = new Set();

  wrmResources.forEach(({ context }) => {
    // eslint-disable-next-line no-shadow
    context.forEach((context) => contexts.add(context));
  });

  return Array.from(contexts).sort();
};

/**
 * @param {WebResourceCoverageMeta[]} [wrmResources]
 * @returns {Object}
 */
export const getWebResourceFilteringOptions = (wrmResources) => {
  const contexts = wrmResources ? getUsedContexts(wrmResources) : [];
  const options = [
    {
      label: 'Filter resources',
      items: [
        { label: 'Show all', value: ALL_RESOURCES },
        { label: 'Loaded by context', value: ALL_CONTEXTS },
        { label: 'Not loaded by context', value: NO_CONTEXT },
      ],
    },

    {
      label: 'Context',
      items: contexts.map((context) => ({ label: getContextLabel(context), value: context })),
    },
  ];

  return options;
};
