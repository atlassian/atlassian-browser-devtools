import { trackEvent } from '../common/analytics/analytics.js';
import { DEVTOOLS_PANEL_OPENED_EVENT } from '../common/events/events.js';
import { devtoolsPanelLogger as logger } from '../common/logger.js';
import { DEVTOOLS_PANEL_PORT } from '../common/ports.js';
import { withSentryWrapper } from '../common/sentry/sentry.js';

/** @typedef {import('./events/events.js').Events} Events */

// Starts the connection with the extension background script
const portConnection = chrome.runtime.connect({
  name: DEVTOOLS_PANEL_PORT,
});

/**
 * Sends a postMessage to the extension background script
 *
 * @template {string} TopicT
 * @template {Object} PayloadT
 * @param {TopicT} topic
 * @param {PayloadT} [payload]
 */
export function dispatchMessage(topic, payload) {
  const message = {
    topic: topic,
    message: payload,
  };

  portConnection.postMessage(message);
}

/* eslint-disable jsdoc/valid-types */
/**
 * @template {Object} T
 * @typedef {(payload: T) => void} MessageListener
 */

const messageListeners = /** @type {Map<string, MessageListener<unknown>[]>} */ (new Map());

/**
 * @param {keyof Events} topic
 * @param {MessageListener<Events[keyof Events]>} listener
 */
export function addMessageListener(topic, listener) {
  let /** @type {MessageListener<unknown>[]} */ listeners;

  if (messageListeners.has(topic)) {
    listeners = /** @type {MessageListener<unknown>[]} */ (messageListeners.get(topic));
  } else {
    listeners = [];
    messageListeners.set(topic, listeners);
  }

  // @ts-expect-error TS2345 issues with type casting
  listeners.push(listener);
}

/**
 * @param {keyof Events} topic
 * @param {MessageListener<Events[keyof Events]>} listener
 */
export function removeMessageListener(topic, listener) {
  if (!messageListeners.has(topic)) {
    return;
  }

  const newListeners = /** @type {MessageListener<unknown>[]} */ (messageListeners.get(topic)).filter((l) => l !== listener);

  messageListeners.set(topic, newListeners);
}
/* eslint-enable jsdoc/valid-types */

const onPortMessage = withSentryWrapper((/** @type {{ topic: keyof Events; message: unknown }} */ { topic, message }) => {
  logger(`Received message "${topic}"`, message);

  if (!messageListeners.has(topic)) {
    return;
  }

  const listeners = /** @type {MessageListener<unknown>[]} */ (messageListeners.get(topic));

  for (const listener of listeners) {
    try {
      listener(message);
    } catch (e) {
      logger(`Error while handling message "${topic}"`, e);
    }
  }
});

export const initConnection = () => {
  logger('Initializing connection with ABD background script');

  portConnection.onMessage.addListener(onPortMessage);

  trackEvent(DEVTOOLS_PANEL_OPENED_EVENT);

  // Send first message to background script to establish a connection
  dispatchMessage(DEVTOOLS_PANEL_OPENED_EVENT, {
    tabId: chrome.devtools.inspectedWindow.tabId,
  });
};

/**
 * Triggers fake incoming postMessage. This function can be used to simulate incoming postMessage in dev mode only.
 *
 * @template {string} TopicT
 * @template {Object} PayloadT
 * @param {TopicT} topic
 * @param {PayloadT} [payload]
 */
export const _triggerIncomingMessage = (topic, payload) => {
  logger('Triggering incoming postMessage', { topic, payload });

  onPortMessage({ topic, message: payload });
};
