import { memo } from 'react';

import styles from './DevtoolsMainPanel.less';
import { IS_DEV_MODE } from '../../../common/devMode.js';
import DevModeToolbar from '../../components/DevModeToolbar/DevModeToolbar.jsx';
import PanelWrapper from '../../components/PanelWrapper/PanelWrapper.jsx';
import SplitPanel from '../../components/SplitPanel/SplitPanel.jsx';
import PanelContent from '../PanelContent/PanelContent.jsx';
import PanelSidebar from '../PanelSidebar/PanelSidebar.jsx';

/** @typedef {import('react')} React */

let banner = null;

if (IS_DEV_MODE) {
  banner = <DevModeToolbar />;
}

const panelMinSizes = [50, 600];
const panelSizes = [20, 80];

/** @type {React.FC} */
const DevtoolsMainPanel = () => (
  <PanelWrapper className={styles.DevtoolsMainPanel}>
    {banner}
    <SplitPanel minSizes={panelMinSizes} sizes={panelSizes}>
      <PanelSidebar />
      <PanelContent />
    </SplitPanel>
  </PanelWrapper>
);

export default memo(DevtoolsMainPanel);
