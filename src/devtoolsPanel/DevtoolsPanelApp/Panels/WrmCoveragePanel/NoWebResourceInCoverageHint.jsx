import { memo } from 'react';

import HintButton from '../../../components/PanelHint/HintButton.jsx';
import PanelHint from '../../../components/PanelHint/PanelHint.jsx';
import { ClearIcon } from '../../../components/ToolbarIconButton/ToolbarIconButton.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ clearCoverage: () => void }>} */
const NoWebResourcesInCoverageHint = ({ clearCoverage }) => (
  <PanelHint>
    {/* eslint-disable-next-line react/no-unescaped-entities */}
    <p>This coverage file doesn't include any information about the web-resources.</p>
    <p>
      Click the <HintButton icon={ClearIcon} title="Clear coverage" onClick={clearCoverage} /> icon to clear the selected coverage and
      select a new one.
    </p>
  </PanelHint>
);

export default memo(NoWebResourcesInCoverageHint);
