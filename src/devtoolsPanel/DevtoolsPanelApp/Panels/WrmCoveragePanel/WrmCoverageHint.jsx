import { memo } from 'react';

import PanelHint from '../../../components/PanelHint/PanelHint.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const WrmCoverageHint = () => (
  <PanelHint>
    <p>Click here to select or Drag and Drop the JSON file with a coverage report.</p>
    <p>
      <a href="https://developers.google.com/web/tools/chrome-devtools/coverage#open" rel="noreferrer nofollow" target="_blank">
        Learn more on how to collect coverage report
      </a>
    </p>
  </PanelHint>
);

export default memo(WrmCoverageHint);
