import { memo } from 'react';

import WrmCoveragePanel from './WrmCoveragePanel.jsx';
import { AnalyticsContextProvider } from '../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import PluginsProvider from '../../Plugins/PluginsProvider.jsx';

const ANALYTICS_EVENTS_GROUP = 'WRM_COVERAGE_PANEL';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const WrmCoveragePanelWrapper = () => (
  <AnalyticsContextProvider analyticsContext={ANALYTICS_EVENTS_GROUP}>
    <PluginsProvider>
      <WrmCoveragePanel />
    </PluginsProvider>
  </AnalyticsContextProvider>
);

export default memo(WrmCoveragePanelWrapper);
