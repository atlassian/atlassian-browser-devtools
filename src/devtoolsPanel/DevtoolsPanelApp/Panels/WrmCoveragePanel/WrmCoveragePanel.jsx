import { memo, useCallback, useState } from 'react';

import NoWebResourcesInCoverageHint from './NoWebResourceInCoverageHint.jsx';
import WrmCoverageHint from './WrmCoverageHint.jsx';
import { getEventName, trackEvent } from '../../../../common/analytics/analytics.js';
import { useAnalyticsContext } from '../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import Dialog from '../../../components/Dialog/Dialog.jsx';
import DropZone from '../../../components/DropZone/DropZone.jsx';
import PanelWrapper from '../../../components/PanelWrapper/PanelWrapper.jsx';
import WrmCoverageBrowser from '../../../components/WrmCoverageBrowser/WrmCoverageBrowser.jsx';
import { readCoverageFile } from '../../../services/coverage/coverageReader.js';
import coverageSchemaValidator from '../../../services/coverage/coverageSchemaValidator.js';
import { useGetPlugins } from '../../Plugins/PluginsProvider.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../../DevtoolsPanelApp/Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

const ACCEPTED_FILES_TYPES = {
  'application/json': ['.json'],
};

/** @type {React.FC} */
const WrmCoveragePanel = () => {
  const analyticsContext = useAnalyticsContext();

  const plugins = useGetPlugins();
  const [coverage, setCoverage] = /** @type {[CoverageJson | null, (arg0: CoverageJson | null) => void]} */ (useState(null));
  const [coverageError, setCoverageError] = useState(false);

  const onClick = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'COVERAGE_FILE', 'CLICKED_TO_LOAD'));
  }, [analyticsContext]);

  const onDrop = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'COVERAGE_FILE', 'FILE_DROPPED'));
  }, [analyticsContext]);

  const onFile = useCallback(
    (files) => {
      // We allot to select only a single file
      const [file] = files;

      trackEvent(getEventName(analyticsContext, 'COVERAGE_FILE', 'FILE_SELECTED'));

      readCoverageFile(file)
        // eslint-disable-next-line consistent-return
        .then(async (fileContent) => {
          if (!(await coverageSchemaValidator(fileContent))) {
            throw new Error('Invalid schema');
          }

          const coverageContent = /** @type {CoverageJson} */ (fileContent);

          trackEvent(getEventName(analyticsContext, 'COVERAGE_FILE', 'COVERAGE_VALID'));

          setCoverageError(false);
          setCoverage(coverageContent);
        })
        .catch(() => {
          trackEvent(getEventName(analyticsContext, 'COVERAGE_FILE', 'COVERAGE_INVALID'));

          setCoverageError(true);
        });
    },
    [analyticsContext]
  );

  const clearCoverage = useCallback(() => {
    setCoverage(null);
    setCoverageError(false);
  }, []);

  const noWebResourcesInCoverageHint = <NoWebResourcesInCoverageHint clearCoverage={clearCoverage} />;

  const hasCoverageFile = Boolean(coverage);

  return (
    <PanelWrapper>
      {coverageError && (
        <Dialog onClose={clearCoverage}>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          <p>The content of your file doesn't contain any coverage data or the data is invalid.</p>
          <p>Try collecting the coverage again.</p>
        </Dialog>
      )}

      {hasCoverageFile ? (
        <WrmCoverageBrowser
          clearCoverage={clearCoverage}
          coverage={coverage}
          noCoverageHint={null}
          noWebResourcesInCoverageHint={noWebResourcesInCoverageHint}
          plugins={plugins}
        />
      ) : (
        <DropZone filesTypes={ACCEPTED_FILES_TYPES} onClick={onClick} onDrop={onDrop} onFile={onFile}>
          <WrmCoverageHint />
        </DropZone>
      )}
    </PanelWrapper>
  );
};

export default memo(WrmCoveragePanel);
