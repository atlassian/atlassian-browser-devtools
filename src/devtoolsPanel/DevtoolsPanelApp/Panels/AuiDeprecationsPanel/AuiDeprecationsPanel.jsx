import { memo } from 'react';
import { useSelector } from 'react-redux';

import DeprecationMessageContainer from './DeprecationMessage.jsx';
import DeprecationsToolbar from './DeprecationsToolbar.jsx';
import Console from '../../../components/Console/Console.jsx';
import EmptyDeprecationsList from '../../../components/EmpyDeprecationsList/EmptyDeprecationsList.jsx';
import PanelWrapper from '../../../components/PanelWrapper/PanelWrapper.jsx';
import { selectors as deprecationsSelectors } from '../../../stores/auiDeprecationsStore.js';

/** @typedef {import('react')} React */
/** @typedef {import('../../../../contentScript/deprecations/types').AuiDeprecationWithMeta} AuiDeprecationWithMeta */

/** @type {React.FC} */
const AuiDeprecationsPanel = () => {
  const deprecations = /** @type {AuiDeprecationWithMeta[]} */ useSelector(deprecationsSelectors.getFilteredDeprecations);

  const deprecationMessages = deprecations.map((/** @type {AuiDeprecationWithMeta} */ deprecation) => {
    const { nodeId, selector, timestamp } = deprecation;
    const key = `${nodeId}-${selector}-${timestamp}`;

    return <DeprecationMessageContainer key={key} deprecation={deprecation} />;
  });

  return (
    <PanelWrapper>
      <DeprecationsToolbar />
      <Console emptyOutput={<EmptyDeprecationsList />}>{deprecationMessages}</Console>
    </PanelWrapper>
  );
};

export default memo(AuiDeprecationsPanel);
