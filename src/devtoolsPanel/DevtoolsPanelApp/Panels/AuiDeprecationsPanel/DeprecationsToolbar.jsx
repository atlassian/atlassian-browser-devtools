import { memo, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Toolbar from '../../../components/Toolbar/Toolbar.jsx';
import ToolbarDivider from '../../../components/ToolbarDivider/ToolbarDivider.jsx';
import ToolbarField from '../../../components/ToolbarField/ToolbarField.jsx';
import ToolbarIconButton, { ClearIcon } from '../../../components/ToolbarIconButton/ToolbarIconButton.jsx';
import ToolbarItem from '../../../components/ToolbarItem/ToolbarItem.jsx';
import ToolbarSpacer from '../../../components/ToolbarSpacer/ToolbarSpacer.jsx';
import ToolbarToggleButton from '../../../components/ToolbarToggleButton/ToolbarToggleButton.jsx';
import { actions as deprecationsActions } from '../../../stores/auiDeprecationsStore.js';
import { actions as highlightingActions, selectors as highlightingSelectors } from '../../../stores/auiHighlightingStore.js';
import { selectors as pageSelectors } from '../../../stores/pageStore.js';

/** @typedef {import('react')} React */
/** @typedef {import('../../../stores/auiHighlightingStore').AuiHighlightingState} AuiHighlightingState */

// @TODO: Extract me
/** @typedef {(arg0: string) => void} OnToggleCallback */

/**
 * @template T
 * @typedef {T} UseSelector
 */

/** @type {React.FC} */
const DeprecationsToolbar = () => {
  const dispatch = useDispatch();
  const hasAui = useSelector(pageSelectors.getHasAui);
  const auiVersion = useSelector(pageSelectors.getAuiVersion);
  const isPageLoaded = useSelector(pageSelectors.getIsPageLoaded);
  const highlightDeprecatedElements = /** @type {UseSelector<boolean>} */ (
    useSelector(highlightingSelectors.getHighlightDeprecatedElements)
  );

  const toggleHighlightDeprecatedElements = /** @type {OnToggleCallback} */ (
    useCallback(() => dispatch(highlightingActions.toggleHighlightDeprecatedElements()), [dispatch])
  );
  const togglePreserveLog = useCallback(() => dispatch(deprecationsActions.togglePreserveLog()), [dispatch]);
  const toggleShowTimeStamps = useCallback(() => dispatch(deprecationsActions.toggleShowTimeStamps()), [dispatch]);
  const clearDeprecations = useCallback(() => dispatch(deprecationsActions.clearDeprecations()), [dispatch]);
  const filterDeprecations = useCallback((filterQuery) => dispatch(deprecationsActions.filterDeprecations(filterQuery)), [dispatch]);

  return (
    <Toolbar>
      <ToolbarToggleButton selected={highlightDeprecatedElements} onToggle={toggleHighlightDeprecatedElements}>
        Auto Highlight Deprecations
      </ToolbarToggleButton>

      <ToolbarDivider />

      <ToolbarItem>
        {isPageLoaded ? (hasAui && `AUI version: ${auiVersion}`) || 'This page is not using AUI' : 'detecting AUI...'}
      </ToolbarItem>

      <ToolbarDivider />

      <ToolbarField placeholder="Filter" onChange={filterDeprecations} />

      <ToolbarSpacer />

      <ToolbarToggleButton
        /* FIXME: fix the missing support for title title="Do not clear output on page reload / navigation" */ onToggle={togglePreserveLog}
      >
        Preserve log
      </ToolbarToggleButton>

      <ToolbarToggleButton onToggle={toggleShowTimeStamps}>Show Timestamps</ToolbarToggleButton>

      <ToolbarIconButton icon={ClearIcon} title="Clear Deprecation Scanner output" onClick={clearDeprecations} />
    </Toolbar>
  );
};

export default memo(DeprecationsToolbar);
