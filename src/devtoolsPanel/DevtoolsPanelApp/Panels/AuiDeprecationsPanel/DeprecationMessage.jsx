import { memo, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import DeprecationMessage from '../../../components/DeprecationMessage/DeprecationMessage.jsx';
import { selectors } from '../../../stores/auiDeprecationsStore.js';
import { actions } from '../../../stores/auiHighlightingStore.js';

/** @typedef {import('react')} React */
/** @typedef {import('../../../../contentScript/deprecations/types').AuiDeprecationWithMeta} AuiDeprecationWithMeta */

/** @type {React.FC<{ deprecation: AuiDeprecationWithMeta }>} */
const DeprecationMessageContainer = (props) => {
  const dispatch = useDispatch();
  const showTimestamps = useSelector(selectors.getShowTimestamps);

  const onHighlight = useCallback((nodeId) => dispatch(actions.highlightNode(nodeId)), [dispatch]);
  const onUnhighlight = useCallback((nodeId) => dispatch(actions.unhighlightNode(nodeId)), [dispatch]);
  const onElementClick = useCallback((nodeId) => dispatch(actions.scrollToNode(nodeId)), [dispatch]);

  return (
    <DeprecationMessage
      {...props}
      showTimestamps={showTimestamps}
      onElementClick={onElementClick}
      onHighlight={onHighlight}
      onUnhighlight={onUnhighlight}
    />
  );
};

export default memo(DeprecationMessageContainer);
