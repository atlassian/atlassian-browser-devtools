import { memo } from 'react';
import { useSelector } from 'react-redux';

import WebResourcesPanel from './WrmInspectorPanel.jsx';
import { AnalyticsContextProvider } from '../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import PanelWrapper from '../../../components/PanelWrapper/PanelWrapper.jsx';
import { selectors as wrmInspectorSelectors } from '../../../stores/wrmInspectorStore.js';
import PluginsProvider from '../../Plugins/PluginsProvider.jsx';

/** @typedef {import('react')} React */

const ANALYTICS_EVENTS_GROUP = 'WRM_INSPECTOR_PANEL';

/** @type {React.FC} */
const WrmInspectorPanelWrapper = () => {
  const webResources = useSelector(wrmInspectorSelectors.getWebResources);

  return (
    <PanelWrapper>
      <AnalyticsContextProvider analyticsContext={ANALYTICS_EVENTS_GROUP}>
        <PluginsProvider>
          <WebResourcesPanel webResources={webResources} />
        </PluginsProvider>
      </AnalyticsContextProvider>
    </PanelWrapper>
  );
};

export default memo(WrmInspectorPanelWrapper);
