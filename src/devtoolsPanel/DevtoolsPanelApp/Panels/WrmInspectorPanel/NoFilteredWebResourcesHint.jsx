import { memo, useCallback } from 'react';

import PanelHint from '../../../components/PanelHint/PanelHint.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ clearFilters: () => void }>} */
const NoFilteredWebResourcesHint = ({ clearFilters }) => {
  const clickHandler = useCallback(
    (e) => {
      e.preventDefault();

      clearFilters();
    },
    [clearFilters]
  );

  return (
    <PanelHint>
      {/* eslint-disable-next-line react/no-unescaped-entities */}
      <p>The applied filters couldn't match any web-resources.</p>
      <p>
        <a href="" onClick={clickHandler}>
          Click here to clear all the filters.
        </a>
      </p>
    </PanelHint>
  );
};

export default memo(NoFilteredWebResourcesHint);
