import { memo } from 'react';
import { useDebouncedCallback } from 'use-debounce';

import Toolbar from '../../../components/Toolbar/Toolbar.jsx';
import ToolbarDivider from '../../../components/ToolbarDivider/ToolbarDivider.jsx';
import ToolbarDropdown from '../../../components/ToolbarDropdown/ToolbarDropdown.jsx';
import ToolbarField from '../../../components/ToolbarField/ToolbarField.jsx';
import ToolbarIconButton, { ClearIcon } from '../../../components/ToolbarIconButton/ToolbarIconButton.jsx';
import ToolbarToggleButton from '../../../components/ToolbarToggleButton/ToolbarToggleButton.jsx';
import styles from '../../../components/WrmCoverageBrowser/WrmCoverageToolbar.less';

/** @typedef {import('react')} React */

// TODO: Reset input and dropdown values

/**
 * @typedef {Object} WrmInspectorToolbarProps
 * @property {unknown[]} webResourcesFilteringOptions
 * @property {(arg0: string) => void} toggleShowBigResources
 * @property {boolean} showBigResources
 * @property {() => void} clearWebResources
 * @property {(arg0: string) => void} toggleHideCorePlugins
 * @property {boolean} hideCorePlugins
 * @property {() => void} onFilterChange
 * @property {string | null} filterValue
 * @property {() => void} onResourceFilteringOption
 * @property {string | null} webResourcesFilteringValue
 */

/** @type {React.FC<WrmInspectorToolbarProps>} */
const WrmInspectorToolbar = ({
  webResourcesFilteringOptions,
  toggleShowBigResources,
  showBigResources,
  clearWebResources,
  toggleHideCorePlugins,
  hideCorePlugins,
  onFilterChange,
  filterValue,
  onResourceFilteringOption,
  webResourcesFilteringValue,
}) => {
  const onFilterChangeDebounced = useDebouncedCallback(onFilterChange, 250, {
    maxWait: 500,
  });

  return (
    <Toolbar>
      <ToolbarIconButton icon={ClearIcon} title="Clear" onClick={clearWebResources} />
      <ToolbarDivider />

      <ToolbarField placeholder="Filter" value={filterValue} onChange={onFilterChangeDebounced} />
      <ToolbarDivider />

      <ToolbarDropdown
        className={styles.WebResourceFilteringSelectorDropdown}
        options={webResourcesFilteringOptions}
        selected={webResourcesFilteringValue}
        onSelect={onResourceFilteringOption}
      />
      <ToolbarToggleButton selected={showBigResources} onToggle={toggleShowBigResources}>
        Show only big
      </ToolbarToggleButton>

      <ToolbarToggleButton selected={hideCorePlugins} onToggle={toggleHideCorePlugins}>
        Hide Core plugins
      </ToolbarToggleButton>
    </Toolbar>
  );
};

export default memo(WrmInspectorToolbar);
