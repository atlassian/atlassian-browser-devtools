import { memo } from 'react';

import PanelHint from '../../../components/PanelHint/PanelHint.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const NoResourcesHint = () => (
  <PanelHint>
    <p>
      Watching for the usage of <strong>WRM</strong> on the page…
    </p>
  </PanelHint>
);

export default memo(NoResourcesHint);
