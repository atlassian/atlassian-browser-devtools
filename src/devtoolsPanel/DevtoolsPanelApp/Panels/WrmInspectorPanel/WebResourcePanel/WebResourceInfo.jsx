import { memo } from 'react';

import styles from './WebResourceInfo.less';
import ResourceSizeFormatted from '../../../../components/WebResources/ResourceSizeFormatted.jsx';
import WebResourceLabel from '../../../../components/WebResources/WebResourceLabel/WebResourceLabel.jsx';
import WebResourceLabelsGroup from '../../../../components/WebResources/WebResourceLabel/WebResourcesGroup.jsx';
import { getContextLabel } from '../../../../helpers/webResoucesUtils.js';
import { usePlugin } from '../../../Plugins/PluginsProvider.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/** @type {React.FC<{ webResource: WebResourceMeta }>} */
const WebResourceInfo = ({ webResource }) => {
  const { isLoadedFromBatch } = webResource;
  const plugin = usePlugin(webResource);

  return (
    <div className={styles.WebResourceInfo}>
      <dl>
        <dt>Resource key</dt>
        <dd>{webResource.wrmKey}</dd>

        <dt>Plugin name</dt>
        <dd>{plugin !== null ? plugin.name : 'unknown'}</dd>

        <dt>Resource file</dt>
        <dd>{webResource.wrmFile}</dd>

        <dt>Type</dt>
        <dd>{webResource.type}</dd>

        <dt>Loaded from batch</dt>
        <dd>{isLoadedFromBatch ? 'Yes' : 'No'}</dd>

        {!isLoadedFromBatch && (
          <>
            <dt>Resource URL</dt>
            <dd>
              <a href={webResource.url} rel="nofollow noreferrer" target="_blank">
                {webResource.url}
              </a>
            </dd>
          </>
        )}

        {isLoadedFromBatch && (
          <>
            <dt>Batch type</dt>
            <dd>{webResource.batchType ? webResource.batchType.toUpperCase() : 'unknown'}</dd>
          </>
        )}

        <dt>Loaded by context</dt>
        <dd>{webResource.context.length ? 'Yes' : 'No'}</dd>

        {isLoadedFromBatch && (
          <>
            <dt>Context(s)</dt>
            <dd>
              <WebResourceLabelsGroup>
                {webResource.context.map((context) => (
                  <WebResourceLabel key={context}>{getContextLabel(context)}</WebResourceLabel>
                ))}
              </WebResourceLabelsGroup>
            </dd>

            <dt>Batch URL</dt>
            <dd>
              <a href={webResource.url} rel="nofollow noreferrer" target="_blank">
                {webResource.url}
              </a>
            </dd>
          </>
        )}

        <dt>Resource size</dt>
        <dd>
          <ResourceSizeFormatted webResource={webResource} />
        </dd>
      </dl>
    </div>
  );
};

export default memo(WebResourceInfo);
