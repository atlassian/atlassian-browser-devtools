import { memo, useCallback } from 'react';

import WebResourceInfo from './WebResourceInfo.jsx';
import styles from './WebResourcePanel.less';
import { getEventName, trackEvent } from '../../../../../common/analytics/analytics.js';
import { useAnalyticsContext } from '../../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import DependencyTree from '../../../../components/DependencyTree/DependencyTree.jsx';
import ErrorWrapper from '../../../../components/ErrorWrapper/ErrorWrapper.jsx';
import PanelHint from '../../../../components/PanelHint/PanelHint.jsx';
import PanelWrapper from '../../../../components/PanelWrapper/PanelWrapper.jsx';
import Tabs, { Tab } from '../../../../components/Tabs/Tabs.jsx';
import WebResourceSourcePanel from '../../../../components/WebResourceSoucePanel/WebResourceSourcePanel.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/** @type {React.FC<{ webResource: WebResourceMeta | null }>} */
const WebResourcePanel = ({ webResource }) => {
  const analyticsContext = useAnalyticsContext();

  const onTabSelected = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'RESOURCE_DETAILS', 'TAB_SELECTED'), { value });
    },
    [analyticsContext]
  );

  const content = webResource ? (
    <Tabs onTabSelected={onTabSelected}>
      <Tab label="Summary">
        <WebResourceInfo webResource={webResource} />
      </Tab>
      <Tab label="Dependency Tree">
        <DependencyTree webResource={webResource} />
      </Tab>
      <Tab label="Source">
        {/* eslint-disable-next-line react/no-unescaped-entities */}
        <ErrorWrapper fallback={<p>We can't display the source code for this resource</p>}>
          <WebResourceSourcePanel webResource={webResource} />
        </ErrorWrapper>
      </Tab>
    </Tabs>
  ) : (
    <PanelHint>Click on the table row item to display more information about the web-resource.</PanelHint>
  );

  return <PanelWrapper className={styles.WebResourcePanel}>{content}</PanelWrapper>;
};

export default memo(WebResourcePanel);
