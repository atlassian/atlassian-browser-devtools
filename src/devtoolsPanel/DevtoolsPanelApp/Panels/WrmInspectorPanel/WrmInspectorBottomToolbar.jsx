import { memo, useMemo } from 'react';
import { useSelector } from 'react-redux';

import BottomToolbar from '../../../components/Toolbar/BottomToolbar.jsx';
import ToolbarDivider from '../../../components/ToolbarDivider/ToolbarDivider.jsx';
import ToolbarItem from '../../../components/ToolbarItem/ToolbarItem.jsx';
import { formatSize } from '../../../helpers/formatSize.js';
import { selectors as wrmSelectors } from '../../../stores/wrmInspectorStore.js';

/** @typedef {import('react')} React */
/** @typedef {import('../../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/**
 * @param {WebResourceMeta[]} resources
 * @returns {number}
 */
const getResourcesSize = (resources) =>
  resources.reduce((sum, resource) => {
    return sum + resource.code.length;
  }, 0);

/** @type {React.FC<{ webResources: WebResourceMeta[]; filteredWebResources: WebResourceMeta[] }>} */
const WrmInspectorBottomToolbar = ({ webResources, filteredWebResources }) => {
  const webResourcesSize = useSelector(wrmSelectors.getWebResourcesSize);

  const webResourcesCounter = useMemo(() => webResources.length, [webResources]);
  const filteredWebResourcesCounter = useMemo(() => filteredWebResources.length, [filteredWebResources]);
  const filteredWebResourcesSize = useMemo(() => getResourcesSize(filteredWebResources), [filteredWebResources]);

  const isFiltered = webResources.length !== filteredWebResources.length;

  return (
    <BottomToolbar>
      <ToolbarItem>
        {isFiltered ? `${filteredWebResourcesCounter} / ${webResourcesCounter} resources` : `${webResourcesCounter} resources`}
      </ToolbarItem>
      <ToolbarDivider />
      <ToolbarItem>
        {isFiltered
          ? `${formatSize(filteredWebResourcesSize)} / ${formatSize(webResourcesSize)} size`
          : `${formatSize(webResourcesSize)} size`}
      </ToolbarItem>
    </BottomToolbar>
  );
};

export default memo(WrmInspectorBottomToolbar);
