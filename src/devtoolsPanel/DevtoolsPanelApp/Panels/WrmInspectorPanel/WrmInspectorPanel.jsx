import { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { unstable_batchedUpdates as batchedUpdates } from 'react-dom';
import { useDispatch } from 'react-redux';

import NoFilteredWebResourcesHint from './NoFilteredWebResourcesHint.jsx';
import NoResourcesHint from './NoResourcesHint.jsx';
import WebResourcePanel from './WebResourcePanel/WebResourcePanel.jsx';
import WrmInspectorBottomToolbar from './WrmInspectorBottomToolbar.jsx';
import WrmInspectorToolbar from './WrmInspectorToolbar.jsx';
import { getEventName, trackEvent } from '../../../../common/analytics/analytics.js';
import { useAnalyticsContext } from '../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import PanelWrapper from '../../../components/PanelWrapper/PanelWrapper.jsx';
import SplitPanel, { PANEL_MIN_HEIGHT } from '../../../components/SplitPanel/SplitPanel.jsx';
import WebResourceRow from '../../../components/WebResourcesTable/WebResourceRow.jsx';
import WebResourcesTable from '../../../components/WebResourcesTable/WebResourcesTable.jsx';
import { getFilters } from '../../../helpers/webResourcesFilters/webResourcesFilters.js';
import { filterWebResources, getWebResourceFilteringOptions } from '../../../helpers/webResourcesTableUtils.js';
import { actions as wrmActions } from '../../../stores/wrmInspectorStore.js';
import { useGetPlugins } from '../../Plugins/PluginsProvider.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../../helpers/webResourcesFilters/webResourcesFilters').FilterFn} FilterFn */
/** @typedef {import('../../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

const panelMinSizes = [200, PANEL_MIN_HEIGHT];
const panelSizes = [80, 20];

/** @type {React.FC<{ webResources: WebResourceMeta[] }>} */
const WrmInspectorPanel = ({ webResources }) => {
  const dispatch = useDispatch();
  const [showBigResources, setShowBigResources] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));
  const [hideCorePlugins, setHideCorePlugins] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));
  const [filterValue, setFilterValue] = /** @type {[string | null, (arg0: string | null) => void]} */ (useState(null));
  // @ts-expect-error FIXME: We have some issues with state setter here I guess...
  const [filters, setFilters] = /** @type {[FilterFn[], (arg0: FilterFn[]) => FilterFn[]]} */ (useState([]));
  const [resourcesFilter, setResourcesFilter] = useState(null);
  const [webResource, setWebResource] = /** @type {[WebResourceMeta | null, (arg0: WebResourceMeta | null) => void]} */ (useState(null));
  const plugins = useGetPlugins();
  const analyticsContext = useAnalyticsContext();

  const onResourceSelected = useCallback(
    (resource) => {
      trackEvent(getEventName(analyticsContext, 'RESOURCE_SELECTED'));

      setWebResource(resource);
    },
    [analyticsContext]
  );

  const webResourcesFilteringOptions = useMemo(() => getWebResourceFilteringOptions(webResources), [webResources]);

  const clearWebResources = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'CLEAR_WEB_RESOURCES'));

    batchedUpdates(() => {
      setFilterValue(null);
      setShowBigResources(false);
      setHideCorePlugins(false);
      setResourcesFilter(null);
      setFilters([]);
      dispatch(wrmActions.clearResources());
    });
  }, [analyticsContext, dispatch]);

  const clearFilters = useCallback(() => {
    batchedUpdates(() => {
      setFilterValue(null);
      setShowBigResources(false);
      setHideCorePlugins(false);
      setResourcesFilter(null);
      setFilters([]);
    });
  }, []);

  const filteredWebResources = useMemo(
    () =>
      filterWebResources(webResources, plugins, {
        filters,
        resourcesFilter,
        showBigResources,
        hideCorePlugins,
      }),
    [webResources, plugins, filters, resourcesFilter, showBigResources, hideCorePlugins]
  );

  // Clean-up selected resource when we remove the current resources
  useEffect(
    function cleanUpSelectedResource() {
      if (!webResources) {
        setWebResource(null);
      }
    },
    [webResources]
  );

  const toggleShowBigResources = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'TOGGLE_SHOW_BIG_RESOURCES'), { value });
      setShowBigResources(value);
    },
    [analyticsContext]
  );

  const toggleHideCorePlugins = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'TOGGLE_HIDE_CORE_PLUGINS'), { value });

      setHideCorePlugins(value);
    },
    [analyticsContext]
  );

  const onFilterChange = useCallback(
    (rawValue) => {
      trackEvent(getEventName(analyticsContext, 'FILTER_CHANGED'), { value: rawValue });

      batchedUpdates(() => {
        setFilterValue(rawValue);
        setFilters(getFilters(rawValue));
      });
    },
    [analyticsContext]
  );

  const onResourceFilteringOption = useCallback(
    (value) => {
      trackEvent(getEventName(analyticsContext, 'RESOURCE_FILTERING_OPTION_SELECTED'), { value });

      setResourcesFilter(value);
    },
    [analyticsContext]
  );

  const noResourcesHint = <NoResourcesHint />;
  const noFilteredWebResourcesHint = <NoFilteredWebResourcesHint clearFilters={clearFilters} />;

  const hasResources = Array.isArray(webResources) && webResources.length > 0;
  const hasFilteredWebResources = Array.isArray(filteredWebResources) && filteredWebResources.length > 0;

  return (
    <SplitPanel direction="vertical" minSizes={panelMinSizes} sizes={panelSizes}>
      <PanelWrapper>
        <WrmInspectorToolbar
          clearWebResources={clearWebResources}
          filterValue={filterValue}
          hideCorePlugins={hideCorePlugins}
          showBigResources={showBigResources}
          toggleHideCorePlugins={toggleHideCorePlugins}
          toggleShowBigResources={toggleShowBigResources}
          webResourcesFilteringOptions={webResourcesFilteringOptions}
          webResourcesFilteringValue={resourcesFilter}
          onFilterChange={onFilterChange}
          onResourceFilteringOption={onResourceFilteringOption}
        />

        {hasResources ? (
          hasFilteredWebResources ? (
            <WebResourcesTable RowComponent={WebResourceRow} webResources={filteredWebResources} onSelected={onResourceSelected} />
          ) : (
            noFilteredWebResourcesHint
          )
        ) : (
          noResourcesHint
        )}

        <WrmInspectorBottomToolbar filteredWebResources={filteredWebResources} webResources={webResources} />
      </PanelWrapper>

      <WebResourcePanel webResource={webResource} />
    </SplitPanel>
  );
};

export default memo(WrmInspectorPanel);
