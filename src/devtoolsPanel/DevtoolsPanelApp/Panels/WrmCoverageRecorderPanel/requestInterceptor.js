// @ts-nocheck
/* eslint-disable */
import { chromeDevtoolsProtocolLogger as logger } from '../../../../common/logger.js';
import { captureException, withSentryWrapper } from '../../../../common/sentry/sentry.js';
import { sendCommand } from './debuggerApi.js';
import { decorateScript } from './wrmBatch.js';

const contextBatchUrlRegExp = /_\/download\/contextbatch\/js\//;

const safeBase64Encode = (input) => btoa(unescape(encodeURIComponent(input)));

/** @param request */
function isWrmJsRequest(request) {
  const { url, method } = request;

  const isWrmUrl = url.match(contextBatchUrlRegExp);

  return Boolean(method.toLowerCase() === 'get' && isWrmUrl);
}

/**
 * @param {Headers} headers
 * @returns {{ name: string; value: string }[]}
 */
const mapResponseHeaders = (headers) => {
  const mappedHeaders = Array.from(headers.entries()).map(([name, value]) => ({ name, value }));

  return [
    ...mappedHeaders,
    {
      name: 'X-ASD',
      value: 'ASD',
    },
  ];
};

/**
 * @param debuggee
 * @param params
 */
async function respondWithDecoratedRequest(debuggee, params) {
  const { request } = params;

  try {
    const response = await fetch(request.url, {
      method: request.method,
      headers: request.headers,
      redirect: 'follow',
      referrerPolicy: request.referrerPolicy,
    });

    if (!response.ok) {
      throw new Error(`Error [${response.status.toString()}]: "${response.statusText}"`);
    }

    const responseBody = await response.text();
    const decoratedBody = decorateScript(request.url, responseBody);

    logger('Responding with a decorated WRM response', {
      url: request.url,
    });

    // https://chromedevtools.github.io/devtools-protocol/tot/Fetch/#method-fulfillRequest
    const responseParams = {
      requestId: params.requestId,
      responseCode: response.status,
      responseHeaders: mapResponseHeaders(response.headers),
      body: safeBase64Encode(decoratedBody), // Encoded as a base64 string when passed over JSON
    };

    await sendCommand(debuggee, 'Fetch.fulfillRequest', responseParams);
  } catch (error) {
    captureException(error);

    // Fallback to a regular response
    logger('Request error', error);

    await continueFetchRequest(debuggee, params);
  }
}

/**
 * @param debuggee
 * @param params
 */
async function continueFetchRequest(debuggee, params) {
  const continueRequestParams = {
    requestId: params.requestId,
  };

  logger('Continuing with a regular response', { url: params.request.url });
  await sendCommand(debuggee, 'Fetch.continueRequest', continueRequestParams);
}

// https://chromedevtools.github.io/devtools-protocol/tot/Fetch/#event-requestPaused
/**
 * @param debuggee
 * @param params
 */
async function handleRequest(debuggee, params) {
  const { request } = params;

  // Intercept the request
  if (isWrmJsRequest(request)) {
    await respondWithDecoratedRequest(debuggee, params);
  } else {
    await continueFetchRequest(debuggee, params);
  }
}

/** @param debuggee */
export async function enableRequestInterceptor(debuggee) {
  await sendCommand(debuggee, 'Fetch.enable', {
    patterns: [
      {
        urlPattern: '*', // TODO: Use only the WRM.contextPath here
        resourceType: 'Script', // https://chromedevtools.github.io/devtools-protocol/tot/Network/#type-ResourceType
      },
    ],
  });

  chrome.debugger.onEvent.addListener(
    withSentryWrapper(async (source, method, params) => {
      switch (method) {
        case 'Fetch.requestPaused':
          await handleRequest(source, params);
          break;
      }
    })
  );
}
