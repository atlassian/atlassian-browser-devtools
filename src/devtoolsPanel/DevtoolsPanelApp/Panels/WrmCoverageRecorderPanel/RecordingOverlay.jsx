import { memo } from 'react';

import Dialog from '../../../components/Dialog/Dialog.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const RecordingOverlay = () => (
  <Dialog showCloseButton={false}>
    <p>Recording WRM Coverage. Please wait...</p>
    <progress style={{ width: '100%' }} />
  </Dialog>
);

export default memo(RecordingOverlay);
