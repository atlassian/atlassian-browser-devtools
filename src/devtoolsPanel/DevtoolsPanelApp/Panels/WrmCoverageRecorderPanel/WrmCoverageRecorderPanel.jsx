import { Fragment, memo, useCallback, useEffect, useMemo, useState } from 'react';

import { useCoverage } from './CoverageProvider.jsx';
import ErrorOverlay from './ErrorOverlay.jsx';
import ExportCoverageButton from './ExportCoverageButton.jsx';
import NoWebResourcesInRecordedCoverageHint from './NoWebResourceInRecordedCoverageHint.jsx';
import RecordCoverageButton from './RecordCoverageButton.jsx';
import RecordCoverageConfirmation from './RecordCoverageConfirmation.jsx';
import RecordCoverageHint from './RecordCoverageHint.jsx';
import RecordingOverlay from './RecordingOverlay.jsx';
import { getEventName, trackEvent } from '../../../../common/analytics/analytics.js';
import { useAnalyticsContext } from '../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import WrmCoverageBrowser from '../../../components/WrmCoverageBrowser/WrmCoverageBrowser.jsx';
import { useGetPlugins } from '../../Plugins/PluginsProvider.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const WrmCoverageRecorderPanel = () => {
  const analyticsContext = useAnalyticsContext();
  const { isFeatureEnabled, coverage, clearCoverage, isRecording, recordCoverage, hasError } = useCoverage();
  const plugins = useGetPlugins();

  const [showRecordingConfirmation, setShowRecordingConfirmation] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));
  const [showError, setShowError] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));

  const showErrorOverlay = useMemo(() => {
    return hasError && showError;
  }, [hasError, showError]);

  const dismissError = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'COVERAGE', 'DISMISS_ERROR'));

    setShowError(false);
  }, [analyticsContext]);

  useEffect(() => {
    if (hasError) {
      trackEvent(getEventName(analyticsContext, 'COVERAGE', 'SHOW_ERROR'));

      setShowError(true);
    }
  }, [analyticsContext, hasError]);

  const openRecordingConfirmation = useCallback(() => {
    if (isRecording) {
      return;
    }

    trackEvent(getEventName(analyticsContext, 'COVERAGE', 'OPEN_RECORDING_CONFIRMATION'));

    setShowRecordingConfirmation(true);
  }, [analyticsContext, isRecording]);

  const onRecordingConfirmed = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'COVERAGE', 'RECORD_COVERAGE'));

    setShowRecordingConfirmation(false);
    recordCoverage();
  }, [analyticsContext, recordCoverage]);

  const onRecordingDeclined = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'COVERAGE', 'DECLINE_RECORDING'));

    setShowRecordingConfirmation(false);
  }, [analyticsContext]);

  const recordCoverageButtons = (
    <Fragment>
      <RecordCoverageButton isRecordingCoverage={isRecording} onStartRecordingCoverage={openRecordingConfirmation} />
      <ExportCoverageButton coverage={coverage} />
    </Fragment>
  );

  const recordCoverageHint = <RecordCoverageHint isRecordingCoverage={isRecording} recordCoverage={openRecordingConfirmation} />;
  const noWebResourcesInCoverageHint = <NoWebResourcesInRecordedCoverageHint clearCoverage={clearCoverage} />;
  const disableToolbar = isRecording || !coverage; // Disable toolbar when recording or no coverage is available

  return (
    <>
      <WrmCoverageBrowser
        clearCoverage={clearCoverage}
        coverage={coverage}
        disableToolbar={disableToolbar}
        noCoverageHint={recordCoverageHint}
        noWebResourcesInCoverageHint={noWebResourcesInCoverageHint}
        plugins={plugins}
        prependToolbarButtons={recordCoverageButtons}
      />

      {isRecording && <RecordingOverlay />}
      {showErrorOverlay && <ErrorOverlay dismiss={dismissError} />}

      {showRecordingConfirmation && (
        <RecordCoverageConfirmation
          coverage={coverage}
          isFeatureEnabled={isFeatureEnabled}
          onRecordingConfirmed={onRecordingConfirmed}
          onRecordingDeclined={onRecordingDeclined}
        />
      )}
    </>
  );
};

export default memo(WrmCoverageRecorderPanel);
