import { memo } from 'react';

import ConfirmationDialog from '../../../components/ConfirmationDialog/ConfirmationDialog.jsx';
import Dialog from '../../../components/Dialog/Dialog.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

/**
 * @typedef {Object} Props
 * @property {boolean} isFeatureEnabled
 * @property {CoverageJson} [coverage]
 * @property {() => void} onRecordingConfirmed
 * @property {() => void} onRecordingDeclined
 */

/** @type {React.FC<Props>} */
const RecordCoverageConfirmation = ({ isFeatureEnabled, coverage, onRecordingConfirmed, onRecordingDeclined }) => {
  const hasCoverage = Boolean(coverage);

  if (!isFeatureEnabled) {
    return (
      <Dialog onClose={onRecordingDeclined}>
        <p>Your browser does not yet support this feature.</p>
        <p>You can use Chrome or another Chromium-based browser like e.g, Edge to record the WRM Coverage.</p>
      </Dialog>
    );
  }

  return (
    <ConfirmationDialog onConfirm={onRecordingConfirmed} onDecline={onRecordingDeclined}>
      <p>Recording the coverage usage requires reloading the page.</p>
      {hasCoverage && <p>Recording new coverage will remove the previous recording.</p>}
      <p>
        You will see the <strong>&apos;Atlassian Server Devtools&apos; stated debugging this browser</strong> notification on top of the
        browser once the coverage recording will start.
      </p>
      <p>Please confirm if you want to reload the page now.</p>
    </ConfirmationDialog>
  );
};

export default memo(RecordCoverageConfirmation);
