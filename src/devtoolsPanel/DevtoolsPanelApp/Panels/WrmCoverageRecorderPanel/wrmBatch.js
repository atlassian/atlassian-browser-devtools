import { processScript } from '../../../services/webResources/webResourcesProcessor.js';

/** @typedef {import('../../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

let resourceUid = 0;

const TIME_START_VAR = '____timeStart';
const DELTA_VAR = '____delta';
const GLOBAL_PERF_VAR = '____asd_perf';

/**
 * @param {WebResourceMeta} webResource
 * @returns {string}
 */
function getTimerTemplate(webResource) {
  const localId = resourceUid++;

  return `
  const ${TIME_START_VAR}${localId} = performance.now();

  ${webResource.code};

  const ${DELTA_VAR}${localId} = performance.now() - ${TIME_START_VAR}${localId};
  window.${GLOBAL_PERF_VAR} = window.${GLOBAL_PERF_VAR} || {};

  window.${GLOBAL_PERF_VAR}['${webResource.wrmKey}'] = ${DELTA_VAR}${localId};`;
}

/**
 * @param {string} url
 * @param {string} code
 * @returns {string}
 */
export function decorateScript(url, code) {
  const webResources = processScript({ url, code });

  // Wrap all webResources with timers
  return webResources
    .map((webResource) => {
      const wrmCommentPrefix = `/* module-key = '${webResource.wrmKey}', location = '${webResource.wrmFile}' */`;

      return `${wrmCommentPrefix}\n${getTimerTemplate(webResource)}`;
    })
    .join('\n');
}
