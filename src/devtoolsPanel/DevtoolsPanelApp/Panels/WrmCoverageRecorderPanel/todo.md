# Collecting WRM Coverage

## Instrumentation

### User can manually cancel the browser instrumentation so the coverage should handle that.

- [x] Check `chrome.debugger` API and handle the cancellation of debugging

### Running coverage will reload the page we should confirm that with the user before reloading the page

- [x] Show the modal dialog with a confirmation question e.g. "We will have to reload the page to collect the coverage. <Confirm> <Cancel>"

### Stopping coverage collection

We need to instrument Chrome when to stop collecting the coverage. Currently, we listen to the `PAGE_LOADED_EVENT` event that is triggered twice. On top of that, the event can be triggered while the page is still loading.

- [ ] Consider, adding "stop collecting coverage" button
- [ ] Stop collecting coverage when the page is fully loaded. What is the best way to terminate that e.g. `DOMContentLoaded` etc. or maybe some custom event?

### Coverage can fail

Collecting coverage can fail, we should handle that:

- [x] Handle errors while collecting the coverage
- [x] Disconnect the attached debugger in case of the error

## UI

Improve the toolbar and UI

- [x] The "record coverage" button should be merged with existing Coverage toolbar
- [x] Show a waiter or the progress bar while collecting the coverage
- [ ] Add don't show that again to the confirmation dialog

## Firefox

- [x] check if we access and run the debugger APIs on Firefox
  - No, we can't
