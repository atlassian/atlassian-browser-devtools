import { createContext, memo, useCallback, useContext, useMemo, useState } from 'react';
import { unstable_batchedUpdates as batchedUpdates } from 'react-dom';

import { collectCoverageFromTarget } from './coverageCollector.js';
import { isDetachedError, runDebuggerCommandWithErrorHandler } from './debuggerUtils.js';
import { devtoolsPanelLogger as logger } from '../../../../common/logger.js';
import { captureException } from '../../../../common/sentry/sentry.js';
import { IS_DEBUGGER_API_SUPPORTED } from '../../../features/features.js';

/** @typedef {import('react')} React */
/** @typedef {import('./debuggerApi').Debuggee} Debuggee */
/** @typedef {import('../../Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

/**
 * @typedef {Object} CoverageProviderContext
 * @property {boolean} isFeatureEnabled
 * @property {CoverageJson} coverage
 * @property {() => void} clearCoverage
 * @property {() => Promise<void>} recordCoverage
 * @property {boolean} isRecording
 * @property {boolean} hasError
 */

const coverageContext = /** @type {React.Context<CoverageProviderContext | null>} */ (createContext(null));

const { Provider } = coverageContext;

const isFeatureEnabled = IS_DEBUGGER_API_SUPPORTED;

/** @type {React.FC<{ children: React.ReactNode }>} */
const CoverageProvider = ({ children }) => {
  const [coverage, setCoverage] = /** @type {[CoverageJson | null, (arg0: CoverageJson | null) => void]} */ (useState(null));
  const [isRecording, setIsRecording] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));
  const [hasError, setHasError] = /** @type {[boolean, (arg0: boolean) => void]} */ (useState(false));

  const clearCoverage = useCallback(() => {
    setCoverage(null);
  }, []);

  const recordCoverage = useCallback(async () => {
    if (!isFeatureEnabled) {
      return;
    }

    if (isRecording) {
      return;
    }

    batchedUpdates(() => {
      setCoverage(null);
      setHasError(false);
      setIsRecording(true);
    });

    try {
      const myTabId = chrome.devtools.inspectedWindow.tabId;
      const debuggee = /** @type {Debuggee} */ { tabId: myTabId };

      const collectedCoverage = /** @type {CoverageJson} */ (await runDebuggerCommandWithErrorHandler(debuggee, collectCoverageFromTarget));

      batchedUpdates(() => {
        setCoverage(collectedCoverage);
        setIsRecording(false);
      });
    } catch (err) {
      const error = /** @type {AggregateError} */ (err);

      captureException(error);
      let wasDebuggerDetached = isDetachedError(error);

      // Handle errors and show error dialog
      logger('Failed recording coverage', {
        reason: error,
        wasDebuggerDetached,
      });

      // TODO: ABD-XXX Add error reporting

      batchedUpdates(() => {
        setHasError(true);
        setIsRecording(false);
      });
    }
  }, [isRecording]);

  // TODO: We could probably remove type casting here once we have other types
  const feature = /** @type {CoverageProviderContext} */ (
    useMemo(
      () => ({
        coverage,
        clearCoverage,
        recordCoverage,
        isRecording,
        hasError,
        isFeatureEnabled,
      }),
      [clearCoverage, coverage, hasError, isRecording, recordCoverage]
    )
  );

  return <Provider value={feature}>{children}</Provider>;
};

export default memo(CoverageProvider);

/**
 * @returns {CoverageProviderContext}
 */
export const useCoverage = () => {
  const feature = useContext(coverageContext);

  if (feature === null) {
    throw new Error(`You can only use "${useCoverage.name}" within the scope of the "CoverageProvider" component`);
  }

  return feature;
};
