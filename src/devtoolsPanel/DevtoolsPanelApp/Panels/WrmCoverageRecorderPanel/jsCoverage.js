import { convertToDisjointRanges } from './coverage.js';
import { sendCommand } from './debuggerApi.js';
import { captureException } from '../../../../common/sentry/sentry.js';

/** @typedef {import('./debuggerApi').Debuggee} Debuggee */
/** @typedef {import('./debuggerApi').DebuggerGetScriptSourceResponse} DebuggerGetScriptSourceResponse */
/** @typedef {import('./debuggerApi').ProfilerScriptCoverage} ProfilerScriptCoverage */

/**
 * @typedef {Object} CoverageJsonRange
 * @property {number} start
 * @property {number} end
 */

/**
 * @typedef {Object} CoverageJsonEntry
 * @property {string} url
 * @property {CoverageJsonRange[]} ranges
 * @property {string | null} [text]
 */

/** @typedef {CoverageJsonEntry[]} CoverageJson */

/**
 * @param {Debuggee} debuggee
 * @param {ProfilerScriptCoverage[]} scriptCoverage
 * @returns {Promise<CoverageJson>}
 */
export async function convertJsCoverageToJsonFormat(debuggee, scriptCoverage) {
  const coverage = /** @type {CoverageJsonEntry[]} */ [];

  for (const entry of scriptCoverage) {
    const { url, functions, scriptId } = entry;

    // Skip empty urls and chrome extensions
    if (url === '' || url.startsWith('chrome-extension://')) {
      continue;
    }

    // Retrieve the script from debugger
    let text;
    try {
      const { scriptSource } = /** @type {DebuggerGetScriptSourceResponse} */ (
        await sendCommand(debuggee, 'Debugger.getScriptSource', {
          scriptId,
        })
      );

      text = scriptSource;
    } catch (err) {
      const error = /** @type {Error} */ (err);

      captureException(error);

      text = null;
    }

    if (!text || !url) {
      continue;
    }

    // Combine, flatten, and covert the ranges
    const flattenRanges = functions.flatMap((func) => func.ranges);
    const ranges = convertToDisjointRanges(flattenRanges);

    coverage.push(
      /** @type {CoverageJsonEntry} */ ({
        url,
        ranges,
        text,
      })
    );
  }

  return coverage;
}
