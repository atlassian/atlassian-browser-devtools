import { memo } from 'react';

import HintButton from '../../../components/PanelHint/HintButton.jsx';
import PanelHint from '../../../components/PanelHint/PanelHint.jsx';
import { RecordIcon } from '../../../components/ToolbarIconButton/ToolbarIconButton.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ isRecordingCoverage: boolean; recordCoverage: () => void }>} */
const RecordCoverageHint = ({ isRecordingCoverage, recordCoverage }) => {
  if (isRecordingCoverage) {
    // FIXME: Check if we can return null instead of a div here
    return <PanelHint>{null}</PanelHint>;
  }

  return (
    <PanelHint>
      <p>
        Click the <HintButton icon={RecordIcon} title="Record coverage" onClick={recordCoverage} /> icon to start recording WRM Coverage.
      </p>
    </PanelHint>
  );
};

export default memo(RecordCoverageHint);
