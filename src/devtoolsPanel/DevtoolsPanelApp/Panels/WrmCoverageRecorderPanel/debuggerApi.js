import { PAGE_LOADED_EVENT } from '../../../../common/events/events.js';
import { chromeDevtoolsProtocolLogger as logger } from '../../../../common/logger.js';
import { withSentryWrapper } from '../../../../common/sentry/sentry.js';
import { addMessageListener, removeMessageListener } from '../../../portConnection.js';

/**
 * @template T
 * @typedef {import('../../../portConnection.js').MessageListener<T>} MessageListener
 */
/** @typedef {import('../../../../common/events/events.js').PageLoadedEventPayload} PageLoadedEventPayload */

/**
 * @typedef {Object} Debuggee
 * @property {number} [tabId]
 * @property {string} [extensionId]
 * @property {string} [targetId]
 */

/**
 * @param {Debuggee} target
 * @param {string} protocol
 * @returns {Promise<void>}
 */
export async function attachDebugger(target, protocol = '1.3') {
  logger('Attaching debugger...');

  return new Promise((resolve, reject) => {
    chrome.debugger.attach(target, protocol, () => {
      if (chrome.runtime.lastError) {
        reject(chrome.runtime.lastError.message);
      }

      logger('Debugger attached');

      resolve();
    });
  });
}

/**
 * @param {Debuggee} target
 * @returns {Promise<void>}
 */
export async function detachDebugger(target) {
  return new Promise((resolve, reject) => {
    chrome.debugger.detach(target, () => {
      if (chrome.runtime.lastError) {
        reject(chrome.runtime.lastError.message);
      }

      logger('Debugger detached');

      resolve();
    });
  });
}

/**
 * @typedef {Object} DebuggerGetScriptSourceResponse - Returns source for the script with given id.
 * @property {string} scriptSource - Script source (empty in case of Wasm bytecode).
 * @property {string} bytecode - Wasm bytecode. (Encoded as a base64 string when passed over JSON)
 * @see https://chromedevtools.github.io/devtools-protocol/tot/Debugger/#method-getScriptSource
 */

/**
 * @typedef {Object} ProfilerCoverageRange - Coverage data for a source range.
 * @property {number} startOffset - JavaScript script source offset for the range start.
 * @property {number} endOffset - JavaScript script source offset for the range end.
 * @property {number} count - Collected execution count of the source range.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/Profiler/#type-CoverageRange
 */

/**
 * @typedef {Object} ProfilerFunctionCoverage - Coverage data for a JavaScript function.
 * @property {string} functionName - JavaScript function name.
 * @property {ProfilerCoverageRange[]} ranges - Source ranges inside the function with coverage data.
 * @property {boolean} isBlockCoverage - Whether coverage data for this function has block granularity.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/Profiler/#type-FunctionCoverage
 */

/**
 * @typedef {Object} ProfilerScriptCoverage - Coverage data for a JavaScript script.
 * @property {string} scriptId - JavaScript script id.
 * @property {string} url - JavaScript script name or url.
 * @property {ProfilerFunctionCoverage[]} functions - Functions contained in the script that has coverage data.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/Profiler/#type-ScriptCoverage
 */

/**
 * @typedef {Object} ProfilerTakePreciseCoverageResponse - Collect coverage data for the current isolate, and resets execution counters. Precise code coverage needs to have started.
 * @property {ProfilerScriptCoverage[]} result - Coverage data for the current isolate.
 * @property {number} timestamp - Monotonically increasing time (in seconds) when the coverage update was taken in the backend.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/Profiler/#method-takePreciseCoverage
 */

/**
 * @typedef {Object} CssRuleUsage - CSS coverage information.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/CSS/#type-RuleUsage
 * @property {string} styleSheetId - The css style sheet identifier (absent for user agent stylesheet and user-specified stylesheet rules) this rule came from.
 * @property {number} startOffset - Offset of the start of the rule (including selector) from the beginning of the stylesheet.
 * @property {number} endOffset - Offset of the end of the rule body from the beginning of the stylesheet.
 * @property {boolean} used - Indicates whether the rule was actually used by some element in the page.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/CSS/#type-RuleUsage
 */

/**
 * @typedef {Object} CssStopRuleUsageTrackingResponse - Stop tracking rule usage and return the list of rules that were used since last call to takeCoverageDelta (or since start of coverage instrumentation)
 * @property {CssRuleUsage[]} ruleUsage - Coverage data for the CSS rule collection.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/CSS/#method-stopRuleUsageTracking
 */

/**
 * @typedef {Object} CssGetStyleSheetTextResponse - Returns the current textual content for a stylesheet.
 * @property {string} text - The stylesheet text.
 * @see https://chromedevtools.github.io/devtools-protocol/tot/CSS/#method-getStyleSheetText
 */

/**
 * @template {unknown} T
 * @param {Debuggee} target
 * @param {string} command
 * @param {Object} [params]
 * @returns {Promise<T>}
 */
export async function sendCommand(target, command, params) {
  logger(`Sending command "${command}"...`, params);

  return new Promise((resolve, reject) => {
    chrome.debugger.sendCommand(target, command, params, (rawResult) => {
      const result = /** @type {T} */ (rawResult);

      if (chrome.runtime.lastError) {
        logger(`Sending command "${command}" failed`, {
          error: chrome.runtime.lastError.message,
        });
        reject(chrome.runtime.lastError.message);
        return;
      }

      logger(`Command "${command}" was delivered`, result);

      resolve(result);
    });
  });
}

/**
 * @param {Debuggee} target
 * @param {string} command
 * @param {(arg0?: Object) => void} handler
 * @returns {() => void}
 */
export function addEventHandler(target, command, handler) {
  /**
   * @param {Debuggee} source
   * @param {string} method
   * @param {Object} [params]
   * @returns {void}
   */
  const listener = withSentryWrapper((source, method, params) => {
    if (source.tabId !== target.tabId) {
      return;
    }

    if (method !== command) {
      return;
    }

    logger(`Calling the event handler for "${command}" command`, {
      command,
      target,
      handler,
      params,
    });

    handler(params);
  });

  chrome.debugger.onEvent.addListener(listener);

  return () => {
    chrome.debugger.onEvent.removeListener(listener);
  };
}

/**
 * @param {Debuggee} target
 * @param {(reason: string) => void} handler
 * @returns {() => void}
 */
export function addDetachEventHandler(target, handler) {
  /**
   * @param {Debuggee} source
   * @param {string} reason
   * @returns {void}
   */
  const listener = withSentryWrapper((source, reason) => {
    if (source.tabId !== target.tabId) {
      return;
    }

    logger(`onDetach event was triggered`, {
      target,
      handler,
      reason,
    });

    handler(reason);
  });

  chrome.debugger.onDetach.addListener(listener);

  return () => {
    chrome.debugger.onDetach.removeListener(listener);
  };
}

/**
 * @returns {Promise<void>}
 */
export async function waitFotPageToLoad() {
  logger('Loading page...');

  return new Promise((resolve) => {
    addMessageListener(
      PAGE_LOADED_EVENT,
      /** @type {MessageListener<PageLoadedEventPayload>} */ function pageLoadedHandler() {
        removeMessageListener(PAGE_LOADED_EVENT, pageLoadedHandler);

        // TODO: A silly approach for now
        setTimeout(() => {
          logger('Page was loaded');
          resolve();
        }, 2000);
      }
    );
  });
}
