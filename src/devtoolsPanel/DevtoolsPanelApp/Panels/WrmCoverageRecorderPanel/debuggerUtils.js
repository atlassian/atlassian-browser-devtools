import { addDetachEventHandler, attachDebugger, detachDebugger } from './debuggerApi.js';
import { captureException } from '../../../../common/sentry/sentry.js';

/** @typedef {import('./debuggerApi').Debuggee} Debuggee */

export class DebuggerDetachedError extends Error {}

/**
 * @template T
 * @template {(target: Debuggee) => Promise<T>} CommandT
 * @param {Debuggee} target
 * @param {CommandT} commandFn
 * @returns {Promise<T | Error>}
 */
export const runDebuggerCommandWithErrorHandler = async (target, commandFn) => {
  let isDebuggerAttached = false;

  /** @type {() => void} */
  let detachListenerCleanUp;

  const detachListenerPromise = new Promise((resolve, reject) => {
    detachListenerCleanUp = addDetachEventHandler(target, function handleDebuggingTermination(reason) {
      reject(new DebuggerDetachedError(reason));
    });
  });

  try {
    await attachDebugger(target);

    isDebuggerAttached = true;

    const coverageOrError = await Promise.race([
      commandFn(target),
      detachListenerPromise, // A detach listener as a promise
    ]);

    // @ts-expect-error Duh, TS we are checking if we can call this function...
    if (typeof detachListenerCleanUp === 'function') {
      detachListenerCleanUp();
    }

    await detachDebugger(target);

    isDebuggerAttached = false;

    return coverageOrError;
  } catch (err) {
    const error = /** @type {Error} */ (err);

    captureException(error);

    // Re-throw the error
    throw error;
  } finally {
    // Clean-up as much as we can
    // @ts-expect-error Duh, TS we are checking if we can call this function...
    if (typeof detachListenerCleanUp === 'function') {
      detachListenerCleanUp();
    }

    if (isDebuggerAttached) {
      try {
        await detachDebugger(target);
      } catch (err) {
        const error = /** @type {Error} */ (err);

        captureException(error);
      }
    }
  }
};

/**
 * @param {AggregateError | Error} error
 * @returns {boolean}
 */
export function isDetachedError(error) {
  let isDetachedErr = false;

  if (error instanceof AggregateError) {
    isDetachedErr = error.errors.some((innerError) => innerError instanceof DebuggerDetachedError);
  }

  return isDetachedErr;
}
