import { memo } from 'react';

import ToolbarIconButton, { RecordIcon, RecordingIcon } from '../../../components/ToolbarIconButton/ToolbarIconButton.jsx';

/** @typedef {import('react')} React */

/**
 * @typedef {Object} Props
 * @property {() => void} onStartRecordingCoverage
 * @property {boolean} isRecordingCoverage
 */

/** @type {React.FC<Props>} */
const RecordCoverageButton = ({ onStartRecordingCoverage, isRecordingCoverage }) => {
  const label = isRecordingCoverage ? 'Recording...' : 'Record coverage';

  return <ToolbarIconButton icon={isRecordingCoverage ? RecordingIcon : RecordIcon} title={label} onClick={onStartRecordingCoverage} />;
};

export default memo(RecordCoverageButton);
