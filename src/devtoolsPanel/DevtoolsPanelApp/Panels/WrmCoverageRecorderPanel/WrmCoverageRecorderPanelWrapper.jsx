import { memo } from 'react';

import CoverageProvider from './CoverageProvider.jsx';
import WrmCoverageRecorderPanel from './WrmCoverageRecorderPanel.jsx';
import { AnalyticsContextProvider } from '../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import PanelWrapper from '../../../components/PanelWrapper/PanelWrapper.jsx';
import PluginsProvider from '../../Plugins/PluginsProvider.jsx';

/** @typedef {import('react')} React */

const ANALYTICS_EVENTS_GROUP = 'WRM_COVERAGE_RECORDER_PANEL';

/** @type {React.FC} */
const WrmCoverageRecorderPanelWrapper = () => {
  return (
    <AnalyticsContextProvider analyticsContext={ANALYTICS_EVENTS_GROUP}>
      <CoverageProvider>
        <PluginsProvider>
          <PanelWrapper>
            <WrmCoverageRecorderPanel />
          </PanelWrapper>
        </PluginsProvider>
      </CoverageProvider>
    </AnalyticsContextProvider>
  );
};

export default memo(WrmCoverageRecorderPanelWrapper);
