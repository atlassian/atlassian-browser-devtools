import { memo } from 'react';

import Dialog from '../../../components/Dialog/Dialog.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ dismiss: () => void }>} */
const ErrorOverlay = ({ dismiss }) => (
  <Dialog onClose={dismiss}>
    <p>There was an error while recording and collecting WRM Coverage.</p>
    <p>Try again recording the coverage.</p>
  </Dialog>
);

export default memo(ErrorOverlay);
