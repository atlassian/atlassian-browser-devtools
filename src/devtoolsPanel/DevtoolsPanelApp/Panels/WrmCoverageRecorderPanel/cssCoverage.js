import { convertToDisjointRanges } from './coverage.js';
import { addEventHandler, sendCommand } from './debuggerApi.js';

/** @typedef {import('./debuggerApi').Debuggee} Debuggee */
/** @typedef {import('./debuggerApi').CssRuleUsage} CssRuleUsage */
/** @typedef {import('./debuggerApi').CssGetStyleSheetTextResponse} CssGetStyleSheetTextResponse */

/**
 * @typedef {Object} CoverageJsonEntry
 * @property {string} url
 * @property {{ start: number; end: number }[]} ranges
 * @property {string | null} [text]
 */

/** @typedef {CoverageJsonEntry[]} CoverageJson */

/**
 * Based on: https://github.com/puppeteer/puppeteer/blob/c65b10c5249d061b7955f66c624088932bbc5fe8/src/common/Coverage.ts#L363-L388
 *
 * @param {Debuggee} debuggee
 * @param {CssRuleUsage[]} cssRulesUsage
 * @param {Map<string, string>} styleSheetUrls
 * @returns {Promise<CoverageJson>}
 */
export async function convertCssCoverageToJsonFormat(debuggee, cssRulesUsage, styleSheetUrls) {
  // aggregate by styleSheetId
  const styleSheetIdToCoverage = new Map();

  for (const entry of cssRulesUsage) {
    let ranges = styleSheetIdToCoverage.get(entry.styleSheetId);

    if (!ranges) {
      ranges = [];
      styleSheetIdToCoverage.set(entry.styleSheetId, ranges);
    }

    ranges.push({
      startOffset: entry.startOffset,
      endOffset: entry.endOffset,
      count: entry.used ? 1 : 0,
    });
  }

  // Create final coverage map
  const coverage = /** @type {CoverageJson[]} */ [];

  for (const styleSheetId of styleSheetIdToCoverage.keys()) {
    const url = styleSheetUrls.has(styleSheetId) ? styleSheetUrls.get(styleSheetId) : '<<unknown>>';

    // Retrieve the Stylesheet from debugger
    let text;
    try {
      const { text: stylesheetText } = /** @type {CssGetStyleSheetTextResponse} */ (
        await sendCommand(debuggee, 'CSS.getStyleSheetText', {
          styleSheetId,
        })
      );

      text = stylesheetText;
    } catch (e) {
      text = null;
    }

    if (!url || !text) {
      continue;
    }

    const ranges = convertToDisjointRanges(styleSheetIdToCoverage.get(styleSheetId) || []);

    coverage.push({ url, ranges, text });
  }

  return coverage;
}

/**
 * @param {Debuggee} target
 * @param {Map<string, string>} styleSheetUrls
 * @returns {() => void}
 */
export function addInsertStyleSheetListener(target, styleSheetUrls) {
  return addEventHandler(target, 'CSS.styleSheetAdded', ({ header }) => {
    const { styleSheetId, sourceURL } = header;

    styleSheetUrls.set(styleSheetId, sourceURL);
  });
}
