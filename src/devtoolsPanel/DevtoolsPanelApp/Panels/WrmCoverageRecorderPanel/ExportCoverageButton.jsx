import snakeCase from 'lodash/snakeCase';
import { memo, useCallback } from 'react';

import { getEventName, trackEvent } from '../../../../common/analytics/analytics.js';
import { devtoolsPanelLogger as logger } from '../../../../common/logger.js';
import { captureException } from '../../../../common/sentry/sentry.js';
import { useAnalyticsContext } from '../../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import ToolbarIconButton, { ExportIcon } from '../../../components/ToolbarIconButton/ToolbarIconButton.jsx';

/** @typedef {import('react')} React */
/** @typedef {import('../../Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

/**
 * @typedef {Object} Props
 * @property {() => void} onStartRecordingCoverage
 * @property {boolean} isRecordingCoverage
 */

/** @type {React.FC<{ coverage: CoverageJson }>} */
const ExportCoverageButton = ({ coverage }) => {
  const analyticsContext = useAnalyticsContext();

  const onExportCoverage = useCallback(() => {
    logger('Exporting coverage as JSON file');
    trackEvent(getEventName(analyticsContext, 'COVERAGE', 'EXPORT_COVERAGE'));

    try {
      const fileName = `coverage-${snakeCase(new Date().toLocaleString())}.json`;

      // Create blog and url
      const json = JSON.stringify(coverage, null, 2);
      const blob = new Blob([json], { type: 'octet/stream' });
      const url = window.URL.createObjectURL(blob);

      // Force download and save file
      const anchor = document.createElement('a');
      anchor.style.display = 'none';
      anchor.href = url;
      anchor.download = fileName;
      anchor.click();

      anchor.remove();

      window.URL.revokeObjectURL(url);
    } catch (err) {
      const error = /** @type {Error} */ (err);

      captureException(error);

      logger('Error while exporting coverage: ', err);

      trackEvent(getEventName(analyticsContext, 'COVERAGE', 'EXPORT_COVERAGE_ERROR'));
    }
  }, [analyticsContext, coverage]);

  const hasCoverage = Boolean(coverage);

  return <ToolbarIconButton disabled={!hasCoverage} icon={ExportIcon} title="Export coverage..." onClick={onExportCoverage} />;
};

export default memo(ExportCoverageButton);
