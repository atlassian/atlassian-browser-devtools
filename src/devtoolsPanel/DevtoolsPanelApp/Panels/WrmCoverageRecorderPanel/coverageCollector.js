import { addInsertStyleSheetListener, convertCssCoverageToJsonFormat } from './cssCoverage.js';
import { sendCommand, waitFotPageToLoad } from './debuggerApi.js';
import { convertJsCoverageToJsonFormat } from './jsCoverage.js';
import { chromeDevtoolsProtocolLogger as logger } from '../../../../common/logger.js';
import { captureException } from '../../../../common/sentry/sentry.js';

/** @typedef {import('./debuggerApi').Debuggee} Debuggee */
/** @typedef {import('./debuggerApi').ProfilerTakePreciseCoverageResponse} ProfilerTakePreciseCoverageResponse */
/** @typedef {import('./debuggerApi').CssStopRuleUsageTrackingResponse} CssStopRuleUsageTrackingResponse */
/** @typedef {import('../../../DevtoolsPanelApp/Panels/WrmCoverageRecorderPanel/jsCoverage').CoverageJson} CoverageJson */

const cleanUpListeners = (eventListeners) => {
  if (Array.isArray(eventListeners)) {
    for (let removeListener of eventListeners) {
      try {
        removeListener();
      } catch (e) {
        // eslint-disable-next-line no-empty
      }
    }

    eventListeners.length = 0;
  }
};

/**
 * @param {Debuggee} target
 * @returns {Promise<CoverageJson>}
 */
export const collectCoverageFromTarget = async (target) => {
  const styleSheetUrls = /** @type {Map<string, string>} */ new Map();
  const eventListeners = [];

  try {
    // Turn on JS and CSS coverage
    await sendCommand(target, 'Profiler.enable');
    await sendCommand(target, 'Profiler.startPreciseCoverage', {
      callCount: false,
      detailed: true,
    });
    await sendCommand(target, 'DOM.enable'); // Required to collect CSS usage
    await sendCommand(target, 'CSS.enable');
    await sendCommand(target, 'CSS.startRuleUsageTracking');

    // Avoid pausing debugger on any "debugger;" statement or breakpoints
    await sendCommand(target, 'Debugger.enable');
    await sendCommand(target, 'Debugger.setSkipAllPauses', { skip: true });

    const removeListener = addInsertStyleSheetListener(target, styleSheetUrls);

    eventListeners.push(removeListener);

    // Reload the page, wait for page to be loaded, and collect the coverage result
    await sendCommand(target, 'Page.reload', {
      ignoreCache: true,
    });
    await waitFotPageToLoad();

    const { result: scriptCoverage } = /** @type {ProfilerTakePreciseCoverageResponse} */ (
      await sendCommand(target, 'Profiler.takePreciseCoverage')
    );
    const { ruleUsage: cssUsage } = /** @type {CssStopRuleUsageTrackingResponse} */ (
      await sendCommand(target, 'CSS.stopRuleUsageTracking')
    );

    // Disable collecting coverage
    await sendCommand(target, 'Profiler.stopPreciseCoverage');
    await sendCommand(target, 'Profiler.disable');

    // Convert collected coverage and get content from the sources
    const jsonJsCoverage = /** @type {CoverageJson} */ (await convertJsCoverageToJsonFormat(target, scriptCoverage));
    const jsonCssCoverage = /** @type {CoverageJson} */ (await convertCssCoverageToJsonFormat(target, cssUsage, styleSheetUrls));

    // Clean-up
    await sendCommand(target, 'CSS.disable');
    await sendCommand(target, 'DOM.disable');
    await sendCommand(target, 'Debugger.disable');

    logger('Setting page coverage', {
      css: jsonCssCoverage,
      js: jsonJsCoverage,
    });

    return [...jsonJsCoverage, ...jsonCssCoverage];
  } catch (err) {
    const error = /** @type {Error} */ (err);
    captureException(error);

    // Re-throw the error
    throw error;
  } finally {
    cleanUpListeners(eventListeners);
  }
};
