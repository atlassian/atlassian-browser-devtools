import { memo, StrictMode } from 'react';
import { Provider } from 'react-redux';

import './DevtoolsPanelApp.css';
import './DevtoolsPanelScrollbars.css';
import DevtoolsMainPanel from './DevtoolsMainPanel/DevtoolsMainPanel.jsx';
import { ThemeProvider } from '../components/Theme/Theme.jsx';

/** @typedef {import('react')} React */

const devtoolsTheme = chrome.devtools.panels.themeName;

/** @type {React.FC<{ store: import('redux').Store }>} */
const DevtoolsPanelApp = ({ store }) => {
  return (
    <StrictMode>
      <ThemeProvider theme={devtoolsTheme}>
        <Provider store={store}>
          <DevtoolsMainPanel />
        </Provider>
      </ThemeProvider>
    </StrictMode>
  );
};

export default memo(DevtoolsPanelApp);
