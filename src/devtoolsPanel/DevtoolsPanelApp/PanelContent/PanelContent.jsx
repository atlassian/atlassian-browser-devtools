import { memo, Suspense } from 'react';
import { useSelector } from 'react-redux';

import { panelViewsMap } from './panels.js';
import PanelWrapper from '../../components/PanelWrapper/PanelWrapper.jsx';
import { selectors } from '../../stores/panelsStore.js';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const PanelContent = () => {
  const activePanel = useSelector(selectors.getActivePanel);
  const PanelViewComponent = panelViewsMap[activePanel];

  return (
    <PanelWrapper>
      <Suspense fallback={''}>
        <PanelViewComponent />
      </Suspense>
    </PanelWrapper>
  );
};

export default memo(PanelContent);
