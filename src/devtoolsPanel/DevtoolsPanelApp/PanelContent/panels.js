import { lazy } from 'react';

export const DEPRECATION_LOGGER = 'DEPRECATION_LOGGER';
export const WRM_COVERAGE = 'WRM_COVERAGE';
export const WRM_INSPECTOR = 'WRM_INSPECTOR';
export const WRM_COVERAGE_RECORDER = 'WRM_COVERAGE_RECORDER';

const AuiDeprecationsInspectorPanel = lazy(() =>
  import(/* webpackPreload: true, webpackChunkName: "AuiDeprecationsPanel" */ '../Panels/AuiDeprecationsPanel/AuiDeprecationsPanel.jsx')
);

const WrmCoveragePanel = lazy(() =>
  import(/* webpackPreload: true, webpackChunkName: "WrmCoveragePanel" */ '../Panels/WrmCoveragePanel/WrmCoveragePanelWrapper.jsx')
);

const WrmInspectorPanel = lazy(() =>
  import(/* webpackPreload: true, webpackChunkName: "WrmInspectorPanel" */ '../Panels/WrmInspectorPanel/WrmInspectorPanelWrapper.jsx')
);

const WrmCoverageRecorderPanel = lazy(() =>
  import(
    /* webpackPreload: true, webpackChunkName: "WrmCoverageRecorderPanel" */ '../Panels/WrmCoverageRecorderPanel/WrmCoverageRecorderPanelWrapper.jsx'
  )
);

export const panelViewsMap = {
  [DEPRECATION_LOGGER]: AuiDeprecationsInspectorPanel,
  [WRM_COVERAGE]: WrmCoveragePanel,
  [WRM_INSPECTOR]: WrmInspectorPanel,
  [WRM_COVERAGE_RECORDER]: WrmCoverageRecorderPanel,
};
