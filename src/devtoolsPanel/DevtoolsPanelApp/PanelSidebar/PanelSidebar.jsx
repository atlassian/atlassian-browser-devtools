import { memo, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styles from './PanelSidebar.less';
import SidebarToolbar from './SidebarToolbar.jsx';
import { trackEvent } from '../../../common/analytics/analytics.js';
import Icon, { ExperimentIcon } from '../../components/Icon/Icon.jsx';
import PanelWrapper from '../../components/PanelWrapper/PanelWrapper.jsx';
import Sidebar, { SidebarActionItemClassName } from '../../components/Sidebar/Sidebar.jsx';
import SidebarActionItem from '../../components/SidebarActionItem/SidebarActionItem.jsx';
import { actions, selectors } from '../../stores/panelsStore.js';
import { DEPRECATION_LOGGER, WRM_COVERAGE, WRM_COVERAGE_RECORDER, WRM_INSPECTOR } from '../PanelContent/panels.js';

/** @typedef {import('react')} React */

/** @type {React.FC} */
const PanelSidebar = () => {
  const dispatch = useDispatch();
  const activePanel = useSelector(selectors.getActivePanel);
  const selectPanel = useCallback(
    async (panelId) => {
      dispatch(actions.setActivePanel(panelId));

      await trackEvent('SELECT_PANEL', { panelId });
    },
    [dispatch]
  );

  return (
    <PanelWrapper className={styles.PanelSidebarWrapper}>
      <Sidebar className={styles.PanelSidebarSidebar}>
        <SidebarActionItem
          active={activePanel === DEPRECATION_LOGGER}
          className={SidebarActionItemClassName}
          panelId={DEPRECATION_LOGGER}
          onClick={selectPanel}
        >
          AUI Deprecations Inspector
        </SidebarActionItem>

        <SidebarActionItem
          active={activePanel === WRM_INSPECTOR}
          className={SidebarActionItemClassName}
          panelId={WRM_INSPECTOR}
          onClick={selectPanel}
        >
          WRM Inspector
        </SidebarActionItem>

        <SidebarActionItem
          active={activePanel === WRM_COVERAGE}
          className={SidebarActionItemClassName}
          panelId={WRM_COVERAGE}
          onClick={selectPanel}
        >
          WRM Coverage Browser
        </SidebarActionItem>

        <SidebarActionItem
          active={activePanel === WRM_COVERAGE_RECORDER}
          className={SidebarActionItemClassName}
          panelId={WRM_COVERAGE_RECORDER}
          onClick={selectPanel}
        >
          WRM Coverage Recorder <Icon icon={ExperimentIcon} />
        </SidebarActionItem>
      </Sidebar>
      <SidebarToolbar />
    </PanelWrapper>
  );
};

export default memo(PanelSidebar);
