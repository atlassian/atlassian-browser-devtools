import { memo, useCallback } from 'react';

import styles from './HelpDialog.less';
import { getEventName, trackEvent } from '../../../common/analytics/analytics.js';
import { PROJECT_URL, REPO_URL } from '../../../common/urls.js';
import { EXTENSION_VERSION } from '../../../common/version.js';
import { useAnalyticsContext } from '../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import Dialog from '../../components/Dialog/Dialog.jsx';

/** @typedef {import('react')} React */

/** @type {React.FC<{ dismiss: () => void }>} */
const HelpDialog = ({ dismiss }) => {
  const analyticsContext = useAnalyticsContext();

  const navigateToProject = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'SIDEBAR', 'NAVIGATE_TO_PROJECT'));
  }, [analyticsContext]);

  const learnMore = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'SIDEBAR', 'LEARN_MORE'));
  }, [analyticsContext]);

  const sourceCode = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'SIDEBAR', 'SOURCE_CODE'));
  }, [analyticsContext]);

  return (
    <Dialog onClose={dismiss}>
      <div className={styles.HelpDialog}>
        <h1 className={styles.HelpDialogHeader}>Help and Feedback</h1>

        <h3 className={styles.HelpDialogVersion}>
          <strong>Atlassian Browser DevTools</strong> <code className={styles.HelpDialogVersionNumber}>version {EXTENSION_VERSION}</code>
        </h3>

        <img alt="" className={styles.HelpDialogLogo} src="../../../icons/icon128.png" />

        <h2>Found a bug or have a suggestion?</h2>
        <p>
          If you have found a bug or have a suggestion about improvements please file an issue on the{' '}
          <a href={PROJECT_URL} rel="noreferrer nofollow" target="_blank" onClick={navigateToProject}>
            <strong>Atlassian Browser DevTools</strong> project page.
          </a>
        </p>

        <h2>How to use this extension?</h2>
        <p>
          <a href="/abd_onboarding.html" rel="nofollow noreferrer" target="_blank" onClick={learnMore}>
            Click here
          </a>{' '}
          to learn more on how to use the <strong>ABD</strong> extension.
        </p>

        <h2>Source code</h2>
        <p>
          This project is an open-source project. You can find the source code on{' '}
          <a href={REPO_URL} rel="nofollow noreferrer" target="_blank" onClick={sourceCode}>
            Bitbucket
          </a>
          .
        </p>
      </div>
    </Dialog>
  );
};

export default memo(HelpDialog);
