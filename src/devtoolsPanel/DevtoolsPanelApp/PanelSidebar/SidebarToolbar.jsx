import { memo, useCallback, useState } from 'react';

import HelpDialog from './HelpDialog.jsx';
import styles from './SidebarToolbar.less';
import { getEventName, trackEvent } from '../../../common/analytics/analytics.js';
import { AnalyticsContextProvider, useAnalyticsContext } from '../../components/AnalyticsContextProvider/AnalyticsContextProvider.jsx';
import BottomToolbar from '../../components/Toolbar/BottomToolbar.jsx';
import ToolbarButton from '../../components/ToolbarButton/ToolbarButton.jsx';

/** @typedef {import('react')} React */

const ANALYTICS_EVENTS_GROUP = 'HELP_DIALOG';

/** @type {React.FC} */
const SidebarToolbar = () => {
  const analyticsContext = useAnalyticsContext();

  const [isHelpDialogOpen, setIsHelpDialogOpen] = useState(false);

  const openHelpDialog = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'OPEN_HELP_DIALOG'));
    setIsHelpDialogOpen(true);
  }, [analyticsContext]);

  const closeHelpDialog = useCallback(() => {
    trackEvent(getEventName(analyticsContext, 'CLOSE_HELP_DIALOG'));

    setIsHelpDialogOpen(false);
  }, [analyticsContext]);

  return (
    <AnalyticsContextProvider analyticsContext={ANALYTICS_EVENTS_GROUP}>
      <BottomToolbar className={styles.SidebarToolbar}>
        <ToolbarButton onClick={openHelpDialog}>Help and Feedback</ToolbarButton>
      </BottomToolbar>
      {isHelpDialogOpen && <HelpDialog dismiss={closeHelpDialog} />}
    </AnalyticsContextProvider>
  );
};

export default memo(SidebarToolbar);
