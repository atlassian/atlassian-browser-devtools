import { createContext, memo, useContext, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import { fetchPlugins } from './pluginsDataProvider.js';
import { selectors } from '../../stores/pageStore.js';

/** @typedef {import('react')} React */
/** @typedef {import('../../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */
/** @typedef {import('../../services/coverage/coverageProcessor').WebResourceCoverageMeta} WebResourceCoverageMeta */

const pluginsContext = /** @type {React.Context<null | UpmAtlassianPlugins>} */ (createContext(null));

const { Provider } = pluginsContext;

/** @type {React.FC<{ children: React.ReactNode }>} */
const PluginsProvider = ({ children }) => {
  const [loadedPlugins, setLoadedPlugins] =
    /** @type {[Map<UpmAtlassianPlugin['key'], UpmAtlassianPlugin>, (arg0: Map<UpmAtlassianPlugin['key'], UpmAtlassianPlugin>) => void]} */ (
      useState(() => {
        return new Map();
      })
    );
  const pageUrl = useSelector(selectors.getPageUrl);

  useEffect(() => {
    let isMounted = true;

    /**
     * @returns {Promise<void>}
     */
    async function getPlugins() {
      const plugins = await fetchPlugins(pageUrl);

      if (!isMounted) {
        return;
      }

      /** @type {[UpmAtlassianPlugin['key'], UpmAtlassianPlugin][]} */
      const pluginsEntries = plugins.map((plugin) => [plugin.key, plugin]);

      /** @type {Map<UpmAtlassianPlugin['key'], UpmAtlassianPlugin>} */
      const pluginsMap = new Map(pluginsEntries);

      setLoadedPlugins(pluginsMap);
    }

    getPlugins();

    return () => {
      isMounted = false;
    };
  }, [pageUrl]);

  return <Provider value={loadedPlugins}>{children}</Provider>;
};

/**
 * @typedef {Object} UpmAtlassianPlugin
 * @property {string} userInstalled
 * @property {string} key
 * @property {string} name
 */

/** @typedef {Map<string, UpmAtlassianPlugin>} UpmAtlassianPlugins */

/**
 * @returns {UpmAtlassianPlugins}
 */
export const useGetPlugins = () => {
  const plugins = useContext(pluginsContext);

  if (plugins === null) {
    throw new Error(`You can only use "${useGetPlugins.name}" within the scope of the "PluginsProvider" component`);
  }

  return plugins;
};

/**
 * @param {WebResourceMeta | WebResourceCoverageMeta} webResource
 * @returns {UpmAtlassianPlugin | null}
 */
export const usePlugin = (webResource) => {
  const plugins = useGetPlugins();

  /** @type {UpmAtlassianPlugin | null} */
  const plugin = useMemo(() => {
    const pluginKey = webResource.wrmKey.split(':')[0];

    return (webResource && plugins.has(pluginKey) && plugins.get(pluginKey)) || null;
  }, [webResource, plugins]);

  return plugin;
};

export default memo(PluginsProvider);
