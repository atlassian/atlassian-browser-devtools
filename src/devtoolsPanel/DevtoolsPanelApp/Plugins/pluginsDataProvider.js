import { devtoolsPanelLogger as logger } from '../../../common/logger.js';

/** @typedef {import('../../DevtoolsPanelApp/Plugins/PluginsProvider').UpmAtlassianPlugin} UpmAtlassianPlugin */

/**
 * @param {string} pageUrl
 * @returns {Promise<UpmAtlassianPlugin[]>}
 */
export async function fetchPlugins(pageUrl) {
  let plugins = [];

  try {
    logger(`Fetching plugins list from UPM...`);

    const response = await fetch(`${pageUrl}/rest/plugins/1.0/`, { method: 'GET' });
    const json = await response.json();

    plugins = json.plugins.map((plugin) => {
      const { userInstalled, key, name } = plugin;

      return {
        key,
        userInstalled,
        name,
      };
    });

    logger(`Plugins list from UPM was loaded`, plugins);
  } catch (e) {
    logger(`Error while trying to list plugins from UPM`, plugins);
  }

  return plugins;
}
