import { createAction, createSelector, createSlice } from '@reduxjs/toolkit';

/**
 * @typedef {Object} PageState
 * @property {string | null} auiVersion
 * @property {boolean} hasAui
 * @property {boolean} isPageLoaded
 * @property {boolean} hasWrm
 * @property {string | null} wrmContextPath
 * @property {string | null} baseUrl
 */

const storeNamespace = 'PAGE';

/* eslint-disable jsdoc/valid-types */
/** @typedef {import('redux').CombinedState<{ [typeof storeNamespace]: PageState }>} RootStateSlice */
/* eslint-enable jsdoc/valid-types */

const initialState = /** @type {PageState} */ ({
  auiVersion: null,
  hasAui: false,
  isPageLoaded: false,
  hasWrm: false,
  wrmContextPath: null,
  baseUrl: null,
});

const PAGE_IS_LOADING = `${storeNamespace}/PAGE_IS_LOADING`;

export const setPageIsLoading = /** @type {ReturnType<import('@reduxjs/toolkit').createAction<boolean, string>>} */ (
  createAction(PAGE_IS_LOADING)
);

const pageSlice = createSlice({
  name: storeNamespace,
  initialState,
  reducers: {
    /**
     * @param {PageState} state
     * @returns {void}
     */
    setPageWasLoaded(state) {
      state.isPageLoaded = true;
    },

    /**
     * @param {PageState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<boolean>} action
     * @returns {void}
     */
    setHasAui(state, action) {
      state.hasAui = action.payload;
    },

    /**
     * @param {PageState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<string>} action
     * @returns {void}
     */
    setAuiVersion(state, action) {
      state.auiVersion = action.payload;
    },

    /**
     * @param {PageState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<boolean>} action
     * @returns {void}
     */
    setHasWrm(state, action) {
      state.hasWrm = action.payload;
    },

    /**
     * @param {PageState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<string>} action
     * @returns {void}
     */
    setWrmContextPath(state, action) {
      state.wrmContextPath = action.payload;
    },

    /**
     * @param {PageState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<string>} action
     * @returns {void}
     */
    setBaseUrl(state, action) {
      state.baseUrl = action.payload;
    },
  },

  extraReducers: (/** @type {import('@reduxjs/toolkit').ActionReducerMapBuilder<PageState>} */ builder) => {
    builder.addCase(
      setPageIsLoading,
      /**
       * @param {PageState} state
       * @returns {void}
       */
      (state) => {
        // Reset the state when the page is loading
        state.isPageLoaded = false;
        state.hasAui = false;
        state.auiVersion = null;
        state.hasWrm = false;
        state.baseUrl = null;
        state.wrmContextPath = null;
      }
    );
  },
});

const allActions = pageSlice.actions;

export const actions = {
  setPageIsLoading,
  ...allActions,
};

export const { reducer, name } = pageSlice;

/**
 * @param {RootStateSlice} state
 * @returns {PageState}
 */
const getSliceState = (state) => state[pageSlice.name];

/**
 * @param {RootStateSlice} state
 * @returns {string | null}
 */
const getAuiVersion = createSelector(
  getSliceState,
  /**
   * @param {PageState} state
   * @returns {string | null}
   */
  ({ auiVersion }) => auiVersion
);

/**
 * @param {RootStateSlice} state
 * @returns {boolean}
 */
const getHasAui = createSelector(
  getSliceState,
  /**
   * @param {PageState} state
   * @returns {boolean}
   */
  ({ hasAui }) => hasAui
);

/**
 * @param {RootStateSlice} state
 * @returns {boolean}
 */
const getHasWrm = createSelector(
  getSliceState,
  /**
   * @param {PageState} state
   * @returns {boolean}
   */
  ({ hasWrm }) => hasWrm
);

/**
 * @param {RootStateSlice} state
 * @returns {boolean}
 */
const getIsPageLoaded = createSelector(
  getSliceState,
  /**
   * @param {PageState} state
   * @returns {boolean}
   */
  ({ isPageLoaded }) => isPageLoaded
);

/**
 * @param {RootStateSlice} state
 * @returns {string | null}
 */
const getWrmContextPath = createSelector(
  getSliceState,
  /**
   * @param {PageState} state
   * @returns {string | null}
   */
  ({ wrmContextPath }) => wrmContextPath
);

/**
 * @param {RootStateSlice} state
 * @returns {string | null}
 */
const getBaseUrl = createSelector(
  getSliceState,
  /**
   * @param {PageState} state
   * @returns {string | null}
   */
  ({ baseUrl }) => baseUrl
);

/**
 * Returns a page URL without trailing slash
 *
 * @param {RootStateSlice} state
 * @returns {string}
 */
const getPageUrl = createSelector(
  getBaseUrl,
  getWrmContextPath,
  /**
   * @param {string | null} baseUrl
   * @param {string | null} contextPath
   * @returns {string}
   */
  (baseUrl, contextPath) => {
    if (!baseUrl) {
      return '';
    }

    const normalizedContextPath = contextPath ? contextPath.replace(/^\//, '').replace(/\/$/, '') : '';

    return `${baseUrl.replace(/\/$/, '')}/${normalizedContextPath}`.replace(/\/$/, '');
  }
);

export const selectors = {
  getAuiVersion,
  getHasAui,
  getIsPageLoaded,
  getHasWrm,
  getWrmContextPath,
  getBaseUrl,
  getPageUrl,
};
