import { createSlice, createSelector } from '@reduxjs/toolkit';

import { setPageIsLoading } from './pageStore.js';
import { processScript } from '../services/webResources/webResourcesProcessor.js';

/** @typedef {import('../services/webResources/webResourcesProcessor').WebResourceMeta} WebResourceMeta */

/**
 * @typedef {Object} WrmResource
 * @property {string} url
 * @property {string} code
 */

/**
 * @typedef {Object} WrmInspectorState
 * @property {WrmResource[]} resources
 */

const storeNamespace = 'WRM_INSPECTOR';

/* eslint-disable jsdoc/valid-types */
/** @typedef {import('redux').CombinedState<{ [typeof storeNamespace]: WrmInspectorState }>} RootStateSlice */
/* eslint-enable jsdoc/valid-types */

const initialState = /** @type {WrmInspectorState} */ ({
  resources: [],
});

const wrmInspectorSlice = createSlice({
  name: storeNamespace,
  initialState,
  reducers: {
    /**
     * @param {WrmInspectorState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<WrmResource>} action
     */
    storeResource(state, action) {
      state.resources.push(action.payload);
    },

    /**
     * @param {WrmInspectorState} state
     */
    clearResources(state) {
      state.resources = [];
    },
  },

  extraReducers: (/** @type {import('@reduxjs/toolkit').ActionReducerMapBuilder<WrmInspectorState>} */ builder) => {
    builder.addCase(
      setPageIsLoading,
      /**
       * @param {WrmInspectorState} state
       */
      (state) => {
        // Clear the web resources when page is loading
        state.resources = [];
      }
    );
  },
});

export const { actions, reducer, name } = wrmInspectorSlice;

/**
 * @param {RootStateSlice} state
 * @returns {WrmInspectorState}
 */
const getSliceState = (state) => state[wrmInspectorSlice.name];

/**
 * @param {RootStateSlice} state
 * @returns {WrmResource[]}
 */
const getResources = (state) => getSliceState(state).resources;

/**
 * @param {RootStateSlice} state
 * @returns {WebResourceMeta[]}
 */
const getWebResources = createSelector(
  getResources,
  /**
   * @param {WrmResource[]} resources
   * @returns {WebResourceMeta[]}
   */
  (resources) => {
    // Convert script code into web-resources
    return resources.map(({ url, code }) => processScript({ url, code })).flat();
  }
);

/**
 * @param {RootStateSlice} state
 * @returns {number}
 */
const getWebResourcesSize = createSelector(
  getResources,
  /**
   * @param {WrmResource[]} resources
   * @returns {number}
   */
  (resources) => {
    return resources.reduce((sum, resource) => {
      return sum + resource.code.length;
    }, 0);
  }
);

export const selectors = {
  getWebResources,
  getWebResourcesSize,
};
