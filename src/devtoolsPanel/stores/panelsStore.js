import { createSlice, createSelector } from '@reduxjs/toolkit';

import { WRM_INSPECTOR } from '../DevtoolsPanelApp/PanelContent/panels.js';

/**
 * @typedef {Object} PanelsState
 * @property {string} activePanel
 */

const storeNamespace = 'PANELS';

const initialState = /** @type {PanelsState} */ ({
  activePanel: WRM_INSPECTOR,
});

/* eslint-disable jsdoc/valid-types */
/** @typedef {import('redux').CombinedState<{ [typeof storeNamespace]: PanelsState }>} RootStateSlice */
/* eslint-enable jsdoc/valid-types */

const panelsSlice = createSlice({
  name: storeNamespace,
  initialState,
  reducers: {
    /**
     * @param {PanelsState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<string>} action
     */
    setActivePanel: (state, action) => {
      state.activePanel = action.payload;
    },
  },
});

export const { actions, reducer, name } = panelsSlice;

/**
 * @param {RootStateSlice} state
 * @returns {PanelsState}
 */
const getSliceState = (state) => state[panelsSlice.name];

/**
 * @returns Boolean
 */
export const getActivePanel = createSelector(getSliceState, ({ activePanel }) => activePanel);

export const selectors = {
  getActivePanel,
};
