import { createAction, createSlice, createSelector } from '@reduxjs/toolkit';

import { setPageIsLoading } from './pageStore.js';

/** @typedef {import('../../contentScript/deprecations/types').AuiDeprecationWithMeta} AuiDeprecationWithMeta */

/**
 * @typedef {Object} AuiDeprecationsState
 * @property {boolean} preserveLog
 * @property {AuiDeprecationWithMeta[]} deprecations
 * @property {string | null} filterQuery
 * @property {boolean} showTimestamps
 */

const storeNamespace = 'AUI_DEPRECATIONS';

/* eslint-disable jsdoc/valid-types */
/** @typedef {import('redux').CombinedState<{ [typeof storeNamespace]: AuiDeprecationsState }>} RootStateSlice */
/* eslint-enable jsdoc/valid-types */

const CLEAR_DEPRECATIONS_ACTION_TYPE = `${storeNamespace}/CLEAR_DEPRECATIONS`;

export const clearDeprecations = /** @type {ReturnType<import('@reduxjs/toolkit').createAction<void, string>>} */ (
  createAction(CLEAR_DEPRECATIONS_ACTION_TYPE)
);

const initialState = /** @type {AuiDeprecationsState} */ ({
  preserveLog: false,
  deprecations: [],
  filterQuery: null,
  showTimestamps: false,
});

const auiHighlightingSlice = createSlice({
  name: storeNamespace,
  initialState,
  reducers: {
    /**
     * @param {AuiDeprecationsState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<AuiDeprecationWithMeta>} action
     */
    storeDeprecation(state, action) {
      state.deprecations.push(action.payload);
    },

    /**
     * @param {AuiDeprecationsState} state
     */
    clearDeprecations(state) {
      state.deprecations = [];
    },

    /**
     * @param {AuiDeprecationsState} state
     */
    togglePreserveLog(state) {
      state.preserveLog = !state.preserveLog;
    },

    /**
     * @param {AuiDeprecationsState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<AuiDeprecationWithMeta>} action
     */
    filterDeprecations(state, action) {
      state.filterQuery = action.payload;
    },

    /**
     * @param {AuiDeprecationsState} state
     */
    toggleShowTimeStamps(state) {
      state.showTimestamps = !state.showTimestamps;
    },
  },

  extraReducers: (/** @type {import('@reduxjs/toolkit').ActionReducerMapBuilder<AuiDeprecationsState>} */ builder) => {
    builder.addCase(
      setPageIsLoading,
      /**
       * @param {AuiDeprecationsState} state
       */
      (state) => {
        // Clear the deprecations when the page is loading
        if (!state.preserveLog) {
          state.deprecations = [];
        }
      }
    );
  },
});

export const { actions, reducer, name } = auiHighlightingSlice;

// Selectors
/**
 * @param {RootStateSlice} state
 * @returns {AuiDeprecationsState}
 */
const getSliceState = (state) => state[auiHighlightingSlice.name];

/**
 * @param {RootStateSlice} state
 * @returns {AuiDeprecationWithMeta[]}
 */
const getDeprecations = (state) => getSliceState(state).deprecations;

/**
 * @param {RootStateSlice} state
 * @returns {string | null}
 */
const getDeprecationsFilter = (state) => getSliceState(state).filterQuery;

/**
 * @param {RootStateSlice} state
 * @returns {AuiDeprecationWithMeta[]}
 */
const getFilteredDeprecations = createSelector(
  getDeprecations,
  getDeprecationsFilter,
  /**
   * @param {AuiDeprecationWithMeta[]} deprecations
   * @param {string | null} filter
   * @returns {AuiDeprecationWithMeta[]}
   */
  (deprecations, filter) => {
    if (!filter) {
      return deprecations;
    }

    const filterRegExp = new RegExp(filter, 'i');

    return deprecations.filter((deprecation) =>
      Object.values(deprecation)
        .filter(Boolean)
        .some((value) => value.toString().match(filterRegExp))
    );
  }
);

/**
 * @param {RootStateSlice} state
 * @returns {boolean}
 */
const getShowTimestamps = createSelector(
  getSliceState,
  /**
   * @param {AuiDeprecationsState} state
   * @returns {boolean}
   */ ({ showTimestamps }) => showTimestamps
);

export const selectors = {
  getFilteredDeprecations,
  getShowTimestamps,
};
