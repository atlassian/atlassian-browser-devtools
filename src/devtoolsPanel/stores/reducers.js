import { combineReducers } from 'redux';

import { name as auiDeprecationsNamespace, reducer as auiDeprecationsReducer } from './auiDeprecationsStore.js';
import { name as auiHighlightingNamespace, reducer as auiHighlightingReducer } from './auiHighlightingStore.js';
import { name as pageReducerNamespace, reducer as pageReducerReducer } from './pageStore.js';
import { name as panelsNamespace, reducer as panelsReducer } from './panelsStore.js';
import { name as wrmInspectorNamespace, reducer as wrmInspectorReducer } from './wrmInspectorStore.js';

export default combineReducers({
  [auiDeprecationsNamespace]: auiDeprecationsReducer,
  [auiHighlightingNamespace]: auiHighlightingReducer,
  [panelsNamespace]: panelsReducer,
  [wrmInspectorNamespace]: wrmInspectorReducer,
  [pageReducerNamespace]: pageReducerReducer,
});
