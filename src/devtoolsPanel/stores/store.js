import { configureStore } from '@reduxjs/toolkit';

import reducers from './reducers.js';
import attachListeners from '../events/attachListeners.js';

const store = configureStore({ reducer: reducers });

/** @typedef {ReturnType<typeof store.getState>} RootState */

attachListeners(store);

// eslint-disable-next-line no-undef
if (process.env.NODE_ENV === 'development') {
  // @ts-expect-error we are setting a global variable
  window.__store = store;
}

export default store;
