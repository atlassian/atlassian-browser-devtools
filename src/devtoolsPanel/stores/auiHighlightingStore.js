import { createSlice, createSelector } from '@reduxjs/toolkit';

import { clearDeprecations } from './auiDeprecationsStore.js';
import * as highlighting from '../events/emitters/highlithing.js';

/**
 * @typedef {Object} AuiHighlightingState
 * @property {boolean} highlightDeprecatedElements
 */

const storeNamespace = 'AUI_DEPRECATION_HIGHLIGHTING';

/* eslint-disable jsdoc/valid-types */
/** @typedef {import('redux').CombinedState<{ [typeof storeNamespace]: AuiHighlightingState }>} RootStateSlice */
/* eslint-enable jsdoc/valid-types */

const initialState = /** @type {AuiHighlightingState} */ ({
  highlightDeprecatedElements: false,
});

const auiHighlightingSlice = createSlice({
  name: storeNamespace,
  initialState,
  reducers: {
    /**
     * @param {AuiHighlightingState} state
     */
    toggleHighlightDeprecatedElements(state) {
      const shouldHighlight = !state.highlightDeprecatedElements;

      highlighting.toggleHighlightNodes(shouldHighlight);

      state.highlightDeprecatedElements = shouldHighlight;
    },

    /**
     * @param {AuiHighlightingState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<string>} action
     */
    highlightNode(state, action) {
      const nodeId = action.payload;

      highlighting.highlightNode(nodeId);
    },

    /**
     * @param {AuiHighlightingState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<string>} action
     */
    unhighlightNode(state, action) {
      const nodeId = action.payload;

      if (!state.highlightDeprecatedElements) {
        highlighting.unhighlightNode(nodeId);
      }
    },

    /**
     * @param {AuiHighlightingState} state
     * @param {import('@reduxjs/toolkit').PayloadAction<string>} action
     */
    scrollToNode(state, action) {
      const nodeId = action.payload;

      highlighting.scrollToNode(nodeId);
    },
  },
  extraReducers: (/** @type {import('@reduxjs/toolkit').ActionReducerMapBuilder<AuiHighlightingState>} */ builder) => {
    builder.addCase(clearDeprecations, () => {
      // Remove highlighting on all nodes when deprecations are cleared
      highlighting.removeHighlightingOnAllNodes();
    });
  },
});

export const { actions, reducer, name } = auiHighlightingSlice;

/**
 * @param {RootStateSlice} state
 * @returns {AuiHighlightingState}
 */
const getSliceState = (state) => state[auiHighlightingSlice.name];

/**
 * @param {RootStateSlice} state
 * @returns {boolean}
 */
const getHighlightDeprecatedElements = createSelector(
  getSliceState,
  /**
   * @param {AuiHighlightingState} state
   * @returns {boolean}
   */
  ({ highlightDeprecatedElements }) => highlightDeprecatedElements
);

export const selectors = {
  getHighlightDeprecatedElements,
};
