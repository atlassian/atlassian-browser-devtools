// @ts-expect-error scrollIntoViewIfNeeded might be defined
const hasScrollIntoViewIfNeeded = typeof Element.prototype.scrollIntoViewIfNeeded === 'function';

/**
 * @param {Element} node
 * @param {ScrollIntoViewOptions} options
 */
export const scrollIntoView = (node, options) => {
  if (hasScrollIntoViewIfNeeded) {
    // @ts-expect-error scrollIntoViewIfNeeded might be defined
    // eslint-disable-next-line no-restricted-syntax
    node.scrollIntoViewIfNeeded(options);
  } else {
    node.scrollIntoView(options);
  }
};
