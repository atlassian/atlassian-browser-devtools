export const IS_FIREFOX = navigator.userAgent.includes('Firefox');
export const IS_CHROME = navigator.userAgent.includes('Chrome');

// We are mocking the runtime ID when we are running the local dev mode with a local webpack dev server
const isLocalDevMode = chrome.runtime.id === '__DEV_MODE__';

/**
 * The `update_url` field is added by Chrome Web Store when the application is installed from the marketplace
 * {@link https://stackoverflow.com/questions/12830649/check-if-chrome-extension-installed-in-unpacked-mode/}
 *
 * Unfortunately, Firefox does not provide this value. So we need to check if the extension id ends with the `@temporary-addon` suffix.
 */

const FIREFOX_TEMP_ADDON_ID_SUFFIX = '@temporary-addon';
const isFirefoxDevMode = isLocalDevMode || chrome.runtime.id.endsWith(FIREFOX_TEMP_ADDON_ID_SUFFIX);

const isChromeDevMode = isLocalDevMode || !('update_url' in chrome.runtime.getManifest());

export const IS_DEV_MODE = (IS_FIREFOX && isFirefoxDevMode) || (IS_CHROME ? isChromeDevMode : false);
