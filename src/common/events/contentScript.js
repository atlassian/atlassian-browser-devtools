// Messages that are sent from the:
// Content Script --> Devtools Panel
export const CONTENT_SCRIPT_INJECTED_EVENT = 'CONTENT_SCRIPT_INJECTED';

/** @typedef {void} ContentScriptInjectedEventPayload */
