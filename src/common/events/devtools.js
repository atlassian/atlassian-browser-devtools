// Messages that are sent from the:
// Devtools Panel --> Background Page
export const DEVTOOLS_PANEL_OPENED_EVENT = 'DEVTOOLS_PANEL_OPENED';

// Background Page --> Content Script
export const DEVTOOLS_CLOSED_EVENT = 'DEVTOOLS_CLOSED';
