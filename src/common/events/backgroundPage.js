// Messages that are sent from the:
// Background Page --> Content Script
// Background Page --> Devtools Panel
export const PAGE_LOADING_EVENT = 'PAGE_LOADING';

/** @typedef {void} PageLoadingEventPayload */

export const PAGE_LOADED_EVENT = 'PAGE_LOADED';

/** @typedef {void} PageLoadedEventPayload */
