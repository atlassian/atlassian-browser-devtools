/** @typedef {import('../../contentScript/deprecations/types').AuiDeprecation} AuiDeprecation */

// ABD Extension ContentScript --> ABD Background Page --> ABD Devtools Panel
/** @typedef {'AUI_DEPRECATION'} AUI_DEPRECATION_EVENT */
/** @typedef {AuiDeprecation} AuiDeprecationEventPayload */
export const /** @type {AUI_DEPRECATION_EVENT} */ AUI_DEPRECATION_EVENT = 'AUI_DEPRECATION';

// ABD Devtools Panel --> ABD Background Page --> ABD Extension ContentScript
/** @typedef {'AUI_HIGHLIGHT_NODE'} AUI_HIGHLIGHT_NODE_EVENT */
/**
 * @typedef {Object} AuiHighlightNodeEventPayload
 * @property {string} nodeId
 */
export const /** @type {AUI_HIGHLIGHT_NODE_EVENT} */ AUI_HIGHLIGHT_NODE_EVENT = 'AUI_HIGHLIGHT_NODE';

// ABD Devtools Panel --> ABD Background Page --> ABD Extension ContentScript
/** @typedef {'AUI_UNHIGHLIGHT_NODE'} AUI_UNHIGHLIGHT_NODE_EVENT */
/**
 * @typedef {Object} AuiUnhighlightNodeEventPayload
 * @property {string} nodeId
 */
export const /** @type {AUI_UNHIGHLIGHT_NODE_EVENT} */ AUI_UNHIGHLIGHT_NODE_EVENT = 'AUI_UNHIGHLIGHT_NODE';

// ABD Devtools Panel --> ABD Background Page --> ABD Extension ContentScript
/** @typedef {'AUI_SCROLL_TO_NODE'} AUI_SCROLL_TO_NODE_EVENT */
/**
 * @typedef {Object} AuiScrollToNodeEventPayload
 * @property {string} nodeId
 */
export const /** @type {AUI_SCROLL_TO_NODE_EVENT} */ AUI_SCROLL_TO_NODE_EVENT = 'AUI_SCROLL_TO_NODE';

// ABD Devtools Panel --> ABD Background Page --> ABD Extension ContentScript
/** @typedef {'AUI_TOGGLE_HIGHLIGHTING_NODES'} AUI_TOGGLE_HIGHLIGHTING_NODES_EVENT */
/**
 * @typedef {Object} AuiToggleNodeEventPayload
 * @property {boolean} highlight
 */
export const /** @type {AUI_TOGGLE_HIGHLIGHTING_NODES_EVENT} */ AUI_TOGGLE_HIGHLIGHTING_NODES_EVENT = 'AUI_TOGGLE_HIGHLIGHTING_NODES';

// ABD Devtools Panel --> ABD Background Page --> ABD Extension ContentScript
/** @typedef {'AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES'} AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES_EVENT */
/** @typedef {void} AuiRemoveHighlightingOnAllNodesEventPayload */
export const /** @type {AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES_EVENT} */ AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES_EVENT =
    'AUI_REMOVE_HIGHLIGHTING_ON_ALL_NODES';
