// Messages that are sent from the:
// ABD Extension ContentScript --> ABD Background Page --> ABD Devtools Panel
export const WRM_JS_RESOURCE_EVENT = 'WRM_JS_RESOURCE';

/**
 * @typedef {Object} WrmJsResourceEventPayload
 * @property {string} url
 */

export const WRM_CSS_RESOURCE_EVENT = 'WRM_CSS_RESOURCE';

/**
 * @typedef {Object} WrmCssResourceEventPayload
 * @property {string} url
 */
