import amplitude from 'amplitude-js';

import { IS_DEV_MODE } from '../devMode.js';
import { analyticsLogger as logger } from '../logger.js';

// Amplitude ID for ABD Chrome extension
const AMP_CLIENT_ID = 'd57241af21af3e407e10f46e205300d1';

const EVENT_NAME_SEPARATOR = '|';

// Configure Amplitude
const ampInstance = amplitude.getInstance();
ampInstance.init(AMP_CLIENT_ID);
const manifest = chrome.runtime.getManifest();
ampInstance.setVersionName(manifest.version);

const isObject = (value) => Object.prototype.toString.call(value) === '[object Object]';

/**
 * @param {string} eventName
 * @param {Object} [customProperties]
 * @returns {Promise<void>}
 */
export async function trackEvent(eventName, customProperties) {
  // Don't send the analytics from within a dev mode
  if (IS_DEV_MODE) {
    logger('Tacking new analytics event', eventName, customProperties);
    return;
  }

  let eventProperties;

  if (isObject(customProperties)) {
    eventProperties = Object.assign({}, customProperties);
  }

  await ampInstance.logEvent(eventName, eventProperties);
}

/**
 * @param {...(string | string[] | null)} names
 * @returns {string}
 */
export const getEventName = (...names) => {
  return names
    .flat()
    .filter((val) => val)
    .join(EVENT_NAME_SEPARATOR);
};
