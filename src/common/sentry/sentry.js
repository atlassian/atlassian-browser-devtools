import * as Sentry from '@sentry/browser';
import { wrap as sentryWrap } from '@sentry/browser/esm/helpers';
import { Integrations } from '@sentry/tracing';

import { IS_DEV_MODE } from '../devMode.js';

const SENTRY_DSN = 'https://f4fa91ff71e7467ba2eebad463dd6873@o55978.ingest.sentry.io/6135113';

const manifest = chrome.runtime.getManifest();
const extensionVersion = manifest.version;

/**
 * @param {string} appName
 */
export function initSentry(appName) {
  Sentry.init({
    dsn: SENTRY_DSN,
    integrations: [new Integrations.BrowserTracing()],
    tracesSampleRate: 1.0,
    environment: IS_DEV_MODE ? 'development' : 'production',
    release: extensionVersion,
    initialScope: {
      tags: {
        app: appName,
      },
    },
  });

  globalThis.addEventListener('unhandledrejection', (event) => {
    Sentry.captureEvent(event.reason);
  });
}

/**
 * Based on https://github.com/getsentry/sentry-javascript/commit/01cefd85c12f7d5ec9ccce525779997aa2631b55
 *
 * @param {(...args: any[]) => any} fn
 * @returns {(...args: any[]) => any}
 */
export function withSentryWrapper(fn) {
  return sentryWrap(fn);
}

/**
 * @param {Error} error
 * @returns {void}
 */
export function captureException(error) {
  Sentry.captureException(error);
}
