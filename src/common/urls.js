// Jira Ecosystem Project URL
export const PROJECT_URL = 'https://ecosystem.atlassian.net/secure/CreateIssue.jspa?issuetype=10006&pid=28449';

// Bitbucket repository URL
export const REPO_URL = 'https://bitbucket.org/atlassian/atlassian-browser-devtools';
