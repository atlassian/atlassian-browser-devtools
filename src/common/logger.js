const BACKGROUND_PAGE_COLOR = '#ffffff';
const BACKGROUND_PAGE_BG_COLOR = '#008b8b';

const DEVTOOLS_PANEL_COLOR = '#ffffff';
const DEVTOOLS_PANEL_BG_COLOR = '#c20ea5';

const CONTENT_SCRIPT_COLOR = '#ffffff';
const CONTENT_SCRIPT_BG_COLOR = '#ff8c00';

const ANALYTICS_COLOR = '#ffffff';
const ANALYTICS_BG_COLOR = '#2c72d7';

// const CDP_COLOR = '#ffffff';
// const CDP_BG_COLOR = '#002366';

const CDP_COLOR = '#ffffff';
const CDP_BG_COLOR = '#139407';

const getLogger =
  ({ color, bgColor, prefix }) =>
  (label, ...args) => {
    // eslint-disable-next-line no-undef
    if (process.env.NODE_ENV === 'production') {
      return;
    }

    if (typeof label !== 'string') {
      throw new Error('Please use distinguish label name when calling logger');
    }

    const coloredLabel = [`%c${prefix}: ${label}`, `color: ${color} ${bgColor ? `; background-color: ${bgColor}` : ''}`];

    console.log(...coloredLabel, ...args);
  };

export const backgroundPageLogger = getLogger({
  color: BACKGROUND_PAGE_COLOR,
  bgColor: BACKGROUND_PAGE_BG_COLOR,
  prefix: 'ABD Background',
});

export const contentScriptLogger = getLogger({
  color: CONTENT_SCRIPT_COLOR,
  bgColor: CONTENT_SCRIPT_BG_COLOR,
  prefix: 'ABD Content Script',
});

export const devtoolsPanelLogger = getLogger({
  color: DEVTOOLS_PANEL_COLOR,
  bgColor: DEVTOOLS_PANEL_BG_COLOR,
  prefix: 'ABD Devtools',
});

export const analyticsLogger = getLogger({
  color: ANALYTICS_COLOR,
  bgColor: ANALYTICS_BG_COLOR,
  prefix: 'Amplitude',
});

export const chromeDevtoolsProtocolLogger = getLogger({
  color: CDP_COLOR,
  bgColor: CDP_BG_COLOR,
  prefix: 'CDP',
});
