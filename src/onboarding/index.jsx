import { Children, createElement } from 'react';
import { render } from 'react-dom';
import ReactMarkdown from 'react-markdown';

import './styles.css';
import readme from '../../README.md';
import { IS_DEV_MODE } from '../common/devMode.js';
import { REPO_URL } from '../common/urls.js';
import { EXTENSION_VERSION } from '../common/version.js';

/** @typedef {import('react')} React */

const docsUrlRegExp = /^\.\/docs\//;

const getAssetsBaseUrl = () => {
  // In dev mode we will fallback to using locally included assets under the "docs" folder
  if (IS_DEV_MODE) {
    return chrome.runtime.getURL('');
  }

  // In production mode we will use the assets hosted from Bitbucket
  const baseAssetUrl = new URL(REPO_URL.replace(/\/$/, ''));
  const gitTagVersion = `v${EXTENSION_VERSION}`;

  baseAssetUrl.pathname += `/raw/${gitTagVersion}/`;

  return baseAssetUrl.toString();
};

const BASE_ASSET_URL = getAssetsBaseUrl();

const convertAssetUrlToExternal = (url) => {
  // Prepend repo URL
  const assetUrl = new URL(BASE_ASSET_URL);
  assetUrl.pathname += url;

  return assetUrl.toString();
};

const imageUrlTransformer = (imageUrl) => {
  if (!imageUrl.match(docsUrlRegExp)) {
    return imageUrl;
  }

  return convertAssetUrlToExternal(imageUrl);
};

const flatten = (text, child) => (typeof child === 'string' ? text + child : Children.toArray(child.props.children).reduce(flatten, text));

/** @type {React.FC<{ children: React.ReactNode; level: number }>} */
const HeadingRendererWithAnchor = ({ children, level }) => {
  const newChildren = Children.toArray(children);
  const text = newChildren.reduce(flatten, '');
  const slug = text.toLowerCase().replace(/\W/g, '-');

  return createElement(`h${level}`, { id: slug }, newChildren);
};

// Create our substitution for the *.gif image, so we can load *.mp4 video instead

/** @type {React.FC<{ src: string; node: any }>} */
const ImageComponent = ({ src: imageSrc, ...props }) => {
  delete props.node;

  if (!imageSrc.endsWith('.gif')) {
    return createElement('img', { ...props, src: imageSrc });
  }

  const movieUrl = imageSrc.replace(/\.gif$/, '.mp4');

  // Substitute with *.mp4 file
  return createElement(
    'video',
    {
      controls: true,
      width: '600px',
    },
    createElement('source', { src: movieUrl, type: 'video/mp4' })
  );
};

// Create our heading substitutions
const headingsEntries = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].map((headingTag) => [headingTag, HeadingRendererWithAnchor]);
const components = {
  ...Object.fromEntries(headingsEntries),
  img: ImageComponent,
};

render(
  <ReactMarkdown components={components} transformImageUri={imageUrlTransformer}>
    {readme}
  </ReactMarkdown>,
  document.getElementById('root')
);
