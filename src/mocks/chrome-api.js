import { PAGE_LOADED_EVENT } from '../common/events/events.js';

// import fakeCssRuleUsageTracking from './fixtures/fakeCssRuleUsageTracking.json';
// import fakeJsPreciseCoverage from './fixtures/fakeJsPreciseCoverage.json';
// import fakeJsContent from 'raw-loader!./fixtures/fakeWrmJsScript';

const fakeCssRuleUsageTracking = {
  ruleUsage: [],
};
const fakeJsPreciseCoverage = {
  result: [],
};
const fakeJsContent = 'JS YOLO!';

const FAKE_TAB_ID = Date.now();

window.chrome = window.chrome || {};

chrome.runtime = chrome.runtime || {};

chrome.management = chrome.management || {};

chrome.runtime.id = chrome.runtime.id || '__DEV_MODE__';

chrome.runtime.connect =
  chrome.runtime.connect ||
  function fakeConnect() {
    return {
      postMessage: function fakePostMessage() {},
      onMessage: {
        addListener: function fakeAddListener() {},
      },
    };
  };

chrome.devtools = chrome.devtools || {
  inspectedWindow: {
    tabId: FAKE_TAB_ID,
    eval: function fakeEval() {},
  },
  panels: {
    themeName: window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'default',
  },
};

chrome.runtime.getManifest =
  chrome.runtime.getManifest ||
  function fakeGetManifest() {
    return {
      version: '0.0.0-local',
    };
  };

// Debugger
chrome.debugger = chrome.debugger || {};

const createHandlerWithSuccessfulCallback = (fnName, responseOrResponseController, { timeout = 200 } = {}) => {
  const myHandler = (...args) => {
    // Naive assumption that the last element should be a callback
    const callback = args.pop();

    setTimeout(() => {
      const response =
        typeof responseOrResponseController === 'function' ? responseOrResponseController(...args) : responseOrResponseController;
      const params = Array.isArray(response) ? response : [response];

      callback(...params);
    }, timeout);
  };

  Object.defineProperty(myHandler, 'name', {
    value: `fake${fnName.substr(0, 1).toUpperCase()}${fnName.substr(1)}`,
  });

  return myHandler;
};

/**
 * @param {unknown} target
 * @param {string} command
 * @returns {unknown}
 */
// eslint-disable-next-line consistent-return
function sendCommandResponseController(target, command) {
  switch (command) {
    case 'Profiler.takePreciseCoverage':
      return fakeJsPreciseCoverage;

    case 'CSS.stopRuleUsageTracking':
      return fakeCssRuleUsageTracking;

    case 'Debugger.getScriptSource':
      return { scriptSource: fakeJsContent };

    case 'CSS.getStyleSheetText':
      return { text: 'CSS yolo!' };

    case 'Page.reload':
      // Handle page reload event and trigger our event
      setTimeout(async () => {
        const { _triggerIncomingMessage: triggerIncomingMessage } = await import('../devtoolsPanel/portConnection.js');

        triggerIncomingMessage(PAGE_LOADED_EVENT);
      }, 200);
      break;
  }
}

/**
 * @returns {[{ tabId: string }, null, Record<string, unknown>]}
 */
function addListenerController() {
  const source = {
    tabId: FAKE_TAB_ID,
  };
  const method = null;
  const params = {};

  return [source, method, params];
}

chrome.debugger.attach = chrome.debugger.attach || createHandlerWithSuccessfulCallback('attach');
chrome.debugger.detach = chrome.debugger.detach || createHandlerWithSuccessfulCallback('detach');

chrome.debugger.sendCommand =
  chrome.debugger.sendCommand || createHandlerWithSuccessfulCallback('sendCommand', sendCommandResponseController);

chrome.debugger.onEvent = chrome.debugger.onEvent || {};
chrome.debugger.onEvent.addListener =
  chrome.debugger.onEvent.addListener || createHandlerWithSuccessfulCallback('onEvenAddListener', addListenerController);
chrome.debugger.onEvent.removeListener = chrome.debugger.onEvent.removeListener || function fakeOnEventRemoveListener() {};

chrome.debugger.onDetach = chrome.debugger.onDetach || {};
chrome.debugger.onDetach.addListener = chrome.debugger.onDetach.addListener || function fakeOnDetachAddListener() {};
chrome.debugger.onDetach.removeListener = chrome.debugger.onDetach.removeListener || function fakeOnDetachRemoveListener() {};
