import { IS_DEV_MODE } from '../common/devMode.js';

let TAB_NAME = 'ABD';

if (IS_DEV_MODE) {
  TAB_NAME = 'ABD (⚠️ local version)';
}

// The abd_panel.js file is defined and created with webpack.
// The source of the file is stored at src/devtoolsPanel directory.
chrome.devtools.panels.create(TAB_NAME, '', 'abd_panel.html', () => {
  // TODO: Handle show and hide
});
