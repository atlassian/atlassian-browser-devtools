import * as semver from 'semver';

import { withSentryWrapper } from '../common/sentry/sentry.js';

// eslint-disable-next-line no-unused-vars
const currentVersion = semver.coerce(chrome.runtime.getManifest().version);
const extensionId = chrome.runtime.id;

chrome.runtime.onInstalled.addListener(
  withSentryWrapper((details) => {
    const { previousVersion: rawPreviousVersion, reason } = details;
    const previousVersion = semver.coerce(rawPreviousVersion);
    let showOnBoarding = false;

    if (previousVersion !== null) {
      const version201 = '2.0.1';
      if (reason === 'update' && semver.satisfies(previousVersion, `< ${version201}`)) {
        showOnBoarding = true;
      }
    }

    if (reason === 'install') {
      showOnBoarding = true;
    }

    if (showOnBoarding) {
      // The abd_onboarding.html page is defined and created with webpack.
      // The source of the file is stored at src/onboarding directory.
      window.open(`chrome-extension://${extensionId}/abd_onboarding.html`);
    }
  })
);
