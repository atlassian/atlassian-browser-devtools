import { backgroundPageLogger as logger } from '../common/logger.js';
import { withSentryWrapper } from '../common/sentry/sentry.js';

/**
 * @param {chrome.runtime.Port} portOne
 * @param {chrome.runtime.Port} portTwo
 * @returns {{ postMessage: (message: string) => void }}
 */
export function doublePipe(portOne, portTwo) {
  const listenerOne = withSentryWrapper((message, senderPort) => {
    if (portTwo === senderPort.sender) {
      return;
    }

    logger(`Forwarding the message from "${portOne.name}" to "${portTwo.name}"`, message);

    // eslint-disable-next-line consistent-return
    return portTwo.postMessage(message);
  });

  portOne.onMessage.addListener(listenerOne);

  const listenerTwo = withSentryWrapper((message, senderPort) => {
    if (portOne === senderPort.sender) {
      return;
    }

    logger(`Forwarding the message from "${portTwo.name}" to "${portOne.name}"`, message);

    // eslint-disable-next-line consistent-return
    return portOne.postMessage(message);
  });

  portTwo.onMessage.addListener(listenerTwo);

  const shutdown = withSentryWrapper(() => {
    portOne.onMessage.removeListener(listenerOne);
    portTwo.onMessage.removeListener(listenerTwo);
  });

  portOne.onDisconnect.addListener(shutdown);
  portTwo.onDisconnect.addListener(shutdown);

  return {
    postMessage(message) {
      portOne.postMessage(message);
      portTwo.postMessage(message);
    },
  };
}
