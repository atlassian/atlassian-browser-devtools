import { doublePipe } from './connections.js';
import { initPortConnection } from './messaging.js';
import { PAGE_LOADED_EVENT, PAGE_LOADING_EVENT, DEVTOOLS_CLOSED_EVENT, DEVTOOLS_PANEL_OPENED_EVENT } from '../common/events/events.js';
import { backgroundPageLogger as logger } from '../common/logger.js';
import { DEVTOOLS_PANEL_PORT, CONTENT_SCRIPT_PORT } from '../common/ports.js';
import { withSentryWrapper } from '../common/sentry/sentry.js';

import './onInstalled.js';

const connections = {};

const injectContentScript = (tabId) => {
  // The abd_content_script.js file is defined and created with webpack.
  // The source of the file is stored at src/contentScript directory.
  chrome.tabs.executeScript(tabId, { file: 'abd_content_script.js' });
};

const isMainPage = (tab) => {
  // https://developers.chrome.com/extensions/webNavigation
  // 0 indicates the navigation happens in the tab content window;

  return tab.frameId === 0;
};

const getConnection = (tabId) => {
  const connection = connections[tabId];

  if (!connection) {
    logger(`B: No connections for tab ${tabId}`);

    return null;
  }

  return connection;
};

const getDevtoolsPanelPort = (tabId) => {
  const connection = getConnection(tabId);

  if (!connection) {
    return null;
  }

  const devtoolsPanelPort = connection.devtoolsPanel;

  // Devtools for this tab are not opened
  if (!devtoolsPanelPort) {
    logger(`No connection with Devtools Panel for tab ${tabId}.`);

    return null;
  }

  return devtoolsPanelPort;
};

// Initialize the connection between the background script and:
// - Devtool Panel
// - Content Script (opened by Devtool Panel)
chrome.runtime.onConnect.addListener(
  withSentryWrapper((/** @type {!chrome.runtime.Port} */ port) => {
    logger(`Connected to a port "${port.name}"`);

    // Init Devtools panel connection and store port
    if (port.name === DEVTOOLS_PANEL_PORT) {
      initPortConnection(port, (topic, message) => {
        if (topic === DEVTOOLS_PANEL_OPENED_EVENT) {
          const { tabId } = message;

          logger(`Devtools Panel opened on the page ${tabId}`);
          logger(`Injecting a new Content Script on the page ${tabId}`);

          injectContentScript(tabId);

          connections[tabId] = connections[tabId] || {};
          connections[tabId].devtoolsPanel = port;

          // Listen to disconnected event when the devtools panel is closed
          port.onDisconnect.addListener(
            withSentryWrapper((disconnectedPort) => {
              logger(`Disconnecting Devtools Panel port "${disconnectedPort.name}"`, disconnectedPort);

              const contentScriptPort = connections[tabId].contentScript;

              if (contentScriptPort) {
                connections[tabId].contentScript.postMessage({ topic: DEVTOOLS_CLOSED_EVENT });

                delete connections[tabId].devtoolsPanel;
              }
            })
          );
        }
      });
    } else if (port.name === CONTENT_SCRIPT_PORT) {
      const portSender = /** @type {chrome.runtime.MessageSender} */ (port.sender);
      const tabId = /** @type {chrome.tabs.Tab} */ (portSender.tab).id;

      connections[tabId].contentScript = port;

      // Add this point we have both panel and content script in place
      const panelPort = connections[tabId].devtoolsPanel;
      const contentScriptPort = connections[tabId].contentScript;

      doublePipe(panelPort, contentScriptPort);
    }
  })
);

// When page is about to get closed or fully reloaded
chrome.webNavigation.onBeforeNavigate.addListener(
  withSentryWrapper((tab) => {
    if (!isMainPage(tab)) {
      return;
    }

    const { tabId } = tab;

    logger('[navigation] page is about to get closed', tabId);

    const connection = getConnection(tabId);

    if (!connection) {
      return;
    }

    delete connection.contentScript;
  })
);

// When page just stated loading
chrome.webNavigation.onCommitted.addListener(
  withSentryWrapper((tab) => {
    if (!isMainPage(tab)) {
      return;
    }

    const { tabId } = tab;

    logger('[navigation] page started loading', tabId);

    const devtoolsPanelPort = getDevtoolsPanelPort(tabId);

    // Devtools for this tab are not opened
    if (!devtoolsPanelPort) {
      return;
    }

    devtoolsPanelPort.postMessage({
      topic: PAGE_LOADING_EVENT,
    });
  })
);

// When page is loaded
chrome.webNavigation.onCompleted.addListener(
  withSentryWrapper((tab) => {
    if (!isMainPage(tab)) {
      return;
    }

    const { tabId } = tab;

    logger('[navigation] page was loaded', tabId);

    const devtoolsPanelPort = getDevtoolsPanelPort(tabId);

    // Devtools for this tab are not opened
    if (!devtoolsPanelPort) {
      return;
    }

    // Page was loaded
    devtoolsPanelPort.postMessage({
      topic: PAGE_LOADED_EVENT,
    });

    injectContentScript(tabId);
  })
);
