///<reference types="chrome"/>

import { backgroundPageLogger as logger } from '../common/logger.js';
import { withSentryWrapper } from '../common/sentry/sentry.js';
/**
 * @param {chrome.runtime.Port} port
 * @param {(topic: string, message: { tabId: string }) => void} listener
 */
export const initPortConnection = (port, listener) => {
  const messagesListener = withSentryWrapper(({ topic, message }) => {
    logger(`Received a message from "${port.name}" port`, { topic, message });

    listener(topic, message);
  });

  const disconnectListener = withSentryWrapper(() => {
    logger(`Disconnecting port "${port.name}"`);

    port.onMessage.removeListener(messagesListener);
    port.onDisconnect.removeListener(disconnectListener);
  });

  logger(`Establishing message listeners for "${port.name}"`);

  port.onMessage.addListener(messagesListener);
  port.onDisconnect.addListener(disconnectListener);
};
