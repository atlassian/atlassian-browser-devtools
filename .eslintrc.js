// @ts-nocheck
/* eslint-env node */
const path = require('path');

const restrictedGlobals = require('confusing-browser-globals');

// A list of pre-defined TS types and their corresponding source files.
const preDefinedJsDocTsTypes = [
  'ScrollIntoViewOptions', // lib.dom.d.ts
  'RegExpMatchArray', // built-in ES5 type
  'unknown', // TS core type
  'Record', // TS core type
  'Pick', // TS core type
  'ReturnType', // TS core type
];

module.exports = {
  root: true,
  parser: '@babel/eslint-parser',
  plugins: ['jsdoc', 'import', 'promise', 'react', 'react-hooks', 'abd'],
  extends: [
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:react/recommended',
    'plugin:react/jsx-runtime',
    'plugin:react-hooks/recommended',
    'plugin:promise/recommended',
    'plugin:abd/recommended',
    'plugin:jsdoc/recommended',
    'prettier', // Prettier needs to be a last one
  ],

  env: {
    browser: true,
    es6: true,
    webextensions: true,
    es2021: true,
  },

  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020,

    babelOptions: {
      configFile: path.resolve(__dirname, 'babel.config.js'),
    },
  },

  globals: {
    // Env. "extension" means we are running app as an installed extension (either local extension or extension from the market)
    IS_EXTENSION: true,

    // Env. "browser" means we are running the app as regular browser page (not extension) e.g. we are running app with the webpack dev server
    IS_BROWSER: true,
  },

  settings: {
    'import/resolver': {
      node: {},
    },

    react: {
      version: 'detect',
    },

    // JSDoc settings
    // https://www.npmjs.com/package/eslint-plugin-jsdoc#eslint-plugin-jsdoc-installation
    jsdoc: {
      mode: 'typescript',
      preferredTypes: {
        object: 'Object',
      },
    },
  },

  rules: {
    'no-use-before-define': ['error', { functions: false }],
    'no-shadow': 'error',
    'no-undef': 'error',
    'no-param-reassign': 'error',
    'no-return-assign': 'error',
    'class-methods-use-this': 'error',
    'consistent-return': 'error',
    'no-prototype-builtins': 'error',
    'no-unused-expressions': 'error',

    'import/no-webpack-loader-syntax': 'off',
    'import/prefer-default-export': 'off',
    'import/no-cycle': 'error',
    'import/named': 'error',
    'import/no-extraneous-dependencies': 'error',
    'import/no-useless-path-segments': 'error',
    'import/extensions': ['error', 'ignorePackages'],
    'import/no-unresolved': [
      'error',
      {
        caseSensitive: true,
        ignore: ['bitbucket/util/state'],
      },
    ],

    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        groups: ['builtin', 'external', 'internal', ['parent', 'sibling', 'index'], 'unknown'],
        pathGroupsExcludedImportTypes: ['builtin', 'external', 'internal'],
        alphabetize: {
          order: 'asc',
          orderImportKind: 'asc',
          caseInsensitive: true,
        },
      },
    ],

    // Ban direct usage of the scrollIntoViewIfNeeded
    'no-restricted-syntax': [
      'error',
      {
        selector: "CallExpression[callee.property.name='scrollIntoViewIfNeeded']",
        message:
          'Calling the "nodeElement.scrollIntoViewIfNeeded" is not supported by Firefox: https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollIntoViewIfNeeded\nImport and use the "scrollIntoViewIfNeeded(nodeElement)" helper instead.',
      },
    ],

    // Custom React settings
    'react/jsx-no-bind': 'error', // We don't want to use e.g. raw arrow functions in props.
    'react/prop-types': 'off', // We don't use prop types definitions in our code base. We use TypeScript instead.
    'react/jsx-sort-props': [
      'error',
      {
        reservedFirst: true,
        shorthandFirst: true,
        callbacksLast: true,
      },
    ],
    'react/jsx-filename-extension': 'error',
    'react/hook-use-state': 'error',

    // Globals
    'no-restricted-globals': ['error', ...restrictedGlobals],

    // New here?
    'react/no-unstable-nested-components': [
      'error',
      {
        allowAsProps: false,
      },
    ],

    // Promises
    'promise/always-return': 'off', // Promises without returns are fine

    // JSDoc
    'jsdoc/valid-types': 'error',
    'jsdoc/require-jsdoc': [
      'warn',
      {
        // require: {
        // TODO: Fixme
        // FunctionDeclaration: true,
        // ClassDeclaration: true,
        // ArrowFunctionExpression: true,
        // FunctionExpression: true,
        // ClassExpression: true,
        // },
        enableFixer: false,
      },
    ],
    'jsdoc/require-param-description': 'off', // We don't require to provide description for params
    'jsdoc/require-returns-description': 'off', // We don't require to provide description for returns
    'jsdoc/require-property-description': 'off', // We don't require to provide description for properties
    'jsdoc/require-description': 'off', // We don't require to provide description for functions
    'jsdoc/require-param': [
      'error',
      {
        enableFixer: false, // TODO: Check me
      },
    ],
    'jsdoc/require-param-type': 'error', // We do require param types, so we can keep TS happy!
    'jsdoc/require-returns-type': 'error', // We do require return types, so we can keep TS happy!
    'jsdoc/no-undefined-types': ['warn', { definedTypes: preDefinedJsDocTsTypes }],
    'jsdoc/check-param-names': 'error', // Param names should match
    'jsdoc/check-indentation': 'warn',
    'jsdoc/check-syntax': 'warn',
    'jsdoc/no-bad-blocks': 'error',
    'jsdoc/no-defaults': 'warn',
    // Make the comment block look more consistent
    'jsdoc/tag-lines': [
      'error',
      'never',
      {
        dropEndLines: true,
      },
    ],
    'jsdoc/require-asterisk-prefix': 'warn',

    // Allow using only "@type" and "@typedef" tags in single line. At the same time, prefer multiline blocks only for
    // a function definition comments.
    'jsdoc/multiline-blocks': [
      'warn',
      {
        noSingleLineBlocks: true,
        noFinalLineBlocks: true,
        allowMultipleTags: false,
        multilineTags: ['typedef', 'param', 'returns'],
        minimumLengthForMultiline: 200,
        noMultilineBlocks: true,
        singleLineTags: ['type', 'typedef'],
      },
    ],

    'jsdoc/no-restricted-syntax': [
      'error',
      {
        contexts: [
          {
            comment: 'JsdocBlock:has(JsdocTag[tag="type"]:has([value=/FunctionComponent/]))',
            context: 'any',
            message: 'The `FunctionComponent` type is not allowed. Please use `FC` instead.',
          },
        ],
      },
    ],
  },

  overrides: [
    // TypeScript
    {
      files: ['*.ts', '*.d.ts'],

      parserOptions: {
        project: [path.resolve(__dirname, './**/tsconfig.json')],
      },

      parser: '@typescript-eslint/parser',

      settings: {
        'import/parsers': {
          '@typescript-eslint/parser': ['.ts', '.tsx'],
        },

        'import/resolver': {
          node: {
            extensions: ['.js', '.jsx', '.ts', '.tsx'],
          },
        },
      },

      extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/strict',
        'plugin:import/typescript',
      ],
      rules: {
        '@typescript-eslint/no-useless-constructor': 'error',
        '@typescript-eslint/no-explicit-any': 'error',
        '@typescript-eslint/consistent-type-imports': [
          'error',
          {
            prefer: 'type-imports',
            disallowTypeAnnotations: true,
            fixStyle: 'separate-type-imports',
          },
        ],

        // Use TS version
        'no-unused-vars': 'off',
        '@typescript-eslint/no-unused-vars': 'error',

        // Use TS version
        'no-use-before-define': 'off', // Needs to be disabled to use TS version
        '@typescript-eslint/no-use-before-define': 'error',

        '@typescript-eslint/ban-ts-comment': [
          'error',
          {
            'ts-expect-error': 'allow-with-description',
            'ts-ignore': true,
            'ts-nocheck': true,
            'ts-check': false,
            minimumDescriptionLength: 3,
          },
        ],
      },
    },
  ],
};
