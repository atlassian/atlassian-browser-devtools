/* eslint-env node */

import { promises as fs } from 'fs';

console.log('Updating versions in manifest file...');

/**
 * @param {string} location
 * @returns {JSON}
 */
async function readJson(location) {
  const data = await fs.readFile(location, 'utf8');

  return JSON.parse(data);
}

try {
  const pkg = await readJson(new URL('../package.json', import.meta.url));
  const manifest = await readJson(new URL('../src/manifest.json', import.meta.url));

  const newManifest = Object.assign({}, manifest, { version: pkg.version, version_name: pkg.version });
  const newManifestContent = JSON.stringify(newManifest, null, '  ');

  await fs.writeFile(new URL('../src/manifest.json', import.meta.url), newManifestContent);

  console.log('✅ Manifest file updated');
} catch (error) {
  console.error('❌ Error while updating manifest file', error);

  process.exitCode = 1;
}
