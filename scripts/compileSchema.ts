import { promises as fs } from 'fs';
import * as path from 'path';
import * as process from 'process';
import * as url from 'url';

import Ajv from 'ajv';
import standaloneCode from 'ajv/dist/standalone';
import S from 'deindent';

const schemaInputFile = './src/devtoolsPanel/services/coverage/coverage.schema.json';
const schemaOutputFile = './src/devtoolsPanel/services/coverage/coverageSchemaValidatorCompiled.js';

try {
  const SCHEMA_ID = '___coverageSchema___';

  const ROOT = path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), '..');

  // Compile schema
  const schemaContent = await fs.readFile(path.join(ROOT, schemaInputFile));
  const schema = JSON.parse(schemaContent.toString());

  // Rename schema to use unique name we re-use to re-export the validator
  schema.$id = SCHEMA_ID;

  const ajv = new Ajv({
    strict: true,
    strictSchema: true,
    strictTuples: false,
    code: {
      es5: false,
      source: true,
      esm: true,
      lines: true,
      optimize: true,
    },
  });

  ajv.addSchema(schema);

  // Output as code
  const moduleCode = standaloneCode(ajv);

  // Prepend @ts-nocheck
  const compiledSchema = S`// @ts-nocheck
  /* eslint-env node */

  ${moduleCode}

  export default ${SCHEMA_ID};`;

  // Write to file
  await fs.writeFile(path.join(ROOT, schemaOutputFile), compiledSchema);

  console.log('✅ Schema compiled successfully');
} catch (err) {
  const error = err as Error;

  console.error(" ❌ Can't compile schema", error);

  // @ts-expect-error exitCode is not readonly
  process.exitCode = 1;
}
