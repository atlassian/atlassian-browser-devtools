import { promises as fs } from 'fs';
import { createRequire } from 'module';
import path from 'path';
import { fileURLToPath } from 'url';

import { build } from 'esbuild';
import alias from 'esbuild-plugin-alias';
import { filesize } from 'filesize';

const startTime = performance.now();

const require = createRequire(import.meta.url);
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const root = path.resolve(__dirname, '..');

// This could use import.meta.resolve... but it doesn't work
const resolveNodeModuleFile = async (filePath) => {
  return require.resolve(filePath);
};

// Each monaco worker comes from own standalone NPM package or pre-compiled from the "monaco-editor" package
// https://github.com/microsoft/monaco-editor/blob/92d6800a00070f876faad9ee7997e9efea7c5e4b/metadata.js
const entryPoints = [
  resolveNodeModuleFile('monaco-typescript/src/ts.worker.ts'),
  resolveNodeModuleFile('monaco-editor/esm/vs/editor/editor.worker.js'),
  resolveNodeModuleFile('monaco-editor/esm/vs/language/css/css.worker.js'),
];

try {
  const outDir = path.resolve(root, 'dist');

  const result = await build({
    entryPoints: await Promise.all(entryPoints),
    bundle: true,
    outdir: outDir,
    plugins: [
      // We are aliasing the lib/lib.ts to the custom mocked file. The original files includes mostly a text that is used by
      // Monaco editor to provide standard lib typings as autocompletion in edit mode.
      // We can provide a custom empty fixture instead since we are using Monaco in the read-only mode.
      // https://github.com/microsoft/monaco-typescript/blob/main/src/lib/lib.ts
      // https://raw.githubusercontent.com/microsoft/monaco-typescript/main/src/lib/lib.ts
      alias({
        './lib/lib': path.resolve(root, 'fixtures/monaco-typescript/src/lib/lib.ts'),
      }),
    ],
    minify: true,
    metafile: true,
  });

  // Post-processing: Flatten the output and move file to the out directory
  const outputs = Object.entries(result.metafile.outputs);
  const outputsFileSize = {};

  for (const [outputFile, details] of outputs) {
    const fileName = path.basename(outputFile);

    await fs.rename(path.join(root, outputFile), path.join(outDir, fileName));

    outputsFileSize[fileName] = details.bytes;
  }

  // Clean-up and remove extra directories created by esbuild
  // https://esbuild.github.io/api/#outdir
  for (const [outputFile] of outputs) {
    const firstDir = outputFile.split(path.sep)[1]; // First dir is actually output dir
    const dir = path.join(outDir, firstDir);

    await fs.rm(dir, {
      recursive: true,
      force: true,
    });
  }

  // Output build information
  for (const [fileName, bytes] of Object.entries(outputsFileSize)) {
    const size = filesize(bytes, { standard: 'iec', round: 2 });

    console.log(fileName, size);
  }

  const delta = performance.now() - startTime;

  console.log(`✅ Done in ${(delta / 1000).toFixed(3)} second(s)`);
} catch (e) {
  console.error('❌ Error', e);

  process.exitCode = 1;
}
