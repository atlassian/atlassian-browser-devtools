const path = require('path');

module.exports = {
  // This config should inherit all the TS overrides from the base config.
  env: {
    node: true,
  },

  rules: {
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: true,

        // We need to point to the root package since the local one is used only to verify TypeScript compilation
        packageDir: [__dirname, path.resolve(__dirname, '..')],
      },
    ],
  },

  overrides: [
    // TypeScript overrides
    {
      files: ['**/*.ts'],
      rules: {
        // We don't need JDoc comments in native TypeScript files
        'jsdoc/require-jsdoc': 'off',

        // TypeScript's imports don't like to include the file extension
        'import/extensions': 'off',
      },
    },
  ],
};
