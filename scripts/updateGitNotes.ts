import { promises as fs } from 'fs';
import path from 'path';
import process from 'process';

import fg from 'fast-glob';
import { $ } from 'zx';
// eslint-disable-next-line import/no-unresolved
import { retry, expBackoff } from 'zx/experimental';

import { ROOT } from './paths';

// disable verbose logging
$.verbose = false;

async function main() {
  console.error('Adding reports into GIT notes...');

  try {
    // Get the reports
    const reportsPaths: string[] = await fg(['reports/*.json'], {
      cwd: ROOT,
      absolute: true,
    });

    const reports: [string, string][] = [];

    // Read the reports as JSON
    for (const reportPath of reportsPaths) {
      const reportName = path.basename(reportPath, path.extname(reportPath));
      const rawReport = await fs.readFile(reportPath);
      const report = JSON.parse(rawReport.toString());

      reports.push([reportName, report]);
    }

    // Store reports in git notes
    const allNotes = Object.fromEntries(reports);
    const stringifiedNotes = JSON.stringify(allNotes);

    // TODO: Add retries when this part fails
    await retry(10, expBackoff(), async () => {
      await $`git fetch origin "refs/notes/*:refs/notes/*"`;
      await $`git notes add -f -m ${stringifiedNotes}`;
      await $`git push origin "refs/notes/*"`;
    });

    console.error('✅ Successfully added reports into GIT notes');
  } catch (err) {
    console.error('❌ Failed storing reports in GIT notes', err);

    process.exitCode = 1;
  }
}

main();
