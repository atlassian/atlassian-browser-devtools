// This check ensures that the lock file version is correct. NPM 7+ uses lock file v2.

import { promises as fs } from 'fs';
import * as path from 'path';
import * as process from 'process';
import * as url from 'url';

try {
  const ROOT = path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), '..');

  const lockFilePath = path.join(ROOT, './package-lock.json');
  const content = await fs.readFile(lockFilePath);
  const lockFile = JSON.parse(content.toString());

  if (lockFile.lockfileVersion < 2) {
    console.error(
      '❌ The package-lock.json file was created with an older version of NPM. Please ensure you are using NPM 7+ and run `npm install` again.'
    );

    // @ts-expect-error exitCode is not readonly
    process.exitCode = 1;
  } else {
    console.log(`✅ The package-lock.json file was generated with the correct version of NPM.`);
  }
} catch (err) {
  const error = err as Error;
  console.error('❌ Error', error.message);

  // @ts-expect-error exitCode is not readonly
  process.exitCode = 1;
}
