const path = require('path');

const PATHS = {
  ROOT: path.resolve(__dirname, '..'),
  SRC: path.resolve(__dirname, '../src'),
  DIST: path.resolve(__dirname, '../dist'),
};

module.exports = PATHS;
