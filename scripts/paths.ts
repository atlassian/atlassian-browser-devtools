import * as path from 'path';

const __dirname = new URL('.', import.meta.url).pathname;

export const ROOT = path.resolve(__dirname, '..');
export const SRC = path.resolve(__dirname, '../src');
export const DIST = path.resolve(__dirname, '../dist');
