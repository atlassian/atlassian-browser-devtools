import { promises as fs } from 'fs';
import * as path from 'path';

import createDebug from 'debug';
import { filesize } from 'filesize';
import picocolors from 'picocolors';
import type { Compiler, Compilation, StatsChunk } from 'webpack';
import { $ } from 'zx';

import type { GitNotesBundleProviderOptions } from './gitNotesBundleReportProvider';
import gitNotesBundleReportProvider from './gitNotesBundleReportProvider';
import type { PrevBundleReportProvider } from './types';
import type { BundleEntrypointSize, BundleReport, BundleReportSizes, BundleNamedChunkSize, PrevBundleReport } from '../bundleTypes';

interface PluginOptions {
  reportLocation: string;
  logOutput: boolean;
  prevBundleReportProvider: PrevBundleReportProvider;
}

const debug = createDebug('bundle-guard:bundle-reporter:webpack-plugin');

// disable verbose logging
$.verbose = false;

export function getGitNotesBundleReportProvider(
  reportName: string,
  options?: Partial<GitNotesBundleProviderOptions>
): PrevBundleReportProvider {
  return () => gitNotesBundleReportProvider(reportName, options);
}

const JS_EXTS = new Set(['.js']);
const CSS_EXTS = new Set(['.css']);
const IMAGE_EXTS = new Set(['.png', '.gif', '.jpg', '.jpeg', '.svg', '.webp', '.apng', '.avif']);

class WebpackBundleReporterPlugin {
  private readonly reportLocation: string | null;
  private readonly logOutput: boolean;
  private readonly prevBundleReportProvider: PrevBundleReportProvider | undefined;
  private prevBundleReportPromise?: Promise<PrevBundleReport>;

  constructor(options: Partial<PluginOptions> = {}) {
    this.reportLocation = options.reportLocation ?? null;
    this.logOutput = options.logOutput ?? true;

    this.prevBundleReportProvider = options.prevBundleReportProvider;
  }

  // TODO: Extract me
  private static formatSize(size: number) {
    return filesize(size, { standard: 'iec' }) as string;
  }

  // TODO: Extract me
  private static formatSizeWithDelta(
    currentSize: BundleReportSizes,
    sizeType: keyof BundleReportSizes,
    prevSize?: BundleReportSizes
  ): string {
    const formattedSize = picocolors.bold(WebpackBundleReporterPlugin.formatSize(currentSize[sizeType]));

    if (!prevSize) {
      return formattedSize;
    }

    // Prev formatted
    const prevFormattedSize = WebpackBundleReporterPlugin.formatSize(prevSize[sizeType]);

    // Calculate delta
    const delta = currentSize[sizeType] - prevSize[sizeType];
    const didIncrease = delta > 0;
    const didDecrease = delta < 0;
    const formattedDelta = `${picocolors[didIncrease ? 'red' : didDecrease ? 'green' : 'reset'](
      picocolors.bold(`Δ ${didIncrease ? '+' : ''}${WebpackBundleReporterPlugin.formatSize(delta)}`)
    )}`;

    return `${formattedSize} ${formattedDelta} (prev ${picocolors.bold(prevFormattedSize)})`;
  }

  private static getEntrypointAssetsSizeByType(
    allAssets: Compilation['assets'],
    relevantAssets: Compilation['assets'] | StatsChunk['assets']
  ) {
    let js = 0;
    let css = 0;
    let image = 0;
    let total = 0;

    for (const asset of relevantAssets) {
      const extension = path.extname(asset.name);
      const size = allAssets[asset.name].size();

      if (JS_EXTS.has(extension)) {
        js += size;
      } else if (CSS_EXTS.has(extension)) {
        css += size;
      } else if (IMAGE_EXTS.has(extension)) {
        image += size;
      }

      total += size;
    }

    return { js, css, image, total };
  }

  private static getAllAssetsSize(allAssets: Compilation['assets']) {
    let js = 0;
    let css = 0;
    let image = 0;
    let total = 0;

    for (const [assetName, asset] of Object.entries(allAssets)) {
      const extension = path.extname(assetName);
      const size = asset.size();

      if (JS_EXTS.has(extension)) {
        js += size;
      } else if (CSS_EXTS.has(extension)) {
        css += size;
      } else if (IMAGE_EXTS.has(extension)) {
        image += size;
      }

      total += size;
    }

    return { js, css, image, total };
  }

  apply(compiler: Compiler) {
    this.prevBundleReportPromise =
      typeof this.prevBundleReportProvider === 'function' ? this.prevBundleReportProvider() : Promise.resolve(null);

    compiler.hooks.done.tapAsync('stats', async (stats, callback) => {
      const {
        compilation: { assets },
      } = stats;

      const statsJson = stats.toJson({
        all: false,
        entrypoints: true,
        chunkGroups: true,
      });

      const entrypoints = Array.from(stats.compilation.entrypoints.keys());
      const namedChunkGroups = Array.from(stats.compilation.namedChunkGroups.keys());

      const assetSizesByEntrypoint: BundleEntrypointSize[] = [];
      const assetSizesByNamedChunks: BundleNamedChunkSize[] = [];

      for (const entrypoint of entrypoints) {
        // Check if we have stats for this entrypoint
        if (!statsJson.entrypoints || !(entrypoint in statsJson.entrypoints)) {
          continue;
        }

        const entrypointAssets = statsJson.entrypoints[entrypoint].assets;

        const entryPointSize = WebpackBundleReporterPlugin.getEntrypointAssetsSizeByType(assets, entrypointAssets);

        assetSizesByEntrypoint.push({ entrypoint, ...entryPointSize });
      }

      for (const namedChunk of namedChunkGroups) {
        // Skip entrypoints
        if (entrypoints.includes(namedChunk)) {
          continue;
        }

        // Check if we have stats for this named chunk
        if (!statsJson.namedChunkGroups || !(namedChunk in statsJson.namedChunkGroups)) {
          continue;
        }

        const namedChunkAssets = statsJson.namedChunkGroups[namedChunk].assets;
        const namedChunkSize = WebpackBundleReporterPlugin.getEntrypointAssetsSizeByType(assets, namedChunkAssets);

        assetSizesByNamedChunks.push({ namedChunk, ...namedChunkSize });
      }

      // calculate the overall assets size by type
      const totalSize = WebpackBundleReporterPlugin.getAllAssetsSize(assets);

      const report: BundleReport = {
        location: 'webpack-bundle',
        entrypoints: assetSizesByEntrypoint,
        namedChunks: assetSizesByNamedChunks,
        totalSize,
      };

      // Output the simplified report
      if (this.logOutput) {
        const prevBundleReport = await this.prevBundleReportPromise;

        debug('Previous Bundle Report', prevBundleReport);

        const beautifiedReport = [
          {
            title: 'Total assets size',
            value: WebpackBundleReporterPlugin.formatSizeWithDelta(totalSize, 'total', prevBundleReport?.totalSize),
          },
          {
            title: 'Total JS assets size',
            value: WebpackBundleReporterPlugin.formatSizeWithDelta(totalSize, 'js', prevBundleReport?.totalSize),
          },
          {
            title: 'Total CSS assets size',
            value: WebpackBundleReporterPlugin.formatSizeWithDelta(totalSize, 'css', prevBundleReport?.totalSize),
          },
          {
            title: 'Total images size',
            value: WebpackBundleReporterPlugin.formatSizeWithDelta(totalSize, 'image', prevBundleReport?.totalSize),
          },
        ];

        console.log(picocolors.bold('Bundle size report'));
        if (prevBundleReport) {
          console.log(picocolors.dim(`Previous bundle sizes are provided by: ${prevBundleReport.location}`));
        }
        console.log(beautifiedReport.map(({ title, value }) => `${title}: ${value}`).join('\n'));
        console.log();
      }

      // Save report to a file
      if (this.reportLocation) {
        const dirName = path.dirname(this.reportLocation);

        // Check if output directory exists
        try {
          await fs.access(dirName);
        } catch (e) {
          await fs.mkdir(dirName, {
            recursive: true,
          });
        }

        // Dump the report to a file
        await fs.writeFile(this.reportLocation, JSON.stringify(report, null, 2), 'utf8');
      }

      callback();
    });
  }
}

export default WebpackBundleReporterPlugin;
