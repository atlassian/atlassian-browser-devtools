import createDebug from 'debug';
import { $ } from 'zx';

import type { PrevBundleReportProvider } from './types';
import type { BundleReport } from '../bundleTypes';

// disable verbose logging
$.verbose = false;

const debug = createDebug('bundle-guard:reporter:prev-bundle-reporter');

export interface GitNotesBundleProviderOptions {
  gitDefaultBranchName: string;
  gitDefaultRemoteName: string;
  gitFetchDefaultBranch: boolean;
  gitFetchNotes: boolean;
  gitNotesRef: string;
}

const defaultOptions: Partial<GitNotesBundleProviderOptions> = {
  gitDefaultBranchName: 'master',
  gitDefaultRemoteName: 'origin',
  gitNotesRef: 'refs/notes/*:refs/notes/*',
};

const gitNotesBundleReportProvider: PrevBundleReportProvider = async (
  reportName: string,
  options?: Partial<GitNotesBundleProviderOptions>
): ReturnType<PrevBundleReportProvider> => {
  try {
    const {
      gitDefaultBranchName,
      gitDefaultRemoteName,
      gitNotesRef,
      gitFetchDefaultBranch,
      gitFetchNotes,
    }: Partial<GitNotesBundleProviderOptions> = {
      ...defaultOptions,
      ...options,
    };

    // Fetch default remote branch and notes only when configured or when we are running on CI
    const currentBranch = (await $`git rev-parse --abbrev-ref HEAD`).toString().trim();
    const isOnDefaultBranch = currentBranch === gitDefaultBranchName;

    debug('Is on default branch', {
      currentBranch,
      isOnDefaultBranch,
    });

    const shouldFetchDefaultBranch: boolean = gitFetchDefaultBranch !== undefined ? gitFetchDefaultBranch : Boolean(process.env.CI);
    const shouldFetchGitNotes: boolean = gitFetchNotes !== undefined ? gitFetchNotes : Boolean(process.env.CI);

    if (shouldFetchDefaultBranch && !isOnDefaultBranch) {
      debug('Fetching GIT default branch', {
        gitDefaultRemoteName,
        gitDefaultBranchName,
      });

      await $`git fetch ${gitDefaultRemoteName} ${gitDefaultBranchName}:${gitDefaultBranchName}`;
    }

    if (shouldFetchGitNotes) {
      debug('Fetching GIT notes', {
        gitDefaultRemoteName,
        gitNotesRef,
      });

      await $`git fetch ${gitDefaultRemoteName} ${gitNotesRef}`;
    }

    // Find the parent commit hash
    let parentCommitHash;
    // We should be using "${gitDefaultRemoteName}/${gitDefaultBranchName}" but we can't due to the Pipelines limitation
    const defaultBranchRef = `${gitDefaultBranchName}`;

    if (isOnDefaultBranch) {
      // When we are running Pipelines on the default branch, we assume we just merged the changes, so let's use the commit of the parent branch
      parentCommitHash = (await $`git rev-parse ${defaultBranchRef}^1`).toString().trim();
    } else {
      const gitCommonAncestorHash = (await $`git merge-base ${defaultBranchRef} HEAD`).toString().trim();

      debug(`Found hash of a GIT common ancestor between default and the current branch`, {
        defaultBranchRef,
        gitCommonAncestorHash,
      });

      parentCommitHash = gitCommonAncestorHash;
    }

    debug('Found parent commit hash', { parentCommitHash });

    const gitNotesRaw = (await $`git notes show ${parentCommitHash}`).toString().trim();

    debug(`Found GIT notes`, gitNotesRaw);

    const reports = JSON.parse(gitNotesRaw);

    if (!(reportName in reports)) {
      throw new Error(`No report named ${reportName} found in git notes`);
    }

    const bundleReport = reports[reportName] as BundleReport;
    bundleReport.location = `git notes (${gitDefaultBranchName}) ${parentCommitHash.toString().substring(0, 7)}`;

    return reports[reportName];
  } catch (err) {
    const error = err as Error;
    debug('Error while fetching previous bundle report with GIT notes', error);

    return null;
  }
};

export default gitNotesBundleReportProvider;
