import type { PrevBundleReport } from '../bundleTypes';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type PrevBundleReportProvider = (...args: any[]) => Promise<PrevBundleReport>;
