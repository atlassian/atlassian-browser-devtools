export interface BundleReportSizes {
  js: number;
  css: number;
  image: number;
  total: number;
}

export interface BundleEntrypointSize extends BundleReportSizes {
  entrypoint: string;
}

export interface BundleNamedChunkSize extends BundleReportSizes {
  namedChunk: string;
}

export interface BundleReport {
  location: string;
  entrypoints: BundleEntrypointSize[];
  namedChunks: BundleNamedChunkSize[];
  totalSize: BundleReportSizes;
}

export type PrevBundleReport = BundleReport | null;
