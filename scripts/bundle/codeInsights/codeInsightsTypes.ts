export type CodeInsightsData =
  | {
      type: 'BOOLEAN';
      title: string;
      value: boolean;
    }
  | {
      type: 'NUMBER' | 'DATE' | 'DURATION' | 'PERCENTAGE';
      title: string;
      value: number;
    }
  | {
      type: 'TEXT';
      title: string;
      value: string;
    }
  | {
      type: 'LINK';
      title: string;
      value: string;
    };

export interface CodeInsightsReport {
  uuid?: string;
  title: string;
  details: string;
  report_type: 'SECURITY' | 'COVERAGE' | 'TEST' | 'BUG';
  external_id?: string;
  reporter?: string;
  link?: string;
  remote_link_enabled?: true;
  logo_url?: string;
  result?: 'PASSED' | 'FAILED' | 'PENDING';
  data?: CodeInsightsData[];
  created_on?: string;
  updated_on?: string;
}

// https://support.atlassian.com/bitbucket-cloud/docs/code-insights/#Annotations
export interface CodeInsightsAnnotation {
  uuid?: string;
  annotation_type: 'VULNERABILITY' | 'CODE_SMELL' | 'BUG';
  summary: string;
  external_id?: string;
  path?: string;
  line?: number;
  details?: string;
  result?: 'PASSED' | 'FAILED' | 'SKIPPED' | 'IGNORED';
  severity?: 'CRITICAL' | 'HIGH' | 'MEDIUM' | 'LOW';
  link?: string;
  created_on?: string;
  updated_on?: string;
}

export type CodeInsightsAnnotations = CodeInsightsAnnotation[];
