import type { BitbucketEnvVariables } from './bibucketPipelines';
import { CODE_INSIGHTS_REPORT_ID } from './bundleReport';
import type { CodeInsightsAnnotations } from './codeInsightsTypes';
import { createBundleAnnotations } from './createBundleAnnotations';
import { sendCodeInsightsAnnotations } from './sendAnnotations';
import type { BundleReport, PrevBundleReport } from '../bundleTypes';

export async function sendBundleEntrypointsAnnotations(
  bitbucketEnvVariables: BitbucketEnvVariables,
  bundleReport: BundleReport,
  prevBundleReport: PrevBundleReport
): Promise<CodeInsightsAnnotations> {
  const annotations = createBundleAnnotations(bundleReport, prevBundleReport);

  return sendCodeInsightsAnnotations(bitbucketEnvVariables, CODE_INSIGHTS_REPORT_ID, annotations);
}
