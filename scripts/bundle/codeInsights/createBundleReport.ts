import type { BitbucketEnvVariables } from './bibucketPipelines';
import { CODE_INSIGHTS_REPORT_NAME, CODE_INSIGHTS_REPORTER_NAME } from './bundleReport';
import type { CodeInsightsReport, CodeInsightsData } from './codeInsightsTypes';
import { formatSize, formatSizeWithDelta } from './formatters';
import type { BundleReport, PrevBundleReport } from '../bundleTypes';

const getPipelineStepUrl = (bitbucketRepoFullName: string, bitbucketBuildNumber: string, bitbucketPipelineUuid: string): string =>
  new URL(
    `https://bitbucket.org/${bitbucketRepoFullName}/pipelines/results/${bitbucketBuildNumber}/steps/${encodeURI(bitbucketPipelineUuid)}`
  ).toString();

type CodeInsightsDataText = Extract<CodeInsightsData, { type: 'TEXT' }>;

const getCodeInsightsDataText = (title: string, value: string): CodeInsightsDataText => ({
  type: 'TEXT',
  title,
  value,
});

export function createBundleCodeInsightsReport(
  { bitbucketRepoFullName, bitbucketPipelineBuildNumber, bitbucketPipelineStepUuid }: BitbucketEnvVariables,
  bundleReport: BundleReport,
  prevBundleReport: PrevBundleReport
): CodeInsightsReport {
  const { totalSize } = bundleReport;
  const prevTotalSize = prevBundleReport?.totalSize;

  const isCodeInsightsData = function (data: CodeInsightsDataText | undefined): data is CodeInsightsDataText {
    return data !== undefined;
  };

  const data: CodeInsightsDataText[] = [
    getCodeInsightsDataText('Bundle size', formatSizeWithDelta(totalSize, 'total', prevTotalSize)),
    prevTotalSize ? getCodeInsightsDataText('Prev. bundle size', formatSize(prevTotalSize.total)) : undefined,

    getCodeInsightsDataText('JS files size', formatSizeWithDelta(totalSize, 'js', prevTotalSize)),
    prevTotalSize ? getCodeInsightsDataText('Prev. JS files size', formatSize(prevTotalSize.js)) : undefined,

    getCodeInsightsDataText('CSS files size', formatSizeWithDelta(totalSize, 'css', prevTotalSize)),
    prevTotalSize ? getCodeInsightsDataText('Prev. CSS files size', formatSize(prevTotalSize.css)) : undefined,

    getCodeInsightsDataText('Images size', formatSizeWithDelta(totalSize, 'image', prevTotalSize)),
    prevTotalSize ? getCodeInsightsDataText('Prev. image size', formatSize(prevTotalSize.image)) : undefined,
  ].filter(isCodeInsightsData);

  return {
    title: CODE_INSIGHTS_REPORT_NAME,
    details: prevBundleReport ? `Previous bundle sizes are provided by: ${prevBundleReport.location}` : `Previous bundle sizes are missing`,
    reporter: CODE_INSIGHTS_REPORTER_NAME,
    report_type: 'COVERAGE',
    result: 'PASSED',
    data,
    link: getPipelineStepUrl(bitbucketRepoFullName, bitbucketPipelineBuildNumber, bitbucketPipelineStepUuid),
  };
}
