import createHttpProxyAgent from 'http-proxy-agent';
import type { Response } from 'node-fetch';
import fetch from 'node-fetch';

import type { BitbucketEnvVariables } from './bibucketPipelines';
import type { CodeInsightsReport } from './codeInsightsTypes';
import { debug } from './debug';

export async function sendCodeInsightsReport(
  { bitbucketRepoFullName, bitbucketCommitHash }: BitbucketEnvVariables,
  reportId: string,
  codeInsightsReport: CodeInsightsReport
): Promise<CodeInsightsReport> {
  // https://support.atlassian.com/bitbucket-cloud/docs/code-insights/#Authentication
  // https://developer.atlassian.com/cloud/bitbucket/rest/api-group-reports/#api-repositories-workspace-repo-slug-commit-commit-reports-reportid-put
  const url = `https://api.bitbucket.org/2.0/repositories/${bitbucketRepoFullName}/commit/${bitbucketCommitHash}/reports/${reportId}`;

  debug('Code Insights url', url);
  debug('Code Insights report', codeInsightsReport);

  let response: Response;

  try {
    response = await fetch(url, {
      method: 'PUT',
      body: JSON.stringify(codeInsightsReport),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      // Yes, we need to use HTTP not HTTPS to send the request with proxy locally inside the Pipeline
      // https://support.atlassian.com/bitbucket-cloud/docs/push-back-to-your-repository/
      // https://support.atlassian.com/bitbucket-cloud/docs/code-insights/#Authentication
      agent: createHttpProxyAgent({
        host: 'localhost',
        protocol: 'http',
        secureProxy: false,
        port: 29418,
      }),
    });

    if (!response.ok) {
      // eslint-disable-next-line @typescript-eslint/no-throw-literal
      throw response;
    }

    if (response.status !== 200 && response.status !== 201) {
      // eslint-disable-next-line @typescript-eslint/no-throw-literal
      throw response;
    }
  } catch (e) {
    response = e as Response;

    throw new Error(`${response.status} ${response.statusText} ${await response.text()}`);
  }

  const reportResponse = (await response.json()) as CodeInsightsReport;

  debug('Code Insights report was created', reportResponse);

  return reportResponse;
}
