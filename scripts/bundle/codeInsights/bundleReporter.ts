import process from 'process';

import { $ } from 'zx';

import { getBitbucketEnvVariables } from './bibucketPipelines';
import { debug } from './debug';
import { sendBundleEntrypointsAnnotations } from './sendBundleEntrypointsAnnotations';
import { sendBundleReport } from './sendBundleReport';
import type { BundleReport } from '../bundleTypes';
import gitNotesBundleReportProvider from '../webpackPlugin/gitNotesBundleReportProvider';

// We need to disable the too aggressive escaping of the command parameters
// https://github.com/google/zx/issues/164
$.quote = (s: string) => s;

// disable verbose logging
$.verbose = false;

const GIT_NOTES_BUNDLE_REPORT_NAME = 'webpack-bundle-report';

interface ReporterOptions {
  gitDefaultBranchName: string;
  gitDefaultRemoteName: string;
  gitNotesRef: string;
}

const defaultOptions: ReporterOptions = {
  gitDefaultBranchName: 'master',
  gitDefaultRemoteName: 'origin',
  gitNotesRef: 'refs/notes/*:refs/notes/*',
};

async function main() {
  console.error('Creating Build report for Code Insights...');

  const { gitDefaultRemoteName, gitDefaultBranchName, gitNotesRef }: ReporterOptions = {
    // options
    ...defaultOptions,
  };

  try {
    const bitbucketEnvVariables = getBitbucketEnvVariables();

    await $`git fetch ${gitDefaultRemoteName} ${gitNotesRef}`;
    const gitNotesRaw = await $`git notes show HEAD`;
    const reports = JSON.parse(gitNotesRaw.toString().trim());

    if (!(GIT_NOTES_BUNDLE_REPORT_NAME in reports)) {
      throw new Error(`No report named ${GIT_NOTES_BUNDLE_REPORT_NAME} found in git notes`);
    }

    const bundleReport = reports[GIT_NOTES_BUNDLE_REPORT_NAME] as BundleReport;
    debug('Bundle Report', bundleReport);

    const prevBundleReport = await gitNotesBundleReportProvider(GIT_NOTES_BUNDLE_REPORT_NAME, {
      gitDefaultRemoteName,
      gitDefaultBranchName,
      gitNotesRef,
      gitFetchDefaultBranch: true,
      gitFetchNotes: false,
    });

    debug('Previous Bundle Report', prevBundleReport);

    await sendBundleReport(bitbucketEnvVariables, bundleReport, prevBundleReport);
    await sendBundleEntrypointsAnnotations(bitbucketEnvVariables, bundleReport, prevBundleReport);

    console.error('✅ Successfully created Code Insights report');
  } catch (err) {
    console.error('❌ Failed preparing and creating Code Insights report', err);

    process.exitCode = 1;
  }
}

main();
