export const CODE_INSIGHTS_REPORT_ID = 'bundle-guard-webpack-bundle-report';
export const CODE_INSIGHTS_REPORT_NAME = 'Webpack bundle size report';
export const CODE_INSIGHTS_REPORTER_NAME = 'Bundle Guard';
