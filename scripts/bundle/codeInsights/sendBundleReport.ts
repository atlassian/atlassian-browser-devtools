import type { BitbucketEnvVariables } from './bibucketPipelines';
import { CODE_INSIGHTS_REPORT_ID } from './bundleReport';
import type { CodeInsightsReport } from './codeInsightsTypes';
import { createBundleCodeInsightsReport } from './createBundleReport';
import { sendCodeInsightsReport } from './sendReport';
import type { BundleReport, PrevBundleReport } from '../bundleTypes';

export async function sendBundleReport(
  bitbucketEnvVariables: BitbucketEnvVariables,
  bundleReport: BundleReport,
  prevBundleReport: PrevBundleReport
): Promise<CodeInsightsReport> {
  const codeInsightsReport = createBundleCodeInsightsReport(bitbucketEnvVariables, bundleReport, prevBundleReport);

  return sendCodeInsightsReport(bitbucketEnvVariables, CODE_INSIGHTS_REPORT_ID, codeInsightsReport);
}
