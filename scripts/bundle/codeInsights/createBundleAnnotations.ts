import type { CodeInsightsAnnotation, CodeInsightsAnnotations } from './codeInsightsTypes';
import { formatSize, formatSizeWithDelta } from './formatters';
import type { BundleEntrypointSize, BundleNamedChunkSize, BundleReport, BundleReportSizes, PrevBundleReport } from '../bundleTypes';

// TODO: Refactor common parts with named chunks
const findPrevEntrypointSizes = (entrypointName: string, prevBundleReport: PrevBundleReport): BundleEntrypointSize | undefined => {
  if (!prevBundleReport) {
    return undefined;
  }

  const foundEntrypoint = prevBundleReport.entrypoints.find((entrypointSize) => {
    return entrypointSize.entrypoint === entrypointName;
  });

  return foundEntrypoint;
};

// TODO: Refactor common parts with entrypoints
const findPrevChunkSizes = (chunkName: string, prevBundleReport: PrevBundleReport): BundleNamedChunkSize | undefined => {
  if (!prevBundleReport) {
    return undefined;
  }

  const foundNamedChunk = prevBundleReport.namedChunks.find((entrypointSize) => {
    return entrypointSize.namedChunk === chunkName;
  });

  return foundNamedChunk;
};

const getCombinedSizes = (sizes: BundleReportSizes): string[] => {
  return [`JS files size: ${formatSize(sizes.js)}`, `CSS files size: ${formatSize(sizes.css)}`, `Images size: ${formatSize(sizes.image)}`];
};

const combineSizes = (...sizes: string[]): string => {
  return sizes.join(' | ');
};

export function createBundleAnnotations(bundleReport: BundleReport, prevBundleReport: PrevBundleReport): CodeInsightsAnnotations {
  const { entrypoints, namedChunks } = bundleReport;

  // TODO: Refactor common parts with named chunks
  const entrypointsAnnotations: CodeInsightsAnnotations = entrypoints.map(({ entrypoint, ...totalSize }) => {
    const prevTotalSizes = findPrevEntrypointSizes(entrypoint, prevBundleReport);
    const entryPointSummary = `Entrypoint "${entrypoint}" size: ${formatSizeWithDelta(totalSize, 'total', prevTotalSizes, true)}`;

    const summary = combineSizes(entryPointSummary, ...getCombinedSizes(totalSize));
    const details: string = prevTotalSizes
      ? combineSizes(`Previous entrypoint size: ${formatSize(prevTotalSizes.total)}`, ...getCombinedSizes(prevTotalSizes))
      : 'No previous entrypoint size found';

    const annotation: CodeInsightsAnnotation = {
      annotation_type: 'CODE_SMELL',
      external_id: `entrypoint:${entrypoint}`,
      summary,
      details,
    };

    return annotation;
  });

  // TODO: Refactor common parts with entrypoints
  const namedChunksAnnotations: CodeInsightsAnnotations = namedChunks.map(({ namedChunk, ...totalSize }) => {
    const prevChunkSizes = findPrevChunkSizes(namedChunk, prevBundleReport);
    const chunkSummary = `Chunk "${namedChunk}" size: ${formatSizeWithDelta(totalSize, 'total', prevChunkSizes, true)}`;

    const summary = combineSizes(chunkSummary, ...getCombinedSizes(totalSize));
    const details: string = prevChunkSizes
      ? combineSizes(`Previous chunk size: ${formatSize(prevChunkSizes.total)}`, ...getCombinedSizes(prevChunkSizes))
      : 'No previous chunk size found';

    const annotation: CodeInsightsAnnotation = {
      annotation_type: 'CODE_SMELL',
      external_id: `chunk:${namedChunk}`,
      summary,
      details,
    };

    return annotation;
  });

  return [...entrypointsAnnotations, ...namedChunksAnnotations];
}
