import { filesize } from 'filesize';

import type { BundleReportSizes } from '../bundleTypes';

export const formatSize = (size: number): string => filesize(size, { standard: 'iec' }) as string;

export function formatSizeWithDelta(
  currentSize: BundleReportSizes,
  sizeType: keyof BundleReportSizes,
  prevSize?: BundleReportSizes,
  showPercentage = false
): string {
  const formattedSize = formatSize(currentSize[sizeType]);

  if (!prevSize) {
    return formattedSize;
  }

  // Calculate delta
  const delta = currentSize[sizeType] - prevSize[sizeType];
  const didIncrease = delta > 0;
  const didDecrease = delta < 0;
  const changePrefix = didIncrease ? '+' : didDecrease ? '-' : '';

  const percentageChange = (Math.abs(delta) / prevSize[sizeType]) * 100;

  const formattedDelta = `Δ ${changePrefix}${formatSize(Math.abs(delta))}`;
  const formattedPercentage = `(${changePrefix}${percentageChange == 0 ? 0 : percentageChange.toFixed(2)}%)`;

  return `${formattedSize} ${formattedDelta} ${showPercentage ? formattedPercentage : ''}`;
}
