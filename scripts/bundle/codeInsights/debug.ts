import createDebug from 'debug';

export const debug = createDebug('bundle-guard:reporter:code-insights');
