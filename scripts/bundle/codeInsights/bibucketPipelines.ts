import process from 'process';

export interface BitbucketEnvVariables {
  bitbucketRepoFullName: string;
  bitbucketCommitHash: string;
  bitbucketPipelineBuildNumber: string;
  bitbucketPipelineStepUuid: string;
}

export const getBitbucketEnvVariables = (): BitbucketEnvVariables => {
  // https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/
  const { BITBUCKET_REPO_FULL_NAME, BITBUCKET_COMMIT, BITBUCKET_BUILD_NUMBER, BITBUCKET_STEP_UUID } = process.env;

  // TODO: Ensure the above variables are present during the build

  /* eslint-disable @typescript-eslint/no-non-null-assertion */
  const bitbucketEnvVariables: BitbucketEnvVariables = {
    bitbucketRepoFullName: BITBUCKET_REPO_FULL_NAME!,
    bitbucketCommitHash: BITBUCKET_COMMIT!,
    bitbucketPipelineBuildNumber: BITBUCKET_BUILD_NUMBER!,
    bitbucketPipelineStepUuid: BITBUCKET_STEP_UUID!,
  };
  /* eslint-enable @typescript-eslint/no-non-null-assertion */

  return bitbucketEnvVariables;
};
