// @ts-nocheck
/* eslint-env node */

const fs = require('fs');
const path = require('path');
const htmlWebpackTemplate = path.resolve(__dirname, './src/htmlWebpackTemplate.html');

/**
 * @param {string} entrypoint
 * @returns {[string, string]}
 */
function includeSentry(entrypoint) {
  const entryPointDir = path.dirname(entrypoint);
  const sentryEntryPoint = path.join(entryPointDir, 'sentry.js');

  if (!fs.existsSync(sentryEntryPoint)) {
    console.error(`The sentry file "${sentryEntryPoint}" for "${entrypoint}" entrypoint does not exists. Please create it.`);
    process.exit(1);
  }

  return [sentryEntryPoint, entrypoint];
}

const htmlWebpackPluginConfig = ({ filename, chunks, title, prependScripts }) => ({
  filename,
  chunks,
  title,
  template: htmlWebpackTemplate,
  appMountId: 'root',
  prependScripts,
  inject: false,
  minify: {
    useShortDoctype: true,
    keepClosingSlash: true,
    collapseWhitespace: true,
    preserveLineBreaks: true,
  },
});

module.exports = {
  htmlWebpackPluginConfig,
  includeSentry,
};
