# Contributing to Atlassian Browser DevTools

## Prerequisites

All guides in the document requires having a proper Node and NPM versions installed in your local environment.
You will need the following tools in your development environment:

- Node 16+ (you can use `nvm`)
- Chrome browser

You can use the [`nvm`][nvm] command to install the required version of Node.

The project is using:

- [TypeScript with JSDoc][ts-jsdoc]
- [webpack][webpack]
- [Conventional Commits][conventional-commits]
- [Conventional Changelog][conventional-changelog]

You should get familiar with those tools before doing any changes in the repository or before creating a release.

## Getting started

In the root folder, run `npm install`. This will install all the required dependencies.

### Linux

On Linux based OS you might have to install PNG related libs before you run `npm install`.

If you have an error related to the `pngquant` NPM package run this command firts:

```
sudo apt-get install -y build-essential gcc make pkg-config libpng-dev libimagequant-dev
```

Once the required libs are installed try again with running `npm install`.

## Development

### Running extension locally

There are two ways you can run the extension locally:

- as a browser extension with a **watch mode**
- as a regular webp page in **dev mode**

The difference between the **watch mode** and **dev mode** are:

- in **watch mode** you have access to all the browser extensions APIs. You are running a regular browser extension.
- in **dev mode** you don't have access to any of the browser or the extension APIs. The browser extension APIs are only stubbed, so the app can work but it won't be able to simulate any browser <-> extension communication.

### Running extension with a watch mode

1. Run the build step `npm start` and wait for the webpack to finish.
2. Open Chrome and navigate to `chrome://extensions` page.
3. Toggle the Developer mode on top right corner. ![extensions-developer-mode](./guides/extensions-developer-mode.png)
4. Click on the "**Load unpacked**" button from top toolbar, navigate to the project root location on your disk and select "dist" directory.
5. As a result you should see a new extension tile with the information about unpacked extension: ![unpacked extension](./guides/unpacked-extension.png)
6. Now, to check if the extension was loaded correctly. You can navigate to [https://jira.atlassian.com/browse/BSERV](https://jira.atlassian.com/browse/BSERV) and open Chrome Devtools.
7. At this point the Devtools should have a new tab called "ABD (⚠️ local version)" that you can click on.
8. If you don't see a new tab, please check if the extension was loaded correctly.

The ABD devtools panel should look like this:

![abd panel](./guides/abd-console-panel-watch-mode.png)

### Rebuilding extension locally with watch mode

Developing Chrome extensions is not the easiest task and dev loop is not developer friendly. You will have to manually reload the unpacked extension after every time you made change in the code.

There is a special extension called [Extensions Reloader](https://chrome.google.com/webstore/detail/extensions-reloader/fimgfedafeadlieiabdeeaodndnlbhid?hl=en)
that can help you... reloading extensions.

1. Run the `npm start` command to start the webpack in watch mode.
2. Make a desired change in the code and save the changes.
3. Wait for webpack to rebuild your changes.
4. Inside the browser click on the "**Reload Extensions**" button ![reload button](./guides/reload-button.png) and wait to see the "OK" label:
   ![reload button](./guides/reload-button-reloaded.png)

5. If the browser devtools are opened you will have to reload the working ABD extension panel by opening the context menu and selecting "**Reload Frame**" option:
   ![reload extension](./guides/reload-panel.png)
6. If the reload option is missing, close and reopen the browser devtools.

7. Repeat steps 3-6 every time you make and save the changes.

### Connecting React Devtools

The ABD Devtools panels is build using React. Unfortunately, opening a Devtools for ABD Devtools will not show you React Devtools.

To address that problem you can run external React devtools.

1. Install the `react-devtools` NPM package globally with `npm install -g react-devtools`.
2. Inside the project root run the `npm start` command to start the webpack in watch mode.
3. In a new terminal window run `npx react-devtools` to start external React Devtools app.
4. Now, open some page in the browser that you want to debug e.g. [https://jira.atlassian.com/browse/BSERV](https://jira.atlassian.com/browse/BSERV)
5. Open browser devtools and select the "ABD (⚠️ local version)" tab.
6. Finally, focus on the **React DevTools** window and check if the DevTools are showing you React components tree: ![reload extension](./guides/react-devtools.png)

In case the React DevTools shows you the **Waiting for React to connect...** message, you can try reloading the working ABD extension panel by opening the context menu and selecting "**Reload Frame**" option: ![reload extension](./guides/reload-panel.png)

### Running extension in dev mode

The project setup allows you to run an extension devtools panel shell in dev mode. The running application in dev mode will **not be able** to access or communicate with the native Chrome extensions APIs or DevTools Console API.

However, this might be enough for you to use the ABD devtools panel. To run the extension app in dev mode follow the instructions:

1. Run the `npm run dev` command to start a webpack dev server.
2. In the browser navigate to [http://0.0.0.0:3000/](http://0.0.0.0:3000/)
3. This page will render the content of ABD devtools panel and allow you to make changes in the code without needing to manually reload the page.

### Running extension in Firefox

TBD

## Release

The release process is manual and requires creating a zip artifact that needs to be uploaded to the [Chrome Web Store][chrome-web-store] and [Firefox Add-ons Store][firefox-addon-store].

### Prepare release

To prepare the release, bump the version of the extension, and create a zip artifact you need to run the following commands:

```sh
npm run release:prepare
```

### Publishing to Chrome Web Store and Firefox Add-ons Store

Login to the [Chrome Web Store][chrome-web-store] and [Firefox Add-ons Store][firefox-addon-store] to publish a new version.

#### Creating a source zip

The Firefox Add-ons Store requires a source zip file. To create a zip file you need to run the following command:

```sh
git archive --format zip -o abd-source.zip HEAD . ":!docs" ":!guides" ":!screenshots"
```

We can ignore the `/docs` and `/guides` directories from the source archive since those include a big size files like videos and images.

[nvm]: https://github.com/nvm-sh/nvm
[webpack]: https://webpack.js.org/
[conventional-commits]: https://www.conventionalcommits.org/en/v1.0.0/
[conventional-changelog]: https://github.com/conventional-changelog/conventional-changelog
[chrome-web-store]: https://chrome.google.com/webstore/detail/atlassian-browser-devtool/ifdlelpkchpppgmhmidenfmlhdafbnke?hl=en
[firefox-addon-store]: https://addons.mozilla.org/en-US/firefox/addon/atlassian-browser-devtools/
[ts-jsdoc]: https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html
