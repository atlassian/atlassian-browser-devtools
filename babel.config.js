/* eslint-env node */
module.exports = (api) => {
  api.cache(true);

  return {
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            browsers: ['last 2 chrome versions', 'last 2 firefox versions'],
          },
          useBuiltIns: false,
        },
      ],
      [
        '@babel/preset-react',
        {
          runtime: 'automatic',
        },
      ],
    ],
    plugins: ['@babel/plugin-proposal-class-properties'],
  };
};
