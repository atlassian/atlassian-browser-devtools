# Atlassian Browser DevTools (ABD)

A browser extension for Developer Tools that helps you inspect and debug the Frontend code of Atlassian products.
The extension adds a new "ABD" tab to the browser devtools.

The ABD extension provides an addition to the browser Developer Tools that can help you inspect and debug Atlassian core products and plugins:

- [**AUI Deprecations Inspector**](#aui-deprecations-inspector)
- [**WRM Inspector**](#wrm-inspector)
- [**WRM Coverage Browser**](#wrm-coverage-browser)
- [**WRM Coverage Recorder**](#wrm-coverage-recorder) (**added in ABD v2.8**)

## Installation

You can download and install the extension for your browsers:

- [Chrome extension](https://chrome.google.com/webstore/detail/atlassian-browser-devtools/ifdlelpkchpppgmhmidenfmlhdafbnke)
- [Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/atlassian-browser-devtools/)

## How to use it?

1.  Install the extension for your browser using one of the provided links.
2.  Go to any page that is built with the [AUI library](https://aui.atlassian.com/) or is using [Web-Resource Manager (WRM)](http://go/wrm-docs).
3.  Open the Developer Tools by using the action from the browser menu or pressing the context menu and selecting the **Inspect** option.
4.  From the Developer Tools panel, select the **ABD** tab.

## AUI Deprecations Inspector

![AUI Deprecations Inspector](./docs/aui-depreciations-inspector.gif)

The AUI Deprecations Inspector can help you discover the usage of deprecated HTML and CSS patterns from the [AUI library](https://docs.atlassian.com/aui).

### How to use it

After opening the Developer Tools and selecting the **ABD** tab, click on the **AUI Deprecations Inspector** item.
Inside the panel you might see the result of the **AUI deprecations scanner**. The panel will display the new deprecations whenever the page is using deprecated HTML or CSS AUI patterns.

You can filter and persist the deprecation results by using actions from the toolbar.

The depreciation item contains four information:

- Deprecation message with optional hints of what you can do next.
- Deprecation level (background): **Error** (red) or **Warning** (yellow).
- In what version of AUI, a pattern was deprecated. The text is also a link to the AUI documentation page and/or upgrade guide.
- The HTML node that you can hover the mouse on to see the element highlighted on the page. You can also click on the element, and the page will scroll to the place where an HTML node is visible.

### AUI

If the page is using **AUI**, you should be able to see the information about the version on top of the panel:

![AUI Version](./docs/aui-version.png)

### Deprecation levels

There are two deprecation levels:

- **Error** (red)

  ![Error](./docs/aui-depreciations-error.png)

- **Warning** (yellow)

  ![Warning](./docs/aui-depreciations-warning.png)

If the panels contains the **Error** deprecation, it means that the HTML or CSS pattern was deprecated in the current version of AUI that you are using on the page.

The **Warning** level tells you that this pattern will be deprecated in the newer version of AUI (after upgrading AUI). It helps you to prepare and fix your application before upgrading the AUI library to the latest version.

## WRM Inspector

The WRM Inspector panel will display a list of all the web-resources that are loaded on the current page by the **Web Resource Manager**.
The list can be filtered and sorted to understand better what was loaded on the page.

![WRM Inspector](./docs/wrm-inspector.gif)

### Summary Panel

By clicking on the web-resource item, you will open a summary panel below the table. The summary panel contains all the information about the web-resource including:

- **Resource key** - The name of web-resource key that can be used to locate the resource in application source code
- **Resource file** - The name of the file that can be used to locate the resource in the application source code
- **Batch type** - e.g. CSS or JS
- **Loaded by context** - if the resource was loaded using a context
- **Context(s)** - What kind of context was used to load the resource
- **Batch URL** - URL to the batch file that loaded the web-resource
- **Resource size** - the size of the resource

![Summary Panel](./docs/wrm-inspector-summary-panel.png)

### Dependency Tree

The **Dependency Tree** tab will display the stack of all the dependencies for the selected web-resource.
This information can help you discover why the selected web-resource was loaded on the page.

![Summary Panel](./docs/wrm-inspector-dependency-tree.png)

## WRM Coverage Browser

The **WRM Coverage Browser** panel can help you find out what is the actual usage of all the web-resources loaded on the page.

### How to use it?

To display the WRM coverage, first you will have to collect the coverage report from the Chrome Devtools:

1. Inside the Developer Tools, open the coverage tab and record the coverage using the built-in Coverage panel. [Refer to the official Chrome documentation for the Coverage feature.](https://developers.google.com/web/tools/chrome-devtools/coverage#open)
2. Save the coverage report as a JSON file on your disk.
3. Inside Developer Tools, open the ABD tab and select the **WRM Coverage Browser** panel.
4. Drag and drop the JSON coverage file or click on the panel to select the coverage file from your disk.

![WRM Coverage Browser](./docs/wrm-coverage-browser.gif)

The WRM coverage panel will display a list of all the web-resources from the coverage report.
The coverage panel works similar to the [**WRM Inspector**](#wrm-coverage) panel, and you can use the same features like e.g. sorting and filtering.

Additionally, both the web-resources list and the summary panel includes the information about:

- **used bytes** and **used percentage**
- **unused bytes** and **unused percentage**

![WRM Coverage Browser Summary](./docs/wrm-coverage-panel.png)

The information about unused bytes can help you find the resources that are not used on the page.
The unused resources can be either removed from the page runtime, or you can [defer loading the web-resources until you need them](https://hello.atlassian.net/wiki/spaces/SVRFE/pages/937707609/How+to+code+split+using+the+WRM).

## WRM Coverage Recorder

> #### The WRM Coverage Recorder feature is only supported in Chrome and Chromium-based browsers like, e.g. Edge.

The **WRM Coverage Recorder** panel can help you to record and browse the coverage of the web-resources loaded on the page.

### How to use it?

1. To record the coverage report, navigate to the page you want to record the coverage for.
2. Open ABD and select the **WRM Coverage Recorder** panel.
3. Next, locate the "**Record Coverage**" button ![Record Button](./docs/wrm-coverage-recorder-record-button.png) and click on it.
4. You will see a confirmation dialog that will ask you to confirm reloading the page:
   ![Record Confirmation](./docs/wrm-coverage-recorder-confirm-dialog.png)
5. After confirming the page reload, the recorder will start recording the coverage:
   ![WRM Coverage Recording](./docs/wrm-coverage-recorder-recording.png)
6. Once the recording is finished, you will be able to browse the coverage report similar to the [**WRM Coverage Browser**](#wrm-coverage-browser) panel.

### Exporting Coverage

You can export the coverage report as a JSON file by clicking on the **Export coverage...** button.

The exporter coverage file can be later used to browse the coverage report with the [**WRM Coverage Browser**](#wrm-coverage-browser) panel.

## Support and feedback

If you found a bug or have ideas about improvements, please report them on the project issues page:
[https://ecosystem.atlassian.net/jira/software/projects/ABD/issues/](https://ecosystem.atlassian.net/jira/software/projects/ABD/issues/)
