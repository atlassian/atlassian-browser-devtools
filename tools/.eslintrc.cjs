const path = require('path');

module.exports = {
  // This config should inherit all the TS overrides from the base config.
  env: {
    node: true,
  },

  rules: {
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: true,

        packageDir: [
          path.resolve(__dirname, '..'), // We need to point to the root package since the local one is used only to verify TypeScript compilation
          path.resolve(__dirname),
        ],
      },
    ],

    'import/no-unresolved': 'off', // TypeScript and import rule don't play nice
    'import/extensions': 'off', // TypeScript's imports don't like to include the file extension
  },
};
