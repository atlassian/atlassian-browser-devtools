import fs from 'fs';
import path from 'path';

import type { Rule } from 'eslint';
import kebabCase from 'lodash/kebabCase';

const projectName = 'abd';
const rulesDir = 'rules';

const ruleFiles = fs
  .readdirSync(path.join(__dirname, rulesDir))
  .filter((file) => !file.match(/\.test\.[a-z]+$/))
  .map((file) => path.join(rulesDir, file));

const getRuleName = (ruleFilePath: string) => {
  const fileName = path.basename(ruleFilePath, path.extname(ruleFilePath));

  return kebabCase(fileName);
};

const getRuleNameWithPluginPrefix = (ruleFilePath: string) => {
  const ruleName = getRuleName(ruleFilePath);

  return `${projectName}/${ruleName}`;
};

// Default configs and recommended rules
const configs = {
  recommended: {
    plugins: [projectName],
    rules: Object.fromEntries(ruleFiles.map((ruleFilePath) => [getRuleNameWithPluginPrefix(ruleFilePath), 'error'])),
  },
};

const rules: Record<string, Rule.RuleModule> = Object.fromEntries(
  ruleFiles.map((ruleFilePath) => {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    return [getRuleName(ruleFilePath), require('./' + ruleFilePath).default];
  })
);

export { configs, rules };
