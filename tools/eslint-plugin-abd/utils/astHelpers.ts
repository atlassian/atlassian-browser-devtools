import type {
  ArrowFunctionExpression,
  CallExpression,
  FunctionExpression,
  Node,
  MemberExpression,
  Identifier,
  VariableDeclarator,
  VariableDeclaration,
  FunctionDeclaration,
  ExportNamedDeclaration,
} from 'estree';

export const isMemberExpression = (node: Node): node is MemberExpression => node.type === 'MemberExpression';
export const isIdentifier = (node: Node): node is Identifier => node.type === 'Identifier';
export const isCallExpression = (node: Node): node is CallExpression => node.type === 'CallExpression';
export const isFunctionExpression = (node: Node): node is FunctionExpression => node.type === 'FunctionExpression';
export const isArrowFunctionExpression = (node: Node): node is ArrowFunctionExpression => node.type === 'ArrowFunctionExpression';
export const isVariableDeclarator = (node: Node): node is VariableDeclarator => node.type === 'VariableDeclarator';
export const isFunctionDeclaration = (node: Node): node is FunctionDeclaration => node.type === 'FunctionDeclaration';

export const isCallable = (node: Node): node is CallExpression | ArrowFunctionExpression | FunctionExpression =>
  isCallExpression(node) || isArrowFunctionExpression(node) || isFunctionExpression(node);

export const isVariableDeclaration = (node: Node): node is VariableDeclaration => node.type === 'VariableDeclaration';

export const isExportNamedDeclaration = (node: Node): node is ExportNamedDeclaration => node.type === 'ExportNamedDeclaration';

export const isArrowFunctionVariableDeclarator = (node: Node): node is VariableDeclaration => {
  if (!isVariableDeclaration(node)) {
    return false;
  }

  // Ensure we have a single arrow function variable declaration
  if (node.declarations.length !== 1) {
    return false;
  }

  const arrowFunctionDeclaration = node.declarations[0];

  return isVariableDeclarator(arrowFunctionDeclaration) && arrowFunctionDeclaration.init
    ? isArrowFunctionExpression(arrowFunctionDeclaration.init)
    : false;
};
