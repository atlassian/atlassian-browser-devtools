import type { Rule } from 'eslint';
import { RuleTester } from 'eslint';
import { test } from 'vitest';

// Based on https://eslint.org/docs/developer-guide/nodejs-api
const ruleTester = new RuleTester({
  parser: require('@babel/eslint-parser'),
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020,
    ecmaFeatures: {
      jsx: true,
    },
  },
});

// @ts-expect-error RuleTester.it is not defined
RuleTester.describe = (text: string, method: (...args: unknown[]) => void) => {
  // @ts-expect-error RuleTester.it is not defined
  RuleTester.it.title = text;
  return method.call(this);
};

// @ts-expect-error RuleTester.it is not defined
RuleTester.it = function (text: string, method: unknown) {
  // @ts-expect-error RuleTester.it is not defined
  test(RuleTester.it.title + ': ' + text, method);
};

type ValidRuleTestCase = string | RuleTester.ValidTestCase;
type InvalidRuleTestCase = RuleTester.InvalidTestCase;

export const runRuleTests = (
  ruleName: string,
  rule: Rule.RuleModule,
  validCases: ValidRuleTestCase[],
  invalidTestCase: InvalidRuleTestCase[]
) => {
  ruleTester.run(ruleName, rule, {
    valid: validCases,
    invalid: invalidTestCase,
  });
};
