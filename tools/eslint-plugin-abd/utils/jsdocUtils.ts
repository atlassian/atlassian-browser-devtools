import { getJSDocComment, parseComment } from '@es-joy/jsdoccomment';
import type { Spec, Block } from 'comment-parser';
import type { Rule, SourceCode, AST } from 'eslint';

export { type Spec };

import { isExportNamedDeclaration } from './astHelpers';

type Settings = Record<string, unknown>;

export { getSettings } from 'eslint-plugin-jsdoc/dist/iterateJsdoc.js';

export const getJSDocCommentBlock = (sourceCode: SourceCode, node: Rule.Node, settings: Settings): AST.Token | null => {
  let localNode: Rule.Node = node;

  // Traverse up to the parent since the element can include export keyword
  if (isExportNamedDeclaration(node.parent)) {
    localNode = node.parent;
  }

  return getJSDocComment(sourceCode, localNode, settings);
};

const getIndentAndJSDoc = (lines: string[], jsdocNode: AST.Token): [string, Block] => {
  const sourceLine = lines[jsdocNode.loc.start.line - 1];
  const indnt = sourceLine.charAt(0).repeat(jsdocNode.loc.start.column);
  const jsdocBlock = parseComment(jsdocNode, indnt);

  return [indnt, jsdocBlock];
};

export const getTagsFromJSDocBlock = (sourceCode: SourceCode, node: AST.Token): Spec[] => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [indent, jsdocBlock] = getIndentAndJSDoc(sourceCode.lines, node);

  return jsdocBlock.tags;
};
