/* eslint-disable import/no-duplicates */

declare module 'eslint-plugin-jsdoc/dist/iterateJsdoc.js' {
  import type { Rule } from 'eslint';

  const getSettings: (context: Rule.RuleContext) => Record<string, unknown> | false;

  export { getSettings };
}

declare module '@es-joy/jsdoccomment' {
  import type { Block } from 'comment-parser';
  import type { Rule, SourceCode, AST } from 'eslint';

  type Settings = Record<string, unknown>;

  const getJSDocComment: (sourceCode: SourceCode, node: Rule.Node, settings: Settings) => AST.Token | null;

  const parseComment: (commentNode: AST.Token, indent: string) => Block;

  export { getJSDocComment, parseComment };
}
