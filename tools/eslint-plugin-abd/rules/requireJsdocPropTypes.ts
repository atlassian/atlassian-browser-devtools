import type { Rule } from 'eslint';
import type { VariableDeclaration } from 'estree';

import { isArrowFunctionVariableDeclarator, isIdentifier } from '../utils/astHelpers';
import type { Spec } from '../utils/jsdocUtils';
import { getJSDocCommentBlock, getSettings, getTagsFromJSDocBlock } from '../utils/jsdocUtils';

const getVariableIdentifierName = (node: VariableDeclaration): string => {
  const [declaration] = node.declarations;

  if (!isIdentifier(declaration.id)) {
    return 'unknown';
  }

  return declaration.id.name;
};

const isReactComponentDefinition = (node: VariableDeclaration): node is VariableDeclaration => {
  if (!isArrowFunctionVariableDeclarator(node)) {
    return false;
  }

  // Naively check if the variable name starts with uppercase letters
  // TODO: In the future this should check if the function body returns JSX
  const arrowFunctionName = getVariableIdentifierName(node);

  return Boolean(arrowFunctionName.match(/^[A-Z]/));
};

const lookupForTypeTag = (tags: Spec[]): Spec | null => {
  return tags.find((tag) => tag.tag === 'type') ?? null;
};

const hasTypeTag = (tags: Spec[]): boolean => Boolean(lookupForTypeTag(tags));

const hasValidJSDocTags = (tags: Spec[], allowedComponentTypes: string[]): boolean => {
  const tagType = lookupForTypeTag(tags);

  if (!tagType) {
    return false;
  }

  const tagValue: string = tagType.type;

  return allowedComponentTypes.some((allowedType) => {
    const regExp = new RegExp(`^(?:${allowedType}<.*>|${allowedType})$`);

    return Boolean(tagValue.match(regExp));
  });
};

interface RuleOptions {
  allowedComponentTypes: string[];
}

const defaultOptions: RuleOptions = {
  allowedComponentTypes: ['React.FC', 'React.FunctionComponent', 'React.ForwardRedExoticComponent', 'React.ComponentClass'],
};

const requireJsdocPropTypes: Rule.RuleModule = {
  meta: {
    type: 'suggestion',
    docs: {
      description: 'Require prop types definitions inside a JSDoc block comment for React component definition',
      category: 'Possible Errors',
      recommended: true,
    },

    messages: {
      invalidComponentType:
        'The JSDoc block comment for `{{componentName}}` React component has an invalid value for the `@type` tag.\nAllowed components types are: {{allowedComponentTypes}}',
      missingJsdocBlockComment:
        'The `{{componentName}}` React component has a missing requires JSDoc block comment and is lacking prop types definitions',
      missingTypeTag:
        'The JSDoc block comment for `{{componentName}}` React component is missing a `@type` tag with prop types definitions',
    },

    schema: [
      {
        type: 'object',
        properties: {
          allowedComponentTypes: {
            type: 'array',
            items: {
              type: 'string',
            },
          },
        },
        additionalProperties: false,
      },
    ],
  },

  create(context: Rule.RuleContext & { options: RuleOptions[] }): Rule.RuleListener {
    const sourceCode = context.getSourceCode();
    const settings = getSettings(context);

    // TODO: Report error that `eslint-plugin-jsdoc` is missing
    if (!settings) {
      return {};
    }

    // Allowed component types
    const options = { ...defaultOptions, ...context.options[0] };

    return {
      VariableDeclaration(node) {
        if (!isReactComponentDefinition(node)) {
          return;
        }

        const componentName = getVariableIdentifierName(node);
        const jsDocNode = getJSDocCommentBlock(sourceCode, node, settings);

        // Report that we don't have a JSDoc comment for this node
        if (!jsDocNode) {
          context.report({
            node,
            messageId: 'missingJsdocBlockComment',
            data: {
              componentName,
            },
          });
          return;
        }

        // Parse the JSDoc comment block and get tags
        const tags = getTagsFromJSDocBlock(sourceCode, jsDocNode);

        if (!hasTypeTag(tags)) {
          context.report({
            node,
            messageId: 'missingTypeTag',
            data: {
              componentName,
            },
          });
          return;
        }

        if (!hasValidJSDocTags(tags, options.allowedComponentTypes)) {
          context.report({
            node,
            messageId: 'invalidComponentType',
            data: {
              componentName,
              allowedComponentTypes: options.allowedComponentTypes.join(', '),
            },
          });
          return;
        }
      },
    };
  },
};

export const ruleName = 'require-jsdoc-prop-types';

export default requireJsdocPropTypes;
