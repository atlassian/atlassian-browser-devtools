import requireJsdocPropTypes, { ruleName } from './requireJsdocPropTypes';
import { runRuleTests } from '../utils/ruleTester';

runRuleTests(
  ruleName,
  requireJsdocPropTypes,
  [
    `
    /** @type {React.FC} */
    const ComponentWithJsDoc = ({ children }) => <p>{children}</p>
    `,

    `
    /** @type {React.FC<{ children: React.ReactNode }>} */
    let ComponentWitJsDoc = ({ children }) => <p>{children}</p>
    `,

    `
    /** @type {React.FC<{ children: React.ReactNode }>} */
    export const ExportedComponentWithJsDoc = ({ children }) => <p>{children}</p>
    `,

    `
    /** @type {React.FunctionComponent} */
    const FunctionComponent = ({ children }) => <p>{children}</p>
    `,

    `
    /** @type {React.FunctionComponent< { children: React.ReactNode } >} */
    const FunctionComponent = ({ children }) => <p>{children}</p>
    `,

    `
    /** @type {React.ForwardRedExoticComponent< { children: React.ReactNode } >} */
    const FunctionComponent = ({ children }) => <p>{children}</p>
    `,

    `
    /** @type {React.ForwardRedExoticComponent} */
    const FunctionComponent = ({ children }) => <p>{children}</p>
    `,

    /* TODO: This is not yet supported
    `
    /** @type {React.ComponentClass<{ children: React.ReactNode }>} * /
    class Foo extends Component {
      render() {
        return <p>{this.props.children}</p>
      }
    }
    `,
    */
  ],

  [
    {
      code: `const ComponentWithoutJsDoc = ({ children }) => <p>{children}</p>`,
      errors: [
        {
          messageId: 'missingJsdocBlockComment',
          data: {
            componentName: 'ComponentWithoutJsDoc',
          },
        },
      ],
    },

    {
      code: `// some inline comment
             const ComponentWithInlineComment = ({ children }) => <p>{children}</p>`,
      errors: [
        {
          messageId: 'missingJsdocBlockComment',
          data: {
            componentName: 'ComponentWithInlineComment',
          },
        },
      ],
    },

    {
      code: `/* */
             const ComponentWithEmptyEmptyJsDocComment = ({ children }) => <p>{children}</p>`,
      errors: [
        {
          messageId: 'missingJsdocBlockComment',
          data: {
            componentName: 'ComponentWithEmptyEmptyJsDocComment',
          },
        },
      ],
    },

    {
      code: `/** */
             const ComponentWithJsDocWithoutTypeTag = ({ children }) => <p>{children}</p>`,
      errors: [
        {
          messageId: 'missingTypeTag',
          data: {
            componentName: 'ComponentWithJsDocWithoutTypeTag',
          },
        },
      ],
    },

    {
      code: `/** @foo */
             const ComponentWithInvalidJsDocComment = ({ children }) => <p>{children}</p>`,
      errors: [
        {
          messageId: 'missingTypeTag',
          data: {
            componentName: 'ComponentWithInvalidJsDocComment',
          },
        },
      ],
    },

    {
      code: `/** @type {boolean} */
             const ComponentWithInvalidTypeValue = ({ children }) => <p>{children}</p>`,
      options: [
        {
          allowedComponentTypes: ['React.FunctionComponent'],
        },
      ],
      errors: [
        {
          messageId: 'invalidComponentType',
          data: {
            componentName: 'ComponentWithInvalidTypeValue',
            allowedComponentTypes: 'React.FunctionComponent',
          },
        },
      ],
    },

    {
      code: `/** @type {React.FunctionComponent} */
             const ComponentWithForbiddenType = ({ children }) => <p>{children}</p>`,
      options: [
        {
          allowedComponentTypes: ['React.FC'],
        },
      ],
      errors: [
        {
          messageId: 'invalidComponentType',
          data: {
            componentName: 'ComponentWithForbiddenType',
            allowedComponentTypes: 'React.FC',
          },
        },
      ],
    },
  ]
);
