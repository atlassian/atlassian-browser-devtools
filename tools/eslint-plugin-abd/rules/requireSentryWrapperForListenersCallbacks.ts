import type { Rule } from 'eslint';
import type { ArrowFunctionExpression, CallExpression, FunctionExpression, Node } from 'estree';

import {
  isArrowFunctionExpression,
  isCallable,
  isCallExpression,
  isFunctionDeclaration,
  isFunctionExpression,
  isIdentifier,
  isMemberExpression,
  isVariableDeclarator,
} from '../utils/astHelpers';

const isChromeEventListerCallExpression = (node: CallExpression): boolean => {
  if (!isMemberExpression(node.callee) || !isIdentifier(node.callee.property)) {
    return false;
  }

  if (!isMemberExpression(node.callee.object) || !isIdentifier(node.callee.object.property)) {
    return false;
  }

  const calleeProperty = node.callee.property;
  const objectProperty = node.callee.object.property;

  // Skip if this is not `addListener` identifier
  if (calleeProperty.name !== 'addListener') {
    return false;
  }

  if (!objectProperty.name.match(/^on[A-Z][A-Za-z]+$/)) {
    return false;
  }

  return true;
};

const isInlineCallbackFunction = (node: Node): node is CallExpression | ArrowFunctionExpression | FunctionExpression => {
  return isCallable(node);
};

/* eslint-disable consistent-return */
const isCallbackExpressionWrappedWithWrapperFunction = (callbackNode: Node, wrapperFunctionName: string): boolean | undefined => {
  // Callback is just a function definition without a wrapper
  if (isFunctionDeclaration(callbackNode)) {
    return false;
  }

  // Callback is just an arrow function without a wrapper
  if (isArrowFunctionExpression(callbackNode)) {
    return false;
  }

  // Callback is just a function definition without a wrapper
  if (isFunctionExpression(callbackNode)) {
    return false;
  }

  // Callback *might* include wrapper function...
  if (isCallExpression(callbackNode)) {
    // Ignore
    if (!isIdentifier(callbackNode.callee)) {
      return;
    }

    // All's good here. We are using a wrapper function!
    if (callbackNode.callee.name === wrapperFunctionName) {
      return;
    }

    // We are not using the wrapper function, but we should!
    return false;
  }

  return;
};
/* eslint-enable consistent-return */

const requireSentryWrapperForListenersCallbacks: Rule.RuleModule = {
  meta: {
    type: 'suggestion',
    docs: {
      description: 'Requires usage of the `withSentryWrapper` function on a callback when adding listeners to chrome API events',
      category: 'Possible Errors',
      recommended: true,
      suggestion: true,
    },
    hasSuggestions: true,
    messages: {
      preferInlineCallbackWithSentryWrapper:
        'You should wrap the inline event callback definition with a `{{wrapperName}}(...)` function wrapper',
      preferCallbackDefinitionWithSentryWrapper:
        'You should wrap the `{{callbackName}}` event callback definition with a `{{wrapperName}}(...)` function wrapper',
    },
    schema: [],
  },

  create(context: Rule.RuleContext): Rule.RuleListener {
    const sentryWrapperFunctionName = 'withSentryWrapper';

    const declaredCallbacks = new Map<string, Node>();

    return {
      FunctionDeclaration(node) {
        if (!isFunctionDeclaration(node) || !node.id || !isIdentifier(node.id)) {
          return;
        }

        // Store the node so we can check is later
        const callbackName = node.id.name;

        declaredCallbacks.set(callbackName, node);
      },

      VariableDeclarator(node) {
        if (!isVariableDeclarator(node) || !isIdentifier(node.id)) {
          return;
        }

        // Check if the variable is a function declaration
        if (!node.init || !isCallable(node.init)) {
          return;
        }

        // Store the node so we can check is later
        const callbackName = node.id.name;
        const callbackNode = node.init;

        declaredCallbacks.set(callbackName, callbackNode);
      },

      CallExpression(node) {
        if (!isChromeEventListerCallExpression(node)) {
          return;
        }

        const listenerArg0 = node.arguments[0];

        // Check if the `addListener` call has a callback as an argument
        if (isInlineCallbackFunction(listenerArg0)) {
          const callbackCallExpression = listenerArg0;

          // Check if the callback is a wrapped with the wrapper function
          const isWrapped = isCallbackExpressionWrappedWithWrapperFunction(callbackCallExpression, sentryWrapperFunctionName);

          if (isWrapped === false) {
            // Report the error
            context.report({
              node,
              messageId: 'preferInlineCallbackWithSentryWrapper',
              data: {
                wrapperName: sentryWrapperFunctionName,
              },
            });
          }
        }
        // Check if the `addListener` call uses a reference to a function definition as an argument
        else if (isIdentifier(listenerArg0)) {
          const callbackName = listenerArg0.name;

          // Lookup the callback definition
          const callbackNode = declaredCallbacks.get(callbackName);

          if (!callbackNode) {
            return;
          }

          // Check if the callback is a wrapped with the wrapper function
          const isWrapped = isCallbackExpressionWrappedWithWrapperFunction(callbackNode, sentryWrapperFunctionName);

          if (isWrapped === false) {
            // Report the error
            context.report({
              node,
              messageId: 'preferCallbackDefinitionWithSentryWrapper',
              data: {
                wrapperName: sentryWrapperFunctionName,
                callbackName: callbackName,
              },
            });
          }
        }
      },
    };
  },
};

export const ruleName = 'require-sentry-wrapper-for-listeners-callbacks';

export default requireSentryWrapperForListenersCallbacks;
