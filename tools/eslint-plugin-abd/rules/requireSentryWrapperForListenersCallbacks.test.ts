import requireSentryWrapperForListenersCallbacks, { ruleName } from './requireSentryWrapperForListenersCallbacks';
import { runRuleTests } from '../utils/ruleTester';

runRuleTests(
  ruleName,
  requireSentryWrapperForListenersCallbacks,
  [
    // Variable callback definitions
    `const myCallbackArrowFunction = withSentryWrapper(() => {});
    chrome.runtime.onConnect.addListener(myCallbackArrowFunction);`,

    `const myCallbackAnonymousFunction = withSentryWrapper(function() {});
    chrome.runtime.onConnect.addListener(myCallbackAnonymousFunction);`,

    `const myCallbackNamedFunction = withSentryWrapper(function myCallback() {});
    chrome.runtime.onConnect.addListener(myCallbackNamedFunction);`,

    // Function callback definitions
    `
    function myCallback() {}

    const myCallbackWithWrapper = withSentryWrapper(myCallback);
    chrome.runtime.onConnect.addListener(myCallbackWithWrapper);`,

    // Inlined callbacks
    `chrome.runtime.onConnect.addListener(withSentryWrapper(() => {}));`,

    `chrome.runtime.onConnect.addListener(withSentryWrapper(function() {}));`,

    `chrome.runtime.onConnect.addListener(withSentryWrapper(function callback() {}));`,
  ],

  [
    // Variable callback definitions

    // Arrow function used as a callback definition
    {
      code: `const myCallbackArrowFunction = () => {};
             chrome.runtime.onConnect.addListener(myCallbackArrowFunction);`,
      errors: [
        {
          messageId: 'preferCallbackDefinitionWithSentryWrapper',
          data: {
            wrapperName: 'withSentryWrapper',
            callbackName: 'myCallbackArrowFunction',
          },
        },
      ],
    },

    // Anonymous function used as a callback definition
    {
      code: `const myCallbackFunction = function() {};
             chrome.runtime.onConnect.addListener(myCallbackFunction);`,
      errors: [
        {
          messageId: 'preferCallbackDefinitionWithSentryWrapper',
          data: {
            wrapperName: 'withSentryWrapper',
            callbackName: 'myCallbackFunction',
          },
        },
      ],
    },

    // Named function used as a callback definition
    {
      code: `const myCallbackFunction = function callback() {};
             chrome.runtime.onConnect.addListener(myCallbackFunction);`,
      errors: [
        {
          messageId: 'preferCallbackDefinitionWithSentryWrapper',
          data: {
            wrapperName: 'withSentryWrapper',
            callbackName: 'myCallbackFunction',
          },
        },
      ],
    },

    // Function callback definitions

    // Named function used as a callback definition
    {
      code: `function myCallback() {};
            chrome.runtime.onConnect.addListener(myCallback);`,

      errors: [
        {
          messageId: 'preferCallbackDefinitionWithSentryWrapper',
          data: {
            wrapperName: 'withSentryWrapper',
            callbackName: 'myCallback',
          },
        },
      ],
    },

    // Inline callbacks

    // Arrow function used as inline callbacks
    {
      code: `chrome.runtime.onConnect.addListener(() => {});`,
      errors: [
        {
          messageId: 'preferInlineCallbackWithSentryWrapper',
        },
      ],
    },

    // Anonymous function used as inline callbacks
    {
      code: `chrome.runtime.onConnect.addListener(function() {});`,
      errors: [
        {
          messageId: 'preferInlineCallbackWithSentryWrapper',
        },
      ],
    },

    // Named function used as inline callbacks
    {
      code: `chrome.runtime.onConnect.addListener(function myCallback() {});`,
      errors: [
        {
          messageId: 'preferInlineCallbackWithSentryWrapper',
        },
      ],
    },
  ]
);
