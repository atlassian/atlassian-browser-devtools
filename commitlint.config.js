/* eslint-env node */
module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    // Check https://github.com/conventional-changelog/commitlint/blob/master/docs/reference-rules.md
    'subject-case': [0, 'always', 'sentence-case'],
    'header-max-length': [0, 'always', 120],
    'body-max-line-length': [2, 'always', 500],
  },
};
