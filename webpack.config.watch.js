/* eslint-env node */
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const { HotModuleReplacementPlugin } = require('webpack');
const { merge } = require('webpack-merge');

const getWebpackConfig = require('./webpack.config.js');

module.exports = (env, argv) => {
  const baseConfig = getWebpackConfig(env, argv);

  return merge(baseConfig, {
    devtool: 'eval-cheap-module-source-map',

    plugins: [new HotModuleReplacementPlugin(), new ReactRefreshWebpackPlugin()],

    optimization: {
      minimize: false,
    },
  });
};
